﻿using System;
using Xamarin.Forms;

namespace PP
{
    public class TransparentEntryExtended:TransparentEntry
    {
        
        public static readonly BindableProperty ShowUnderlineProperty = BindableProperty.Create(nameof(ShowUnderline), typeof(bool), typeof(TransparentEntryExtended), true,
               propertyChanged: (bindable, oldValue, newValue) =>
               {
                    var view = bindable as TransparentEntryExtended;
                   
               }
          );
        public bool ShowUnderline
        {
            get { return (bool)GetValue(ShowUnderlineProperty); }
            set { SetValue(ShowUnderlineProperty, value); }
        }


        public static readonly BindableProperty UnderlineColorProperty = BindableProperty.Create(nameof(UnderlineColor), typeof(Color), typeof(TransparentEntryExtended), Color.Green,
               propertyChanged: (bindable, oldValue, newValue) =>
               {
                   var view = bindable as TransparentEntryExtended;

               }
          );
        public Color UnderlineColor
        {
            get { return (Color)GetValue(UnderlineColorProperty); }
            set { SetValue(UnderlineColorProperty, value); }
        }


        public delegate void BackspaceEventHandler(object sender, EventArgs e);

        public event BackspaceEventHandler OnBackspace;

        public void OnBackspacePressed()
        {
            if (OnBackspace != null)
            {
                OnBackspace(null, null);
            }
        }

        public TransparentEntryExtended()
        {
            
        }


    }
}
