﻿using System;

using Xamarin.Forms;

namespace PP
{
	public class CorneredView : ContentView
	{
        public static readonly BindableProperty CornerRadiusProperty =
    		BindableProperty.Create<CorneredView, float>(x => x.CornerRadius, 0);
        //public static readonly BindableProperty CornerRadiusProperty = BindableProperty.Create("CornerRadius", typeof(float), typeof(CorneredView), 0);

		public static readonly BindableProperty BorderColorProperty =
			BindableProperty.Create<CorneredView, Color>(x => x.BorderColor, Color.Transparent);
        //public static readonly BindableProperty BorderColorProperty = BindableProperty.Create("BorderColor", typeof(Color), typeof(CorneredView), Color.Transparent);

		public static readonly BindableProperty BorderWidthProperty =
	    	BindableProperty.Create<CorneredView, float>(x => x.BorderWidth, 0);
        //public static readonly BindableProperty BorderWidthProperty = BindableProperty.Create("BorderWidth", typeof(float), typeof(CorneredView), 0);

		public float CornerRadius
		{
            get { return (float)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
		}
		public Color BorderColor
		{
			get { return (Color)GetValue(BorderColorProperty); }
			set { SetValue(BorderColorProperty, value); }
		}
		public float BorderWidth
		{
			get { return (float)GetValue(BorderWidthProperty); }
			set { SetValue(BorderWidthProperty, value); }


		}
		protected override void OnChildAdded(Element child)
		{
			base.OnChildAdded(Resizer.scaleChild(child));
		}


	}
}

