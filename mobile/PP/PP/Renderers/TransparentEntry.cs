﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace PP
{
    public class TransparentEntry:Entry
    {

        public enum TextGravity
        {
            START_TOP,
            START_CENTER,
            START_BOTTOM,
            CENTER_TOP,
            CENTER,
            CENTER_BOTTOM,
            END_TOP,
            END_CENTER,
            END_BOTTOM
        }

        public static readonly BindableProperty TAProperty = BindableProperty.Create(nameof(TextAlignment), typeof(TextGravity), typeof(TransparentEntry), default(TextGravity),
                propertyChanged: (bindable, oldValue, newValue) =>
                {
                    var view = bindable as TransparentEntry;
                    
                }
        );
        public TextGravity TextGravityAlignment
        {
            get { return (TextGravity)GetValue(TAProperty); }
            set { SetValue(TAProperty, value); }
        }


		public static readonly BindableProperty AutoCapitalizationProperty = BindableProperty.Create(nameof(AutoCapitalization), typeof(bool), typeof(TransparentEntry), true,
               propertyChanged: (bindable, oldValue, newValue) =>
               {
                   var view = bindable as TransparentEntry;

                   var AutoCapitalizationKeyboard = ((bool)newValue) ? KeyboardFlags.CapitalizeSentence : KeyboardFlags.None;

                   if (view.AutoCorrectEnabled)
                   {
                           view.Keyboard = Keyboard.Create(KeyboardFlags.Spellcheck | AutoCapitalizationKeyboard);
                    }

			   }
		  );
		public bool AutoCapitalization
		{
			get { return (bool)GetValue(AutoCapitalizationProperty); }
			set { SetValue(AutoCapitalizationProperty, value); }
		}

		public static readonly BindableProperty AutoCorrectProperty = BindableProperty.Create(nameof(AutoCorrectProperty), typeof(bool), typeof(TransparentEntry), true,
			   propertyChanged: (bindable, oldValue, newValue) =>
			   {
				   var view = bindable as TransparentEntry;

                    var AutoCorrectKeyboard =((bool)newValue)?KeyboardFlags.Spellcheck:KeyboardFlags.None;

                    if(view.AutoCapitalization){
                       view.Keyboard = Keyboard.Create(KeyboardFlags.CapitalizeSentence | AutoCorrectKeyboard);
                    }

			   }
	   );
		public bool AutoCorrectEnabled
		{
			get { return (bool)GetValue(AutoCorrectProperty); }
			set { SetValue(AutoCorrectProperty, value); }
		}







        public TransparentEntry()
        {
            TextGravityAlignment = TextGravity.START_TOP;

            //this.TextChanged += (sender, e) => {


            //    //force invoke perword capitalization, sorry
            //    bool x = this.PerWordCapitalization;

            //    this.PerWordCapitalization = !x;
            //    this.PerWordCapitalization = x;
            //};
        }
    }
}
