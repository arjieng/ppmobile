﻿using System;

using Xamarin.Forms;

namespace PP
{
	public class TransparentNavBarPage : NavigationPage
	{
		public Color barColor { get; set; }
		public TransparentNavBarPage(Page content):base(content){
			barColor = Color.Transparent;
		}
	}
}

