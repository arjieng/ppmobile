﻿using System;
using Xamarin.Forms;

namespace PP
{
    public class CustomPicker:Picker
    {
        public CustomPicker() : base()
        {
        }

        public enum Alignment{
            START_ALIGN,
            CENTER_ALIGN,
            END_ALIGN
        }

		public static readonly BindableProperty FontSizeProperty = BindableProperty.Create(nameof(FontSize), typeof(double), typeof(CustomPicker), 12.0 ,
				propertyChanged: (bindable, oldValue, newValue) =>
				{
					var view = bindable as CustomPicker;
				}
		);
		public double FontSize
		{
			get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value ); }
		}

		public static readonly BindableProperty PlaceholderColorProperty = BindableProperty.Create(nameof(PlaceholderColor), typeof(Color), typeof(CustomPicker), Color.Gray,
				propertyChanged: (bindable, oldValue, newValue) =>
				{
					var view = bindable as CustomPicker;
				}
		);
        public Color PlaceholderColor
		{
			get { return (Color)GetValue(PlaceholderColorProperty); }
			set { SetValue(PlaceholderColorProperty, value); }
		}

		public static readonly BindableProperty TitleAlignmentProperty = BindableProperty.Create(nameof(TitleAlignment), typeof(Alignment), typeof(CustomPicker), Alignment.START_ALIGN,
				propertyChanged: (bindable, oldValue, newValue) =>
				{
					var view = bindable as CustomPicker;
				}
		);
        public Alignment TitleAlignment
		{
			get { return (Alignment)GetValue(TitleAlignmentProperty); }
			set { SetValue(TitleAlignmentProperty, value); }
		}
    }
}
