﻿using System;
using Xamarin.Forms;

namespace PP
{
    public class ExtendedLabel:Label
    {
        public ExtendedLabel()
        {
        }

		public static readonly BindableProperty IsUnderlineProperty = BindableProperty.Create(nameof(IsUnderline), typeof(bool), typeof(ExtendedLabel), default(bool),
				propertyChanged: (bindable, oldValue, newValue) =>
				{
					var view = bindable as ExtendedLabel;
				}
		);
		public bool IsUnderline
		{
			get { return (bool)GetValue(IsUnderlineProperty); }
			set { SetValue(IsUnderlineProperty, value); }
		}


		public static readonly BindableProperty IsStrikeThroughProperty = BindableProperty.Create(nameof(IsStrikeThrough), typeof(bool), typeof(ExtendedLabel), false,
				propertyChanged: (bindable, oldValue, newValue) =>
				{
					var view = bindable as ExtendedLabel;
				}
		);
		public bool IsStrikeThrough
		{
			get { return (bool)GetValue(IsStrikeThroughProperty); }
			set { SetValue(IsStrikeThroughProperty, value); }
		}

		public static readonly BindableProperty IsBoldProperty = BindableProperty.Create(nameof(IsBold), typeof(bool), typeof(ExtendedLabel), false,
				propertyChanged: (bindable, oldValue, newValue) =>
				{
					var view = bindable as ExtendedLabel;
				}
		);
		public bool IsBold
		{
			get { return (bool)GetValue(IsBoldProperty); }
			set { SetValue(IsBoldProperty, value); }
		}


    }
}
