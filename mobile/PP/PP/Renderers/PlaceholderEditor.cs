﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PP
{
	public class PlaceholderEditor:Editor
	{

		private String _placeholder = "";
		public String Placeholder
		{
			get{return _placeholder;}
			set{
				_placeholder = value;
			}
		}

		private Color _placeholderColor = Color.Gray;
		public Color PlaceholderColor
		{
			get
			{
				return _placeholderColor;
			}
			set
			{
				_placeholderColor = value;
				Unfocus();
			}
		}

		private Color _realTextColor;
		public PlaceholderEditor()
		{
			this.Focused += (object sender, FocusEventArgs e) =>
			{
				MyEditor_Focused(sender, e);
			};

			this.Unfocused += (sender, e) => { MyEditor_Unfocused(sender, e);};
			Text = Text==null?"":Text;

            //await initPlaceholder(20);
            initializerOfPlacehoder();
		}

        async void initializerOfPlacehoder(){
            await initPlaceholder(20);
        }

		async Task initPlaceholder(int millis)
		{
			await Task.Delay(millis);
			_realTextColor = TextColor;
			MyEditor_Unfocused(null, null);

		}

		private void MyEditor_Focused(object sender, FocusEventArgs e) //triggered when the user taps on the Editor to interact with it
		{
			if (Text.Equals(Placeholder)) //if you have the placeholder showing, erase it and set the text color to black
			{
				Text = "";
				TextColor = _realTextColor;
			}
		}

		private void MyEditor_Unfocused(object sender, FocusEventArgs e) //triggered when the user taps "Done" or outside of the Editor to finish the editing
		{
			if (Text.Equals("")) //if there is text there, leave it, if the user erased everything, put the placeholder Text back and set the TextColor to gray
			{
				Text = Placeholder;
				TextColor = _placeholderColor;
			}
		}
	}
}
