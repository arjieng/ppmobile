﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace PP.tools
{
    public class PointToPointRouteFinder:RouteFinder.IRouteFinderCallback
    {

        RouteFinder routeFinder;
        Pin startPin, endPin;
        Map plottingMap;
        Polyline currentPolyline;
        Color routeColor;
        bool arrived ;
        Label travelTimeDisplay;

        public PointToPointRouteFinder(double startLatitude, double startLongitude, double endLatitude, double endLongitude)
        {
            StartLatitude = startLatitude;
            EndLatitude = endLatitude;
            StartLongitude = startLongitude;
            EndLongitude = endLongitude;
        }

        public void StartRouteFinderUpdates(Map map){
            plottingMap = map;


            routeFinder = new RouteFinder(StartLatitude, StartLongitude, EndLatitude, EndLongitude);
            routeFinder.setRouteFinderCallbacks(this);
            routeFinder.startFinding();

        }

        public void didFindRouteSuccess(Polyline polyline, string travelTime, RouteFinder caller)
        {
            if (currentPolyline != null)
                plottingMap.Polylines.Remove(currentPolyline);

            currentPolyline = polyline;

            polyline.StrokeColor = RouteColor;
            polyline.StrokeWidth = 2;

            Log.e("Polylines Position Counts:", "" +polyline.Positions.Count);
            if (polyline.Positions.Count > 1)
                plottingMap.Polylines.Add(polyline);

            if(StartPin!=null){
                plottingMap.Pins.Remove(StartPin);
                plottingMap.Pins.Add(StartPin);
            }
               
            if (EndPin != null)
            {
                plottingMap.Pins.Remove(EndPin);
                plottingMap.Pins.Add(EndPin);
            }

            if (RouteFound != null)
                RouteFound(this,EventArgs.Empty);

            if (polyline.Positions.Count <= 1 && RouteArrived != null)
            {
                RouteArrived(this, EventArgs.Empty);
                arrived = true;
            }
            else
                arrived = false;

            if (TravelTimeDisplay != null)
                TravelTimeDisplay.Text = travelTime;
            
        }

        public void didFindRouteFailed(string error, RouteFinder caller)
        {
            
        }


        public event EventHandler RouteFound;
        public event EventHandler RouteArrived;

        void UpdatePinPositions(){
            if(StartPin!=null ){
                StartPin.Position = new Position(StartLatitude, StartLongitude);
            }

            if (EndPin != null)
            {
                EndPin.Position = new Position(EndLatitude, EndLongitude);
            }
            //if(routeFinder!=null)
            //routeFinder.startFinding();
        }

        public Pin StartPin{
            get{
                return startPin;
            }
            set{
                startPin = value;
            }
        }
        public Pin EndPin{
            get
            {
                return endPin;
            }
            set
            {
                endPin = value;
            }
        }


        public Color RouteColor{
            get { return routeColor; }
            set { routeColor = value; }
        }
        public Polyline Polyline{
            get { return currentPolyline; }
        }
        public bool Arrived{
            get { return arrived; }
        }

        public Label TravelTimeDisplay{
            get;set;
        }

        private double _StartLatitude;
        public double StartLatitude
        {
            get
            {
                return _StartLatitude;
            }

            set
            {
                _StartLatitude = value;
                UpdatePinPositions();
            }

        }

        private double _StartLongitude;
        public double StartLongitude
        {
            get
            {
                return _StartLongitude;
            }

            set {
                _StartLongitude = value;
                UpdatePinPositions();
            }

        }

        private double _EndLatitude;
        public double EndLatitude
        {
            get
            {
                return _EndLatitude;
            }

            set
            {
                _EndLatitude = value;
                UpdatePinPositions();
            }

        }

        private double _EndLongitude;
        public double EndLongitude
        {
            get
            {
                return _EndLongitude;
            }

            set
            {
                _EndLongitude = value;
                UpdatePinPositions();
            }

        }
    }
}
