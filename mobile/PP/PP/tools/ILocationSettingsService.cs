﻿using System;
using System.Threading.Tasks;

namespace PP.tools
{
    public interface ILocationSettingsService
    {
        Task<String> OpenLocationSettings();
    }
}
