﻿using System;
namespace PP.tools
{
    public interface IInterstitialAdMobService
    {
        IInterstitialAdMobService InitWithAdUnitId(string adunitid);
        bool IsReady();
        void ShowInterstitialAd();
    }
}
