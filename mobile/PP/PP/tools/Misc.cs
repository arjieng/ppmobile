﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Plugin.Connectivity;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using PP.Pages;
using Xamarin.Forms;

namespace PP.tools
{
    public static class Misc
    {

        public class CheckReturn{
            public bool returnValue;
            public string errorMessage;
            public CheckReturn(bool retval, string errorMessage){
                this.returnValue = retval;
                this.errorMessage = errorMessage;
            }
        }


		public static async Task<CheckReturn> checkRequiredFeatures()
		{

			//check connectivity
			System.Diagnostics.Debug.WriteLine("Internet: " + CrossConnectivity.Current.IsConnected);
			if (CrossConnectivity.Current.IsConnected)
			{
				//passed

				//check geolocation


				var status = await checkLocationPermission();
				//System.Diagnostics.Debug.WriteLine("Geolocator: " + CrossGeolocator.Current.IsGeolocationEnabled);
				//QRUtils.CreateQRCodeImage("www.facebook.com", frame, 50, 50);



				//Application.Current.MainPage = new SplashScreen();

				if (CrossGeolocator.Current.IsGeolocationEnabled && status == PermissionStatus.Granted)
				{
					//QRUtils.CreateQRCodeImage("transaction_id: 4524563563", frame, 50, 50);
					//Application.Current.MainPage = new LoginPage();

                    return new CheckReturn(true, "Everything is okay"); //everything is okayy
				}
                if(!CrossGeolocator.Current.IsGeolocationEnabled)
                    return new CheckReturn(false, "Location Services is disabled");

                if (status != PermissionStatus.Granted)
                    return new CheckReturn(false, "PP is not allowed to check for locations.");

			}
            return new CheckReturn(false,"Device is not connected to internet.");
		}

		static async Task<PermissionStatus> checkLocationPermission()
		{

			var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
			if (status != PermissionStatus.Granted)
			{
				if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
				{
					//await DisplayAlert("Need location", "Gunna need that location", "Okay");
				}

                var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
				status = results[Permission.Location];
			}

			if (status == PermissionStatus.Granted)
			{
                if (CrossGeolocator.Current.IsGeolocationEnabled){
					var browser = new WebView();
					//var results = await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(2));
					//browser.Source = "https://forecast.io/?mobile=1#/f/" + "Lat: " + results.Latitude + " Long: " + results.Longitude;

                    //System.Diagnostics.Debug.WriteLine("Results: {0} - {1}" , results.Latitude,results.Longitude);
                }
                else{
                    await Application.Current.MainPage.DisplayAlert("Enable Location", "Please enable location settings.", "Okay");
                    var locSettings = DependencyService.Get<ILocationSettingsService>();
                    if (locSettings != null){

                      await locSettings.OpenLocationSettings();
                       //await checkLocationPermission();
                    }
                }

			}
            else if(status == PermissionStatus.Disabled){
                await Application.Current.MainPage.DisplayAlert("Enable Location", "Please enable location settings.", "Okay");
                var locSettings = DependencyService.Get<ILocationSettingsService>();
                if (locSettings != null)
                {

                    await locSettings.OpenLocationSettings();
                    //await checkLocationPermission();
                }
            }
            else if(status == PermissionStatus.Denied){
                if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                {
                    //await DisplayAlert("Need location", "Gunna need that location", "Okay");
                }

                var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                status = results[Permission.Location];
            }
			else if (status != PermissionStatus.Unknown)
			{
				//await DisplayAlert("Location Denied", "Please enable location services for PP.", "Okay");
			}

			return status;
		}


        public static bool DoubleEquals(double obj1, double obj2){
            //tolerance difference para maconsider nga equal sila
            double difference = 0.00000001;

            if (Math.Abs(obj1 - obj2) <= difference)
                return true;
            else
                return false;

        }

        public static double getCoordinatesDistanceInMeters(Position MyCoord, Position MarkerCoord)
        {
            int Radius = 6371;// radius of earth in Km
            double lat1 = MyCoord.Latitude;
            double lat2 = MarkerCoord.Latitude;
            double lon1 = MyCoord.Longitude;
            double lon2 = MarkerCoord.Longitude;
            double dLat = ToRadians(lat2 - lat1);
            double dLon = ToRadians(lon2 - lon1);
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2)
                           + Math.Cos(ToRadians(lat1))
                           * Math.Cos(ToRadians(lat2)) * Math.Sin(dLon / 2)
                    * Math.Sin(dLon / 2);
            double c = 2 * Math.Asin(Math.Sqrt(a));
            double valueResult = Radius * c;
            double km = valueResult / 1;


            //NumberFormatInfo nfi = new NumberFormatInfo();


            //DecimalFormat newFormat = new DecimalFormat("####");


            int kmInDec = int.Parse(km.ToString("####"));
            double meter = valueResult % 1000;
            int meterInDec = int.Parse(meter.ToString("####"));
            //Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
            //+ " Meter   " + meterInDec);
            //return (Radius * c) * 0.621371;//(kilometers)*miles
            return km;
        }

       static double ToRadians(double value){
            return (Math.PI / 180) * value;
        }

       
    }

}
