﻿using System;
using System.Collections.Generic;
using Xamarin.Forms.Maps;

namespace PP.tools
{
    public static class MapHelper
    {


		public static double DegToRad(this double degrees)
		{
			return degrees * Math.PI / 180;
		}

		public static double RadToDeg(this double radians)
		{
			return radians * 180 / Math.PI;
		}

		/// <summary>
		/// Calculates distance between points in kilometers.
		/// </summary>
		/// <returns>The to.</returns>
		/// <param name="p1">P1.</param>
		/// <param name="p2">P2.</param>
		public static double DistanceTo(this Position p1, Position p2)
		{
			// convert to radians
			double phi_1, phi_2, lambda_1, lambda_2;
			phi_1 = p1.Latitude.DegToRad();
			phi_2 = p2.Latitude.DegToRad();
			lambda_1 = p1.Longitude.DegToRad();
			lambda_2 = p2.Longitude.DegToRad();

			return 6371 * Math.Acos(Math.Sin(phi_1) * Math.Sin(phi_2) + Math.Cos(phi_1) * Math.Cos(phi_2) * Math.Cos(lambda_2 - lambda_1));
		}

		
		/// <summary>
		/// Calculates total distance from a set of coordinates.
		/// </summary>
		/// <returns>The distance in kilometers.</returns>
		public static double TotalDistance(List<Xamarin.Forms.Maps.Position> points)
		{
			double distance = 0;
			for (int i = 0; i < points.Count - 1; i++)
			{
				distance += points[i].DistanceTo(points[i + 1]);
			}
			return distance;
		}

		

		public static Xamarin.Forms.Maps.Position GetCentralGeoCoordinate(List<Xamarin.Forms.Maps.Position> geoCoordinates)
		{
			if (geoCoordinates.Count == 1)
			{
				return geoCoordinates[0];
			}

			double x = 0;
			double y = 0;
			double z = 0;

			foreach (var geoCoordinate in geoCoordinates)
			{
				var latitude = geoCoordinate.Latitude.DegToRad();
				var longitude = geoCoordinate.Longitude.DegToRad();

				x += Math.Cos(latitude) * Math.Cos(longitude);
				y += Math.Cos(latitude) * Math.Sin(longitude);
				z += Math.Sin(latitude);
			}

			var total = geoCoordinates.Count;

			x = x / total;
			y = y / total;
			z = z / total;

			var centralLongitude = Math.Atan2(y, x);
			var centralSquareRoot = Math.Sqrt(x * x + y * y);
			var centralLatitude = Math.Atan2(z, centralSquareRoot);

			return new Xamarin.Forms.Maps.Position(centralLatitude.RadToDeg(), centralLongitude.RadToDeg());
		}
    }
}
