﻿using System;
namespace PP.tools
{
    public interface IIdleTimerService
    {
        void startIdleTimer();
        void didIdleTimerStarted();
        void didIdleTimedOut();
        void didIdleTimerCancelledOrReset();
        void didElapseTime(int secondsRemaining);
    }
}
