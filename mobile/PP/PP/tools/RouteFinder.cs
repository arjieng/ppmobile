﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Maps;

namespace PP.tools
{
    public class RouteFinder
    {

        public interface IRouteFinderCallback
        {
            void didFindRouteSuccess(Polyline polyline, String travelTime, RouteFinder caller);
            void didFindRouteFailed(String error, RouteFinder caller);
        }

        public class RouteFinderCallback : IRouteFinderCallback
        {
            public void didFindRouteFailed(string error, RouteFinder caller)
            {
                throw new NotImplementedException();
            }

            public void didFindRouteSuccess(Polyline polyline, String travelTime, RouteFinder caller)
            {
                throw new NotImplementedException();
            }
        }

        private double originLatitude;
        private double originLongitude;
        private double destinationLatitude;
        private double destinationLongitude;
        private IRouteFinderCallback callback = new RouteFinderCallback();

        HttpClient client;


        public String travelTime;

        public Polyline polyline;

        public RouteFinder(double originLatitude, double originLongitude, double destinationLatitude, double destinationLongitude)
        {

			this.originLatitude = originLatitude;
			this.originLongitude = originLongitude;
			this.destinationLatitude = destinationLatitude;
			this.destinationLongitude = destinationLongitude;
        }


        public void setRouteFinderCallbacks(IRouteFinderCallback callback){
            this.callback = callback;
        }

        async public void startFinding(){
            await getRequest();
        }

        private String buildUrl(){
				// Origin of route
				String str_origin = "origin=" + originLatitude + "," + originLongitude;

				// Destination of route
				String str_dest = "destination=" + destinationLatitude + "," + destinationLongitude;

				// Sensor enabled
				String sensor = "sensor=false";

				String parameters = str_origin + "&" + str_dest + "&" + sensor;//+"&"+waypoints;

				// Output format
				String output = "json";

				// Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key="+Constants.GOOGLE_DIRECTIONS_KEY;

                
				return url;
			
        }


		private async Task<JObject> getRequest()
		{

			
			var uri = new Uri(buildUrl());
			JObject result = null;



			try
			{


                client = new HttpClient();
				HttpResponseMessage response = null;
				response = await client.GetAsync(uri);

				var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				result = JObject.Parse(stringResult);
				//System.Diagnostics.Debug.WriteLine("result: {0}", stringResult);

				//convert response to steps
                polyline = parseRouteResponse(result["routes"].Value<JArray>());
                if (polyline.Positions.Count > 1)
                    callback.didFindRouteSuccess(polyline, travelTime, this);
				//else
				//callback.didFindRouteFailed("You are close to your destination", this);

                else{
                    travelTime = "Arrived";
					callback.didFindRouteSuccess(polyline, travelTime, this);
                }
				



				return result;

			}
			catch (WebException ex)
			{
				System.Diagnostics.Debug.WriteLine("Exception" + ex.Message);
				var respons = (HttpWebResponse)ex.Response;
				var dataStream = respons.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				var serverMessage = reader.ReadToEnd();
				result = JObject.Parse(serverMessage);

                callback.didFindRouteFailed(serverMessage, this);
				return result;

			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Another exception: " + ex.Message);
                Log.e("dsad",ex.StackTrace);

                callback.didFindRouteFailed(ex.Message, this);

                return null;
			}
		}

        Polyline parseRouteResponse(JArray routes){


            Polyline lines = new Polyline();
            lines.StrokeColor = Color.Aqua;
            lines.StrokeWidth = 2;

            if(routes == null)
                return lines;
            if (routes.Count < 1)
                return lines; 
            //get route steps
            JArray legs = routes[0].Value<JObject>()["legs"].Value<JArray>();

            JArray steps = legs[0].Value<JObject>()["steps"].Value<JArray>();

            foreach(JObject step in steps){
                String polyline = step["polyline"]["points"].Value<String>();
                List<Xamarin.Forms.GoogleMaps.Position>  list = decodePolyline(polyline);

                foreach(Xamarin.Forms.GoogleMaps.Position p in list){
                    lines.Positions.Add(new Xamarin.Forms.GoogleMaps.Position(p.Latitude,p.Longitude));
                }

            }


            //get travel time
            //JArray legs = routes["legs"].Value<JArray>();
            if ( legs != null && legs.Count > 0)
			{
				
				travelTime = legs[0]["duration"]["text"].Value<String>() ;
				
			}

            return lines;
        }

        List<Xamarin.Forms.GoogleMaps.Position> decodePolyline(String encodedPolyline){


			List<Xamarin.Forms.GoogleMaps.Position> poly = new List<Xamarin.Forms.GoogleMaps.Position>();
            int index = 0, len = encodedPolyline.Length;
			int lat = 0, lng = 0;

			while (index < len)
			{
				int b, shift = 0, result = 0;
				do
				{
					b = encodedPolyline.ToCharArray()[index++] - 63;
					result |= (b & 0x1f) << shift;
					shift += 5;
				} while (b >= 0x20);
				int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
				lat += dlat;

				shift = 0;
				result = 0;
				do
				{
					b = encodedPolyline.ToCharArray()[index++] - 63;
					result |= (b & 0x1f) << shift;
					shift += 5;
				} while (b >= 0x20);
				int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
				lng += dlng;

                Xamarin.Forms.GoogleMaps.Position p = new Xamarin.Forms.GoogleMaps.Position((((double)lat / 1E5)),
						(((double)lng / 1E5)));
				poly.Add(p);
			}

			return poly;
        }
    }
}
