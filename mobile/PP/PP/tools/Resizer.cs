﻿using System;
using Xamarin.Forms;

namespace PP
{
	public static class Resizer
	{

		static float multiplier = App.screenScale;

		public static Element scaleChild(Element child)
		{

			/**
			 * 
			 * For preview purposes
			 * 
			 */
			//if (isEpsilonZero(multiplier))
			//	multiplier = 1.0f;


			//setting the frame

			if ((child as VisualElement) != null)
			{

				//System.Diagnostics.Debug.WriteLine("TYPE: Visual Element");
				VisualElement ve = ((VisualElement)child);
				ve.HeightRequest = ve.HeightRequest < 0 ? ve.HeightRequest : ve.HeightRequest * multiplier;
				ve.WidthRequest = ve.WidthRequest < 0 ? ve.WidthRequest : ve.WidthRequest * multiplier;

				ve.MinimumWidthRequest = ve.MinimumWidthRequest * multiplier;
				ve.MinimumHeightRequest = ve.MinimumHeightRequest * multiplier;

				//check for tendencies of height becoming zero, especially those HeightRequest that are not specified in xaml
				//ve.HeightRequest = isEpsilonZero(ve.HeightRequest) ? 5 : ve.HeightRequest;
			}


			//setting margin
			if ((child as View) != null)
			{

				//System.Diagnostics.Debug.WriteLine("TYPE:View");
				View v = ((View)child);
				var labelMargin = v.Margin;
				v.Margin = new Thickness(labelMargin.Left * multiplier, labelMargin.Top * multiplier, labelMargin.Right * multiplier, labelMargin.Bottom * multiplier);

			}
			// set padding
			if ((child as Layout) != null)
			{

				//System.Diagnostics.Debug.WriteLine("TYPE: Layout");
				Layout l = ((Layout)child);
				Thickness pad = l.Padding;
				l.Padding = new Thickness(pad.Left * multiplier, pad.Top * multiplier, pad.Right * multiplier, pad.Bottom * multiplier);

			}

			////setting font
			if ((child as Label) != null)
			{
				Label lbl = child as Label;
				double size = lbl.FontSize;
				lbl.FontSize = size * multiplier;


				//check for tendencies of height becoming zero, especially those that are not specified in xaml
				lbl.HeightRequest = isEpsilonZero(lbl.HeightRequest) ? lbl.FontSize : lbl.HeightRequest;

				//System.Diagnostics.Debug.WriteLine("TYPE: Label: " + lbl.Text + " fontSize: " + lbl.FontSize + " scale:" + multiplier
				//								   + " MArgin: (" + lbl.Margin.Left + "," + lbl.Margin.Top + "," + lbl.Margin.Right + ", " + lbl.Margin.Bottom + ")");

			}

			if ((child as Button) != null)
			{
				//System.Diagnostics.Debug.WriteLine("TYPE: Button");
				Button btn = child as Button;
				double size = btn.FontSize;
				btn.FontSize = size * multiplier;

				//check for tendencies of height becoming zero, especially those that are not specified in xaml
				//btn.HeightRequest = isEpsilonZero(btn.HeightRequest) ? btn.FontSize : btn.HeightRequest;
			}

			if ((child as Entry) != null)
			{
				//System.Diagnostics.Debug.WriteLine("TYPE: Button");
				Entry en = child as Entry;
				double size = en.FontSize;
				en.FontSize = size * multiplier;

				//check for tendencies of height becoming zero, especially those that are not specified in xaml
				//en.HeightRequest = isEpsilonZero(en.HeightRequest) ? en.FontSize : en.HeightRequest;
			}
			if ((child as CustomPicker) != null)
			{
				//System.Diagnostics.Debug.WriteLine("TYPE: Button");
				CustomPicker en = child as CustomPicker;
				double size = en.FontSize;
				en.FontSize = size * multiplier;

				//check for tendencies of height becoming zero, especially those that are not specified in xaml
				//en.HeightRequest = isEpsilonZero(en.HeightRequest) ? en.FontSize : en.HeightRequest;
			}


			if ((child as ListView) != null)
			{
				ListView lv = child as ListView;
				lv.RowHeight = (int) (lv.RowHeight * multiplier);
			}

			if ((child as StackLayout) != null)
			{
				StackLayout sl = child as StackLayout;
				sl.Spacing = sl.Spacing * multiplier;
			}

			//Log.e("HAHAHAHA", "TUMOY");
			return child;

		}

		public static bool isEpsilonZero(float value)
		{

			const float ERR = 0.0001f;
			return value <= ERR && value >= -1 * ERR;
		}

		public static bool isEpsilonZero(double value)
		{

			const float ERR = 0.0001f;
			return value <= ERR && value >= -1 * ERR;
		}
	}
}
