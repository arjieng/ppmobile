﻿using System;
using System.IO;
using Xamarin.Forms;

namespace PP.tools
{
    public static class QRUtils
    {

        public static void CreateQRCodeImage(String data, Image toSetImage, int codeWidth = 100, int codeHeight = 100){

            if (data == null)
                throw new NullReferenceException("No data to encode");

			Stream s = DependencyService.Get<IQRCodeCreatorService>().ConvertImageStream(data, codeWidth, codeHeight);
            if(toSetImage!=null)
			    toSetImage.Source = ImageSource.FromStream(() => { return s; });

        }

    }
}
