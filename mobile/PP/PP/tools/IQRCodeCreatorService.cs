﻿using System;
using System.IO;

namespace PP.tools
{
	public interface IQRCodeCreatorService
	{
		Stream ConvertImageStream(String data, int width = 300, int height = 300);
	}
}
