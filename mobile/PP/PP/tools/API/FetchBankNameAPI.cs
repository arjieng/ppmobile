﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class FetchBankNameAPI:PP.BaseAPI
    {
        public FetchBankNameAPI(string auth_token, string routing_number)
        {
			parameters = new JObject();
			parameters.Add("auth_token", auth_token);
			parameters.Add("routing_number", routing_number);
        }

        public override string getMethod()
        {
            return "GET";
        }

        public override string getURLTail()
        {
            return "/banks/bank_name";
        }
    }
}
