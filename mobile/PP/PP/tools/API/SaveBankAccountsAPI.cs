﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class SaveBankAccountsAPI:PP.BaseAPI
    {
        public SaveBankAccountsAPI(string auth_token,
                                   JArray bank_accounts,
                                   JArray removeAccounts,
                                   bool changed_profile,

                                   String address,
                                   String city,
                                   String state, 
                                   String zip,
                                   String phone
                                  )
        {

            parameters = new JObject();

			parameters.Add("auth_token", auth_token);
			parameters.Add("bank_accounts", bank_accounts);
            parameters.Add("remove_accounts", removeAccounts);
			parameters.Add("changed_profile", changed_profile);

            if(changed_profile){

                JObject userDetail = new JObject();
				userDetail.Add("address",address);
				userDetail.Add("city",city);
				userDetail.Add("state",state);
				userDetail.Add("zip",zip);
				userDetail.Add("phone",phone);
                parameters.Add("user_detail",userDetail);

            }

            Log.e("SaveAccountsParams",parameters.ToString());
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            return "/users/bank_accounts/update_accounts";
        }
    }
}
