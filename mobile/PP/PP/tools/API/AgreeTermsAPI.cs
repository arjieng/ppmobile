﻿using System;
namespace PP.tools.API
{
    public class AgreeTermsAPI:PP.BaseAPI
    {
        public AgreeTermsAPI(string term_version)
        {

            parameters = new Newtonsoft.Json.Linq.JObject();
            parameters.Add("auth_token",Constants.currentUser.token);
            parameters.Add("term_version",term_version);
        }

        public override string getMethod()
        {
            return "post";
        }

        public override string getURLTail()
        {
            return "/users/accept_term";
        }
    }
}
