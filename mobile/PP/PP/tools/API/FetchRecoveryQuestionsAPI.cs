﻿using System;
namespace PP.tools.API
{
    public class FetchRecoveryQuestionsAPI:PP.BaseAPI
    {
        public FetchRecoveryQuestionsAPI(string username)
        {

            parameters = new Newtonsoft.Json.Linq.JObject();
            parameters.Add("username",username);
        }

        public override string getMethod()
        {
            return "GET";
        }

        public override string getURLTail()
        {
            return "/users/fetch_recovery_questions";
        }
    }
}
