﻿using System;
namespace PP.tools.API
{
    public class FetchTransactionsSummaryAPI:PP.BaseAPI
    {
        public FetchTransactionsSummaryAPI(string auth_token, int merchant_status)
        {
            parameters = new Newtonsoft.Json.Linq.JObject();
            parameters.Add("auth_token",auth_token);
            parameters.Add("merchant_status", merchant_status);
        }

        public override string getMethod()
        {
            return "GET";
        }

        public override string getURLTail()
        {
            return "/users/transactions/financial_statistics";
        }
    }
}
