﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PP.tools.models;

namespace PP.tools.API
{
    public class SignupAPI:PP.BaseAPI
    {
        public SignupAPI(
            String username,
            String password,
            String confirmPassword,
            String email,
            String birthdate,
            String image,
            String firstName,
            String lastName,
            String phone,
            String term_version,
			List<Question> questions,
			List<String> answers,
            String referralCode = null
        )
        {
            image = "data:image/jpeg;base64," + image;


            JObject user = new JObject();
			user.Add("username",username);
			user.Add("password",password);
			user.Add("password_confirmation",confirmPassword);
			user.Add("email",email);
			user.Add("image",image);
            user.Add("birthdate", birthdate);
			JObject userDetail = new JObject();
			userDetail.Add("first_name",firstName);
			userDetail.Add("last_name",lastName);
            if (referralCode != null)
			userDetail.Add("referral_code",referralCode);
			userDetail.Add("phone",phone);
            userDetail.Add("term_version",term_version);

            JArray jQuestions = new JArray();
            foreach(Question q in questions){
                JObject j = new JObject();
                j.Add("id",q.question_id);
                j.Add("ask",q.question);
                j.Add("answer",answers[questions.IndexOf(q)]);

                jQuestions.Add(j);
            }



            parameters = new JObject();
            parameters.Add("user",user);
            parameters.Add("user_detail",userDetail);
            parameters.Add("questions",jQuestions);
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            return "/sign-up";
        }
    }
}
