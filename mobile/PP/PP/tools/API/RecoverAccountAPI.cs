﻿﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class RecoverAccountAPI:PP.BaseAPI
    {
        public RecoverAccountAPI(string username,
                                 string questionOneId,
                                string questionOneAnswer,
                                string questionTwoId,
                                string questionTwoAnswer,
                                string questionThreeId,
                                string questionThreeAnswer)
        {


            parameters = new Newtonsoft.Json.Linq.JObject();
            parameters.Add("username", username);

            JArray recQuestions = new JArray();

            JObject q1 = new JObject();
            q1.Add("id",questionOneId);
            q1.Add("answer",questionOneAnswer);

			JObject q2 = new JObject();
			q2.Add("id", questionTwoId);
			q2.Add("answer", questionTwoAnswer);

			JObject q3 = new JObject();
			q3.Add("id", questionThreeId);
			q3.Add("answer", questionThreeAnswer);


			recQuestions.Add(q1);
			recQuestions.Add(q2);
			recQuestions.Add(q3);

            parameters.Add("recovery_questions",recQuestions);
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            return "/recover_account";
        }
    }
}
