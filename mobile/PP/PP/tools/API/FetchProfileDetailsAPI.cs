﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class FetchProfileDetailsAPI:PP.BaseAPI
    {
        private string UserId;
        public FetchProfileDetailsAPI(String user_id, String auth_token)
        {
            this.UserId = user_id;
            parameters = new JObject();
            parameters.Add("auth_token",auth_token);
        }

        public override string getMethod()
        {
            return "GET";
        }

        public override string getURLTail()
        {
            return "/users/"+this.UserId;
        }
    }
}
