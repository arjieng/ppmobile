﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class FetchMoreTransactionsAPI:PP.BaseAPI
    {
        public FetchMoreTransactionsAPI(string paginationUrl)
        {
			parameters = new JObject();

			Uri uri = new Uri(Constants.ROOT_URL + paginationUrl);

			Dictionary<string, string> decodedParams = URIHelper.DecodeQueryParameters(uri);

			parameters.Add("auth_token", decodedParams["auth_token"]);
			parameters.Add("offset", decodedParams["offset"]);

		}

        public override string getMethod()
        {
            return "GET";
        }

        public override string getURLTail()
        {

            return "/users/transactions";
        }
    }
}
