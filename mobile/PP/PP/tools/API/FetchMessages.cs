﻿using System;
namespace PP.tools.API
{
    public class FetchMessages : PP.BaseAPI
    {
        string IDtran;
        public FetchMessages(string id)
        {
            IDtran = id;
        }

        public override string getMethod()
        {
            return "get";
        }

        public override string getURLTail()
        {
            return "/users/transactions/"+IDtran+"/conversations?auth_token="+Constants.currentUser.token;
        }
    }
}
