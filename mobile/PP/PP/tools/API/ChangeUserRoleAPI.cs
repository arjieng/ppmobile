﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class ChangeUserRoleAPI:PP.BaseAPI
    {
        public ChangeUserRoleAPI(String authToken, Constants.MERCHANT_STATUS role)
        {
            parameters = new JObject();
            parameters.Add("auth_token",authToken);
            parameters.Add("role",(int)role);

        }

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            return "/users/change_role";
        }
    }
}
