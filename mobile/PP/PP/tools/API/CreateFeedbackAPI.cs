﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class CreateFeedbackAPI:PP.BaseAPI
    {
        public string ratedUserId;

        public CreateFeedbackAPI(
                string raterId,
                string raterToken,
                string ratedId,
                string transactionId,
                int rating,
                string feedbackMessage
            
            )
        {
            ratedUserId = ratedId;

            parameters = new JObject();

            parameters.Add("user_id", ratedId);
            parameters.Add("auth_token",raterToken);

            JObject feedback = new JObject();
            feedback.Add("rater_id", raterId);
            feedback.Add("user_transaction_id",transactionId);
            feedback.Add("rating",rating);
            feedback.Add("body", feedbackMessage);

            parameters.Add("feedback", feedback);


        }

        public override string getMethod()
        {
            return "post";
        }

        public override string getURLTail()
        {
            return "/users/"+ratedUserId+"/feedbacks";
        }
    }
}
