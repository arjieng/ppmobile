﻿using System;
namespace PP.tools.API
{
    public class FetchUSStatesAPI:PP.BaseAPI
    {
        public FetchUSStatesAPI(string auth_token)
        {
            parameters = new Newtonsoft.Json.Linq.JObject();
            parameters.Add("auth_token", auth_token);
        }

        public override string getMethod()
        {
            return "get";
        }

        public override string getURLTail()
        {
            return "/users/fetch_states";
        }
    }
}
