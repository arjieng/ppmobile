﻿using System;
namespace PP.tools.API.BaseAPI
{
    public class FetchTermVersionAPI : PP.BaseAPI
    {
        public FetchTermVersionAPI()
        {
			parameters = new Newtonsoft.Json.Linq.JObject();
            parameters.Add("auth_token", Constants.currentUser.token);
        }

        public override string getMethod()
        {
			return "GET";
        }

        public override string getURLTail()
        {
           return "/users/fetch_term_version";
        }
    }
}
