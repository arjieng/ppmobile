﻿using System;
namespace PP.tools.API
{
    public class FetchSignupTermsAPI:PP.BaseAPI
    {
        public FetchSignupTermsAPI()
        {
        }

        public override string getMethod()
        {
            return "get";
        }

        public override string getURLTail()
        {
            return "/users/fetch_signup_term";
        }
    }
}
