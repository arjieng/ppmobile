﻿using System;
namespace PP.tools.API
{
    public class FetchDefaultQuestionsAPI:PP.BaseAPI
    {
        public FetchDefaultQuestionsAPI()
        {
        }

        public override string getMethod()
        {
            return "GET";
        }

        public override string getURLTail()
        {
            return "/users/fetch_default_quesions";
        }
    }
}
