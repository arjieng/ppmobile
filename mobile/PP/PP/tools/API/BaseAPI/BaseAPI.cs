﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace PP
{
	public abstract class BaseAPI
	{

		public abstract String getURLTail();
		public abstract String getMethod();

	



		HttpClient client;
		IBaseAPIInterface callback = new BaseAPIInterface();
		private String API_URL = Constants.SERVER_URL;
		private String REQUEST_URL = "";

		public JObject parameters;

        //public bool HasFormData = false;
        //public List<StreamContent> uploadImages = null;
        //public List<StringContent> uploadListData = null;

		public BaseAPI()
		{
			client = new HttpClient();
			client.MaxResponseContentBufferSize = 256000;
			client.DefaultRequestHeaders.Accept.Clear ();
			client.DefaultRequestHeaders.Accept.Add (new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue ("application/json"));
		}

 			public  BaseAPI getResponse()
		{


			REQUEST_URL = API_URL + "" + getURLTail();

			if (getMethod().ToLower().Equals("get")){
				System.Diagnostics.Debug.WriteLine("GetRequest() running..");
				System.Diagnostics.Debug.WriteLine(REQUEST_URL);
				 getRequest();

			}
			else if (getMethod().ToLower().Equals("post"))
			{
				System.Diagnostics.Debug.WriteLine("postRequest() running..");
				System.Diagnostics.Debug.WriteLine(REQUEST_URL);
                //if(HasFormData){
                //    sendImage(REQUEST_URL, uploadImages,uploadListData);
                //}
                //else
				 postRequest(parameters.ToString());
			}else if (getMethod().ToLower().Equals("patch"))
			{
				System.Diagnostics.Debug.WriteLine("putRequest() running..");
				System.Diagnostics.Debug.WriteLine(REQUEST_URL);
				 putRequest(parameters.ToString());
			}


			return this;
		}

		public void setCallbacks(IBaseAPIInterface callback)
		{
			this.callback = callback;
		}



		public async Task<JObject> postRequest(string json)
		{	
			REQUEST_URL = API_URL + "" + getURLTail();
			var uri = new Uri(REQUEST_URL);
			JObject result = null;


			//if (Constants.isDebug)
			//{
			//	callback.OnSuccess(result, this);
			//	return null;
			//}

			try
			{
				var content = new StringContent(json, Encoding.UTF8, "application/json");

				//Log.e("POST", "1>>" + content.ToString() + " URL : " + REQUEST_URL);
				HttpResponseMessage response = null;
                //response = await client.PostAsync(uri, content);


                var progressContent = new ProgressStreamContent(content,4096,(sent, total) => {

                    if ((callback as IBaseAPIProgressInterface) != null)
                        (callback as IBaseAPIProgressInterface).OnProgressChanged(sent, total, this);
                });

                response = await client.PostAsync(uri, progressContent);




				//Log.e("POST", "2>>" + response);

				var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				//Log.e("POST", "3>>" + stringResult);
				result = JObject.Parse(stringResult);
                Log.e("POST", "Response>>" + result);

                int code = (int)response.StatusCode;

                Device.BeginInvokeOnMainThread(()=>{
					if (code == 200)
					{

						//callback.OnErrorCode(response.StatusCode.GetType(), this);


						try
                        {
                            if (result.HasValues)

                                if (result["error"] != null)
                                {
                                    callback.OnError(result["error"].ToString(), this);
                                }
                                else if (result["errors"] != null)
                                {

                                    if (result["status"].Value<int>() == 401)
                                    {
                                        UserUnauthorized();
                                    }
                                    else if (result["errors"].Value<JArray>()[0] != null)
                                        callback.OnError(result["errors"][0].Value<String>(), this);

                                }
                                else
                                {
                                    if (result["status"].Value<int>() == 300)
                                    {
                                    callback.OnError(result.ToString(), this);
                                    }
                                    else if (result["status"].Value<int>() == 200)
                                    {
                                        callback.OnSuccess(result, this);
                                    }
                                    else if (result["status"].Value<int>() == 401)
                                    {
                                        //user is unauthorized(token no longer valid), needs to login again.
                                        UserUnauthorized();

                                    }
                                }
						} catch (Exception ex) {
						    
						}


					}
					else
					{
						callback.OnErrorCode(code, this);
					}


				});
				
				return result;

			}
			catch (WebException ex)
			{
				var respons = (HttpWebResponse)ex.Response;
				var dataStream = respons.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				var serverMessage = reader.ReadToEnd();
				result = JObject.Parse(serverMessage);
                Device.BeginInvokeOnMainThread(()=>{

                callback.OnError(result.ToString(), this);
                });
				return result;

			}
            catch(Exception e){
                Log.e("Exception", e.Message); 
                Device.BeginInvokeOnMainThread(() => {
                callback.OnError(e.Message,this);

				});
                return result;
            }
		}

        private void UserUnauthorized(){

			Device.BeginInvokeOnMainThread(() => {
				Constants.Logout();
				Application.Current.MainPage.DisplayAlert("Logged out", "You have been logged out, please try logging in again.", "Okay");
			});
        }

		private async Task<JObject> putRequest(string json)
		{
			REQUEST_URL = API_URL + "" + getURLTail();
			var uri = new Uri(REQUEST_URL);
			JObject result = null;
			try
			{
				var content = new StringContent(json, Encoding.UTF8, "application/json");

				HttpResponseMessage response = null;
				response = await client.PutAsync(uri, content);
				var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				result = JObject.Parse(stringResult);

				Log.e("Result", "" + result);
				
				if (response.StatusCode != HttpStatusCode.OK)
				{

					if (result["error"].Value<string>() != null)
					{
						callback.OnError(result["error"].ToString(), this);
					}
					else

					callback.OnError(response.ReasonPhrase,this);
				}
				else
				{

           
					//Callback
					callback.OnSuccess(result, this);
				}

				return result;

			}
			catch (WebException ex)
			{
				var respons = (HttpWebResponse)ex.Response;
				var dataStream = respons.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				var serverMessage = reader.ReadToEnd();
				result = JObject.Parse(serverMessage);
				return result;
			}
		}

		private async Task<JObject> sendImage(string urlString, List<StreamContent> images = null, List<StringContent> listOfData = null)
		{
			try
			{
				using (var multipart = new MultipartFormDataContent())
				{
					if (images != null)
						foreach (var image in images)
							multipart.Add(image);
					if (listOfData != null)
						foreach (var data in listOfData)
							multipart.Add(data);
					var response = await client.PostAsync(urlString, multipart);
					var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					if (images != null)
						foreach (var image in images)
							try { image.Dispose(); } catch (Exception ex) { }
					if (listOfData != null)
						foreach (var data in listOfData)
							try { data.Dispose(); } catch (Exception ex) { }
					JObject j = JObject.Parse(stringResult);
                    return j;
				}
			}
			catch
			{
				return null;
			}
		}

		private async Task<JObject> UpdateProfile(string urlString, List<StreamContent> images = null, List<StringContent> listOfData = null)
		{
			try
			{
				using (var multipart = new MultipartFormDataContent())
				{
					if (images != null)
						foreach (var image in images)
							multipart.Add(image);
					if (listOfData != null)
						foreach (var data in listOfData)
							multipart.Add(data);
					var response = await client.PutAsync(urlString, multipart);
					var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					if (images != null)
						foreach (var image in images)
							try { image.Dispose(); } catch (Exception ex) { }
					if (listOfData != null)
						foreach (var data in listOfData)
							try { data.Dispose(); } catch (Exception ex) { }
					return JObject.Parse(stringResult);
				}
			}
			catch
			{
				return null;
			}
		}

		private async Task<JObject> getRequest()
		{

			//convert json parameters to query string
			if (parameters != null)
			{
				//var s = String.Join("&", parameters.Children().Cast<JProperty>() .Select(jp => jp.Name + "=" +  HttpUtility.UrlEncode(jp.Value.ToString())));
				var s = String.Join("&", parameters.Children().Cast<JProperty>().Select(jp => jp.Name + "=" + Uri.EscapeUriString(jp.Value.ToString())));
				REQUEST_URL = REQUEST_URL+"?" + s;


			}
            Log.e("Get","URL: "+REQUEST_URL);

			var uri = new Uri(REQUEST_URL);
			JObject result = null;

			//if (Constants.isDebug)
			//{
			//	callback.OnSuccess(result, this);
			//	return null;
			//}


			try
			{



				HttpResponseMessage response = null;
                response = await client.GetAsync(uri);

				//System.Diagnostics.Debug.WriteLine("response:" + response.ToString());
				var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				result = JObject.Parse(stringResult);
				System.Diagnostics.Debug.WriteLine("result: {0}", stringResult);

     //           if(response.StatusCode != HttpStatusCode.OK){

					//if (result["error"].Value<string>() != null)
					//{
					//	callback.OnError(result["error"].ToString(), this);
					//}
					//else if (result["errors"].Value<JArray>() != null)
					//{
					//	if (result["errors"].Value<JArray>()[0] != null)
					//		callback.OnError(result["errors"][0].Value<String>(), this);
					//}
					//else
						//callback.OnError(response.ReasonPhrase, this);
                //}
                //else
                //callback.OnSuccess(result, this);

                Device.BeginInvokeOnMainThread(()=>{
                    
				    int code = (int)response.StatusCode;
    				if (code == 200)
    				{

    					//callback.OnErrorCode(response.StatusCode.GetType(), this);

    					if (result.HasValues)

    						if (result["error"] != null)
    						{
    							callback.OnError(result["error"].ToString(), this);
    						}
    						else if (result["errors"] != null)
    						{
    							if (result["status"].Value<int>() == 401)
    							{
    								UserUnauthorized();
    							}
    							else if (result["errors"].Value<JArray>()[0] != null)
    								callback.OnError(result["errors"][0].Value<String>(), this);
    						}
    						else
    						{
    							if (result["status"].Value<int>() == 300)
    							{
                                    callback.OnError(result.ToString(),this);
    							}
    							else if (result["status"].Value<int>() == 200)
    							{
    								callback.OnSuccess(result, this);
    							}
                                else if(result["status"].Value<int>()==401){
									Device.BeginInvokeOnMainThread(() => {
										//user is unauthorized(token no longer valid), needs to login again.
										Constants.Logout();

										Application.Current.MainPage.DisplayAlert("Logged out", "You have been logged out, please try logging in again.", "Okay");
                                    });
                                }

    						}


    				}
    				else
    				{
    					callback.OnErrorCode(code, this);
    				}


				});

				return result;

			}
			catch (WebException ex)
			{
				System.Diagnostics.Debug.WriteLine("Exception" + ex.Message);
				var respons = (HttpWebResponse)ex.Response;
				var dataStream = respons.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				var serverMessage = reader.ReadToEnd();
				result = JObject.Parse(serverMessage);
				return result;

			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Another exception " + ex.Message);
				callback.OnError(ex.Message, this);
                //throw new Exception(ex.Message);

                return null;

			}
		}

		public void OnSuccess(JObject response, BaseAPI caller)
		{
			throw new NotImplementedException();
		}

		public void OnError(string errMsg, BaseAPI caller)
		{
			throw new NotImplementedException();
		}

		public void OnErrorCode(int errorCode, BaseAPI caller)
		{
			throw new NotImplementedException();
		}
	}
}
