﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace PP.tools.API.BaseAPI
{
    public class HelpR 
    {
        HttpClient client;

		public async Task<JObject> postRequest(string urlString, string json)
		{
			var uri = new Uri(urlString);
			JObject result = null;
			try
			{
				var content = new StringContent(json, Encoding.UTF8, "application/json");

				Log.e("POST", "1>>" + content.ToString() + " URL : " + urlString);
				HttpResponseMessage response = null;
				response = await client.PostAsync(uri, content);
				Log.e("POST", "2>>" + response);
				var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				Log.e("POST", "3>>" + stringResult);
				result = JObject.Parse(stringResult);
				Log.e("POST", "4>>" + result);
				return result;

			}
			catch (WebException ex)
			{
				var respons = (HttpWebResponse)ex.Response;
				if (respons != null)
				{
					var dataStream = respons.GetResponseStream();
					StreamReader reader = new StreamReader(dataStream);
					var serverMessage = reader.ReadToEnd();
					result = JObject.Parse(serverMessage);
					return result;
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("Internal server error");
					return null;
				}

			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine("Something went wrong"+e);
				return null;
			}
		}

		public async Task<JObject> putRequest(string urlString, string json)
		{
			var uri = new Uri(urlString);
			JObject result = null;
			try
			{
				var content = new StringContent(json, Encoding.UTF8, "application/json");

				HttpResponseMessage response = null;
				response = await client.PutAsync(uri, content);

				var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				Log.e("sadada", "STRING RESULT:" + stringResult);

				result = JObject.Parse(stringResult);
				return result;

			}
			catch (WebException ex)
			{
				var respons = (HttpWebResponse)ex.Response;
				if (respons != null)
				{
					var dataStream = respons.GetResponseStream();
					StreamReader reader = new StreamReader(dataStream);
					var serverMessage = reader.ReadToEnd();
					result = JObject.Parse(serverMessage);
					return result;
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("Internal server error");
					return null;
				}
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine("Something went wrong"+e);
				return null;
			}
		}

		public async Task<JObject> sendImage(string urlString, List<StreamContent> images = null, List<StringContent> listOfData = null)
		{
			try
			{
				using (var multipart = new MultipartFormDataContent())
				{
					if (images != null)
						foreach (var image in images)
							multipart.Add(image);
					if (listOfData != null)
						foreach (var data in listOfData)
							multipart.Add(data);
					var response = await client.PostAsync(urlString, multipart);
					System.Diagnostics.Debug.WriteLine("response: {0}", response.ToString());
					var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					if (images != null)
						foreach (var image in images)
                            try { image.Dispose(); } catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex); }
					if (listOfData != null)
						foreach (var data in listOfData)
                            try { data.Dispose(); } catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex); }
					return JObject.Parse(stringResult);
				}
			}
			catch (Exception e)
			{
                System.Diagnostics.Debug.WriteLine(e);
				return null;
			}
		}

		public async Task<JObject> UpdateProfile(string urlString, List<StreamContent> images = null, List<StringContent> listOfData = null)
		{
			try
			{
				using (var multipart = new MultipartFormDataContent())
				{
					if (images != null)
						foreach (var image in images)
							multipart.Add(image);
					if (listOfData != null)
						foreach (var data in listOfData)
							multipart.Add(data);
					var response = await client.PutAsync(urlString, multipart);
					var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
					if (images != null)
						foreach (var image in images)
                            try { image.Dispose(); } catch (Exception ex) {System.Diagnostics.Debug.WriteLine(ex); }
					if (listOfData != null)
						foreach (var data in listOfData)
                            try { data.Dispose(); } catch (Exception ex) {System.Diagnostics.Debug.WriteLine(ex); }
					return JObject.Parse(stringResult);
				}
			}
			catch
			{
				return null;
			}
		}

		public async Task<JObject> getRequest(string urlString)
		{
			var uri = new Uri(urlString);
			JObject result = null;
			try
			{
				HttpResponseMessage response = null;
				response = await client.GetAsync(uri);

				System.Diagnostics.Debug.WriteLine("got response:" + response.Content.ToString());
				var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				result = JObject.Parse(stringResult);
				System.Diagnostics.Debug.WriteLine("result: {0}", stringResult);

				return result;

			}
			catch (WebException ex)
			{
				System.Diagnostics.Debug.WriteLine("Exception" + ex.Message);
				var respons = (HttpWebResponse)ex.Response;
				if (respons != null)
				{
					var dataStream = respons.GetResponseStream();
					StreamReader reader = new StreamReader(dataStream);
					var serverMessage = reader.ReadToEnd();
					result = JObject.Parse(serverMessage);
					return result;
				}
				else
				{
					return null;
				}

			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Another exception" + ex.Message);
				return null;
			}
		}

		public async Task<JObject> deleteRequest(string urlString)
		{
			var uri = new Uri(urlString);
			JObject result = null;
			try
			{
				Log.e("DELETE", "1>> URL : " + urlString);
				HttpResponseMessage response = null;
				response = await client.DeleteAsync(urlString);
				Log.e("DELETE", "2>>" + response);
				var stringResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				Log.e("DELETE", "3>>" + stringResult);
				result = JObject.Parse(stringResult);
				Log.e("DELETE", "4>>" + result);
				return result;

			}
			catch (WebException ex)
			{
				var respons = (HttpWebResponse)ex.Response;
				if (respons != null)
				{
					var dataStream = respons.GetResponseStream();
					StreamReader reader = new StreamReader(dataStream);
					var serverMessage = reader.ReadToEnd();
					result = JObject.Parse(serverMessage);
					return result;
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("Internal server error");
					return null;
				}

			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine("Something went wrong");
				return null;
			}
		}
    }
}

