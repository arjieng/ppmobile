﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace PP
{

	/// <summary>
	/// Progress stream content.
	/// </summary>

	//Usage:
	//var content = new StringContent(json, Encoding.UTF8, "application/json");
    //HttpResponseMessage response = null;
	//var progressContent = new ProgressStreamContent(content, 4096, (sent, total) =>
	//{
	//	System.Diagnostics.Debug.Write("progress: " + sent + " of " + total);
	//});
	//response = await client.PostAsync(uri, progressContent);


    public class ProgressStreamContent:HttpContent
    {

		private const int defaultBufferSize = 5 * 4096;

		private HttpContent content;
		private int bufferSize;
		//private bool contentConsumed;
		private Action<long, long> progress;


		public ProgressStreamContent(HttpContent content, Action<long, long> progress) : this(content, defaultBufferSize, progress) { }

		public ProgressStreamContent(HttpContent content, int bufferSize, Action<long, long> progress)
		{
			if (content == null)
			{
                //required param ang content
				throw new ArgumentNullException("content");
			}
			if (bufferSize <= 0)
			{
                // ahak negative ang size oi
				throw new ArgumentOutOfRangeException("bufferSize");
			}

			this.content = content;
			this.bufferSize = bufferSize;
			this.progress = progress;
            //
            // Open ug headers
			foreach (var h in content.Headers)
			{
				this.Headers.Add(h.Key, h.Value);
			}
		}


		protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
        {
			return Task.Run(async () =>
			{
				var buffer = new Byte[this.bufferSize];
				long size;
				TryComputeLength(out size);
				var uploaded = 0;

                //read ug stream sa params nga content
				using (var sinput = await content.ReadAsStreamAsync())
				{
					while (true)
					{
						var length = sinput.Read(buffer, 0, buffer.Length);
						if (length <= 0) break;

						//downloader.Uploaded = uploaded += length;
						uploaded += length;
						progress?.Invoke(uploaded, size);

						//System.Diagnostics.Debug.WriteLine($"Bytes sent {uploaded} of {size}");

						stream.Write(buffer, 0, length);
                        //remove used stuffsss
						stream.Flush();
					}
				}
				stream.Flush();
			});
        }

        protected override bool TryComputeLength(out long length)
        {
			length = content.Headers.ContentLength.GetValueOrDefault();
			return true;
        }
        protected override void Dispose(bool disposing)
        {
            //clean up 
            if (disposing)
                content.Dispose();
            
            base.Dispose(disposing);
        }
    }
}
