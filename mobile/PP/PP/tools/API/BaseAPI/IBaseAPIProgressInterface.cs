﻿using System;
namespace PP
{
    public interface IBaseAPIProgressInterface:IBaseAPIInterface
    {
        void OnProgressChanged(long progress, long totalSize, BaseAPI caller);
    }
}
