﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class SetEasyPinAPI:PP.BaseAPI
    {
        public SetEasyPinAPI(string auth_token,string easy_pin)
        {
            
            parameters = new JObject();
            JObject user = new JObject();
            user.Add("easy_pin",easy_pin);

            parameters.Add("auth_token",auth_token);
            parameters.Add("user",user);
        }

        public override string getMethod()
        {
            return "post";
        }

        public override string getURLTail()
        {
            return "/users/set_easy_pin";
        }
    }
}
