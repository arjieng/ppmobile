﻿using System;
namespace PP.tools.API
{
    public class CheckEasyPinAPI:PP.BaseAPI
    {
        public CheckEasyPinAPI(string auth_token, string pin)
        {
            parameters = new Newtonsoft.Json.Linq.JObject();

            parameters.Add("auth_token",auth_token);
            parameters.Add("pin",pin);
        }

        public override string getMethod()
        {
            return "post";
        }

        public override string getURLTail()
        {
            return "/users/verify_pin";
        }
    }
}
