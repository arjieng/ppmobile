﻿using System;
namespace PP.tools.API
{
    public class FetchTransactionsAPI:PP.BaseAPI
    {
        public FetchTransactionsAPI(string auth_token)
        {
            parameters = new Newtonsoft.Json.Linq.JObject();
            parameters.Add("auth_token",auth_token);
        }

        public override string getMethod()
        {
            return "GET";
        }

        public override string getURLTail()
        {
            return "/users/transactions";
        }
    }
}
