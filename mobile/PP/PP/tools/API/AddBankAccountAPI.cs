﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class AddBankAccountAPI:PP.BaseAPI
    {
		public AddBankAccountAPI(string auth_token, string aba, string account_number, int account_type, string account_name, bool is_default, 
                                   String address,
								   String city,
								   String state,
								   String zip,
								   String phone)
        {
            parameters = new JObject();


            JObject bankAccount = new JObject();
			bankAccount.Add("aba", aba);
			bankAccount.Add("account_number", account_number);
			bankAccount.Add("account_type", account_type);
			bankAccount.Add("is_default", is_default);
			bankAccount.Add("account_name", account_name);

            parameters.Add("auth_token", auth_token);
            parameters.Add("bank_account", bankAccount);

			JObject userDetail = new JObject();
			userDetail.Add("address", address);
			userDetail.Add("city", city);
			userDetail.Add("state", state);
			userDetail.Add("zip", zip);
			userDetail.Add("phone", phone);
			parameters.Add("user_detail", userDetail);
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            return "/users/bank_accounts";
        }
    }
}
