﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class ChangeUserRoleWithFixedLocationAPI : PP.BaseAPI
    {
        public ChangeUserRoleWithFixedLocationAPI(String authToken, Constants.MERCHANT_STATUS role, double latitude, double longitude)
        {
            parameters = new JObject();
            parameters.Add("auth_token", authToken);
            parameters.Add("role", (int)role);
            parameters.Add("latitude", latitude);
            parameters.Add("longitude", longitude);

        }

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            return "/users/change_role";
        }
    }
}
