﻿using System;
namespace PP.tools.API
{
    public class CheckEasyPinExistAPI:PP.BaseAPI
    {
        public CheckEasyPinExistAPI(String token)
        {
            parameters = new Newtonsoft.Json.Linq.JObject();

            parameters.Add("auth_token",token); 
        }

        public override string getMethod()
        {
            return "get";
        }

        public override string getURLTail()
        {
            return "/users/configured_easypin";
        }
    }
}
