﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class UpdateUserLocationAPI:PP.BaseAPI
    {
        public UpdateUserLocationAPI(String authToken, double latitude, double longitude)
        {
            JObject user = new JObject();
            user.Add("latitude",latitude);
			user.Add("longitude", longitude);



            parameters = new JObject();
            parameters.Add("auth_token", authToken);
            parameters.Add("user",user);


		}

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            return "/users/update_location";
        }
    }
}
