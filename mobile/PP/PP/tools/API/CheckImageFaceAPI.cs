﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;
using Plugin.Media.Abstractions;

namespace PP.tools.API
{
    public class CheckImageFaceAPI:PP.BaseAPI
    {
        public CheckImageFaceAPI(String base64Image)
        {
            parameters = new JObject();
            base64Image = "data:image/jpeg;base64," + base64Image;
            parameters.Add("image",base64Image);
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            return "/users/verify_profile_image";
        }

       
    }
}
