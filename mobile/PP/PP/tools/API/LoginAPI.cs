﻿using System;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace PP.tools.API
{
    public class LoginAPI:PP.BaseAPI
    {
        public LoginAPI(String username, String password)
        {
            

            Guid g = Guid.NewGuid();

            parameters = new JObject();

            JObject user = new JObject();
			user.Add("username", username);
			user.Add("password", password);
			user.Add("latitude", 0);
			user.Add("longitude", 0);
            user.Add("device_token", g.ToString());
            user.Add("device_type", Constants.platform);

            parameters.Add("user",user);

        }

        public override string getMethod()
		{
			return "POST";
        }

        public override string getURLTail()
		{
			return "/sign-in";
        }
    }
}
