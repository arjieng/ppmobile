﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class FetchMoreBankAccountsAPI:PP.BaseAPI
    {
        string pageUrl;
        public FetchMoreBankAccountsAPI(string paginationUrl)
        {
			parameters = new JObject();

            Uri uri = new Uri(Constants.ROOT_URL+ paginationUrl);

            Dictionary < string, string> decodedParams = URIHelper.DecodeQueryParameters(uri);

			parameters.Add("auth_token", decodedParams["auth_token"]);
            parameters.Add("offset", decodedParams["offset"]);

        }

        public override string getMethod()
        {
            return "GET";
        }

        public override string getURLTail()
        {
            return "/users/bank_accounts";
        }


		
    }

    public static class URIHelper{
		public static Dictionary<string, string> DecodeQueryParameters(this Uri uri)
		{
			if (uri == null)
				throw new ArgumentNullException("uri");

			if (uri.Query.Length == 0)
				return new Dictionary<string, string>();

			return uri.Query.TrimStart('?')
							.Split(new[] { '&', ';' }, StringSplitOptions.RemoveEmptyEntries)
							.Select(parameter => parameter.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries))
							.GroupBy(parts => parts[0],
									 parts => parts.Length > 2 ? string.Join("=", parts, 1, parts.Length - 1) : (parts.Length > 1 ? parts[1] : ""))
							.ToDictionary(grouping => grouping.Key,
										  grouping => string.Join(",", grouping));
		}
    }
	
}
