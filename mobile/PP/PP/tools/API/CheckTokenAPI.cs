﻿using System;
namespace PP.tools.API
{
    public class CheckTokenAPI:PP.BaseAPI
    {
        public CheckTokenAPI(string token)
        {
            parameters = new Newtonsoft.Json.Linq.JObject();
            parameters.Add("auth_token", token);
        }

        public override string getMethod()
        {
            return "get";
        }

        public override string getURLTail()
        {
            return "/users/validate_authentication";
        }
    }
}
