﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class FetchBankAccountsAPI:PP.BaseAPI
    {
        public FetchBankAccountsAPI(String auth_token)
        {
            parameters = new JObject();
            parameters.Add("auth_token",auth_token);
        }

        public override string getMethod()
        {
            return "GET";
        }

        public override string getURLTail()
        {
            return "/users/bank_accounts";
        }
    }
}
