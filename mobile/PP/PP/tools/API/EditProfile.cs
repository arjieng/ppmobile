﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API.BaseAPI
{
    public class EditProfile : PP.BaseAPI
    {
        public EditProfile(string fname, string lname, string imageBase64)
        {

			var user_detail = new JObject();
			user_detail.Add("first_name", fname);
			user_detail.Add("last_name", lname);


            var user = new JObject();
			if (imageBase64 != "")
            {
                user.Add("image", imageBase64);
			}

			parameters = new JObject();
			parameters.Add("auth_token", Constants.currentUser.token);
			parameters.Add("user_detail", user_detail);
            parameters.Add("user", user);

        }

        public override string getMethod()
        {
            return "PATCH";
        }

        public override string getURLTail()
        {
            return "/users/" + Constants.currentUser.id;
        }
    }
}
