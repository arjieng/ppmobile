﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class ChangePasswordAPI : PP.BaseAPI
    {
        public ChangePasswordAPI(String token,String password, String confirmPassword)

        {

            JObject obj = new JObject();

            obj.Add("token", token);
            obj.Add("password",password);
            obj.Add("password_confirmation",confirmPassword);

            parameters = obj;
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            //return "/user/change_password";
            return "/reset_password";
        }
    }
}
