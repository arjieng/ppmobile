﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class FetchNearbySellersAPI:PP.BaseAPI
    {
        public FetchNearbySellersAPI(String authToken, double latitude, double longitude, double radius)
        {

            //JObject user = new JObject();
            //user.Add("latitude",latitude);
            //user.Add("longitude", longitude);


            parameters = new JObject();
            parameters.Add("auth_token",authToken);
            //included as one instead of separating user jobject 
            // for the purpose of uri escaping this parameters
			parameters.Add("user[latitude]", latitude);
            parameters.Add("user[longitude]", longitude);
            parameters.Add("user[radius]", radius);

        }

        public override string getMethod()
		{
			return "GET";
        }

        public override string getURLTail()
		{
			return "/users/fetch_nearby_sellers";
        }
    }
}
