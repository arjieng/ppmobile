﻿using System;
using Newtonsoft.Json.Linq;

namespace PP.tools.API
{
    public class CreateProduct : PP.BaseAPI
    {
        public CreateProduct(string name, float cost, float sellingPrice, float quantity, JArray imgURLS)
        {
            var product = new JObject();
            product.Add("name", name);
            product.Add("cost", cost);
            product.Add("selling_price", sellingPrice);
            product.Add("quantity", quantity);

            parameters = new JObject();
            parameters.Add("auth_token", Constants.currentUser.token);
            parameters.Add("product", product);
            parameters.Add("product_images", imgURLS);
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            return "/users/products/";
        }
    }
    //public class EditProduct : PP.BaseAPI
    //{
    //    string ID;
    //    public EditProduct(string productID, float cost, float sellingPrice, float quantity, JArray imgURLS)
    //    {
    //        ID = productID;
    //        var product = new JObject();
    //        product.Add("cost", cost);
    //        product.Add("selling_price", sellingPrice);
    //        product.Add("quantity", quantity);

            //parameters = new JObject();
            //parameters.Add("auth_token", Constants.currentUser.token);
            //parameters.Add("product", product);
            //parameters.Add("product_images", imgURLS);
    //    }

    //    public override string getMethod()
    //    {
    //        return "PATCH";
    //    }

    //    public override string getURLTail()
    //    {
    //        return "/users/products/" + ID;
    //    }
    //}
}
