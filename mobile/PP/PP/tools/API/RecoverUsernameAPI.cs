﻿using System;
namespace PP.tools.API
{
    public class RecoverUsernameAPI:PP.BaseAPI
    {
        public RecoverUsernameAPI(string email)
        {
            parameters = new Newtonsoft.Json.Linq.JObject();
            parameters.Add("email",email);
            
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override string getURLTail()
        {
            return "/users/recover_username";
        }
    }
}
