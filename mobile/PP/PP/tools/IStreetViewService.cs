﻿using System;
namespace PP.tools
{
    public interface IStreetViewService
    {
        void openStreetView(double latitude, double longitude);
    }
}
