﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms.Internals;

namespace PP.tools.models
{
    [Preserve(AllMembers = true)]
    public class Order
    {
        public Order()
        {
        }


		[JsonProperty("id")]
        public String Id { get; set; }

		[JsonProperty("quantity")]
		public String Quantity { get; set; }

        [JsonProperty("total_amount")]
        public double TotalAmount { get; set; }

        [JsonProperty("payment_option")]
        public int PaymentOption { get; set; }

        [JsonProperty("number_of_payment")]
        public int NumberOfPayment { get; set; }


    }
}
