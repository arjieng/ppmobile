﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms.Internals;

namespace PP.tools.models
{
	[Preserve(AllMembers = true)]
    public class BankAccount
    {
        public BankAccount()
        {
        }

		[JsonProperty("id")]
		public String ID { get; set; }


		[JsonProperty("aba")]
        public String ABANumber { get; set; }

        [JsonProperty("account_number")]
        public String AccountNumber { get; set; }


        [JsonProperty("is_default")]
        public bool IsDefault { get; set; }

        [JsonProperty("account_name")]
		public String AccountName { get; set; }

        [JsonProperty("account_type")]
        public String AccountType { get; set; }


        [JsonProperty("bank_name")]
		public String BankName { get; set; }
    }
}
