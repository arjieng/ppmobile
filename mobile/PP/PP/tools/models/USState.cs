﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms.Internals;

namespace PP.tools.models
{

	[Preserve(AllMembers = true)]
    public class USState
    {
        public USState()
        {
        }


		[JsonProperty("state")]
		public string State { get; set; }

		[JsonProperty("abbr")]
		public string Abbreviation { get; set; }
    }
}
