﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Internals;

namespace PP.tools.models
{
	[Preserve(AllMembers = true)]
    public class NearbySeller
    {
        public NearbySeller()
        {
        }

		[JsonProperty("id")]
		public String Id { get; set; }


		[JsonProperty("email")]
		public String Email { get; set; }


        [JsonProperty("image")]
        public String Image { get; set; }


		[JsonProperty("first_name")]
		public String FirstName { get; set; }


		[JsonProperty("last_name")]
		public String LastName { get; set; }


		[JsonProperty("rating")]
		public int Rating { get; set; }


		[JsonProperty("latitude")]
		public double Latitude { get; set; }


		[JsonProperty("longitude")]
		public double Longitude { get; set; }


		[JsonProperty("product_name")]
		public String ProductName { get; set; }

        public Pin LocationPin { get; set; }


    }
}
