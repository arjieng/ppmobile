﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms.Internals;

namespace PP.tools.models
{
	[Preserve(AllMembers = true)]
    public class Feedback
    {
        public Feedback()
        {
        }

		[JsonProperty("user_id")]
		public String RatingUserId { get; set; }

		[JsonProperty("image")]
		public String RatingUserImage { get; set; }

		[JsonProperty("name")]
		public String RatingUserName { get; set; }


        [JsonProperty("rate")]
        public int Rate { get; set; }

		[JsonProperty("message")]
		public String RatingMessage { get; set; }
    }
}
