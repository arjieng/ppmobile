﻿using System;
using Newtonsoft.Json;

namespace PP.tools.models
{
    public class Question
    {
        public Question()
        {
        }

		[JsonProperty("id")]
		public String question_id { get; set; }

		[JsonProperty("body")]
		public String question { get; set; }
    }
}
