﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms.Internals;

namespace PP.tools.models
{
	[Preserve(AllMembers = true)]
    public class Buyer
    {
        public Buyer()
        {
        }


		[JsonProperty("id")]
		public String Id { get; set; }
		[JsonProperty("name")]
		public String Name { get; set; }
		[JsonProperty("image")]
		public String Image { get; set; }
		[JsonProperty("rating")]
		public int Rating { get; set; }
    }
}
