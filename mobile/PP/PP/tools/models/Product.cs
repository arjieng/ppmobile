﻿using System;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Xamarin.Forms.Internals;

namespace PP.tools.models
{

    [Preserve(AllMembers = true)]
    public class Product
    {
        public Product()
        {
        }
		[JsonProperty("id")]
		public String Id { get; set; }

		[JsonProperty("user_id")]
		public String SellerId { get; set; }
		[JsonProperty("name")]
		public String ProductName { get; set; }
		[JsonProperty("cost")]
		public double Cost { get; set; }

		[JsonProperty("selling_price")]
		public double SellingPrice { get; set; }
		[JsonProperty("quantity")]
		public String Quantity { get; set; }
		[JsonProperty("images")]
		public ObservableCollection<ProductImage> Images { get; set; }
    }

	[Preserve(AllMembers = true)]
    public class ProductImage{
        
		[JsonProperty("id")]
		public String Id { get; set; }

		[JsonProperty("url")]
        public String Url { get; set; }

		[JsonProperty("home_thumb")]
		public String HomeThumb { get; set; }

		[JsonProperty("show_thumb")]
		public String ShowThumb { get; set; }

    }

}
