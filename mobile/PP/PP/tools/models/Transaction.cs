﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace PP.tools.models
{
	[Preserve(AllMembers = true)]
    public class Transaction
    {
        public Transaction()
        {
        }

		[JsonProperty("id")]
		public string Id { get; set; }


		[JsonProperty("status")]
		public string Status { get; set; }

        public Color StatusColor{

            get{

                if (Status.ToLower().Equals("completed"))
                {
                    return Color.FromHex("2097ED");
                }
                else if (Status.ToLower().Equals("on-payment plan"))
                {
                    return Color.Orange;
                }
                else if (Status.ToLower().Equals("cancelled"))
                {
                    return Color.Red;
                }
                else
                    return Color.Black;
            }


        }

        [JsonProperty("product_name")]
        public string ProductName { get; set; }


		[JsonProperty("total_amount")]
		public double TotalAmount { get; set; }


		[JsonProperty("started_at")]
		public string StartedAt { get; set; }


		[JsonProperty("user")]
		public User User { get; set; }
    }
}
