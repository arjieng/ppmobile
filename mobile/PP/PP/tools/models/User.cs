﻿using System;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
namespace PP
{
	[Preserve(AllMembers = true)]
    public class User:INotifyPropertyChanged
    {
        public User()
        {

        }
      
        [JsonProperty("token")]
		public string token { get; set; }

		[JsonProperty("id")]
		public string id { get; set; }

		[JsonProperty("email")]
        public string email { get; set; }



        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, e);
        }

        protected void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private string imageUrl;

        public event EventHandler ImagePathChanged;
        protected void OnImagePathChanged(EventArgs e){
            EventHandler handler = ImagePathChanged;
            if (handler != null)
                handler(this, e);
        }


        [JsonProperty("image")]
        public string image { get{

                return imageUrl;
            } set{
                // the full user image url is formed
                if (!value.StartsWith(Constants.ROOT_URL))
                {
                    imageUrl = Constants.ROOT_URL + value;
                }
                else imageUrl = value;

                OnPropertyChanged("UserImageChanged");
                OnImagePathChanged(EventArgs.Empty);
                //imageUrl = Constants.ROOT_URL + value;
            } }


        private string firstName = "";
		[JsonProperty("first_name")]
        public string first_name { get{
                return firstName;
            } set{
                firstName = value;
                OnPropertyChanged("FirstName");
            }}


        private string lastName = "";
		[JsonProperty("last_name")]
        public string last_name { get { return lastName; } set{
                lastName = value;
                OnPropertyChanged("LastName");
            } }


        public string name{
            get { return first_name + " " + last_name; }
        }

		[JsonProperty("phone")]
		public string phone { get; set; }

		[JsonProperty("rating")]
		public int rating { get; set; }

		[JsonProperty("failed_login_attempts")]
		public int failed_login_attempts { get; set; }

		[JsonProperty("last_sign_in_at")]
        public DateTime last_sign_in_at { get; set; }



		//transaction property
		[JsonProperty("role")]
        public string role { get; set; }


		[JsonIgnore]
        public Color role_background_color{
            get{
                if (role.ToLower().Equals("seller"))
                    return Color.Orange;
                else if (role.ToLower().Equals("buyer"))
                    return Color.Red;
                else return Color.Black;
            }
        }

       
    }


}
