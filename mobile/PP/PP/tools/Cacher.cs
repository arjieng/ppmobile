﻿using System;
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace PP.tools
{
    public static class Cacher
    {
        private static ISettings AppSettings = CrossSettings.Current;

        private static string CurrentUserKey = "ppCurrentUserKey";


        public static User GetCachedUser(){
            string stringified = CachedUserString;


            if (!string.IsNullOrEmpty(stringified))
            {
                User u = JsonConvert.DeserializeObject<User>(stringified, new JsonSerializerSettings()
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore, 
                    DefaultValueHandling = DefaultValueHandling.Populate,
                    TypeNameHandling = TypeNameHandling.Auto
                });
                return u;
            }
            else return null;
           

        }

        public static void CacheCurrentUser(){
            string stringified = JsonConvert.SerializeObject(Constants.currentUser, new JsonSerializerSettings()
            {
                DateParseHandling = DateParseHandling.DateTime,
                NullValueHandling = NullValueHandling.Ignore,
				MissingMemberHandling = MissingMemberHandling.Error,
				TypeNameHandling = TypeNameHandling.Auto
            });

            CachedUserString = stringified;
        }

        private static string CachedUserString{
            get{
                return AppSettings.GetValueOrDefault(CurrentUserKey, string.Empty);
            }
            set{
                AppSettings.AddOrUpdateValue(CurrentUserKey,value);
            }
        }

    }
}
