﻿using System;
namespace PP
{
	public class Log
	{
		public Log()
		{
		}

		public static void e(string tag, string message)
		{
			System.Diagnostics.Debug.WriteLine("{0} - {1}", tag, message);
		}

		public static void e(string msg)
		{
			System.Diagnostics.Debug.WriteLine("{0}", msg);
		}
	}
}
