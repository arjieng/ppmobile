﻿using System;
using Xamarin.Forms;

namespace PP.tools.CountdownTimer
{

    public class TimerEventArgs : EventArgs
    {
        public TimeSpan TimeRemaining { get; set; }
    }

    public interface ICountdownTimer
    {
        void Start(TimeSpan CountdownTime);
        void Stop();

        event EventHandler<TimerEventArgs> Ticked;
        event EventHandler Completed;
        event EventHandler Aborted;
    }

    public class CountdownTimer :ICountdownTimer
    {

        private bool _Stopped = false;
        private TimeSpan _Second = new TimeSpan(0, 0, 1);

        private readonly TimeSpan _Interval;
        private TimeSpan _TimeRemaining;

        private EventHandler _TickedEvent;
        private EventHandler _CompletedEvent;
        private EventHandler _AbortedEvent;

        public CountdownTimer()
        {
            _Interval = _Second;
        }


        //event EventHandler ICountdownTimer.Ticked
        //{
        //    add { _TickedEvent += value; }
        //    remove { _TickedEvent -= value; }
        //}

        //event EventHandler ICountdownTimer.Completed
        //{
        //    add { _CompletedEvent += value; }
        //    remove { _CompletedEvent -= value; }
        //}

        //event EventHandler ICountdownTimer.Aborted
        //{
        //    add { _AbortedEvent += value; }
        //    remove { _AbortedEvent -= value; }
        //}


        public event EventHandler<TimerEventArgs> Ticked;
        public event EventHandler Completed;
        public event EventHandler Aborted;

        public void Start(TimeSpan CountdownTime)
        {
            _TimeRemaining = CountdownTime;
            _Stopped = false;

            Device.StartTimer(_Interval, () =>
            {
                if (this._Stopped)
                {
                    _AbortedEvent?.Invoke(this, EventArgs.Empty);
                    return false;
                }

                _TimeRemaining -= _Second;
                _TickedEvent?.Invoke(this, new TimerEventArgs { TimeRemaining = _TimeRemaining });

                _Stopped = _TimeRemaining.Duration() == TimeSpan.Zero;

                if (_Stopped)
                    _CompletedEvent?.Invoke(this, EventArgs.Empty);

                return !_Stopped;
            });
        }

        public void Stop()
        {
            _Stopped = true;
        }
    }
}
