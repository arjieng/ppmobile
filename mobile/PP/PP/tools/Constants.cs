﻿
using System;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator.Abstractions;
using PP.Pages;
using PP.tools;
using PP.tools.models;
using PP.tools.Sockets;
using Xamarin.Forms;

namespace PP
{
    public static class Constants
    {
        //public static bool isDebug = false;

        /// <summary>
        /// Flag to automate app actions.E.g. Automatically tap login button.
        /// </summary>
        public static bool AutomateValidActions = false;

        /// <summary>
        /// Flag to che
        /// </summary>
        public static bool IsStaging = false;

        private static string IP_Address = IsStaging ? "192.168.1.5:3005" : "p2-app.com"; //"localhost:3000";//
        private static string StagingURL = "http://" + IP_Address;
        private static string ProductionURL = "https://" + IP_Address;

        //public static String ROOT_URL = "http://192.168.1.5:3005";
        //public static String ROOT_URL = "https://p2-app.com";
        public static String ROOT_URL = IsStaging ? StagingURL : ProductionURL;

        public static String SERVER_URL = ROOT_URL + "/api/v1";

        //public static String WEBSOCKET_URL = "ws://192.168.1.5:3005/cable";
        //public static String WEBSOCKET_URL = "wss://p2-app.com/cable";
        public static String WEBSOCKET_URL = (IsStaging ? "ws://" : "wss://") + IP_Address + "/cable";

        public static int platform
        {
            get
            {

                if (Device.RuntimePlatform.ToLower().Trim().Equals("android"))
                {
                    return 0;
                }
                else if (Device.RuntimePlatform.ToLower().Trim().Equals("ios"))
                {
                    return 1;
                }
                else return -1;
            }
        }

        public static User currentUser;

        public const String GOOGLE_MAPS_KEY = "AIzaSyB1WOwNC1Ipg1YdtKDJZ646QxBxd_oM-9Q";
        public const String GOOGLE_DIRECTIONS_KEY = "AIzaSyB8gsnfabKnQIJlAG3nya8320-sXmqifQ8";


        public static ActionCableClient accUsersChannel;
        public static ActionCableClient accTransactionsChannel;

        public static Boolean TransactionInProgress = false;
        public static Boolean IsInTransaction = false;
        public static Boolean ResumeTransaction = false;
        public static Boolean isMinimized = false;
        public static Boolean IsSelling = false;

        public static Xamarin.Forms.Maps.Position currentPosition;
        //
        // Social Pages URLS and Mobile Schemes
        // 
        // //Device.OpenUri(new Uri("fb://page/page_id"));
        // //Device.OpenUri(new Uri("twitter://user?user_id=userid"));
        // 
        public static string FACEBOOK_PAGE_URL = "https://www.facebook.com/1";
        public static string FACEBOOK_PAGE_URL_SCHEME = "fb://page/1";
        public static string TWITTER_PAGE_URL = "https://www.twitter.com/1";
        public static string TWITTER_PAGE_URL_SCHEME = "twitter://user?user_id=1";
        //public static string connected;

        static bool _isInBuyer = false;
        public static bool IsInBuyer
        {
            set
            {
                _isInBuyer = value;
            }
            get
            {
                return _isInBuyer;
            }
        }

        static bool _IsInSeller = false;
        public static bool IsInSeller
        {
            set
            {
                _IsInSeller = value;
            }
            get
            {
                return _IsInSeller;
            }
        }
        //Account Types
        public static string PERSONAL_SAVINGS = "Personal Savings";
        public static string PERSONAL_CHECKING = "Personal Checking";
        public static string BUSINESS_CHECKING = "Business Checking";

		public enum BUY_STATE
		{

			IDLE,
			WAITING_SELLER_BUY_RESPONSE,
			ME_SHOULD_SET_MEETUP,
			ME_WAIT_SELLER_MEETUP_RESPONSE,
			ME_WAIT_SELLER_MEETUP,
			ME_SHOULD_RESPOND_TO_MEETUP,
			MOVING_TO_MEETUP
		}

		public enum SELL_STATE
		{

			IDLE,
			ME_SHOULD_RESPOND_TO_BUY_REQUEST,
			ME_WAIT_BUYER_MEETUP,
			ME_SHOULD_RESPOND_TO_BUYER_MEETUP,
			ME_SHOULD_SET_MEETUP,
			ME_WAIT_BUYER_MEETUP_RESPONSE,
			MOVING_TO_MEETUP

		}

        public enum MERCHANT_STATUS{
            IDLE = 0,
            BUYER = 1,
            SELLER = 2
        }



         public static ActionCableClient SubscribeToUserNotifications(){

			ActionCableClient client = new ActionCableClient(Constants.WEBSOCKET_URL + "?auth_token=" + Constants.currentUser.token, "UsersChannel");
			client.IgnorePings = true;

			//JObject subscribeParams = new JObject();
			//subscribeParams.Add("user_id",Constants.currentUser.id);
			JObject extra = new JObject();
			extra.Add("user_id", Constants.currentUser.id);
			client.identifierExtras = extra;

			client.ConnectAsync();

			client.Connected += UsersChannel_Connected;

			Constants.accUsersChannel = client;

		          return client;
	     }
		static void UsersChannel_Connected(object sender, ActionCableEventArgs e)
		{
			Log.e("ActionCable",e.Channel+ " Connected!");
			accUsersChannel.Connected -= UsersChannel_Connected;
		}

		public static ActionCableClient SubscribeToTransactions(String transactionId)
		{

			ActionCableClient client = new ActionCableClient(Constants.WEBSOCKET_URL + "?auth_token=" + Constants.currentUser.token, "UserTransactionsChannel");
			client.IgnorePings = true;

			//JObject subscribeParams = new JObject();
			//subscribeParams.Add("user_id",Constants.currentUser.id);
			JObject extra = new JObject();
			extra.Add("user_transaction_id", transactionId);
			client.identifierExtras = extra;

			client.ConnectAsync();

			client.Connected += TransactionsChannel_Connected;

			Constants.accTransactionsChannel = client;

			return client;
		}
		static void TransactionsChannel_Connected(object sender, ActionCableEventArgs e)
		{
			Log.e("ActionCable", e.Channel + " Connected!");
			accUsersChannel.Connected -= TransactionsChannel_Connected;
		}

        /// <summary>
        /// Global logout call that clears user cache
        /// and moves the app to login screen
        /// </summary>

        public static void Logout(){
			Constants.currentUser = null;
            var parent = Application.Current.MainPage;

            if(parent!=null){
                try{

                    parent.Navigation.PopAsync(true);
                }   catch(InvalidOperationException ex){
                    parent.Navigation.PopModalAsync();
                }
            }
            
			Cacher.CacheCurrentUser();

			Application.Current.MainPage = new LoginPage();
        }


        /// 
        /// Transactions Channel Action types
        /// 

        public static string TRANSACTIONS_SET_MEETUP = "setMeetup";

        public static string TRANSACTIONS_DECLINE_MEETUP = "declineMeetup";
        public static string TRANSACTIONS_ACCEPT_MEETUP = "acceptMeetup";
        public static string TRANSACTIONS_CANCEL_TRANSACTION = "cancelTransaction";
        public static string TRANSACTIONS_COMPLETE = "completeTransaction";


        ///
        /// Users channel acction types
        /// 
        public static string USERS_RESPOND_ORDER = "respondOrder";
        public static string USERS_USER_LOCATION_UPDATE = "userLocationUpdate";



        public static bool shouldStopIdleTimer = false;


        // carousel view
        public static readonly Color CURRENT_INDICATOR_COLOR = Color.FromHex("#50D2C2");
        public static readonly Color INDICATOR_TINT_COLOR = Color.FromHex("#FFFFFF");

        public static readonly String BACKGROUND_IMAGE = "bg_road";
        public static readonly Color MENU_SEPARATOR_COLOR = Color.FromHex("#F5F3F3");
        public static readonly Color MENU_SELECTED_COLOR = Color.FromHex("#1AF5F3F3");



        //admob
        public static readonly String ADMOB_APP_ID_ANDROID = "ca-app-pub-5721146493095294~7422528096";
        public static readonly String ADMOB_APP_ID_IOS = "ca-app-pub-5721146493095294~5030322567";


        public static readonly bool ADMOB_SHOULD_USE_SAMPLE_ADUNIT = true;

        //Test Banner Unit. Refer https://developers.google.com/admob/android/test-ads#sample_ad_units
        public static readonly String ADMOB_SAMPLE_ADUNIT_ANDROID = "ca-app-pub-3940256099942544/6300978111"; //banner
        public static readonly String ADMOB_SAMPLE_ADUNIT_IOS = "ca-app-pub-3940256099942544/2934735716"; //banner

        //actual admob banner adunit
        public static readonly String ADMOB_BANNER_AD_ANDROID = "ca-app-pub-5721146493095294/9214956068";
        public static readonly String ADMOB_BANNER_AD_IOS = "ca-app-pub-5721146493095294/7477356653";

        //Test Interstitial Ad Unit
        public static readonly String ADMOB_SAMPLE_INTERSTITIAL_AD_UNIT_IOS = "ca-app-pub-3940256099942544/4411468910";
        public static readonly String ADMOB_SAMPLE_INTERSTITIAL_AD_UNIT_ANDROID = "ca-app-pub-3940256099942544/1033173712";

        //Actual admob Interstitial Ad Unit
        public static readonly String ADMOB_INTERSTITIAL_AD_ANDROID = "ca-app-pub-5721146493095294/2172897385";
        public static readonly String ADMOB_INTERSTITIAL_AD_IOS = "ca-app-pub-5721146493095294/6572798081";


        //admob test devices, you can combine both android and ios test devices ids here.
        public static readonly String[] TEST_DEVICES = {"85A35CE43ECFC4B45D7DF7B693D0DD04"};
        //if app uses the test ads, test devices shouldn't be used. Otherwise, 
        //if you want to show real ads, register the test devices.
        public static readonly bool SHOULD_USE_TESTDEVICES = !ADMOB_SHOULD_USE_SAMPLE_ADUNIT;
    }




}
