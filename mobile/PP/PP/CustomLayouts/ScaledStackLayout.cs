﻿﻿using System;

using Xamarin.Forms;

namespace PP
{
	public class ScaledStackLayout : StackLayout
	{
        public ScaledStackLayout(): base()
        {

        }

        protected override void OnChildAdded(Element child)
		{
            base.OnChildAdded(Resizer.scaleChild(child));
		}

	}
}

