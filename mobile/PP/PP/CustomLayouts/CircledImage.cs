﻿using System;
using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using Xamarin.Forms;

namespace PP.CustomLayouts
{
    public class CircledImage : CachedImage
    {
        public CircledImage()
        {
            Transformations = new System.Collections.Generic.List<ITransformation>() { 
                new CircleTransformation()};
            TransformPlaceholders = true;
            this.LoadingPlaceholder = "placeholder_image";
            this.ErrorPlaceholder = "placeholder_image";
        }
    }
}

