﻿using System;
using FFImageLoading.Forms;
using Xamarin.Forms;

namespace PP
{
    public class ViewTemplate : DataTemplateSelector
    {

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item != null)
            {
                return new DataTemplate(() =>
                {
                    var dtemp = new CachedImage()
                    {
                        DownsampleWidth =  ScaleCS.ScaleWidth(320),
                        DownsampleHeight = ScaleCS.ScaleHeight(220),
                        Aspect = Aspect.AspectFill,
                        Source = item.ToString()
                    };

                    TapGestureRecognizer tap = new TapGestureRecognizer();
                    tap.Tapped += (sender, e) =>
                    {
                        //((CachedImage)sender).Navigation.PushAsync(new PropertyImageViewerPage(item.ToString()));
                    };

                    dtemp.GestureRecognizers.Add(tap);

                    return dtemp;
                });
            }
            return null;
        }
    }
}

