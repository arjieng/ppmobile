﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using PP.tools.models;
using Xamarin.Forms;

namespace PP.CustomLayouts
{
    public partial class BankAccountListView : ContentView
    {
        public BankAccountListView()
        {
            InitializeComponent();
            ToRemoveAccounts = new ObservableCollection<string>();
            ItemSource = new ObservableCollection<BankAccount>();
        }

        //public void AddBankAccount(BankAccount account){
        //    list.Children.Add(new BankAccountView(){ BankAccount = account});
        //    ItemSource.Add(account);
        //}

        public void AddNewAccount(BankAccount account){
            //ItemSource.Add(account);
            //BankAccountView bav = list.Children[list.Children.Count - 1] as BankAccountView;
            //bav.EditModeEnabled = true;

            //remove the collection listener first so that the
            // list wont be reloaded entirely. This is to prevent 
            // the added changes on other accounts that wasn't saved yet
            // from being discarded

            ItemSource.CollectionChanged -= (sender, e) => refreshLayout();
            ItemSource.Add(account);

            //and add it again
            ItemSource.CollectionChanged += (sender, e) => refreshLayout();
        }

        private void refreshLayout(){
            Log.e("sada","Height:"+list.HeightRequest);
            list.Children.Clear();



            foreach(BankAccount a in ItemSource){
                list.Children.Add(new BankAccountView() { BankAccount = a });//, WidthRequest = 264, HeightRequest = 160});
            }

            //Log.e("List Height", list.Height+"");

        }


        public static readonly BindableProperty ItemSourceProperty = BindableProperty.Create(nameof(ItemSource), typeof(ObservableCollection<BankAccount>), typeof(BankAccountListView), new ObservableCollection<BankAccount>(),
				propertyChanged: (bindable, oldValue, newValue) =>
				{
					var view = bindable as BankAccountListView;

                    view.refreshLayout();
				});


		public ObservableCollection<BankAccount> ItemSource
		{
			get { return (ObservableCollection<BankAccount>)GetValue(ItemSourceProperty); }
            set { 
                SetValue(ItemSourceProperty, value); 

                value.CollectionChanged +=  (sender, e) => refreshLayout();
            }
		}

        public ObservableCollection<string> ToRemoveAccounts{
            get;set;
        }



        //Callback mehtods

        public void RemoveAccount(BankAccount ba){

            if (ba == null)
                return;

            int itemIndex = ItemSource.IndexOf(ba);

            //for api removal
			ToRemoveAccounts.Add(ba.ID);


            ItemSource.Remove(ba);
            //list.Children.RemoveAt(itemIndex);

        }
        public void deselectAllDefaultRadios(){
            foreach(BankAccountView bav in list.Children){
                bav.DeselectDefault();
            }
        }

        /// <summary>
        /// Checks for a currenytly edit enabled bank account.
        /// </summary>
        /// <returns><c>true</c>, if has edit enabled item was checked, <c>false</c> otherwise.</returns>
        /// <param name="exceptItem">Except item. - a account view that is excempted on check (can be null)</param>
        public bool CheckHasEditEnabledItem(BankAccountView exceptItem){

            bool retVal = false;
            foreach(BankAccountView bav in list.Children){
                

                if(bav.EditModeEnabled){

					if (exceptItem != null)
					{
                        if(exceptItem != bav){
                            retVal = true;
                            break;
                        }
					}
                    else{
						retVal = true;
						break;
                    }


                }

            }

            return retVal;
        }
	

        public void AppendNewBankAccounts(ObservableCollection<BankAccount> toAppendAccounts){
            
			ItemSource.CollectionChanged -= (sender, e) => refreshLayout();




			//and add it again
			ItemSource.CollectionChanged += (sender, e) => refreshLayout();
        }
    }


}
