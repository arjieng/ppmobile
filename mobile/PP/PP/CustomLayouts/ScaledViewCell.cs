﻿using System;
using Xamarin.Forms;

namespace PP
{
	public class ScaledViewCell : ViewCell
	{

		//public ScaledViewCell() : base()
		//{
		//}
		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();

			View.MeasureInvalidated += View_MeasureInvalidated;
		}

		protected override void OnChildAdded(Element child)
		{
			base.OnChildAdded(Resizer.scaleChild(child));
		}

		void View_MeasureInvalidated(object sender, EventArgs e)
		{
			var size = View.Measure(double.PositiveInfinity, double.PositiveInfinity, MeasureFlags.None);
			Height = size.Request.Height* App.screenScale;
		}
	}
}
