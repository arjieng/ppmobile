﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace PP.CustomLayouts
{
    public partial class CrossCheckbox : ContentView
    {
        public ExtendedLabel ExtendedLabel
        {
            get { return lblText;  }
        }

        public CrossCheckbox()
        {
            InitializeComponent();

        }
        /// <summary>
        /// The checkbox text property.
        /// </summary>
        public static readonly BindableProperty CheckboxTextProperty = BindableProperty.Create(nameof(CheckboxText), typeof(String), typeof(CrossCheckbox), "Checkbox",
                propertyChanged: (bindable, oldValue, newValue) =>
                {
                    var view = bindable as CrossCheckbox;
                    view.lblText.Text = (string)newValue;
                }
        );
        public String CheckboxText
        {
            get { return (String)GetValue(CheckboxTextProperty); }
            set {  SetValue(CheckboxTextProperty, value);  }
        }

        /// <summary>
        /// The checkbox text color property.
        /// </summary>
        public static readonly BindableProperty CheckboxTextColorProperty = BindableProperty.Create(nameof(CheckboxTextColor), typeof(Color), typeof(CrossCheckbox), Color.Black,
                propertyChanged: (bindable, oldValue, newValue) =>
                {
                    var view = bindable as CrossCheckbox;
                    view.lblText.TextColor = (Color)newValue;
                }
        );
        public Color CheckboxTextColor
        {
            get { return (Color)GetValue(CheckboxTextColorProperty); }
            set { 
                SetValue(CheckboxTextColorProperty, value); }
        }


        /// <summary>
        /// FONT SIZE
        /// </summary>

        public static readonly BindableProperty TextFontSizeProperty = BindableProperty.Create(nameof(TextFontSize), typeof(double), typeof(CrossCheckbox), 10.00,
                propertyChanged: (bindable, oldValue, newValue) =>
                {
                    var view = bindable as CrossCheckbox;
                    view.lblText.FontSize = (double)newValue;
                }
        );

        public double TextFontSize
        {
            get { return (double)GetValue(TextFontSizeProperty); }
            set
            {  SetValue(TextFontSizeProperty, value); }
        }

        /// <summary>
        /// The checkbox size property.
        /// </summary>
        public static readonly BindableProperty CheckboxSizeProperty = BindableProperty.Create(nameof(CheckboxSize), typeof(double), typeof(CrossCheckbox), 10.00,
                propertyChanged: (bindable, oldValue, newValue) =>
                {
                    var view = bindable as CrossCheckbox;
                    view.cvCheckbox.HeightRequest = (double)newValue;
                    view.cvCheckbox.WidthRequest = (double)newValue;


                    view.lblCheckmark.HeightRequest = (double)newValue;
                    view.lblCheckmark.WidthRequest = (double)newValue;
                    //view.lblCheckmark.FontSize = ((double)newValue) - 10;

                    view.lblCheckmark.FontSize = ((double)newValue);
                }
        );

        public double CheckboxSize
        {
            get { return (double)GetValue(CheckboxSizeProperty); }
            set
            { 
                SetValue(CheckboxSizeProperty, value); 

            }
        }

        public static readonly BindableProperty CheckboxVisibleProperty = BindableProperty.Create(nameof(CheckboxVisible), typeof(bool), typeof(CrossCheckbox), true,
                propertyChanged: (bindable, oldValue, newValue) =>
                {
                    var view = bindable as CrossCheckbox;
                    view.cvCheckbox.IsVisible = (bool) newValue;

                   
                }
        );

        public bool CheckboxVisible
        {
            get { return (bool)GetValue(CheckboxVisibleProperty); }
            set
            {
                SetValue(CheckboxVisibleProperty, value);

            }
        }



        /// <summary>
        /// The is checked property.
        /// </summary>

        public static readonly BindableProperty IsCheckedProperty = BindableProperty.Create(nameof(IsChecked), typeof(bool), typeof(CrossCheckbox), false,
                propertyChanged: (bindable, oldValue, newValue) =>
                {
                    var view = bindable as CrossCheckbox;
                    view.lblCheckmark.Text = (bool)newValue ? "✓" : "";
                }
        );

        public bool IsChecked {
            get { return (bool)GetValue(IsCheckedProperty); }
            set {             
                    SetValue(IsCheckedProperty, value); 
                }
        }

        public static readonly BindableProperty IsCheckboxEnabledProperty = BindableProperty.Create(nameof(IsCheckboxEnabled), typeof(bool), typeof(CrossCheckbox), true,
                propertyChanged: (bindable, oldValue, newValue) =>
                {
                    var view = bindable as CrossCheckbox;
                }
        );

        public bool IsCheckboxEnabled
        {
            get { return (bool)GetValue(IsCheckboxEnabledProperty); }
            set
            {
                SetValue(IsCheckboxEnabledProperty, value);
            }
        }





        public void Handle_Tapped(object sender, System.EventArgs e)
        {
            if(IsCheckboxEnabled)
            IsChecked = !IsChecked;
        }
    }
}
