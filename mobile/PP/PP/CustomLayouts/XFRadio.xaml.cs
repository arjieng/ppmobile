﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace PP.CustomLayouts
{
    public partial class XFRadio : ContentView
    {

        public static readonly BindableProperty SelectedImageProperty = BindableProperty.Create(nameof(SelectedImage), typeof(string), typeof(XFRadio), "",
               propertyChanged: (bindable, oldValue, newValue) =>
               {
                    var view = bindable as XFRadio;

               }
          );
        public string SelectedImage
        {
            get { return (string)GetValue(SelectedImageProperty); }
            set { SetValue(SelectedImageProperty, value); }
        }


        public static readonly BindableProperty DefaultImageProperty = BindableProperty.Create(nameof(DefaultImage), typeof(string), typeof(XFRadio), "", propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as XFRadio;

            view.radioImage.Source = view.DefaultImage;
        });

        public string DefaultImage{
            get { return (string)GetValue(DefaultImageProperty); }
            set { 
                    SetValue(DefaultImageProperty, value);
                }
        }

        public static readonly BindableProperty IsSelectedProperty = BindableProperty.Create(nameof(IsSelected), typeof(bool),typeof(XFRadio), false, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as XFRadio;

            view.radioImage.Source = view.IsSelected ? view.SelectedImage : view.DefaultImage;
        });

        public bool IsSelected{
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

		//private string _selectedImage;
		//public string SelectedImage
		//{
		//	get
		//	{
		//		return _selectedImage;
		//	}
		//	set
		//	{
		//		_selectedImage = value;
		//	}
		//}

		//private string _defaultImage;
		//public string DefaultImage
		//{
		//	get
		//	{
		//		return _defaultImage;
		//	}
		//	set
		//	{
		//		_defaultImage = value;
		//		radioImage.Source = _defaultImage;
  //              //radioImage.LoadingPlaceholder = _defaultImage;
		//	}
		//}

		//private bool _isSelected;
		//public bool IsSelected
		//{
		//	get
		//	{
		//		return _isSelected;
		//	}
		//	set
		//	{
  //              //tapped();
  //              radioImage.Source = value ? SelectedImage : DefaultImage;
  //              _isSelected = value;
		//	}
		//}

		public XFRadio()
		{
			InitializeComponent();
			IsSelected = false;
            //radioImage.LoadingPlaceholder = DefaultImage;

		}
		public event EventHandler RadioClicked;
		protected void OnRadioClicked()
		{
			if (RadioClicked != null) RadioClicked(this, EventArgs.Empty);
		}


		void RadioTapped(object sender, System.EventArgs e)
		{
			//tapped();
			OnRadioClicked();
		}


		void tapped()
		{
            if (IsSelected)
            {
            	radioImage.Source = DefaultImage;
            }
            else
            {
                         radioImage.Source = SelectedImage;
            }

            IsSelected = !IsSelected;

                
		}

        ///// <summary>
        ///// Default radio behaviour, once se
        ///// </summary>
        //void rbTapped(){


        //}
    }
}
