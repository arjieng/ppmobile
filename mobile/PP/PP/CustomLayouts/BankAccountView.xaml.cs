﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PP.tools.API;
using PP.tools.models;
using Xamarin.Forms;

namespace PP.CustomLayouts
{
    public partial class BankAccountView : StackLayout,IBaseAPIInterface
    {
        public BankAccountView()
        {

            this.SizeChanged += (sender, e) => {
                Log.e("SIZE_CHANGED", string.Format("Height: {0}", this.Height));
            };

            this.MeasureInvalidated += (sender, e) => {
                Log.e("MEASURE_INVALIDATED", string.Format("Height: {0}", this.Height));
            };

            InitializeComponent();
            EditModeEnabled = false;

        }

        private BankAccount bankAccount;
        public BankAccount BankAccount{
            get
            {
                return bankAccount;
            }set{
                bankAccount = value;
                refreshLayout();
            }
        }


        private bool isEditMode = false;
        public bool EditModeEnabled{
            get{
                return isEditMode;
            }
            set{
                isEditMode = value;

                entryAccountName.IsEnabled = value;
				entryRoutingNumber.IsEnabled = value;
                entryAccountNumber.IsEnabled = value;
                btnSave.IsVisible = value;
                btnEdit.IsVisible = !value;

                if(value)
                entryRoutingNumber.TextChanged += RoutingNumberChanged;
                else

				entryRoutingNumber.TextChanged -= RoutingNumberChanged;
            }
        }


        void refreshLayout(){
            radioDefault.IsSelected = BankAccount.IsDefault;
            entryRoutingNumber.Text = BankAccount.ABANumber != null?BankAccount.ABANumber:"";
            entryAccountNumber.Text = BankAccount.AccountNumber != null ? BankAccount.AccountNumber:"";
            lblBankName.Text = BankAccount.BankName != null ? BankAccount.BankName:"";
            entryAccountName.Text = BankAccount.AccountName != null ? BankAccount.AccountName : "";

            if (BankAccount.AccountType == null)
                return;


            //         if(BankAccount.AccountType.ToLower().Equals(Constants.PERSONAL_SAVINGS.ToLower())){
            //             radioSavings.IsSelected = true;
            //         }
            //         else if (BankAccount.AccountType.ToLower().Equals(Constants.PERSONAL_CHECKING.ToLower()))
            //{
            //	radioPersonal.IsSelected = true;
            //}
            //         else if (BankAccount.AccountType.ToLower().Equals(Constants.BUSINESS_CHECKING.ToLower()))
            //{
            //	radioBusiness.IsSelected = true;
            //}

            lblAccountType.Text = BankAccount.AccountType;
          
        }

		void Handle_RadioClicked(object sender, System.EventArgs e)
		{
            if (!EditModeEnabled)
                return;
            
			//radioPersonal.IsSelected = false;
			//radioSavings.IsSelected = false;
			//radioBusiness.IsSelected = false;


			//var x = sender as XFRadio;

           //Account Type not allowed to be editable
			//x.IsSelected = x.IsSelected ? x.IsSelected : !x.IsSelected;



        }

        /// <summary>
        /// Tapped listener to Default radio button
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
		void DefaultClicked(object sender, System.EventArgs e)
		{

			
			var x = sender as XFRadio;

			bool isAlreadySelected = x.IsSelected;

			if (!isAlreadySelected)
			{
				//clear all selection
				((BankAccountListView)this.Parent.Parent).deselectAllDefaultRadios();
			}

			x.IsSelected = x.IsSelected ? x.IsSelected : !x.IsSelected;


            bankAccount.IsDefault = x.IsSelected;
		}


        void RemoveAccount(object sender, System.EventArgs e)
        {
            //1st Parent is StackLayout, next in heirchy is the content view which is the bank account listview
            // Check BankAccountListView xaml file to check view heirchy
            ((BankAccountListView)this.Parent.Parent).RemoveAccount(BankAccount);
        }

        /// <summary>
        /// Deselects the default radio button on this account item.
        /// This method is called by the BankAccountListView when the deselectAllDefaultRadios() 
        /// is triggered.
        /// </summary>
        public void DeselectDefault(){
            radioDefault.IsSelected = false; 
            bankAccount.IsDefault = false;
        }



		void EditTapped(object sender, System.EventArgs e)
		{

            // Check if there are other EditEnabled Accounts
            bool hasOtherEditEnabled=((BankAccountListView)this.Parent.Parent).CheckHasEditEnabledItem(this);

            if (!hasOtherEditEnabled)
                EditModeEnabled = true;
            else
                showAlert("Please complete editing other accounts.");
		}
        void SaveTapped(object sender, System.EventArgs e)
		{

            if (!entryRoutingNumber.Text.Trim().Equals(""))
            {
                if (!entryAccountNumber.Text.Trim().Equals(""))
                {
                    if (!lblBankName.Text.ToLower().Equals("not found"))
                    {
      //                  if (!radioSavings.IsSelected && !radioPersonal.IsSelected && !radioBusiness.IsSelected)
      //                  {
      //                      //prompt user to select account type
      //                      showAlert("Please select an account type");
      //                  }
						//else
						//{
                            if(string.IsNullOrEmpty(entryAccountName.Text.Trim())){
                                showAlert("Please set an account name");
                            }
                            else{

                                bankAccount.ABANumber = entryRoutingNumber.Text.Trim();
                                bankAccount.AccountName = entryAccountName.Text.Trim();
                                bankAccount.AccountNumber = entryAccountNumber.Text.Trim();

                            //String accountType = "";
                            //if (radioPersonal.IsSelected)
                            //                            accountType = Constants.PERSONAL_CHECKING;
                            //else if (radioBusiness.IsSelected)
                            //                            accountType = Constants.BUSINESS_CHECKING;
                            //else if (radioSavings.IsSelected)
                            //accountType = Constants.PERSONAL_SAVINGS;


                            //bankAccount.AccountType = accountType;
                            bankAccount.AccountType = BankAccount.AccountType;


								EditModeEnabled = false;

							}
                            
                        //}
                    }
                    else
                    {
						//prompt user to add valid routing number

						showAlert("Please use a valid routing number");
                    }
                }
                else
                {
					//prompt user to add account number
					showAlert("Please add an account number");
                }
            }
            else{
				//prompt user to add routing number
				showAlert("Please add a routing number");
            }

		}

        void RoutingNumberChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            // reset the bank name first to disallow saving while the routing number is being verified.
            lblBankName.Text = "Not found";

            FetchBankNameAPI api = new FetchBankNameAPI(Constants.currentUser.token, entryRoutingNumber.Text.Trim());
            api.setCallbacks(this);
            api.getResponse();
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            lblBankName.Text = response["bank_name"]["name"].Value<string>();
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            lblBankName.Text = "Not found";
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            throw new NotImplementedException();
        }

        // Display alert is only for Page instances so we will look back
        // on view heirchy to get the direct reference of the page
        // that is responsible for displaying this layout.
        void showAlert(String alert){

            Page p = GetEarliestPageAncestor(this);

            if(p !=null)
            p.DisplayAlert("Alert", alert, "Okay");

        }

        Page GetEarliestPageAncestor(Element e){
            
            if (e == null)
                return null;
            
            if ((e.Parent as Page) != null)
                return e.Parent as Page;
            else return GetEarliestPageAncestor(e.Parent);

        }
    }


}
