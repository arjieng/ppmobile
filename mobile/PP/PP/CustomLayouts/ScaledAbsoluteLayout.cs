﻿﻿using System;

using Xamarin.Forms;

namespace PP
{
    public class ScaledAbsoluteLayout : AbsoluteLayout
	{
		protected override void OnChildAdded(Element child)
		{

			Element newChild = PP.Resizer.scaleChild(child);

			base.OnChildAdded(newChild);
		}

	}
}

