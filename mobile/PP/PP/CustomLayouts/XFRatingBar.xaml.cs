﻿using System;
using System.Collections.Generic;
using FFImageLoading.Forms;
using Xamarin.Forms;

namespace PP.CustomLayouts
{
    public partial class XFRatingBar : ContentView
    {
        public XFRatingBar()
        {
            InitializeComponent();
        }


        public static readonly BindableProperty RatingProperty = BindableProperty.Create(nameof(Rating), typeof(int), typeof(XFRatingBar), -1,
				propertyChanged: (bindable, oldValue, newValue) =>
				{
					var view = bindable as XFRatingBar;

                    Log.e("RatingBar", "new value: " + (int)newValue + " old value: "+(int)oldValue);

					view.sslStarContainer.Children.Clear();
                    for (var starIndex = 1; starIndex <= view.MaxStars; starIndex++)
					{


                        //create source image for current star
                        String source = starIndex <= (int)newValue ? 
                                view.HighlightStarImageSource : view.DefaultStarImageSource;

                        

                        var img = new RateStar(starIndex)
                        {
                            Source = source,
                            WidthRequest = view.Height,
                            HeightRequest = view.Height,
                            //added for when you are using cached image from ffimageloading
                            LoadingPlaceholder = view.DefaultStarImageSource
                        };

						TapGestureRecognizer tap = new TapGestureRecognizer();
						tap.Tapped += (sender, e) =>
						{
                            if(view.EditingEnabled)
							view.Rating = img.StarIndex;
						};


                        img.GestureRecognizers.Add(tap);
						view.sslStarContainer.Children.Add(img);


					}



                    
				}
		);
		public int Rating
		{
			get { return (int)GetValue(RatingProperty); }
			set {SetValue(RatingProperty, value);}
		}

		public static readonly BindableProperty MaxStarsProperty = BindableProperty.Create(nameof(MaxStars), typeof(int), typeof(XFRatingBar), 5,
			   propertyChanged: (bindable, oldValue, newValue) =>
			   {
				   var view = bindable as XFRatingBar;

                   //fire rating to regenerate the stars
                   int backupvalue = view.Rating;
                   view.Rating = 0;
                   view.Rating = backupvalue;
                    
			   });


		public int MaxStars
		{
			get { return (int)GetValue(MaxStarsProperty); }
			set { SetValue(MaxStarsProperty, value); }
		}


        public static readonly BindableProperty HighlightStarImageProperty = BindableProperty.Create(nameof(HighlightStarImageSource), typeof(String), typeof(XFRatingBar), "",
               propertyChanged: (bindable, oldValue, newValue) =>
               {
                   var view = bindable as XFRatingBar;
				   view.ForceLayout();
               });


        public String HighlightStarImageSource{
			get { return (String)GetValue(HighlightStarImageProperty); }
			set { SetValue(HighlightStarImageProperty, value); }
        }

		public static readonly BindableProperty DefaultStarImageProperty = BindableProperty.Create(nameof(DefaultStarImageSource), typeof(String), typeof(XFRatingBar), "",
			  propertyChanged: (bindable, oldValue, newValue) =>
			  {
				  var view = bindable as XFRatingBar;

                   view.ForceLayout();
			  });


        public String DefaultStarImageSource
		{
			get { return (String)GetValue(DefaultStarImageProperty); }
			set { SetValue(DefaultStarImageProperty, value); }
		}



		public static readonly BindableProperty EditingEnabledProperty = BindableProperty.Create(nameof(EditingEnabled), typeof(bool), typeof(XFRatingBar), true,
			  propertyChanged: (bindable, oldValue, newValue) =>
			  {
				  var view = bindable as XFRatingBar;

			  });


		public bool EditingEnabled
		{
			get { return (bool)GetValue(EditingEnabledProperty); }
			set { SetValue(EditingEnabledProperty, value); }
		}
    }

    public class RateStar:CachedImage{
        public int StarIndex { get; set; }
        public RateStar(int starIndex){
            StarIndex = starIndex;
        }
    }
}
