﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.LocalNotifications;
using PP.Helpers.DrawerHelper;
using PP.Pages.Dialogs;
using PP.Pages.Dialogs.Dynamic;
using PP.tools;
using PP.tools.API;
using PP.tools.models;
using PP.tools.Sockets;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using static PP.Pages.Dialogs.SellerWaitSetupDialog;

namespace PP.Pages.Seller
{
    public partial class SellerHome : ContentPage, IBaseAPIInterface, RouteFinder.IRouteFinderCallback, WaitMethodCallbacks
    {

        private Plugin.Geolocator.Abstractions.Position currentLocation;


        int currentFeedbackIndex = 0;
        Buyer currentBuyer;
        public ObservableCollection<Feedback> currentBuyerFeedbacks;
        static Order currentOrder;
        bool stoppedIsClicked = true;

        static ActionCableClient usersClient, transactionsClient;
        Loading_Dialog loadDialog;
        DisplayDialog d;
        static String currentTransactionId = null;


        bool IsMonitoringMapChanges = false;

        Xamarin.Forms.Maps.Position currentMeetupPosition = new Xamarin.Forms.Maps.Position();
        Xamarin.Forms.Maps.Position currentBuyerPosition = new Xamarin.Forms.Maps.Position();

        PointToPointRouteFinder userToMeetupFinder;
        PointToPointRouteFinder buyerToMeetupFinder;

        Pin buyerPin;
        Pin currentMeetupPin;


        /// <summary>
        /// Boolean flag to check if routefinder is for 
        /// setting new meetup or if for the received meetup from
        /// the other party
        /// </summary>
        bool IsForSetNewMeetup, IsNowMeeting = false;
        bool isForQuit = false;
        bool isLookingForBuyer = false;

        public ObservableCollection<BankAccount> accounts;


        async void back(object sender, System.EventArgs e)
        {

            if (!string.IsNullOrEmpty(currentTransactionId))
            {
                var leave = await LeaveQuestion();

                if (leave)
                {
                    Constants.IsInTransaction = false;
                    QuitPage();
                }
            }
            else if (isLookingForBuyer)
            {
                var stopSelling = await StopSellingPrompt();
                if (stopSelling)
                {
                    StopSellingTapped(null, null);
                }
            }
            else
            {
                QuitPage();
            }
        }

        async Task<bool> LeaveQuestion()
        {
            return await DisplayAlert("Close", "Transaction will be cancelled. Continue?", "Cancel transaction", "Stay");
        }

        void QuitPage()
        {
            isForQuit = true;
            Constants.shouldStopIdleTimer = false;
            ChangeRole(Constants.MERCHANT_STATUS.IDLE);

            loadDialog.Text = "Cleaning up..";
            loadDialog.open();

        }

        async void BackPressedButtonPressed()
        {

            if (!string.IsNullOrEmpty(currentTransactionId))
            {
                var leave = await LeaveQuestion();

                if (leave)
                {
                    Constants.IsInTransaction = false;
                    ActionCableClient acc = Constants.accTransactionsChannel;

                    JObject cancelData = new JObject();
                    cancelData.Add("auth_token", Constants.currentUser.token);
                    cancelData.Add("user_transaction_id", currentTransactionId);
                    cancelData.Add("action", "cancel_transaction");
                    acc.Perform(cancelData);


                    QuitPage();
                }
            }
            else if (isLookingForBuyer)
            {
                var stopSelling = await StopSellingPrompt();
                if (stopSelling)
                {
                    StopSellingTapped(null, null);
                }
            }
            else
            {
                ActionCableClient acc = Constants.accTransactionsChannel;

                JObject cancelData = new JObject();
                cancelData.Add("auth_token", Constants.currentUser.token);
                cancelData.Add("user_transaction_id", currentTransactionId);
                cancelData.Add("action", "cancel_transaction");
                acc.Perform(cancelData);
                QuitPage();
            }

        }

        protected override bool OnBackButtonPressed()
        {
            BackPressedButtonPressed();
            return true;
        }

        async Task<bool> StopSellingPrompt()
        {
            return await DisplayAlert("Close", "Do you want to stop selling?", "Stop selling", "Continue");
        }

        static ContentView cvHeaderDisplayStatic;
        static ScaledStackLayout sslBuyerDetailsStatic;

        public SellerHome(double lat, double lng)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            sslBuyerDetailsStatic = sslBuyerDetails;

            cvHeaderDisplayStatic = cvHeaderDisplay;
            loadDialog = new Loading_Dialog();


            usersClient = Constants.accUsersChannel;
            usersClient.MessageReceived += AccUsersChannel_MessageReceived;


            ChangeRole(Constants.MERCHANT_STATUS.IDLE);



            if (Constants.ResumeTransaction)
            {
                Constants.IsInTransaction = true;
                Constants.TransactionInProgress = true;
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                map.MoveCamera(CameraUpdateFactory.NewPosition(new Xamarin.Forms.GoogleMaps.Position(lat, lng)));
            });
            //map.MoveCamera(CameraUpdateFactory.NewPosition(new Xamarin.Forms.GoogleMaps.Position(lat, lng)));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Constants.IsInSeller = true;

            //setup map
            map.UiSettings.MyLocationButtonEnabled = false;

            startLocationUpdates();
            fetchBankAccounts();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Constants.IsInSeller = false;
            System.Diagnostics.Debug.WriteLine("Disappeared");
        }


        void ChangeRole(Constants.MERCHANT_STATUS status)
        {
            ChangeUserRoleAPI api = new ChangeUserRoleAPI(Constants.currentUser.token, status);
            api.setCallbacks(this);
            api.getResponse();
            if (status.Equals(Constants.MERCHANT_STATUS.IDLE))
            {
                Constants.IsInTransaction = false;
            }
        }

        async void startLocationUpdates()
        {
            var geolocator = CrossGeolocator.Current;

            if (geolocator.IsListening)
                return;

            geolocator.DesiredAccuracy = 1;

            await geolocator.StartListeningAsync(TimeSpan.FromSeconds(3), 1, true, new ListenerSettings()
            {
                ActivityType = ActivityType.AutomotiveNavigation,
                AllowBackgroundUpdates = true,
                DeferLocationUpdates = true,
                DeferralDistanceMeters = 1,
                DeferralTime = TimeSpan.FromSeconds(3),
                ListenForSignificantChanges = true,
                PauseLocationUpdatesAutomatically = false

            });

            //currentLocation = 
            var c = geolocator.GetLastKnownLocationAsync();
            await c;
            if (c.IsCompleted)
            {
                currentLocation = c.Result;
                if (currentLocation != null)
                {
                    await map.MoveCamera(CameraUpdateFactory.NewPosition(new Xamarin.Forms.GoogleMaps.Position(currentLocation.Latitude, currentLocation.Longitude)));
                    LocationChanged(null, new PositionEventArgs(currentLocation));
                }

                geolocator.PositionChanged += LocationChanged;
                geolocator.PositionError += CrossGeolocator_Current_PositionError;
            }

        }

        /// <summary>
        /// Stops location updates.
        /// </summary>
        async void stopLocationUpdates()
        {
            var geolocator = CrossGeolocator.Current;

            await geolocator.StopListeningAsync();
            geolocator.PositionChanged -= LocationChanged;
            geolocator.PositionError -= CrossGeolocator_Current_PositionError;
        }

        void CrossGeolocator_Current_PositionError(object sender, PositionErrorEventArgs e)
        {

            Log.e("Sadad", "Location error: " + e.Error.ToString());
        }

        /// <summary>
        /// Gets called when there are changes on device coordinates/location
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void LocationChanged(object sender, PositionEventArgs e)
        {

            if (currentLocation == null)
            {
                map.MoveCamera(CameraUpdateFactory.NewPosition(new Xamarin.Forms.GoogleMaps.Position(e.Position.Latitude, e.Position.Longitude)));
            }

            currentLocation = e.Position;

            Device.BeginInvokeOnMainThread(() =>
            {

                var p = e.Position;


                Log.e("Sadad", "Location update: " + p.ToString());
                //if there is an ongoing transaction, update the meetup estimates
                if (currentTransactionId != null && IsNowMeeting)
                {
                    updateMeetupEstimates();

                }

                if (Constants.currentUser != null)
                {
                    UpdateUserLocationAPI api = new UpdateUserLocationAPI(Constants.currentUser.token, p.Latitude, p.Longitude);
                    api.setCallbacks(this);
                    api.getResponse();
                }
                else
                {
                    stopLocationUpdates();
                }

            });

        }

        void NavigateMap_Tapped(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DependencyService.Get<iDrawer>().OpenDrawer(currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
            });

        }
        int? selectedPickerIndex = null;
        void StartSellingTapped(object sender, System.EventArgs e)
        {
            //CrossLocalNotifications.Current.Show("Example", "sa");

            if (pickerAccounts.SelectedItem != null)
            {
                selectedPickerIndex = pickerAccounts.SelectedIndex;
                Constants.IsInTransaction = true;
                //Constants.IsSelling = true;
                Navigation.PushPopupAsync(new SellerWaitSetupDialog() { callbacks = this }, true);
            }
            else
                DisplayAlert("Incomplete", "Please select your bank account before selling", "Okay");
        }

        // callback methods when waiting method is set
        public void didSetWaitTime(TimeSpan timeSpan, SellerWaitSetupDialog dialog)
        {
            Log.e("WAIT TIME IS SET", "TIME: " + timeSpan.Minutes + " minutes");
            Constants.IsSelling = true;
            sslSellingSetup.IsVisible = false;
            cvStopSelling.IsVisible = true;
            stoppedIsClicked = false;
            isLookingForBuyer = true;
            ChangeRole(Constants.MERCHANT_STATUS.SELLER);


            //Device.StartTimer(timeSpan,() => {
            //    pickerAccounts.SelectedItem = null;
            //    sslSellingSetup.IsVisible = true;
            //    cvStopSelling.IsVisible = false;
            //    isLookingForBuyer = false;
            //    return false;
            //});

            Device.BeginInvokeOnMainThread(() =>
            {
                PopupNavigation.PopAllAsync(true);
            });


            //tools.CountdownTimer.CountdownTimer timer = new tools.CountdownTimer.CountdownTimer();
            //timer.Ticked += (sender, e) => {
            //    lblStopSelling.Text = "Stop Selling ("+e.TimeRemaining.ToString("hh:mm")+")";
            //};
            //timer.Completed += (sender, e) => {
            //pickerAccounts.SelectedItem = null;
            //sslSellingSetup.IsVisible = true;
            //cvStopSelling.IsVisible = false;
            //isLookingForBuyer = false;

            //lblStopSelling.Text = "Stop Selling";
            //};
            //timer.Start(timeSpan);

            double remainingSeconds = timeSpan.TotalSeconds;
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {


                remainingSeconds -= 1;
                TimeSpan t = TimeSpan.FromSeconds(remainingSeconds);
                lblStopSelling.Text = "Stop Selling (" + t.ToString("hh':'mm':'ss") + ")";
                Log.e("STOP SELLING IN ", t.ToString());

                if (remainingSeconds < 1 || isLookingForBuyer == false)
                {


                    StopSellingTapped(null, null);
                    lblStopSelling.Text = "Stop Selling";
                    return false;
                }
                return true;
            });
        }

        void Compare(TimeSpan now, TimeSpan low, TimeSpan up)
        {
            if (low <= up)
            {
                if (now >= low && now <= up)
                {
                    //save here
                    Debug.WriteLine("TARA NA " + Constants.IsSelling);
                    if (!Constants.IsSelling)
                    {
                        Debug.WriteLine("TARA NA1");

                        if (!stoppedIsClicked)
                        {
                            Constants.IsSelling = true;
                            ChangeRole(Constants.MERCHANT_STATUS.SELLER);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                sslSellingSetup.IsVisible = false;
                                cvStopSelling.IsVisible = true;
                                lblStopSelling.Text = "Stop Selling";
                                lblStopSelling.HorizontalTextAlignment = TextAlignment.Center;
                                lblStopSelling.VerticalTextAlignment = TextAlignment.Center;
                                isLookingForBuyer = true;
                            });
                        }
                    }
                }
                else
                {
                    if (Constants.IsSelling)
                    {
                        StopSellingTapped(null, null);
                    }
                }
            }
            else
            {
                if (now >= low || now <= up)
                {
                    if (!Constants.IsSelling)
                    {
                        if (!stoppedIsClicked)
                        {
                            ChangeRole(Constants.MERCHANT_STATUS.SELLER);
                            Constants.IsSelling = true;
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                sslSellingSetup.IsVisible = false;
                                cvStopSelling.IsVisible = true;
                                lblStopSelling.Text = "Stop Selling";
                                lblStopSelling.HorizontalTextAlignment = TextAlignment.Center;
                                lblStopSelling.VerticalTextAlignment = TextAlignment.Center;
                                isLookingForBuyer = true;
                            });
                        }

                    }
                }
                else
                {
                    if (Constants.IsSelling)
                    {
                        StopSellingTapped(null, null);
                    }

                }
            }
        }

        void TimeComparison(WeekSchedule values, WeekScheduleSellerSetupDialog dialog)
        {
            TimeSpan now = DateTime.Now.TimeOfDay, low = TimeSpan.Parse(values.LowValue), up = TimeSpan.Parse(values.UpperValue);
            Compare(now, low, up);

            Device.StartTimer(TimeSpan.FromSeconds(20), () =>
            {
                now = DateTime.Now.TimeOfDay;
                low = TimeSpan.Parse(values.LowValue);
                up = TimeSpan.Parse(values.UpperValue);

                Compare(now, low, up);
                if (stoppedIsClicked) return false;
                return true;

            });
        }

        public void didSetUntilCancel(WeekSchedule values, WeekScheduleSellerSetupDialog dialog)
        {
            string dayOfWeek = DateTime.Now.DayOfWeek.ToString();
            stoppedIsClicked = false;
            switch (values.Days)
            {
                case "Mon - Fri":
                    if (dayOfWeek != "Saturday" || dayOfWeek != "Sunday")
                    {
                        TimeComparison(values, dialog);
                    }
                    break;

                case "Saturday":
                    if (dayOfWeek == "Saturday")
                    {
                        TimeComparison(values, dialog);
                    }
                    break;

                case "Sunday":
                    if (dayOfWeek == "Sunday")
                    {
                        TimeComparison(values, dialog);
                    }
                    break;
            }



            //double remainingSeconds = timeSpan.TotalSeconds;
            //Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            //{


            //    remainingSeconds -= 1;
            //    TimeSpan t = TimeSpan.FromSeconds(remainingSeconds);
            //    lblStopSelling.Text = "Stop Selling (" + t.ToString("hh':'mm':'ss") + ")";
            //    Log.e("STOP SELLING IN ", t.ToString());

            //    if (remainingSeconds < 1 || isLookingForBuyer == false)
            //    {


            //        StopSellingTapped(null, null);
            //        lblStopSelling.Text = "Stop Selling";
            //        return false;
            //    }
            //    return true;
            //});
        }

        public void CancelFixedLocation_Tapped(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                FixedAddressDisplay.IsVisible = false;
                sslSellingSetup.IsVisible = true;
            });
        }

        public void SetFixedMeetUp_Tapped(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                FixedAddressDisplay.IsVisible = false;
                ciMeetupCenterMarker.IsVisible = false;

                Constants.IsSelling = true;
                sslSellingSetup.IsVisible = false;
                cvStopSelling.IsVisible = true;
                stoppedIsClicked = false;
                lblStopSelling.Text = "Stop Selling";
                lblStopSelling.HorizontalTextAlignment = TextAlignment.Center;
                lblStopSelling.VerticalTextAlignment = TextAlignment.Center;
                isLookingForBuyer = true;

                //ChangeUserRoleAPI api = new ChangeUserRoleAPI(Constants.currentUser.token, Constants.MERCHANT_STATUS.SELLER);
                //api.setCallbacks(this);
                //api.getResponse();

                ChangeUserRoleWithFixedLocationAPI api = new ChangeUserRoleWithFixedLocationAPI(Constants.currentUser.token, Constants.MERCHANT_STATUS.SELLER, 0, 0);
                api.setCallbacks(this);
                api.getResponse();

            });
        }

        public void didSetUntilCancel(SellerWaitSetupDialog dialog, int type)
        {
            switch (type)
            {
                case 1:
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        sslSellingSetup.IsVisible = false;
                        ciMeetupCenterMarker.IsVisible = true;
                        FixedAddressDisplay.IsVisible = true;
                    });
                    break;
                case 2:
                    Constants.IsSelling = true;
                    sslSellingSetup.IsVisible = false;
                    cvStopSelling.IsVisible = true;
                    stoppedIsClicked = false;
                    lblStopSelling.Text = "Stop Selling";
                    lblStopSelling.HorizontalTextAlignment = TextAlignment.Center;
                    lblStopSelling.VerticalTextAlignment = TextAlignment.Center;
                    ChangeRole(Constants.MERCHANT_STATUS.SELLER);
                    isLookingForBuyer = true;
                    break;
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                dialog.close();
            });
        }

        void StopSellingTapped(object sender, System.EventArgs e)
        {
            //pickerAccounts.SelectedItem = selectedPickerIndex;
            ChangeRole(Constants.MERCHANT_STATUS.IDLE);
            stoppedIsClicked = true;
            //pickerAccounts.SelectedItem = null;
            sslSellingSetup.IsVisible = true;
            cvStopSelling.IsVisible = false;
            isLookingForBuyer = false;
            Constants.IsSelling = false;
        }

        void fetchBankAccounts()
        {

            FetchBankAccountsAPI api = new FetchBankAccountsAPI(Constants.currentUser.token);
            api.setCallbacks(this);
            api.getResponse();
        }
        public void OnSuccess(JObject response, BaseAPI caller)
        {
            if ((caller as FetchProfileDetailsAPI) != null)
            {
                currentBuyerFeedbacks = JsonConvert.DeserializeObject<ObservableCollection<Feedback>>(response["feedbacks"].ToString(), new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });
                loadCurrentFeedback();
            }
            else if ((caller as FetchBankAccountsAPI) != null)
            {

                Log.e("bank accounts", response.ToString());


                accounts = JsonConvert.DeserializeObject<ObservableCollection<BankAccount>>(response["bank_accounts"].ToString(), new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });


                pickerAccounts.ItemsSource = accounts;

                if (Constants.ResumeTransaction)
                {
                    Constants.IsInTransaction = true;
                    Constants.TransactionInProgress = true;
                    currentMeetupPosition = Constants.currentPosition;
                    var acs = accounts.Where(x => x.ID == Application.Current.Properties["SellerBankAccount"].ToString());
                    currentBuyer = JsonConvert.DeserializeObject<Buyer>(Application.Current.Properties["currentBuyer"].ToString());
                    currentBuyerPosition = JsonConvert.DeserializeObject<Xamarin.Forms.Maps.Position>(Application.Current.Properties["currentBuyerPosition"].ToString());

                    map.Pins.Add(new Pin() { Label = currentBuyer.Name, Position = new Xamarin.Forms.GoogleMaps.Position(currentBuyerPosition.Latitude, currentBuyerPosition.Longitude), Type = PinType.SavedPin, Icon = BitmapDescriptorFactory.FromBundle("UserLocation.png") });

                    PointToPointRouteFinder rfMeetupSetupFromBuyer = new PointToPointRouteFinder(currentBuyerPosition.Latitude, currentBuyerPosition.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                    rfMeetupSetupFromBuyer.TravelTimeDisplay = lblSellerETA;
                    rfMeetupSetupFromBuyer.RouteColor = Color.Brown;
                    rfMeetupSetupFromBuyer.StartRouteFinderUpdates(map);



                    pickerAccounts.SelectedIndex = accounts.IndexOf(acs.FirstOrDefault());
                    StartSellingTapped(null, null);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cvStopSelling.IsVisible = false;

                        stopLocationUpdates();
                        currentTransactionId = Application.Current.Properties["TransactionId"].ToString();
                        Constants.SubscribeToTransactions(currentTransactionId);
                        transactionsClient = Constants.accTransactionsChannel;
                        Constants.accTransactionsChannel.MessageReceived += AccTransactionsChannel_MessageReceived;
                        updateMeetupEstimates();
                        GetMeetupAddress();

                        lblHeaderDisplay.Text = loadDialog.Text;
                        lblHeaderDisplay.FontSize = 12;
                        lblHeaderDisplay.TextColor = Color.Black;
                        cvHeaderDisplay.IsVisible = true;
                        stopLocationUpdates();

                        //DependencyService.Get<iDrawer>().OpenDrawer(currentMeetupPosition.Latitude, currentBuyerPosition.Longitude);
                    });

                }


            }
            else if ((caller as ChangeUserRoleAPI) != null)
            {
                if (isForQuit)
                {
                    loadDialog.close();
                    //start leaving
                    Navigation.PopModalAsync(true);
                    stopLocationUpdates();
                }
            }
        }



        public void OnError(string errMsg, BaseAPI caller)
        {
            Log.e("ERROR", errMsg);
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            throw new NotImplementedException();
        }




        /// <summary>
        /// Moves feedback navigation to previous feedback
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void PreviousFeedbackTapped(object sender, System.EventArgs e)
        {
            if (currentBuyerFeedbacks == null)
                return;

            if (currentFeedbackIndex == 0)
                currentFeedbackIndex = currentBuyerFeedbacks.Count - 1;
            else currentFeedbackIndex--;


            loadCurrentFeedback();
        }
        /// <summary>
        /// Moves feedback navigation to next feedback
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void NextFeedbackTapped(object sender, System.EventArgs e)
        {

            if (currentBuyerFeedbacks == null)
                return;

            if (currentFeedbackIndex == currentBuyerFeedbacks.Count - 1)
                currentFeedbackIndex = 0;
            else currentFeedbackIndex++;

            loadCurrentFeedback();
        }

        CancellableLoadingDialog cancellableLoadingDialog;

        void CancelTransaction()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                map.Pins.Clear();
            });
            ActionCableClient acc = Constants.accTransactionsChannel;

            JObject cancelData = new JObject();
            cancelData.Add("auth_token", Constants.currentUser.token);
            cancelData.Add("user_transaction_id", currentTransactionId);
            cancelData.Add("action", "cancel_transaction");
            acc.Perform(cancelData);
        }

        void AccUsersChannel_MessageReceived(object sender, tools.Sockets.ActionCableEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
            JObject obj = JsonConvert.DeserializeObject<JObject>(e.Message);
            String actionType = obj["data"]["action_type"].Value<string>();

            switch (actionType)
            {
                case "disconnectRespondCancelTransaction":

                    Application.Current.Properties.Remove("InATransaction");
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayDialog ds = new DisplayDialog();
                        ds.Text = obj["data"]["receiver"]["message"].ToString();
                        ds.open();
                        resetSellerProcess();
                    });

                    break;
                case "buyRequestTimeout":
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        sslBuyerDetails.IsVisible = false;
                        DisplayDialog ds = new DisplayDialog();
                        ds.Text = obj["data"]["message"].ToString();
                        ds.open();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            map.Pins.Clear();
                        });
                    });
                    break;
                case "respondOrder": //Constants.USERS_RESPOND_ORDER
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Constants.TransactionInProgress = false;
                        DisplayDialog loadDialogs = new DisplayDialog();
                        loadDialogs.Text = "You have declined the request.";
                        loadDialogs.open();
                        cvHeaderDisplay.IsVisible = false;
                        sslBuyerDetails.IsVisible = false;
                    });
                    break;

                case "promptOnDisconnect":
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayDialog dialog = new DisplayDialog();
                        dialog.Text = obj["data"]["message"].ToString();
                        //Navigation.PushPopupAsync(dialog, true);
                        dialog.open();
                        //Constants.IsInTransaction = false;
                        Constants.TransactionInProgress = true;
                    });
                    break;

                case "purchaseOrder":
                    Constants.TransactionInProgress = false;
                    currentBuyer = JsonConvert.DeserializeObject<Buyer>(obj["data"]["user"].ToString(), new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    });
                    currentOrder = JsonConvert.DeserializeObject<Order>(obj["data"]["order"].ToString(), new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    });

                    currentBuyerPosition = new Xamarin.Forms.Maps.Position(obj["data"]["user"]["latitude"].Value<double>(), obj["data"]["user"]["longitude"].Value<double>());
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        updateBuyerPosition();
                        lblHeaderDisplay.Text = "A buyer is requesting...";
                        lblHeaderDisplay.FontSize = 18;
                        lblHeaderDisplay.TextColor = Color.Black;
                        cvHeaderDisplay.IsVisible = true;
                        refreshBuyer();
                        sslBuyerDetails.IsVisible = true;
                    });
                    break;

                case "acceptOrder":
                    Constants.TransactionInProgress = false;

                    bool accepted = obj["data"]["is_accepted"].Value<bool>();

                    if (accepted)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            loadDialog.close();

                            if (cancellableLoadingDialog == null)
                                cancellableLoadingDialog = new CancellableLoadingDialog(CancelTransaction);
                            cancellableLoadingDialog.Text = "Transaction confirmed!\nWaiting for buyer to set meetup location.";
                            cancellableLoadingDialog.open();


                            lblHeaderDisplay.FontSize = 12;
                            lblHeaderDisplay.TextColor = Color.Black;
                            cvHeaderDisplay.IsVisible = true;
                            stopLocationUpdates();
                            lblHeaderDisplay.Text = loadDialog.Text;
                            currentTransactionId = obj["data"]["transaction_id"].Value<string>();
                            transactionsClient = Constants.SubscribeToTransactions(currentTransactionId);
                            Constants.accTransactionsChannel.MessageReceived += AccTransactionsChannel_MessageReceived;
                            sslBuyerDetails.IsVisible = false;
                            cvStopSelling.IsVisible = false;
                        });
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {

                            loadDialog.close();
                        });
                    }
                    break;

                case "userLocationUpdate":
                    try
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Log.e("userLocationUpdate: ", "" + obj.ToString());
                            var setterId = obj["data"]["setter"]["id"].Value<string>();
                            if (currentBuyer != null)
                            {
                                if (setterId != null && setterId.Equals(currentBuyer.Id))
                                {
                                    currentBuyerPosition = new Xamarin.Forms.Maps.Position(obj["data"]["setter"]["latitude"].Value<double>(), obj["data"]["setter"]["longitude"].Value<double>());
                                    updateBuyerPosition();
                                }
                                if (IsNowMeeting)
                                    updateMeetupEstimates();
                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex);
                    }
                    break;
            }

        }

        void AccTransactionsChannel_MessageReceived(object sender, tools.Sockets.ActionCableEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
            JObject obj = JsonConvert.DeserializeObject<JObject>(e.Message);
            String actionType = obj["data"]["action_type"].Value<string>();
            switch (actionType)
            {
                case "setMeetup":
                    Constants.TransactionInProgress = false;
                    string setterId = obj["data"]["setter"]["id"].Value<String>();
                    string receiverId = obj["data"]["receiver"]["id"].Value<String>();

                    double latitude = obj["data"]["meet_up"]["meeting_location"]["latitude"].Value<double>();
                    double longitude = obj["data"]["meet_up"]["meeting_location"]["longitude"].Value<double>();

                    currentMeetupPosition = new Xamarin.Forms.Maps.Position(latitude, longitude);

                    if (setterId.Equals(Constants.currentUser.id))
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            //loadDialog.Text = obj["data"]["setter"]["message"].Value<String>();
                            setMeetUpCancellableDialog.Text = obj["data"]["setter"]["message"].Value<String>();
                        });

                    }
                    else if (receiverId.Equals(Constants.currentUser.id))
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            if(setMeetUpCancellableDialog != null){
                                setMeetUpCancellableDialog.close();
                            }
                            lblHeaderDisplay.Text = obj["data"]["receiver"]["message"].Value<String>();

                            if(cancellableLoadingDialog.IsOpen){
                                cancellableLoadingDialog.close();
                            }

                            ///change current meetup location
                            currentMeetupPosition = new Xamarin.Forms.Maps.Position(
                                obj["data"]["meet_up"]["meeting_location"]["latitude"].Value<double>(),
                                obj["data"]["meet_up"]["meeting_location"]["longitude"].Value<double>()
                            );
                            showReceivedMeetup(obj["data"]["meet_up"]["address"].Value<String>());
                        });
                    }
                    break;

                case "declineMeetup":
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        loadDialog.Text = obj["data"]["receiver"]["message"].Value<String>();
                        Constants.TransactionInProgress = false;
                    });
                    break;

                case "acceptMeetup":
                    var lat = obj["data"]["meet_up"]["meeting_location"]["latitude"].Value<double>();
                    var lng = obj["data"]["meet_up"]["meeting_location"]["longitude"].Value<double>();
                    currentMeetupPosition = new Xamarin.Forms.Maps.Position(lat, lng);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        loadDialog.close();
                        hideMeetupSetup();
                        hideReceivedMeetUp();
                        Constants.TransactionInProgress = true;

                        if (Application.Current.Properties.ContainsKey("InATransaction"))
                        {
                            Application.Current.Properties["InATransaction"] = true;
                        }
                        else
                        {
                            Application.Current.Properties.Add("InATransaction", true);
                        }

                        if (Application.Current.Properties.ContainsKey("MERCHANT_STATUS"))
                        {
                            Application.Current.Properties["MERCHANT_STATUS"] = "Seller";
                        }
                        else
                        {
                            Application.Current.Properties.Add("MERCHANT_STATUS", "Seller");
                        }

                        if (Application.Current.Properties.ContainsKey("currentBuyer"))
                        {
                            Application.Current.Properties["currentBuyer"] = JsonConvert.SerializeObject(currentBuyer);
                        }
                        else
                        {
                            Application.Current.Properties.Add("currentBuyer", JsonConvert.SerializeObject(currentBuyer));
                        }

                        if (Application.Current.Properties.ContainsKey("currentBuyerPosition"))
                        {
                            Application.Current.Properties["currentBuyerPosition"] = JsonConvert.SerializeObject(currentBuyerPosition);
                        }
                        else
                        {
                            Application.Current.Properties.Add("currentBuyerPosition", JsonConvert.SerializeObject(currentBuyerPosition));
                        }

                        Application.Current.SavePropertiesAsync();
                        GetMeetupAddress();

                        stopLocationUpdates();
                        updateMeetupEstimates();
                        StartMeetingUpLocationUpdates();
                    });
                    break;

                case "cancelTransaction":
                    d = null;
                    d = new DisplayDialog();
                    Constants.TransactionInProgress = false;
                    String msg = "";

                    if (obj["data"]["setter"]["id"].Value<string>().Equals(Constants.currentUser.id))
                        msg = obj["data"]["setter"]["message"].Value<string>();
                    else if (obj["data"]["receiver"]["id"].Value<string>().Equals(Constants.currentUser.id))
                        msg = obj["data"]["receiver"]["message"].Value<string>();

                    d.Text = msg;

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            map.Pins.Clear();
                        });


                        if (Application.Current.Properties.ContainsKey("InATransaction"))
                        {
                            Application.Current.Properties["InATransaction"] = false;
                            Application.Current.Properties.Remove("InATransaction");
                        }
                        Application.Current.SavePropertiesAsync();
                        if (Constants.isMinimized)
                        {
                            CrossLocalNotifications.Current.Show("Transaction Cancelled", msg);
                        }
                        else
                        {
                            cancellableLoadingDialog.close();
                            if(setMeetUpCancellableDialog != null){
                                setMeetUpCancellableDialog.close();
                            }

                            d.open();
                        }
                        resetSellerProcess();
                    });
                    break;

                case "completeTransaction": //Constants.TRANSACTIONS_COMPLETE

                    Navigation.PushPopupAsync(new RateBuyer(currentBuyer.Id, currentBuyer.Image, currentBuyer.Name, currentTransactionId) { RatedRole = "Buyer" });
                    Constants.TransactionInProgress = false;
                    if (Application.Current.Properties.ContainsKey("InATransaction"))
                    {
                        Application.Current.Properties["InATransaction"] = false;
                        Application.Current.Properties.Remove("InATransaction");
                    }
                    Application.Current.SavePropertiesAsync();

                    resetSellerProcess();
                    break;

                case "sendMessage":
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cvNewMessageIndicator.IsVisible = true;


                        chatIsTapped = false;
                        Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                        {
                            //return true;
                            chatIsTapped = true;
                            chatIsTapped = false;
                            if (cvNewMessageIndicator.IsVisible)
                            {
                                cvNewMessageIndicator.IsVisible = false;
                            }
                            else
                            {
                                cvNewMessageIndicator.IsVisible = true;
                            }


                            if (chatIsTapped)
                                return false;
                            else
                                return true;
                        });
                    });
                    break;

            }

        }

        void CenterToMyLocation(object sender, System.EventArgs e)
        {
            if (map != null && currentLocation != null)
                map.AnimateCamera(CameraUpdateFactory.NewPosition(new Xamarin.Forms.GoogleMaps.Position(currentLocation.Latitude, currentLocation.Longitude)));
        }

        bool chatIsTapped = false;
        void resetSellerProcess()
        {

            hideMeetupSetup();
            hideReceivedMeetUp();
            stopMeetingUpLocationUpdates();

            cvHeaderDisplay.IsVisible = false;
            sslMeetingEstimates.IsVisible = false;
            cvStopSelling.IsVisible = true;


            currentTransactionId = null;
            // subscribe to transactions channel
            Device.BeginInvokeOnMainThread(() =>
            {
                map.Pins.Clear();
                map.Polylines.Clear();
            });

            Constants.accTransactionsChannel.MessageReceived -= AccTransactionsChannel_MessageReceived;
            Constants.accTransactionsChannel.Disconnect();
            sslBuyerDetails.IsVisible = false;

            hideMeetupSetup();
            hideReceivedMeetUp();
        }

        async void StartMeetingUpLocationUpdates()
        {
            var geolocator = CrossGeolocator.Current;

            if (geolocator.IsListening)
                await geolocator.StopListeningAsync();

            geolocator.DesiredAccuracy = 1;

            await geolocator.StartListeningAsync(TimeSpan.FromSeconds(3), 1, true, new ListenerSettings()
            {
                ActivityType = ActivityType.AutomotiveNavigation,
                AllowBackgroundUpdates = true,
                DeferLocationUpdates = true,
                DeferralDistanceMeters = 1,
                DeferralTime = TimeSpan.FromSeconds(3),
                ListenForSignificantChanges = true,
                PauseLocationUpdatesAutomatically = false

            });

            var p = await geolocator.GetLastKnownLocationAsync();
            if (p != null)
                MeetingUpLocationChanged(null, new PositionEventArgs(p));

            geolocator.PositionChanged += MeetingUpLocationChanged;

        }

        void MeetingUpLocationChanged(object sender, PositionEventArgs e)
        {

            if (currentLocation != null)
            {
                //dont update if location is the same
                if (Misc.DoubleEquals(currentLocation.Latitude, e.Position.Latitude) && Misc.DoubleEquals(currentLocation.Longitude, e.Position.Longitude))
                {
                    return;
                }
            }
            currentLocation = e.Position;

            map.Polylines.Remove(map.Polylines.Where(x => x.StrokeColor == Color.DimGray).FirstOrDefault());


            PointToPointRouteFinder newSetup = new PointToPointRouteFinder(currentLocation.Latitude, currentLocation.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
            newSetup.TravelTimeDisplay = lblSellerETA;
            newSetup.RouteColor = Color.DimGray;
            newSetup.StartRouteFinderUpdates(map);

            ActionCableClient transClient = Constants.accTransactionsChannel;
            JObject positionUpdateData = new JObject();
            positionUpdateData.Add("user_transaction_id", currentTransactionId);
            positionUpdateData.Add("user_id", Constants.currentUser.id);
            positionUpdateData.Add("latitude", currentLocation.Latitude);
            positionUpdateData.Add("longitude", currentLocation.Longitude);
            positionUpdateData.Add("action", "user_location_update");

            transClient.Perform(positionUpdateData);


        }

        async void stopMeetingUpLocationUpdates()
        {
            var geolocator = CrossGeolocator.Current;

            if (geolocator.IsListening)
                await geolocator.StopListeningAsync();

            geolocator.PositionChanged -= MeetingUpLocationChanged;
        }



        void updateMeetupEstimates()
        {
            //updateCurrentMeetupPin();
            IsNowMeeting = true;
            sslMeetingEstimates.IsVisible = true;
            FindMeetupRoutes();

            lblHeaderDisplay.Text = "Please proceed to your meetup address.";
        }

        void updateCurrentMeetupPin()
        {
            //if (currentMeetupPin != null)
            //map.Pins.Remove(currentMeetupPin);


            //Device.BeginInvokeOnMainThread(() => {
            //    map.Pins.Remove(map.Pins.Where(x => x.Label == "Meetup Position").FirstOrDefault());
            //});



            //map.Pins.Remove(map.Pins.Where(x => x.Label == "Meetup Position").FirstOrDefault());
            Device.BeginInvokeOnMainThread(() =>
            {
                currentMeetupPin = new Pin()
                {
                    Position = new Xamarin.Forms.GoogleMaps.Position(currentMeetupPosition.Latitude, currentMeetupPosition.Longitude),
                    Label = "Meetup Position",
                    Icon = BitmapDescriptorFactory.FromBundle("MeetUpPin.png")
                };
                map.Pins.Add(currentMeetupPin);
            });
        }

        void updateBuyerPosition()
        {
            if (buyerPin != null)
                map.Pins.Remove(buyerPin);

            buyerPin = new Pin()
            {
                Position = new Xamarin.Forms.GoogleMaps.Position(currentBuyerPosition.Latitude, currentBuyerPosition.Longitude),
                Label = currentBuyer.Name,
                Icon = BitmapDescriptorFactory.FromBundle("UserLocation.png")
            };

            map.Pins.Add(buyerPin);
        }

        void refreshBuyer()
        {

            lblBuyerName.Text = currentBuyer.Name;
            ciBuyerImage.Source = Constants.SERVER_URL + currentBuyer.Image;
            rbBuyerRating.Rating = currentBuyer.Rating;

            lblQuantity.Text = currentOrder.Quantity;

            String paymentOption = "Once";


            switch (currentOrder.PaymentOption)
            {
                case 1:
                    paymentOption = "Weekly for " + currentOrder.NumberOfPayment + " week(s).";
                    break;
                case 2:
                    paymentOption = "Monthly for " + currentOrder.NumberOfPayment + " month(s).";
                    break;
            }

            lblPaymentOptions.Text = paymentOption;

            currentFeedbackIndex = 0;
            FetchProfileDetailsAPI api = new FetchProfileDetailsAPI(currentBuyer.Id, Constants.currentUser.token);
            api.setCallbacks(this);
            api.getResponse();
        }

        void loadCurrentFeedback()
        {

            if (currentBuyerFeedbacks.Count == 0)
            {

                lblFeebackMessage.Text = "No Feedbacks";

                return;
            }

            Feedback f = currentBuyerFeedbacks[currentFeedbackIndex];

            Log.e("Feedback", "Loading " + f.RatingUserName);

            lblFeedbackUserName.Text = f.RatingUserName;
            ciFeedbackUserImage.Source = Constants.ROOT_URL + f.RatingUserImage;

            sslFeedbackStars.Rating = f.Rate;

            lblFeebackMessage.Text = f.RatingMessage;
        }



        void AcceptBuyerTapped(object sender, System.EventArgs e)
        {
            if (pickerAccounts.SelectedItem == null)
            {
                DisplayAlert("Incomplete", "Please select a bank account first", "Okay");
                return;
            }

            JObject buyData = new JObject();
            buyData.Add("auth_token", Constants.currentUser.token);
            buyData.Add("order_id", currentOrder.Id);
            buyData.Add("accepted", true);
            buyData.Add("bank_account", (pickerAccounts.SelectedItem as BankAccount).ID);
            buyData.Add("action", "respond_order");

            usersClient.Perform(buyData);

            if (Application.Current.Properties.ContainsKey("SellerBankAccount"))
            {
                Application.Current.Properties["SellerBankAccount"] = (pickerAccounts.SelectedItem as BankAccount).ID;
            }
            else
            {
                Application.Current.Properties.Add("SellerBankAccount", (pickerAccounts.SelectedItem as BankAccount).ID);
            }

            if (Application.Current.Properties.ContainsKey("currentBuyer"))
            {
                Application.Current.Properties["currentBuyer"] = JsonConvert.SerializeObject(currentBuyer);
            }
            else
            {
                Application.Current.Properties.Add("currentBuyer", JsonConvert.SerializeObject(currentBuyer));
            }

            if (loadDialog == null)
                loadDialog = new Loading_Dialog();
            loadDialog.Text = "Accepting transaction...";
            loadDialog.open();
        }

        void DeclineBuyerTapped(object sender, System.EventArgs e)
        {
            JObject buyData = new JObject();
            buyData.Add("auth_token", Constants.currentUser.token);
            buyData.Add("order_id", currentOrder.Id);
            buyData.Add("accepted", false);
            buyData.Add("action", "respond_order");
            usersClient.Perform(buyData);
            cvHeaderDisplay.IsVisible = false;
            sslBuyerDetails.IsVisible = false;
            Device.BeginInvokeOnMainThread(() =>
            {
                map.Pins.Clear();
            });
        }


        #region Setting Meetup location Section
        void showMeetupSetup()
        {
            map.CameraChanged += Map_CameraChanged;
            IsMonitoringMapChanges = true;
            MonitorMapChanges();

            sslMeetupSection.IsVisible = true;
            sslGeocodeDisplay.IsVisible = true;
            ciMeetupCenterMarker.IsVisible = true;
        }



        void hideMeetupSetup()
        {
            map.CameraChanged -= Map_CameraChanged;
            IsMonitoringMapChanges = false;

            sslMeetupSection.IsVisible = false;
            sslGeocodeDisplay.IsVisible = false;
            ciMeetupCenterMarker.IsVisible = false;
        }

        private DateTime lastDragged;

        private bool geocodingDone = false;

        void MonitorMapChanges()
        {
            Device.StartTimer(TimeSpan.FromMilliseconds(500), () =>
            {
                if (lastDragged.Add(TimeSpan.FromMilliseconds(500)) <= DateTime.Now && !geocodingDone)
                    GetAddress();
                return IsMonitoringMapChanges;
            });
        }

        async void GetAddress()
        {
            try
            {
                lblGeocodedAddress.Text = "Finding...";
                var x = map.CameraPosition.Target;
                Geocoder geocoder = new Geocoder();
                var addresses = await geocoder.GetAddressesForPositionAsync(x);
                List<string> lstAddresses = addresses.ToList();
                String geocodedAddress = lstAddresses.Count() > 0 ? lstAddresses[0] : "Not found";

                Log.e("Found address", geocodedAddress);
                lblGeocodedAddress.Text = geocodedAddress == null ? "Finding..." : geocodedAddress.Replace(System.Environment.NewLine, " ");
                lblMeetupAddress.Text = lblGeocodedAddress.Text;
            }
            catch (Exception e)
            {
                Log.e("sada", e.StackTrace);
            }
            geocodingDone = true;
        }

        void Map_CameraChanged(object sender, CameraChangedEventArgs e)
        {
            lastDragged = DateTime.Now;
            geocodingDone = false;
        }

        void StreetViewTapped(object sender, System.EventArgs e)
        {
            var x = map.CameraPosition.Target;
            DependencyService.Get<IStreetViewService>().openStreetView(x.Latitude, x.Longitude);
        }
        CancellableLoadingDialog setMeetUpCancellableDialog;
        void SetMeetupTapped(object sender, System.EventArgs e)
        {
            //if (loadDialog == null)
            //    loadDialog = new Loading_Dialog();
            //loadDialog.Text = "Finding routes...";
            //loadDialog.open();

            if(setMeetUpCancellableDialog == null){
                setMeetUpCancellableDialog = new CancellableLoadingDialog(CancelTransaction);
            }
            setMeetUpCancellableDialog.Text = "Finding routes...";
            setMeetUpCancellableDialog.open();

            currentMeetupPosition = new Xamarin.Forms.Maps.Position(map.CameraPosition.Target.Latitude, map.CameraPosition.Target.Longitude);

            map.Polylines.Clear();

            IsForSetNewMeetup = true;

            FindSetupMeetupRoutes();
        }

        void centerMapPositions()
        {
            List<Xamarin.Forms.Maps.Position> _positions = new List<Xamarin.Forms.Maps.Position>();
            _positions.Add(new Xamarin.Forms.Maps.Position(currentLocation.Latitude, currentLocation.Longitude));
            _positions.Add(currentBuyerPosition);
            _positions.Add(currentMeetupPosition);

            var distance = MapHelper.TotalDistance(_positions);
            var center = MapHelper.GetCentralGeoCoordinate(_positions);
            map.MoveToRegion(
                MapSpan.FromCenterAndRadius(new Xamarin.Forms.GoogleMaps.Position(center.Latitude, center.Longitude),
                                            Distance.FromKilometers(distance * 0.65)), /// this was supposed to be /2 but the markers gets covered by other views
                true
            );

            ///maps built in
            //List<Xamarin.Forms.GoogleMaps.Position> _positions = new List<Xamarin.Forms.GoogleMaps.Position>();
            //_positions.Add(new Xamarin.Forms.GoogleMaps.Position(currentLocation.Latitude, currentLocation.Longitude));
            //_positions.Add(new Xamarin.Forms.GoogleMaps.Position(currentBuyerPosition.Latitude, currentBuyerPosition.Longitude));
            //_positions.Add(new Xamarin.Forms.GoogleMaps.Position(currentMeetupPosition.Latitude, currentMeetupPosition.Longitude));
            //map.MoveToRegion(MapSpan.FromPositions(_positions),true);
        }

        void FindSetupMeetupRoutes()
        {
            map.Polylines.Clear();
            PointToPointRouteFinder rfMeetupSetupFromUser = new PointToPointRouteFinder(currentLocation.Latitude, currentLocation.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
            rfMeetupSetupFromUser.TravelTimeDisplay = lblBuyerETA;
            rfMeetupSetupFromUser.RouteColor = Color.DimGray;
            rfMeetupSetupFromUser.RouteFound += (sender, e) =>
            {
                MeetupSetupRouteFound();
            };

            rfMeetupSetupFromUser.StartRouteFinderUpdates(map);

            PointToPointRouteFinder rfMeetupSetupFromBuyer = new PointToPointRouteFinder(currentBuyerPosition.Latitude, currentBuyerPosition.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
            rfMeetupSetupFromBuyer.TravelTimeDisplay = lblSellerETA;
            rfMeetupSetupFromBuyer.RouteColor = Color.Brown;
            rfMeetupSetupFromBuyer.StartRouteFinderUpdates(map);

            Device.BeginInvokeOnMainThread(() =>
            {
                map.Pins.Remove(map.Pins.Where(x => x.Label == "Meetup Position").FirstOrDefault());
            });

            map.Pins.Remove(map.Pins.Where(x => x.Label == "Meetup Position").FirstOrDefault());

            //if(map.Pins.Where(x => x.Label.Equals("Meetup Position")).FirstOrDefault() == null){
            map.Pins.Add(new Pin()
            {
                Type = PinType.SavedPin,
                Label = "Meetup Position",
                Position = new Xamarin.Forms.GoogleMaps.Position(currentMeetupPosition.Latitude, currentMeetupPosition.Longitude),
                Icon = BitmapDescriptorFactory.FromBundle("MeetUpPin.png")
            });
            //}
        }

        void MeetupSetupRouteFound()
        {
            if (IsForSetNewMeetup)
            {
                setMeetUpCancellableDialog.Text = "Waiting for buyer to agree on the meeting place...";

                if (transactionsClient == null)
                    transactionsClient = Constants.accTransactionsChannel;

                JObject buyData = new JObject();
                buyData.Add("auth_token", Constants.currentUser.token);
                buyData.Add("user_transaction_id", currentTransactionId);
                buyData.Add("latitude", currentMeetupPosition.Latitude);
                buyData.Add("longitude", currentMeetupPosition.Longitude);

                ///set_another parameter is a reusable parameter when setting up location
                /// true  = to set new location
                /// false = to prompt that the other party has declined set location and is setting
                /// another
                buyData.Add("set_another", true);
                buyData.Add("action", "set_meetup");

                transactionsClient.Perform(buyData);
            }
        }

        void FindMeetupRoutes()
        {
            map.Polylines.Clear();

            //userToMeetupFinder = new RouteFinder(currentLocation.Latitude, currentLocation.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
            //userToMeetupFinder.setRouteFinderCallbacks(this);
            //userToMeetupFinder.startFinding();

            //buyerToMeetupFinder = new RouteFinder(currentBuyerPosition.Latitude, currentBuyerPosition.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
            //buyerToMeetupFinder.setRouteFinderCallbacks(this);
            //buyerToMeetupFinder.startFinding();


            if (userToMeetupFinder == null)
            {
                userToMeetupFinder = new PointToPointRouteFinder(currentLocation.Latitude, currentLocation.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                userToMeetupFinder.TravelTimeDisplay = lblSellerETA;
                userToMeetupFinder.RouteColor = Color.DimGray;

                userToMeetupFinder.StartRouteFinderUpdates(map);
                userToMeetupFinder.RouteFound += (sender, e) =>
                {
                    MeetupRouteFound();
                };
            }
            else
            {
                userToMeetupFinder.EndPin = currentMeetupPin;
                userToMeetupFinder.StartLatitude = currentLocation.Latitude;
                userToMeetupFinder.StartLongitude = currentLocation.Longitude;
                userToMeetupFinder.EndLatitude = currentMeetupPosition.Latitude;
                userToMeetupFinder.EndLongitude = currentMeetupPosition.Longitude;
                userToMeetupFinder.StartRouteFinderUpdates(map);
            }

            if (buyerToMeetupFinder == null)
            {
                buyerToMeetupFinder = new PointToPointRouteFinder(currentBuyerPosition.Latitude, currentBuyerPosition.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                buyerToMeetupFinder.TravelTimeDisplay = lblBuyerETA;
                buyerToMeetupFinder.RouteColor = Color.Brown;

                buyerToMeetupFinder.StartRouteFinderUpdates(map);
                buyerToMeetupFinder.RouteFound += (sender, e) =>
                {
                    MeetupRouteFound();
                };
            }
            else
            {
                buyerToMeetupFinder.EndPin = currentMeetupPin;
                buyerToMeetupFinder.StartPin = buyerPin;
                buyerToMeetupFinder.StartLatitude = currentBuyerPosition.Latitude;
                buyerToMeetupFinder.StartLongitude = currentBuyerPosition.Longitude;
                buyerToMeetupFinder.EndLatitude = currentMeetupPosition.Latitude;
                buyerToMeetupFinder.EndLongitude = currentMeetupPosition.Longitude;

                buyerToMeetupFinder.StartRouteFinderUpdates(map);
            }
        }
        #endregion

        void AgreeMeetupTapped(object sender, System.EventArgs e)
        {
            loadDialog.Text = "Starting meetup...";

            if (transactionsClient == null)
                transactionsClient = Constants.accTransactionsChannel;

            JObject buyData = new JObject();
            buyData.Add("auth_token", Constants.currentUser.token);
            buyData.Add("user_transaction_id", currentTransactionId);
            ///set_another parameter is a reusable parameter when setting up location
            /// true  = to set new location
            /// false = to prompt that the other party has declined set location and is setting
            /// another
            buyData.Add("set_another", false);
            buyData.Add("action", "set_meetup");

            transactionsClient.Perform(buyData);

            loadDialog.close();
            updateMeetupEstimates();

            GetMeetupAddress();
        }

        void SetAnotherMeetupTapped(object sender, System.EventArgs e)
        {
            if (transactionsClient == null)
                transactionsClient = Constants.accTransactionsChannel;

            JObject declineData = new JObject();
            declineData.Add("auth_token", Constants.currentUser.token);
            declineData.Add("user_transaction_id", currentTransactionId);
            declineData.Add("action", "decline_meetup");

            transactionsClient.Perform(declineData);

            hideReceivedMeetUp();
            showMeetupSetup();
        }

        /// <summary>
        /// Will show the receieved meetup location set
        /// by the other party
        /// </summary>
        void showReceivedMeetup(String address)
        {
            hideMeetupSetup();

            cvHeaderDisplay.IsVisible = true;

            sslReceivedMeetup.IsVisible = true;


            lblMeetupAddressRequest.Text = address;
            lblHeaderDisplay.FontSize = 12;
            lblHeaderDisplay.TextColor = Color.Black;

            updateCurrentMeetupPin();

            IsForSetNewMeetup = false;
            FindSetupMeetupRoutes();

            centerMapPositions();

        }

        void hideReceivedMeetUp()
        {
            sslReceivedMeetup.IsVisible = false;
        }

        void MeetupRouteFound()
        {
            if (userToMeetupFinder.Polyline != null && buyerToMeetupFinder.Polyline != null)
            {
                if (IsNowMeeting)
                {
                    if (userToMeetupFinder.Arrived && buyerToMeetupFinder.Arrived)
                    {
                        cvHeaderDisplay.IsVisible = false;
                        lblHeaderDisplay.Text = "Please give the item to the buyer.\nTransaction will complete once you received the payment.";
                    }
                    else
                    {
                        cvHeaderDisplay.IsVisible = true;
                        lblHeaderDisplay.Text = "Please proceed to your meetup address.";
                        lblHeaderDisplay.TextColor = Color.Black;
                    }
                }
            }
        }

        public void didFindRouteSuccess(Polyline polyline, string travelTime, RouteFinder caller)
        {
        }

        public void didFindRouteFailed(string error, RouteFinder caller)
        {

        }

        void ChatTapped(object sender, System.EventArgs e)
        {
            cvNewMessageIndicator.IsVisible = false;
            chatIsTapped = true;
            Navigation.PushModalAsync(new Message.ConversationPage(currentBuyer.Name, currentTransactionId));
        }

        async void CancelTransactionTapped(object sender, System.EventArgs e)
        {
            var canceltransaction = await DisplayAlert("Cancel Transaction", "Do you want to cancel transaction?", "Yes", "No");
            if (canceltransaction)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    map.Pins.Clear();
                });
                ActionCableClient acc = Constants.accTransactionsChannel;

                JObject cancelData = new JObject();
                cancelData.Add("auth_token", Constants.currentUser.token);
                cancelData.Add("user_transaction_id", currentTransactionId);
                cancelData.Add("action", "cancel_transaction");
                acc.Perform(cancelData);
            }
        }

        async void GetMeetupAddress()
        {
            try
            {
                lblGeocodedAddress.Text = "Finding...";
                var x = currentMeetupPosition;
                Geocoder geocoder = new Geocoder();
                var addresses = await geocoder.GetAddressesForPositionAsync(new Xamarin.Forms.GoogleMaps.Position(x.Latitude, x.Longitude));
                List<string> lstAddresses = addresses.ToList();
                String geocodedAddress = lstAddresses.Count() > 0 ? lstAddresses[0] : "Not found";

                Log.e("Found address", geocodedAddress);
                lblMeetupAddress.Text = geocodedAddress;

            }
            catch (Exception e)
            {
                Log.e("GetMeetupAddress", e.StackTrace);
            }
        }
    }

}