﻿using System;

using Xamarin.Forms;

namespace PP.Pages
{
    public class WebViewContentPage : ContentPage
    {
        public WebViewContentPage(string url)
        {
            
            Content = new StackLayout
            {
                Padding = new Thickness(0,22,0,0),
                Orientation = StackOrientation.Vertical,
                Children = {
                    new Image {
                        Source = "ic_prev",
                        WidthRequest = 23,
                        HeightRequest = 23,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                    },
                    new WebView
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Source = url
                    }
                }
            };
        }
    }
}

