﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs
{
    public partial class DisplayDialog : PopupPage
    {
        public DisplayDialog()
        {
            InitializeComponent();
            IsOpen = false;
        }

		public void close()
		{
			if (IsOpen)
			{
                BuyerHome.dialogIsShowed = false;
                BuyerSellerPage.dialogIsShowed = false;
				PopupNavigation.PopAsync(true);
				IsOpen = false;
			}
		}


		void OkayTapped(object sender, System.EventArgs e)
        {
           close();
        }

		public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(String), typeof(DisplayDialog), "Loading...",
			   propertyChanged: (bindable, oldValue, newValue) =>
			   {
            var view = bindable as DisplayDialog;

				   view.lblLoadingText.Text = view.Text;
			   });


		public String Text
		{
			get { return (String)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}
        public bool IsOpen { get; set; }
		public void open()
		{
            if (!IsOpen)
			{
                if(this.Parent==null)
                Navigation.PushPopupAsync(this, true);
				IsOpen = true;
			}
		}
    }
}
