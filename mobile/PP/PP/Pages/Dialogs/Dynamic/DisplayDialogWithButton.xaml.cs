﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs.Dynamic
{
    public partial class DisplayDialogWithButton : PopupPage
    {
        Action continue_callback, cancel_callback;

        public DisplayDialogWithButton(Action continue_callback, Action cancel_callback)
        {
            this.continue_callback = continue_callback;
            this.cancel_callback = cancel_callback;
            InitializeComponent();
            IsOpen = false;
        }

        public void close()
        {
            if (IsOpen)
            {

                PopupNavigation.PopAsync(true);
                Navigation.PopPopupAsync(true);
                //Navigation.RemovePopupPageAsync(this,true);
                //PopupNavigation.PopAllAsync(true);
                IsOpen = false;
            }

        }

        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(String), typeof(DisplayDialogWithButton), "Loading...",
               propertyChanged: (bindable, oldValue, newValue) =>
               {
                   var view = bindable as DisplayDialogWithButton;

                   view.lblLoadingText.Text = view.Text;
               });


        public String Text
        {
            get { return (String)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        void Continue_Clicked(object sender, EventArgs e){
            close();
            this.continue_callback.Invoke();
        }

        void Cancel_Clicked(object sender, EventArgs e){
            close();
            this.cancel_callback.Invoke();
        }

        protected override bool OnBackButtonPressed()
        {
            //return base.OnBackButtonPressed();
            return true;
        }

        public bool IsOpen { get; set; }

        public async void open()
        {
            if (!IsOpen)
            {
                await Navigation.PushPopupAsync(this, true);
                IsOpen = true;
            }
        }
    }
}