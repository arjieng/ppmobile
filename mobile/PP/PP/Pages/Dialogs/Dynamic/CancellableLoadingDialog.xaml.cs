﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PP.tools.Sockets;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs.Dynamic
{
    public partial class CancellableLoadingDialog : PopupPage
    {
        readonly Action callback;

        public CancellableLoadingDialog(Action callback)
        {
            InitializeComponent();
            this.callback = callback;
            IsOpen = false;
        }

        public void close()
        {
            if (IsOpen)
            {

                PopupNavigation.PopAsync(true);
                Navigation.PopPopupAsync(true);
                IsOpen = false;
            }

        }

        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(String), typeof(CancellableLoadingDialog), "Loading...",
               propertyChanged: (bindable, oldValue, newValue) =>
               {
                   var view = bindable as CancellableLoadingDialog;
                   view.lblLoadingText.Text = view.Text;
               });


        public String Text
        {
            get { return (String)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        void OkayTapped(object sender, EventArgs e){
            //ActionCableClient client = Constants.accUsersChannel;

            //JObject buyData = new JObject();
            //buyData.Add("auth_token", Constants.currentUser.token);
            //buyData.Add("seller_id", seller_id);
            //buyData.Add("type", 1);
            //buyData.Add("action", "buy_request_timeout");
            //client.Perform(buyData);
            close();
            this.callback.Invoke();
        }

        protected override bool OnBackButtonPressed()
        {
            //return base.OnBackButtonPressed();
            return true;
        }

        public bool IsOpen { get; set; }

        public async void open()
        {
            if (!IsOpen)
            {
                await Navigation.PushPopupAsync(this, true);
                IsOpen = true;
            }
        }

    }
}
