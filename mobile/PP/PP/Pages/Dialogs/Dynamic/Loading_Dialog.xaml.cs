﻿﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs
{
    public partial class Loading_Dialog : PopupPage
    {
        public Loading_Dialog()
        {
            InitializeComponent();
            IsOpen = false;
        }

        public void close(){
            if (IsOpen){

                PopupNavigation.PopAsync(true);
                Navigation.PopPopupAsync(true);
                //Navigation.RemovePopupPageAsync(this,true);
                //PopupNavigation.PopAllAsync(true);
                IsOpen = false;
            }
            
        }

		public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(String), typeof(Loading_Dialog), "Loading...",
			   propertyChanged: (bindable, oldValue, newValue) =>
			   {
				   var view = bindable as Loading_Dialog;

                   view.lblLoadingText.Text = view.Text;
			   });


		public String Text
		{
			get { return (String)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}


        protected override bool OnBackButtonPressed()
        {
            //return base.OnBackButtonPressed();
            return true;
        }

        private bool IsOpen { get; set; }

        public async void open(){
			if (!IsOpen)
			{
				await Navigation.PushPopupAsync(this, true);
                IsOpen = true;
            }
        }

	}
}
