﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using static PP.Pages.Dialogs.SellerWaitSetupDialog;
using static PP.Pages.Dialogs.SelectLocationType;


namespace PP.Pages.Dialogs
{
    public partial class SellerWaitSetupDialog : PopupPage,TimeRangeSellerSetupDialog.TimeRangeCallbacks,WaitMethodCallbacks, WeekScheduleSellerSetupDialog.WeekScheduleCallbacks, ISelectLocationType
    {

        public WaitMethodCallbacks callbacks;
        static CorneredView cp;

		void closeDialog(object sender, System.EventArgs e)
		{
			close();
		}

		public void close()
		{
            Device.BeginInvokeOnMainThread(() =>
            {
                PopupNavigation.PopAsync();
                Navigation.PopPopupAsync();
            });

    		//PopupNavigation.PopAsync(true);
    		//Navigation.PopPopupAsync(true);

		}

        public interface WaitMethodCallbacks{
            void didSetWaitTime(TimeSpan timeSpan, SellerWaitSetupDialog dialog);
            void didSetUntilCancel(SellerWaitSetupDialog dialog, int type);
            void didSetUntilCancel(WeekSchedule values, WeekScheduleSellerSetupDialog dialog);
        }

        public SellerWaitSetupDialog()
        {
            InitializeComponent();
            cp = choicesPanel;
            callbacks = this;


            if (Constants.ResumeTransaction)
            {
                UntilCancelTapped(null, null);
            }
        }

		void TimeRangeTapped(object sender, System.EventArgs e)
		{
            Device.BeginInvokeOnMainThread(()=>{
                choicesPanel.IsVisible = false;
            });
            //choicesPanel.IsVisible = false;
            Navigation.PushPopupAsync(new TimeRangeSellerSetupDialog(){ callbacks = this}, true);
		}

        public static void ShowPanel()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                cp.IsVisible = true;
            });
        }

        void WeekScheduleTapped(object senxszazder, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() => {
                choicesPanel.IsVisible = false;
            });

            Navigation.PushPopupAsync(new WeekScheduleSellerSetupDialog{ callbacks = this }, true);
        }

		void UntilCancelTapped(object sender, System.EventArgs e)
		{
            Device.BeginInvokeOnMainThread(() =>
            {
                //    callbacks.didSetUntilCancel(this);
                Navigation.PushPopupAsync(new SelectLocationType { callback = this }, true);
            });

		}

        public void didSetTime(TimeSpan time, TimeRangeSellerSetupDialog dialog)
		{
            TimeSpan t = time;
			
            Device.BeginInvokeOnMainThread(()=>{
				dialog.close();
                close();
				callbacks.didSetWaitTime(t, this);
            });

        }

        // Methods gets called when callbacks is not set from caller
        public void didSetWaitTime(TimeSpan timeSpan, SellerWaitSetupDialog dialog)
        {
            
        }

        public void didSetUntilCancel(SellerWaitSetupDialog dialog, int type)
        {

        }

        public void didSetUntilCancel(WeekSchedule values, WeekScheduleSellerSetupDialog dialog)
        {
            Device.BeginInvokeOnMainThread(async () => {
                ShowPanel();
                dialog.close();
                await Task.Delay(300);
                close();
                callbacks.didSetUntilCancel(values, dialog);
            });
        }

        public void SetSelection(SelectLocationType dialog, int type)
        {
            Device.BeginInvokeOnMainThread(async ()=>{
                await Task.Delay(500);
                this.callbacks.didSetUntilCancel(this, type);
            });
        }
    }
}
