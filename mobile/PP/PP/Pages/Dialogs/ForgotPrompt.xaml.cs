﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs
{
    public partial class ForgotPrompt : PopupPage
    {
        public ForgotPrompt()
        {
            InitializeComponent();
        }

		void closeForgotPrompt(object sender, System.EventArgs e)
		{
            saForgotPrompt.IsVisible = false;

            PopupNavigation.PopAsync(true);
		}

		void forgotUsernameTapped(object sender, System.EventArgs e)
		{
			//saForgotUsernameEnterEmail.IsVisible = true;
            PopupNavigation.PopAsync(true);
            Navigation.PushPopupAsync(new ForgotUsernameEnterEmail(), true);
		}


		void forgotPasswordTapped(object sender, System.EventArgs e)
		{
			//saForgotPassword.IsVisible = true;
			PopupNavigation.PopAsync(true);
            Navigation.PushPopupAsync(new ForgotPasswordPrompt(), true);

		}
		
    }
}
