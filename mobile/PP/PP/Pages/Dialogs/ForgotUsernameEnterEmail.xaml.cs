﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PP.tools.API;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs
{
    public partial class ForgotUsernameEnterEmail : PopupPage,IBaseAPIInterface
    {
        public ForgotUsernameEnterEmail()
        {
            InitializeComponent();
        }



        void closeEnterEmailDialog(object sender, System.EventArgs e)
		{
			//saForgotUsernameEnterEmail.IsVisible = false;
            PopupNavigation.PopAsync(true);
		}
		void sendCodeTapped(object sender, System.EventArgs e)
		{
			//lblSendCodeError.Text = "No account is associated with this email.";
			//Device.StartTimer(TimeSpan.FromMilliseconds(3000), () =>
			//{
			//	lblSendCodeError.Text = "";

			//	//saForgotUsernameEnterCode.IsVisible = true;
				
			//	return false;
			//});

			//PopupNavigation.PopAsync(true);
			//Navigation.PushPopupAsync(new ForgotUsernameEnterCode(), true);



            RecoverUsernameAPI api = new RecoverUsernameAPI(entryEmail.Text.Trim());
            api.setCallbacks(this);
            api.getResponse();
		}


		public void OnError(string errMsg, BaseAPI caller)
		{
			DisplayAlert("Send failed for "+entryEmail.Text.Trim(), errMsg ,"Okay");
		}

		public void OnErrorCode(int errorCode, BaseAPI caller)
		{
			
		}

		public void OnSuccess(JObject response, BaseAPI caller)
		{
            Log.e("sadsa","dsadas");


            //var username = response["username"].Value<string>();

            //if(!string.IsNullOrEmpty(username))
            //DisplayAlert("Success", "Your username is "+username,"Thanks");


            string successResponse = response["success"].Value<JArray>()[0].Value<string>();
            DisplayAlert("Success",successResponse, "Okay");

            this.closeEnterEmailDialog(null,null);
                
		}
    }
}
