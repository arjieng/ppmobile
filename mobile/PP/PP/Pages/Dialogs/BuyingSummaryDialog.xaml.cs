﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PP.tools.API;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs
{
    public partial class BuyingSummaryDialog : PopupPage,IBaseAPIInterface
	{
		void closeDialog(object sender, System.EventArgs e)
		{
			//saForgotUsernameEnterEmail.IsVisible = false;
			PopupNavigation.PopAsync(true);
		}

        public void OnSuccess(JObject response, BaseAPI caller)
        {
			if ((caller as FetchTransactionsSummaryAPI) != null)
			{
                float val = response["transactions_statistics"]["total_spent"].Value<float>();
                lblBuyTotal.Text = String.Format("{0:$0,0.00}",val );
			}
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            throw new NotImplementedException();
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            throw new NotImplementedException();
        }

        public BuyingSummaryDialog()
        {
            InitializeComponent();

			FetchTransactionsSummaryAPI api = new FetchTransactionsSummaryAPI(Constants.currentUser.token, 0);
			api.setCallbacks(this);
			api.getResponse();
        }
    }
}
