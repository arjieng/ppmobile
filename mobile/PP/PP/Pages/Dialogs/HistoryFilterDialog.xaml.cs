﻿using System;
using System.Collections.Generic;
using PP.CustomLayouts;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs
{
    public partial class HistoryFilterDialog : PopupPage
    {
        public enum TRANSACTION_FILTER{
            ALL,
            SELLING,
            BUYING,
            DATE_RANGE
        }

        public interface FilterCallbacks{
            void didSetFilter(TRANSACTION_FILTER filter, HistoryFilterDialog dialog);
            void didSetDateRangeFilter(DateTime startDate, DateTime endDate, HistoryFilterDialog dialog);
            void didCancelFilter(HistoryFilterDialog dialog);
        }


        //bool dateRangeSelected;
        Nullable<DateTime> startDate, endDate;

		void closeDialog(object sender, System.EventArgs e)
		{
			//saForgotUsernameEnterEmail.IsVisible = false;
            PopupNavigation.PopAsync(true);
		}


        public HistoryFilterDialog(TRANSACTION_FILTER defaultFilter)
        {
            InitializeComponent();

            switch (defaultFilter){
                case TRANSACTION_FILTER.ALL:
                    radioAll.IsSelected = true;
                    break;
                case TRANSACTION_FILTER.BUYING:
					radioBuying.IsSelected = true;
					break;
                case TRANSACTION_FILTER.DATE_RANGE:
					radioRange.IsSelected = true;
					break;
				case TRANSACTION_FILTER.SELLING:
					radioSelling.IsSelected = true;
					break;
            }

        }

        public FilterCallbacks callbacks;

        public void setCallBacks(FilterCallbacks callbacks){
            this.callbacks = callbacks;
        }
        void Handle_RadioClicked(object sender, System.EventArgs e)
        {
            radioAll.IsSelected =
            radioRange.IsSelected = 
                radioBuying.IsSelected = 
                    radioSelling.IsSelected = false;

            var x = sender as XFRadio;

            x.IsSelected = x.IsSelected ? x.IsSelected : !x.IsSelected;

            if (x != radioRange)
                startDate = null;
        }

        void RadioTextTapped(object sender, System.EventArgs e)
        {

            ScaledStackLayout s = sender as ScaledStackLayout;

            foreach(View v in s.Children){
                if((v as XFRadio) != null){
                    Handle_RadioClicked((v as XFRadio),e);
                    break;
                }
            }
        }
		void DoneTapped(object sender, System.EventArgs e)
		{

            if(!radioRange.IsSelected && !radioBuying.IsSelected && !radioSelling.IsSelected && !radioAll.IsSelected){
                //no filter selected

                return;
            }

            if (radioBuying.IsSelected){
				if (callbacks == null) return;
				callbacks.didSetFilter(TRANSACTION_FILTER.BUYING, this);
            }
            else if (radioSelling.IsSelected){
                
                if (callbacks == null) return;
                callbacks.didSetFilter(TRANSACTION_FILTER.SELLING, this);
            }
            else if(radioAll.IsSelected){
				if (callbacks == null) return;
				callbacks.didSetFilter(TRANSACTION_FILTER.ALL, this);
            }
            else if (radioRange.IsSelected)
			{
				if (callbacks == null) 
                    return;

                if(startDate != null){
					if (!entryDays.Text.Trim().Equals(""))
					{
						endDate = startDate.Value.AddDays(Int32.Parse(entryDays.Text.Trim()));
                        callbacks.didSetDateRangeFilter(startDate.Value,endDate.Value,this);
					}
                    else{
                        DisplayAlert("Incomplete", "Please add number of days.", "Okay");
                        return;
                    }
                     

                }
                else{
					DisplayAlert("Incomplete", "Please add a start date for date range.", "Okay");
					return;
                }
                    

                    
                   


            }
            closeDialog(null,null);

		}

        void StartDateSelected(object sender, Xamarin.Forms.DateChangedEventArgs e)
        {
            lblStartDate.Text = e.NewDate.ToString(pickerStartDate.Format);// pickerStartDate.Date.ToString( pickerStartDate.Format);   
            startDate = e.NewDate;

        }

        void SetDateTapped(object sender, System.EventArgs e)
        {

            if (!radioRange.IsSelected)
                return;
            
            pickerStartDate.MaximumDate = DateTime.Today;

            Device.BeginInvokeOnMainThread(()=>{

                //pickerStartDate.Unfocus();
                pickerStartDate.Focus();
            });
        }

    }
}
