﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PP.tools.API;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using static PP.Pages.Dialogs.RateBuyer;

namespace PP.Pages.Dialogs
{
    public partial class RateBuyer : PopupPage, IRateBuyerInterface,IBaseAPIInterface
    {
        public interface IRateBuyerInterface{
            void didRateBuyer();
            void didRateBuyerCancelled();
        }

        private IRateBuyerInterface ratingInterface;

        private string ToRateUserID, ToRateUserImage, ToRateUserName;
        private string transactionId;

        Loading_Dialog loadDialog;

        public bool RatingRequired{
            get;set;
        }

        void closeDialog(object sender, System.EventArgs e)
        {
            //saForgotUsernameEnterEmail.IsVisible = false;
            PopupNavigation.PopAsync(true);
        }


        public RateBuyer(string UserTobeRatedID, string UserTobeRatedImage, string UserTobeRatedName, string transactionId)
        {
            InitializeComponent();
            ratingInterface = this;
            ToRateUserID = UserTobeRatedID;
            this.transactionId = transactionId;
            this.ToRateUserImage = UserTobeRatedImage;
            this.ToRateUserName = UserTobeRatedName;
            loadDialog = new Loading_Dialog();


            setupValues();
        }
        void setupValues(){
            ciUserImage.Source = ToRateUserImage;
            lblRatedUser.Text = ToRateUserName;
        }

        public void setRatingCallbacks(IRateBuyerInterface ratingCallbacks){
            ratingInterface = ratingCallbacks;
        }
		
        void startRating(){
            
        }
        private string _RatedRole = "";
        public string RatedRole{
            set{
                _RatedRole = value;
                lblRatedRole.Text = "Rate your "+value+"!";
            }
            get { return _RatedRole; }
        }


        public void didRateBuyer()
        {
            
        }

        public void didRateBuyerCancelled()
        {
           
        }

        void DoneTapped(object sender, System.EventArgs e)
        {
            //editorFeedbackMessage
            //rbRatingBar
            //lblRatedUser

            CreateFeedbackAPI api = new CreateFeedbackAPI(Constants.currentUser.id,
                                                          Constants.currentUser.token,
                                                          ToRateUserID,
                                                          transactionId,
                                                          rbRatingBar.Rating,
                                                          editorFeedbackMessage.Text);

            api.setCallbacks(this);
            api.getResponse();

            loadDialog.Text = "Sending feedback";
            loadDialog.open();
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            Log.e("sadsada",response.ToString() );

            loadDialog.close();

            if (response["success"].Value<JArray>().Count > 0)
                ratingInterface.didRateBuyer();
            closeDialog(null,null);
        }

        public void OnError(string errMsg, BaseAPI caller)
        {

            loadDialog.close();
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            loadDialog.close();
        }

        protected override bool OnBackButtonPressed()
        {
            if (RatingRequired){

                DisplayAlert("Rating Required", "Please rate your " + RatedRole + " before proceeding with other transactions.", "Okay");

                return false;
            }

            return base.OnBackButtonPressed();
        }
    }
}
