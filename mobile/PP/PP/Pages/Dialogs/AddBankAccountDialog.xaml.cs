﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PP.CustomLayouts;
using PP.tools.API;
using PP.tools.models;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs
{
    public partial class AddBankAccountDialog : PopupPage,IBaseAPIInterface
    {

        public Page callerPage;
        Loading_Dialog loadDialog;
        String address = "";
        String city= "";
        String state= "";
        String zip= "";
        String phone= "";

		public AddBankAccountDialog(String address,
								   String city,
								   String state,
								   String zip,
								   String phone)
        {
			InitializeComponent();

             this.address = address;
			 this.city = city;
			 this.state = state;
			 this.zip = zip;
			 this.phone = phone;

			IsOpen = false;
            loadDialog = new Loading_Dialog();

        }

		void CloseDialogTapped(object sender, System.EventArgs e)
		{
            //1st Parent is StackLayout, next in heirchy is the content view which is the bank account listview
            // Check BankAccountListView xaml file to check view heirchy
            close();
		}

		public void close()
		{
			if (IsOpen)
			{

				PopupNavigation.PopAsync(true);
				IsOpen = false;
			}

		}

		private bool IsOpen { get; set; }

		public void open()
		{
			if (!IsOpen)
			{
				Navigation.PushPopupAsync(this, true);
				IsOpen = true;
			}
		}





		void RoutingNumberChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			// reset the bank name first to disallow saving while the routing number is being verified.
			lblBankName.Text = "Not found";

			FetchBankNameAPI api = new FetchBankNameAPI(Constants.currentUser.token, entryRoutingNumber.Text.Trim());
			api.setCallbacks(this);
			api.getResponse();
		}

		public void OnSuccess(JObject response, BaseAPI caller)
		{
            if((caller as FetchBankNameAPI) !=null )
			lblBankName.Text = response["bank_name"]["name"].Value<string>();
            else if((caller as AddBankAccountAPI) !=null){

                BankAccount ba = JsonConvert.DeserializeObject<BankAccount>(response["bank_account"].ToString(), new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });

                if ((callerPage as BankAccounts) != null)
                    (callerPage as BankAccounts).AddNewAccount(ba);

                loadDialog.close();
                close();
            }
		}

		public void OnError(string errMsg, BaseAPI caller)
		{
            if ((caller as FetchBankNameAPI) != null)
			lblBankName.Text = "Not found";
		}

		public void OnErrorCode(int errorCode, BaseAPI caller)
		{
			//throw new NotImplementedException();
		}


		void Handle_RadioClicked(object sender, System.EventArgs e)
		{
			

			radioPersonal.IsSelected = false;
			radioSavings.IsSelected = false;
			radioBusiness.IsSelected = false;


			var x = sender as XFRadio;


			x.IsSelected = x.IsSelected ? x.IsSelected : !x.IsSelected;



		}

		void SaveTapped(object sender, System.EventArgs e)
		{

			if (!entryRoutingNumber.Text.Trim().Equals(""))
			{
				if (!entryAccount.Text.Trim().Equals(""))
				{
					if (!lblBankName.Text.ToLower().Equals("not found"))
					{
						if (!radioSavings.IsSelected && !radioPersonal.IsSelected && !radioBusiness.IsSelected)
						{
							//prompt user to select account type
							showAlert("Please select an account type");
						}
                        else{
                            //close();


                            if(string.IsNullOrEmpty(entryAccountName.Text.Trim())){

								showAlert("Please set an account name");
                            }
                            else
                            {
								int accountType = 0;
								if (radioPersonal.IsSelected)
									accountType = 0;
								else if (radioBusiness.IsSelected)
									accountType = 1;
								else if (radioSavings.IsSelected)
									accountType = 2;


								AddBankAccountAPI api = new AddBankAccountAPI(Constants.currentUser.token,
																			 entryRoutingNumber.Text.Trim(),
																			 entryAccount.Text.Trim(),
																			  accountType,
																			  entryAccountName.Text.Trim(),
																			  false,
                                                                              address,city,state,zip,phone
																			 );
								api.setCallbacks(this);
								api.getResponse();
                            }

                        }
					}
					else
					{
						//prompt user to add valid routing number

						showAlert("Please use a valid routing number");
					}
				}
				else
				{
					//prompt user to add account number
					showAlert("Please add an account number");
				}
			}
			else
			{
				//prompt user to add routing number
				showAlert("Please add a routing number");
			}

		}

		void showAlert(String alert)
		{

			Page p = GetEarliestPageAncestor(this);

			if (p != null)
				p.DisplayAlert("Alert", alert, "Okay");

		}

		Page GetEarliestPageAncestor(Element e)
		{

			if (e == null)
				return null;

			if ((e.Parent as Page) != null)
				return e.Parent as Page;
			else return GetEarliestPageAncestor(e.Parent);

		}


	}
}
