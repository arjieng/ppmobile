﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using static PP.Pages.Dialogs.EnterAmountPopUpPage;


namespace PP.Pages.Dialogs
{
    public partial class EnterAmountPopUpPage : PopupPage, IEnterAmount
    {
        public IEnterAmount callback;

        public EnterAmountPopUpPage()
        {
            InitializeComponent();
            //callback = this;
        }

        public void close()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                PopupNavigation.PopAsync();
                Navigation.PopPopupAsync();
            });
        }

        public void SetQuantity(int? quantity)
        {
            
        }

        void closeDialog(object sender, System.EventArgs e)
        {
            close();
        }

        void OK_Tapped(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(entryQuantity.Text.Trim())){
                callback.SetQuantity(null);
            }else{
                callback.SetQuantity(int.Parse(entryQuantity.Text.Trim()));
            }
            close();
        }

        public interface IEnterAmount{
            void SetQuantity(int? quantity);
        }

        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(String), typeof(EnterAmountPopUpPage), "Please enter quantity of items to be purchased.",
        propertyChanged: (bindable, oldValue, newValue) =>
        {
           var view = bindable as EnterAmountPopUpPage;
            view.messageLabel.Text = view.Text;
        });

        public static readonly BindableProperty AmountProperty = BindableProperty.Create(nameof(Amount), typeof(String), typeof(EnterAmountPopUpPage), null,
        propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as EnterAmountPopUpPage;
            view.entryQuantity.Text = view.Amount;
        });


        public String Amount{
            get { return (String)GetValue(AmountProperty); }
            set{SetValue(AmountProperty, value);}
        }

        public String Text
        {
            get { return (String)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
    }
}
