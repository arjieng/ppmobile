﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using PP.CustomLayouts;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.RangeSlider.Common;
using static PP.Pages.Dialogs.WeekScheduleSellerSetupDialog;

namespace PP.Pages.Dialogs
{
    public partial class WeekScheduleSellerSetupDialog : PopupPage, WeekScheduleCallbacks
    {
        public interface WeekScheduleCallbacks
        {
            void didSetUntilCancel(WeekSchedule values, WeekScheduleSellerSetupDialog dialog);
        }

        public WeekScheduleCallbacks callbacks ;
        public WeekScheduleSellerSetupDialog()
        {
            InitializeComponent();
            callbacks = this;
            RangeSlider.LowerValueChanged += LowValueChanged;
            RangeSlider.UpperValueChanged += UpperValueChanged;
            scheduleOptions = new ObservableCollection<string>();

            scheduleOptions.Add("Mon - Fri");
            scheduleOptions.Add("Saturday");
            scheduleOptions.Add("Sunday");

            pickerSchedule.ItemsSource = scheduleOptions;
            pickerSchedule.SelectedIndex = 0;
        }

        ObservableCollection<string> scheduleOptions;

        private void LowValueChanged(object sender, EventArgs e)
        {
            ChangeValue(lowValue, RangeSlider.LowerValue);
        }

        private void UpperValueChanged(object sender, EventArgs e)
        {
            ChangeValue(UpperValue, RangeSlider.UpperValue);
        }

        void ChangeValue(Label target, float val)
        {
            TimeSpan time = TimeSpan.FromSeconds(val);
            //if (time.Days == 1)
            //{
            //    target.Text = "24:00";
            //}
            //else
            //{
            target.Text = time.ToString("hh") + ":" + time.ToString("mm");
            //}
            fullTime.Text = lowValue.Text + " - " + UpperValue.Text;
        }

        void closeDialog(object sender, System.EventArgs e)
        {
            SellerWaitSetupDialog.ShowPanel();
            PopupNavigation.PopAsync(true);

        }

        public void close()
        {
            PopupNavigation.PopAsync(true);
            Navigation.PopPopupAsync(true);

        }

        void StartTapped(object sender, System.EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() => {
                WeekSchedule weekSchedule = new WeekSchedule() { Days = pickerSchedule.SelectedItem as string, LowValue = FloatToTime(RangeSlider.LowerValue), UpperValue = FloatToTime(RangeSlider.UpperValue) };
                callbacks.didSetUntilCancel(weekSchedule, this);
            });
        }

        string FloatToTime(float val){
            return TimeSpan.FromSeconds(val).ToString();
        }

        public void didSetUntilCancel(WeekSchedule values, WeekScheduleSellerSetupDialog dialog)
        {
            
        }
    }

    public class WeekSchedule
    {
        public string Days { get; set; }
        public string LowValue { get; set; }
        public string UpperValue { get; set; }
    }
}