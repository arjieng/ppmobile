﻿using System;
using System.Collections.Generic;
using PP.CustomLayouts;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using static PP.Pages.Dialogs.TimeRangeSellerSetupDialog;

namespace PP.Pages.Dialogs
{
    public partial class TimeRangeSellerSetupDialog : PopupPage, TimeRangeCallbacks
    {


        const int MAX_MINUTES = 59;
        const int MAX_HOURS = 4;

        public interface TimeRangeCallbacks{
            void didSetTime(TimeSpan time,TimeRangeSellerSetupDialog dialog);
        }


        public TimeRangeCallbacks callbacks ;
        public TimeRangeSellerSetupDialog()
        {
            InitializeComponent();
            callbacks = this;
        }

		void closeDialog(object sender, System.EventArgs e)
		{
            //saForgotUsernameEnterEmail.IsVisible = false;
            SellerWaitSetupDialog.ShowPanel();
			PopupNavigation.PopAsync(true);

		}

        public void close()
        {
            

            PopupNavigation.PopAsync(true);
            Navigation.PopPopupAsync(true);

        }
		void StartTapped(object sender, System.EventArgs e)
		{
            //Navigation.PushPopupAsync(new TimeRangeSellerSetupDialog(), true);


            // no time number is set
            if (entryTime.Text.Trim().Equals(""))
                return;

            // no time format is selected
            if (!radioHours.IsSelected && !radioMinutes.IsSelected)
                return;



            if(radioHours.IsSelected)
                callbacks.didSetTime(TimeSpan.FromHours(Int32.Parse(entryTime.Text.Trim())),this);
			else if (radioMinutes.IsSelected)
                callbacks.didSetTime(TimeSpan.FromMinutes(Int32.Parse(entryTime.Text.Trim())),this);


            
		}
		void Handle_RadioClicked(object sender, System.EventArgs e)
		{

            radioMinutes.IsSelected = radioHours.IsSelected = false;

			var x = sender as XFRadio;
			x.IsSelected = x.IsSelected ? x.IsSelected : !x.IsSelected;

            checkTimeEntry();
			
		}

		void RadioTextTapped(object sender, System.EventArgs e)
		{

			ScaledStackLayout s = sender as ScaledStackLayout;

			foreach (View v in s.Children)
			{
				if ((v as XFRadio) != null)
				{
					Handle_RadioClicked((v as XFRadio), e);
					break;
				}
			}
		}

        public void didSetTime(TimeSpan time, TimeRangeSellerSetupDialog dialog)
        {
            
        }

        void TimeTextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            checkTimeEntry();
        }

        void checkTimeEntry(){
            if (entryTime.Text.Trim().Equals(""))
                return;

			int timeValue = Int32.Parse(entryTime.Text);

			if (radioMinutes.IsSelected)
			{
				if (timeValue > MAX_MINUTES)
				{

					entryTime.TextChanged -= TimeTextChanged;
					entryTime.Text = "" + MAX_MINUTES;
					entryTime.TextChanged += TimeTextChanged;
				}
			}
			else if (radioHours.IsSelected)
			{
				//if (timeValue > MAX_HOURS)
				//{
				//	entryTime.TextChanged -= TimeTextChanged;
				//	entryTime.Text = "" + MAX_HOURS;
				//	entryTime.TextChanged += TimeTextChanged;
				//}
			}
        }
    }
}
