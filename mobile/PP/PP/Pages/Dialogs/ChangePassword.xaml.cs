﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PP.tools.API;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs
{
    public partial class ChangePassword : PopupPage,IBaseAPIInterface
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        public String ChangePasswordToken { get; set; }

        public ContentPage CallerPage { get; set; }

		void closeDialog(object sender, System.EventArgs e)
		{
			//saForgotUsernameEnterEmail.IsVisible = false;
			PopupNavigation.PopAsync(true);
		}

		void changePassword(object sender, System.EventArgs e)
		{
            //saForgotUsernameEnterEmail.IsVisible = false;
            //PopupNavigation.PopAsync(true);

            if (lblPassword.Text.Length >= 8)
            {
                if(lblPassword.Text.Equals(lblConfirmPassword.Text)){
                    lblSendCodeError.Text = "";

                    ChangePasswordAPI api = new ChangePasswordAPI(ChangePasswordToken, lblPassword.Text.Trim(), lblConfirmPassword.Text.Trim());
                    api.setCallbacks(this);
                    api.getResponse();
                }
				else
					lblSendCodeError.Text = "Password and confirm password must match.";
            }
            else
                lblSendCodeError.Text = "Password must be at least eight characters long.";
		}

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            //Log.e();

            //if(Constants.isDebug){
            //    response = TestData.getTestData("password_changed.json");
            //}

            Log.e("rsada","r"+response.ToString());

            if((CallerPage as ForgotPasswordQuestions) != null){


				PopupNavigation.PopAsync(true);
                (CallerPage as ForgotPasswordQuestions).didChangePassword(response);
            }

        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            throw new NotImplementedException();
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            throw new NotImplementedException();
        }
    }
}
