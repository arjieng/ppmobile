﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;
using PP.tools.API;
using PP.tools.models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs
{
    public partial class ForgotPasswordPrompt : PopupPage,IBaseAPIInterface
    {

        Loading_Dialog loadDialog;
        public ForgotPasswordPrompt()
        {
            InitializeComponent();
			loadDialog = new Loading_Dialog();
			loadDialog.Text = "Checking...";
        }

		void closeForgotPassword(object sender, System.EventArgs e)
		{
			//saForgotUsernameEnterCode.IsVisible = false;
            PopupNavigation.PopAsync(true);
		}

		void startRecovery(object sender, System.EventArgs e)
		{
            //saForgotUsernameEnterCode.IsVisible = false;


            if (!string.IsNullOrEmpty(entryUsername.Text.Trim()))
            {
                
                loadDialog.open();
                fetchQuestions(entryUsername.Text.Trim());
            }
            else{


				lblGetPasswordError.Text = "Please fill in the username...";
				Device.StartTimer(TimeSpan.FromMilliseconds(3000), () =>
				{
				  lblGetPasswordError.Text = "";
				  return false;
				});

			}


		}

		void fetchQuestions(string username)
		{
			FetchRecoveryQuestionsAPI api = new FetchRecoveryQuestionsAPI(username);
			api.setCallbacks(this);
			api.getResponse();

		}

        public void OnSuccess(JObject response, BaseAPI caller)
        {

            Device.BeginInvokeOnMainThread(()=>{
				loadDialog.close();
            });
			if ((caller as FetchRecoveryQuestionsAPI) != null)
			{
				//if (Constants.isDebug)
				//{
				//	JObject j = TestData.getTestData("recovery_questions.json");
				//	response = j;
				//}


				JArray recQuestions = response["user"]["recovery_questions"].Value<JArray>();
				ObservableCollection<Question> questions = new ObservableCollection<Question>();


				foreach (JObject j in recQuestions)
				{
					Log.e("akhfa", "(" + j["question_id"] + ") " + j["question"]);
					Question q = new Question();
					q.question = j["question"].Value<String>();
					q.question_id = j["question_id"].Value<String>();
					questions.Add(q);
				}

				ForgotPasswordQuestions fpq = new ForgotPasswordQuestions(questions,entryUsername.Text.Trim());

				Navigation.PushModalAsync(fpq, true);
				

			}


			
        }

        public void OnError(string errMsg, BaseAPI caller)
        {

			Device.BeginInvokeOnMainThread(() => {
				loadDialog.close();
			});

            DisplayAlert("Failed", errMsg+" Please try again.", "Okay");
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            throw new NotImplementedException();
        }
    }
}
