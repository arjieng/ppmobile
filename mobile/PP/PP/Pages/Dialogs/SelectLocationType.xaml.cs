﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using static PP.Pages.Dialogs.SelectLocationType;


namespace PP.Pages.Dialogs
{
    public partial class SelectLocationType : PopupPage
    {
        public void close()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                PopupNavigation.PopAsync();
                Navigation.PopPopupAsync();
            });
        }

        public ISelectLocationType callback;

        public interface ISelectLocationType{
            void SetSelection(SelectLocationType dialog, int type);
        }

        void closeDialog(object sender, System.EventArgs e)
        {
            close();
        }

        public SelectLocationType()
        {
            InitializeComponent();
        }

        void FixedLocationTapped(object sender, EventArgs e){
            callback.SetSelection(this, 1);
            Device.BeginInvokeOnMainThread(()=>{
                close();
            });
        }

        void MeetupTapped(object sender, EventArgs e)
        {
            callback.SetSelection(this, 2);
            Device.BeginInvokeOnMainThread(()=>{
                close();
            });
        }
    }
}
