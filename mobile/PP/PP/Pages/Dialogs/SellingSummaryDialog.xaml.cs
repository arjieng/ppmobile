﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PP.tools.API;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages.Dialogs
{
    public partial class SellingSummaryDialog : PopupPage,IBaseAPIInterface
    {
		void closeDialog(object sender, System.EventArgs e)
		{
			//saForgotUsernameEnterEmail.IsVisible = false;
            PopupNavigation.PopAsync(true);

		}


		public SellingSummaryDialog()
        {
            InitializeComponent();

			FetchTransactionsSummaryAPI api = new FetchTransactionsSummaryAPI(Constants.currentUser.token, 1);
			api.setCallbacks(this);
			api.getResponse();
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            throw new NotImplementedException();
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            throw new NotImplementedException();
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
			if ((caller as FetchTransactionsSummaryAPI) != null)
			{
				lblTotalCost.Text = String.Format("{0:$0,0.00}", response["transactions_statistics"]["total_cost"].Value<float>());
				lblRevenue.Text = String.Format("{0:$0,0.00}", response["transactions_statistics"]["revenue"].Value<float>());
				lblProfit.Text = String.Format("{0:$0,0.00}", response["transactions_statistics"]["profit"].Value<float>());
			}
        }
    }
}
