﻿using System;
using Newtonsoft.Json.Linq;
using Plugin.Fingerprint;
using PP.CustomLayouts;
using PP.tools.API;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class EnterEasyPinOrFingerprint : ContentPage,IBaseAPIInterface
    {
        String currentPass = "";

        int passMinLength = 4;

        bool IsLoading = false;

        public EnterEasyPinOrFingerprint()
        {
            InitializeComponent();
            checkFingerprintAvailability();
        }

        void ChangeUser_Tapped(object sender, EventArgs e){
            Constants.Logout();
        }

       async void checkFingerprintAvailability(){
            bool isAvailable =await CrossFingerprint.Current.IsAvailableAsync(false);
            //cvFingerPrint.IsVisible = isAvailable;
        }
       

        void KeyTapped(object sender, System.EventArgs e)
        {
            if (IsLoading){

                Log.e("Is loading...");

                return;
            }

            var key = ((Label)((CorneredView)sender).Content).Text;

            Log.e("sadadads", "Key: " + key);

            //check if its a number
            if (int.TryParse(key, out int n))
            {
                //check if its already full
                if (currentPass.Length < passMinLength)
                {
                    //append new key
                    currentPass = currentPass + "" + key;
                }
                if (currentPass.Length == passMinLength)
                {
                    CheckEasyPinAPI api = new CheckEasyPinAPI(Constants.currentUser.token, currentPass);
                    api.setCallbacks(this);
                    api.getResponse();
                    IsLoading = true;
                }
            }
            //otherwise it's either clear or check
            else
            {

                if (key.ToLower().Equals("C".ToLower()))
                {
                    currentPass = "";
                }
                else
                {

                    //neextt

                    //if (currentPass.Length == 4)
                    //{
                    //    CheckEasyPinAPI api = new CheckEasyPinAPI(Constants.currentUser.token,currentPass);
                    //    api.setCallbacks(this);
                    //    api.getResponse();
                    //}
                    //else
                        //DisplayAlert("Incomplete", "Please fill in " + passMinLength + " keys", "Okay");
                }
            }

            updateCheckboxes();
            Log.e("Current pass", currentPass);
        }
        void updateCheckboxes()
        {

            //radOne.IsSelected = false;
            //radTwo.IsSelected = false;
            //radThree.IsSelected = false;
            //radFour.IsSelected = false;

            //for (int x = 0; x < currentPass.Length; x++)
            //{
            //    if (x == 0)
            //        radOne.IsSelected = true;
            //    if (x == 1)
            //        radTwo.IsSelected = true;
            //    if (x == 2)
            //        radThree.IsSelected = true;
            //    if (x == 3)
            //        radFour.IsSelected = true;
            //}

            //apparently doing previous code cause a flick on
            //custom radio view when changing selected images

            // so, back to the long way


            switch(currentPass.Length){
                case 1:
                    setupRadio(radOne,true);
                    setupRadio(radTwo, false);
                    setupRadio(radThree, false);
                    setupRadio(radFour, false);
                    break;
                case 2:
                    setupRadio(radOne, true);
                    setupRadio(radTwo, true);
                    setupRadio(radThree, false);
                    setupRadio(radFour, false);
                    break;
                case 3:
                    setupRadio(radOne, true);
                    setupRadio(radTwo, true);
                    setupRadio(radThree, true);
                    setupRadio(radFour, false);
                    break;
                case 4:

                    setupRadio(radOne, true);
                    setupRadio(radTwo, true);
                    setupRadio(radThree, true);
                    setupRadio(radFour, true);
                    break;
                default:
                    setupRadio(radOne, false);
                    setupRadio(radTwo, false);
                    setupRadio(radThree, false);
                    setupRadio(radFour, false);
                    break;
            }
        }
        void setupRadio(XFRadio rad, bool select){
            if(select && !rad.IsSelected){
                rad.IsSelected = select;
            }
            else if(!select && rad.IsSelected){
                rad.IsSelected = select;
            }
        }

        public void OnError(string errMsg, BaseAPI caller)
        {

            if((caller as CheckEasyPinAPI) != null){
                DisplayAlert("Wrong pin", "You have entered a wrong pin. Please try again.", "Okay");
                currentPass = "";
                updateCheckboxes();
                IsLoading = false;
            }

        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            IsLoading = false;
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            if ((caller as CheckEasyPinAPI) != null)
            {
                if (response["success"].Value<bool>())
                {
                    SidePanelPage spp = new SidePanelPage();
                    Application.Current.MainPage = new NavigationPage(spp);
                }

                IsLoading = false;
            }
        }


        async void checkFingers()
        {
            if (await CrossFingerprint.Current.IsAvailableAsync() == false){

                await DisplayAlert("Missing", "Fingerprint feature cannot be used. Either your device does not support it or there are no touch IDs registered on your device.", "Okay");
            }
            var res = await CrossFingerprint.Current.AuthenticateAsync("Place your finger on the fingerprint sensor.");
            if (res.Authenticated)
            {
                Log.e("WOW", "Authenticated");
                SidePanelPage spp = new SidePanelPage();
                Application.Current.MainPage = new NavigationPage(spp);
            }
            else
            {
                Log.e("OPS", "Not Authenticated");
            }
        }

        void FingerprintTapped(object sender, System.EventArgs e)
        {
            checkFingers();
        }
    }
}
