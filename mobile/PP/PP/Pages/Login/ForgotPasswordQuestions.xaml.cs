﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PP.Pages.Dialogs;
using PP.tools.API;
using PP.tools.models;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class ForgotPasswordQuestions : ContentPage, IBaseAPIInterface
    {


        ObservableCollection<Question> questions;
        string username;

        public ForgotPasswordQuestions(ObservableCollection<Question> questions, string username)
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            this.questions = questions;
            this.username = username;

			lblQuestionOne.Text = this.questions[0].question;
			lblQuestionTwo.Text = this.questions[1].question;
			lblQuestionThree.Text = this.questions[2].question;
            lblbAccount.Text = this.username;
        }


        void back(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync(true);
        }

    

        void RecoverTapped(object sender, System.EventArgs e)
        {

            RecoverAccountAPI recover = new RecoverAccountAPI(this.username,
                                                              this.questions[0].question_id,
                                                              entryAnswerOne.Text.Trim(),
															 this.questions[1].question_id,
															  entryAnswerTwo.Text.Trim(),
															 this.questions[2].question_id,
															  entryAnswerThree.Text.Trim());
            recover.setCallbacks(this);
            recover.getResponse();
        }


        public void OnError(string errMsg, BaseAPI caller)
        {
            DisplayAlert("Failed", errMsg, "Okay");
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            throw new NotImplementedException();
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            if ((caller as RecoverAccountAPI) != null)
            {
				// if (Constants.isDebug)
				// {
                //   JObject j = TestData.getTestData("recover_account.json");
				//	 response = j;
				// }

                Log.e("results:", response.ToString());
                ChangePassword cp = new ChangePassword();
                cp.CallerPage = this;
                cp.ChangePasswordToken = response["user"]["token"].Value<String>();
                Navigation.PushPopupAsync(cp, true);
            }

        }

        public void didChangePassword(JObject changePassResponse){

            Log.e("DIDCHANGEPASSWORD",""+changePassResponse);
            DisplayAlert("Password Changed", changePassResponse["response"].Value<String>(), "Okay");
            //Navigation.PopAsync(true);
            Navigation.PopModalAsync(true);
        }

	}
}
