﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Fingerprint;
using PP.Pages.Dialogs;
using PP.Pages.Profile;
using PP.tools;
using PP.tools.API;
using PP.tools.API.BaseAPI;
using PP.tools.models;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class LoginPage : ContentPage,IBaseAPIInterface,TermsAndServicesPage.TermsCallbacks
    {

        //instance of the loading dialog
        Loading_Dialog loadDialog;
        bool isClicked = false;
        IInterstitialAdMobService interstitialAd;
        public LoginPage()
        {
            //remove the navigation bar
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            amvBanner.AdUnitId = AdUnitId;

            interstitialAd = DependencyService.Get<IInterstitialAdMobService>().InitWithAdUnitId(InterstitialAdUnit);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    entryUsername.Text = "honeybuiscuit";
                    break;
                case Device.Android:
                    entryUsername.Text = "berrycream";
                    break;
            }
            // this automates login process using a static username password combination
            automateLogin();   
        }

        async void checkFingers(){

            var res = await CrossFingerprint.Current.AuthenticateAsync("Place your finger on the fingerprint sensor.");
            if(res.Authenticated){
                Log.e("WOW","Authenticated");
            }
            else{
               Log.e("OPS", "Not Authenticated");
            }
        }
        public class Error
        {
            public Payload payload { get; set; }
        }
        public class Payload{
            public string text { get; set; }
        }

        async void sendError(){
           


            string sUrl = "https://hooks.slack.com/services/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";
            string sContentType = "application/json";


            JObject oJsonObject = new JObject();
            oJsonObject.Add("text", "Xamarin Error");

            HttpClient oHttpClient = new HttpClient();
            var oTaskPostAsync = oHttpClient.PostAsync(sUrl, new StringContent(oJsonObject.ToString(), Encoding.UTF8, sContentType));
           await oTaskPostAsync.ContinueWith((oHttpResponseMessage) =>
            {
                Log.e("RESP","RESPONSE: " + (oHttpResponseMessage.Status));
            });
        }


        /// <summary>
        /// When the login button gets clicked
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void LoginTapped(object sender, System.EventArgs e)
        {
            if(interstitialAd.IsReady()){

                interstitialAd.ShowInterstitialAd();
                return;
            }
            if (!isClicked)
            {
                isClicked = true;
                //send error

                //sendError();
                //Exception ex =  new Exception("App center test error");
                //Crashes.TrackError(ex, null);
                //return;
                ////

                //checkFingers();
                //return;

                // Check if the username field is empty
                if (entryUsername.Text.Trim().Length == 0)
                {
                    DisplayAlert("Opps", "Please fill in your username", "Okay");
                    return;
                }
                // Check if the password field is empty
                else if (entryPassword.Text.Trim().Length == 0)
                {
                    DisplayAlert("Opps", "Please fill in your password", "Okay");
                    return;
                }

                //initiailize loading dialog
                loadDialog = new Loading_Dialog();

                // Prompt a loading text
                loadDialog.Text = "Logging in...";

                // Display the loading dialog
                loadDialog.open();

                // initiate the login process and initialize the login api using the username and password texts 
                // from the fields.
                LoginAPI api = new LoginAPI(entryUsername.Text.Trim(), entryPassword.Text.Trim());
                api.setCallbacks(this); // makes this class handle the API callback methods
                api.getResponse();
            }
        }

        // Automates the login process using a static username and password
        // which is prefilled to the username & password textfields and is
        // then fires the login button being tapped.
        void automateLogin(){

            if (!Constants.AutomateValidActions)
                return;



            entryUsername.Text = "berrycream";//hogwarts
			entryPassword.Text = "password";
            LoginTapped(null,null);
        }


        //handler when the forgot text is tapped
		void ForgotTapped(object sender, System.EventArgs e)
		{
            //saForgotPrompt.IsVisible = true;
            // opens the Forgot prompt dialog
            Navigation.PushPopupAsync(new ForgotPrompt(), true);

        }

        //handler when the create account button is tapped.
		void CreateAccountTapped(object sender, System.EventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("Create");

            Navigation.PushModalAsync(new SignUpPage());
        }
        // handler when the watch tutorial button is tapped
		void WatchTutorialTapped(object sender, System.EventArgs e)
		{
            //nothing here yet
            Navigation.PushModalAsync(new TutorialPage(),true);
		}

        /// <summary>
        /// BaseAPI callback that handles success responses for API instances
        /// that set this class as callback handler
        /// </summary>
        /// <param name="response">Json response from the web service.</param>
        /// <param name="caller">BaseAPI instance that intiated the API process (BaseAPI's getResponse() method/function).</param>
        public void OnSuccess(JObject response, BaseAPI caller)
        {
            isClicked = false;
            if ((caller as LoginAPI) != null)
            {

                Device.BeginInvokeOnMainThread(() =>
                {

                    //dismisses the loading dialog
                    //loadDialog.close();

                    //creates a user instance of the login response json
                    User u = JsonConvert.DeserializeObject<User>(response["user"].ToString(), new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    });

                    //assigns that user instance to be globally accessible on the app.
                    Constants.currentUser = u;

                    // initiates caching of the logged in user
                    Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                    {
                        Cacher.CacheCurrentUser();
                        return false;
                    });

                    // the full user image url is formed
                    //if(!u.image.StartsWith(Constants.ROOT_URL)){
                    //    u.image = Constants.ROOT_URL + u.image;
                    //}

                    //// open the SidePanel (User homepage with side menu) page
                    //SidePanelPage spp = new SidePanelPage();
                    //Application.Current.MainPage = new NavigationPage(spp);

                    //nope, check for new terms version first
                    checkTermsVersion();

                    //Navigation.PushModalAsync(new SetupEasyPinPage());
                });
            }
            else if ((caller as FetchTermVersionAPI) != null)
            {
                loadDialog.close();
                if (response["has_new_term"].Value<bool>())
                {
                    DisplayAlert("Terms Update", "Terms and conditions have been updated, you need to agree on it to continue using this app.", "Okay");
                    Navigation.PushModalAsync(new TermsAndServicesPage() { IsAgreeingMode = true, callbacks = this });
                }
                else
                {
                    SidePanelPage spp = new SidePanelPage();
                    Application.Current.MainPage = new NavigationPage(spp);
                }
            }
        }

		/// <summary>
        /// BaseAPI callback that handles negative responses (not connection responses) for API instances
		/// that set this class as callback handler
		/// </summary>
		/// <param name="errMsg">Error message response from the web service.</param>
		/// <param name="caller">BaseAPI instance that intiated the API process (BaseAPI's getResponse() method/function).</param>
        public async void OnError(string errMsg, BaseAPI caller)
        {
            isClicked = false;
            
            if ((caller as LoginAPI) != null)		{

				//dismisses the loading dialog
				Device.BeginInvokeOnMainThread(() => {
                     DisplayAlert("Login failed", errMsg, "Okay");

				
                });
                await Task.Delay(100);
                loadDialog.close();

            }	
        }

		/// <summary>
        /// BaseAPI callback that handles negative connection responses (ex. 404, 502)for API instances
		/// that set this class as callback handler
		/// </summary>
		/// <param name="errorCode">Error code response from the web.</param>
		/// <param name="caller">BaseAPI instance that intiated the API process (BaseAPI's getResponse() method/function).</param>
		public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            isClicked = false;
			if ((caller as LoginAPI) != null)
			{

				//dismisses the loading dialog
                Device.BeginInvokeOnMainThread(()=>{
					loadDialog.close();
					//prompt the user of the error code
					DisplayAlert("Login failed", "Error Code: " + errorCode + "", "Okay");
                });

			}
		}


        void checkTermsVersion()
        {

            loadDialog.Text = "Checking terms..please wait";
            //loadDialog.open();

            FetchTermVersionAPI api = new FetchTermVersionAPI();
            api.setCallbacks(this);
            api.getResponse();
        }

        public void didAgree(string termVersion, TermsAndServicesPage termsPage)
        {
            checkTermsVersion();
        }

        public void didDecline(TermsAndServicesPage termsPage)
        {
            
        }


        public String AdUnitId{
            get{
                if (Device.RuntimePlatform == Device.iOS)
                    return Constants.ADMOB_SHOULD_USE_SAMPLE_ADUNIT ?
                                    Constants.ADMOB_SAMPLE_ADUNIT_IOS: Constants.ADMOB_BANNER_AD_IOS;
                else if (Device.RuntimePlatform == Device.Android)
                    return Constants.ADMOB_SHOULD_USE_SAMPLE_ADUNIT ?
                                    Constants.ADMOB_SAMPLE_ADUNIT_ANDROID: Constants.ADMOB_BANNER_AD_ANDROID;
                else return ""; 

            }
        }

        public String InterstitialAdUnit{
            get{
                if (Device.RuntimePlatform == Device.iOS)
                    return Constants.ADMOB_SHOULD_USE_SAMPLE_ADUNIT ?
                                    Constants.ADMOB_SAMPLE_INTERSTITIAL_AD_UNIT_IOS: Constants.ADMOB_INTERSTITIAL_AD_IOS;
                else if (Device.RuntimePlatform == Device.Android)
                    return Constants.ADMOB_SHOULD_USE_SAMPLE_ADUNIT ?
                                    Constants.ADMOB_SAMPLE_INTERSTITIAL_AD_UNIT_ANDROID: Constants.ADMOB_INTERSTITIAL_AD_ANDROID;
                else return ""; 
            }
        }
    }
}
