﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using Plugin.Connectivity;
using PP.Pages.Profile;
using PP.tools;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class MenuPage : ContentPage
    {
        public MenuPage()
        {
            InitializeComponent();


            lblName.Text = Constants.currentUser.first_name + " " + Constants.currentUser.last_name;
            if(Uri.IsWellFormedUriString(Constants.currentUser.image, UriKind.Absolute)){

                Log.e("dasda","url: "+Constants.currentUser.image);
                ciUserImage.Source = Constants.currentUser.image;


            }

            Constants.currentUser.PropertyChanged += (sender, e) => {
                ciUserImage.Source = Constants.currentUser.image;
                lblName.Text = Constants.currentUser.first_name + " " + Constants.currentUser.last_name;
            };

            //var tapT = new TapGestureRecognizer();
            //tapT.Tapped += (sender, e) => {
            //    Navigation.PushModalAsync(new TermsAndServicesPage());
            //};
            //termsE.GestureRecognizers.Add(tapT);
        }
        //void Terms_Tapped(object sender, System.EventArgs e)
        //{
        //    stopHomeTimer();

        //    Navigation.PushModalAsync(new TermsAndServicesPage());
        //}

        void ProfileImageTapped(object sender, System.EventArgs e)
        {
            stopHomeTimer();
            Navigation.PushModalAsync(new ProfilePage());
			((SidePanelPage)Parent).IsPresented = false;
        }
		void BankAccountsTapped(object sender, System.EventArgs e)
        {
            stopHomeTimer();
			Navigation.PushModalAsync(new TransparentNavBarPage(new BankAccounts()));

            ((SidePanelPage)Parent).IsPresented = false;
		}
		public void TransactionsHistoryTapped(object sender, System.EventArgs e)
        {
            stopHomeTimer();

            Navigation.PushModalAsync(new TransparentNavBarPage(new TransactionsHistory()));

			((SidePanelPage)Parent).IsPresented = false;
		}
		void LogoutTapped(object sender, System.EventArgs e)
		{
            //Constants.currentUser = null;
            //var parent = ((SidePanelPage)Parent);

            //parent.Navigation.PopAsync(true);
            //Cacher.CacheCurrentUser();

            //Application.Current.MainPage = new LoginPage();

            stopHomeTimer();
            Constants.Logout();
		}

        //void Help_Tapped(object sender, System.EventArgs e)
        //{
        //    Navigation.PushModalAsync(new HelpPage());
        //    stopHomeTimer();
        //}

        //void FAQ_Tapped(object sender, System.EventArgs e)
        //{
        //    Navigation.PushModalAsync(new FAQPage());
        //    stopHomeTimer();
        //}

        public void Settings_Tapped(object sender, System.EventArgs e)
        {
            stopHomeTimer();

            Navigation.PushModalAsync(new SettingsPage());

           
        }

        //void AboutUs_Tapped(object sender, System.EventArgs e)
        //{
        //    Navigation.PushModalAsync(new AboutUsPage());
        //    stopHomeTimer();
        //}

        void Facebook_Tapped(object sender, System.EventArgs e)
        {
			try
			{

                //try opening the facebook app
				Device.OpenUri(new Uri(Constants.FACEBOOK_PAGE_URL_SCHEME));
			}
			catch (Exception ex)
			{
                // Open the browser if the app does not exist.
				System.Diagnostics.Debug.WriteLine(ex.Message);
				Device.OpenUri(new Uri(Constants.FACEBOOK_PAGE_URL));
            }
            stopHomeTimer();
        }

        void Twitter_Tapped(object sender, System.EventArgs e)
        {

			try
			{
				Device.OpenUri(new Uri(Constants.TWITTER_PAGE_URL_SCHEME));
            }
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
				Device.OpenUri(new Uri(Constants.TWITTER_PAGE_URL));
			}
            stopHomeTimer();
        }
        void ReferAfriend_Tapped(object sender, System.EventArgs e)
        {

            Navigation.PushModalAsync(new ReferAfriendPage());
            stopHomeTimer();
        }

        void stopHomeTimer(){
            //var parent = (Application.Current.MainPage as NavigationPage).CurrentPage as SidePanelPage;
            //BuyerSellerPage home = (parent.Detail as TransparentNavBarPage).CurrentPage as BuyerSellerPage;

            Constants.shouldStopIdleTimer = true;
        }
    }
}
