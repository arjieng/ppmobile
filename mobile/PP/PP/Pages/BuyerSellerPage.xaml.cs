﻿﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using PP.Pages.Dialogs;
using PP.Pages.Dialogs.Dynamic;
using PP.Pages.Profile;
using PP.Pages.Seller;
using PP.tools;
using PP.tools.API;
using PP.tools.Sockets;
using Rg.Plugins.Popup.Extensions;
using Websockets;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class BuyerSellerPage : ContentPage,IBaseAPIInterface, IIdleTimerService
    {
        
        public void didElapseTime(int remainingSeconds)
        {
            if(!Constants.IsInTransaction){
                TimeSpan t = TimeSpan.FromSeconds(remainingSeconds);
                timerLabel.Text = (t.Minutes < 10 ? "0" + t.Minutes : "" + t.Minutes) + ":" + (t.Seconds < 10 ? "0" + t.Seconds : "" + t.Seconds);

                Log.e("Remaining Sec: " + remainingSeconds);
                if (remainingSeconds <= 120 && remainingSeconds > 60)
                {
                    sslTimerContainer.BackgroundColor = Color.Orange;
                    lblTimerPrompt.TextColor = Color.White;
                    timerLabel.TextColor = Color.White;
                    remainingSeconds -= 40;
                }
                else if (remainingSeconds <= 60)
                {
                    sslTimerContainer.BackgroundColor = Color.Red;
                    lblTimerPrompt.TextColor = Color.White;
                    timerLabel.TextColor = Color.White;
                }
                else
                {
                    sslTimerContainer.BackgroundColor = Color.Transparent;
                    lblTimerPrompt.TextColor = Color.Gray;
                    timerLabel.TextColor = Color.Gray;
                }
            }
        }
        public void startIdleTimer()
        {
            
        }

        public void didIdleTimerStarted()
        {
            
        }

        public void didIdleTimedOut()
        {
            if(!Constants.IsInTransaction){
                DisplayAlert("Timeout", "You have been idle for a while, you have been logged out to protect your information", "Okay");
                Constants.Logout();
            }
        }

        public void didIdleTimerCancelledOrReset()
        {
            
        }

        IWebSocketConnection connection;

        ActionCableClient client;


        int counter = 0;


        //private int totalSeconds = 30;//900
        //private int remainingSeconds = 30;//900

        public static bool dialogIsShowed = false;
        ChangeUserRoleAPI toBuyerMerchant, toSellerMerchant, toIdleMerchant;
        Loading_Dialog loadDialog;

        bool isLoadingNewRole = false;
        bool transactionIsAvailable = false;



        public BuyerSellerPage()
        {
            InitializeComponent();
            loadDialog = new Loading_Dialog();
            subscribeUserNotifications();
            Check();
        }
        DisplayDialogWithButton promptUser;


        async void Check(){
            Task timer = Task.Delay(2000);
            await timer;
            if (timer.IsCompleted)
            {
                if (Application.Current.Properties.ContainsKey("InATransaction"))
                {
                    ActionCableClient clients = Constants.accUsersChannel;
                    clients.MessageReceived += AccUsersChannel_MessageReceived;

                    JObject buyData = new JObject();
                    buyData.Add("auth_token", Constants.currentUser.token);
                    buyData.Add("merchant_status", Application.Current.Properties["MERCHANT_STATUS"].ToString());
                    buyData.Add("action", "check_transaction_status");

                    clients.Perform(buyData);
                }


                Debug.WriteLine("===============================================================");

                if (Application.Current.Properties.ContainsKey("BuyerBankAccount"))
                {
                    Debug.WriteLine("Buyer Account: " + Application.Current.Properties["BuyerBankAccount"]);
                }
                if (Application.Current.Properties.ContainsKey("BuyerQuantity"))
                {
                    Debug.WriteLine("Quantity: " + Application.Current.Properties["BuyerQuantity"]);
                }
                if (Application.Current.Properties.ContainsKey("SellerBankAccount"))
                {
                    Debug.WriteLine("Seller Account: " + Application.Current.Properties["SellerBankAccount"]);
                }


                if (Application.Current.Properties.ContainsKey("selectedSeller"))
                {
                    Debug.WriteLine("Selected Seller: " + Application.Current.Properties["selectedSeller"]);
                }



                if (Application.Current.Properties.ContainsKey("currentBuyer"))
                {
                    Debug.WriteLine("current buyer: " + Application.Current.Properties["currentBuyer"]);
                }

                if (Application.Current.Properties.ContainsKey("currentBuyerPosition"))
                {
                    Debug.WriteLine("current buyer: " + Application.Current.Properties["currentBuyerPosition"]);
                }

                Debug.WriteLine("===============================================================");

            }
        }

        private void AccUsersChannel_MessageReceived(object sender, ActionCableEventArgs e)
        {
            Debug.WriteLine(e.Message);
            var b = JsonConvert.DeserializeObject<JObject>(e.Message);

            JObject obj = JsonConvert.DeserializeObject<JObject>(e.Message);
            String actionType = obj["data"]["action_type"].Value<string>();

            switch (actionType)
            {
                case "disconnectRespondCancelTransaction":
                    Application.Current.Properties.Remove("InATransaction");
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (!dialogIsShowed)
                        {
                            dialogIsShowed = true;
                            DisplayDialog d = new DisplayDialog();
                            d.Text = obj["data"]["receiver"]["message"].ToString();
                            d.open();
                            //if(promptUser.IsOpen){
                            //    promptUser.close();
                            //}
                        }
                    });
                    break;

                case "userAlreadyCancelled":
                    Application.Current.Properties.Remove("InATransaction");
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (!dialogIsShowed)
                        {
                            dialogIsShowed = true;
                            DisplayDialog d = new DisplayDialog();
                            d.Text = obj["data"]["message"].ToString();
                            d.open();
                        }
                    });

                    break;

                case "transactionStillAvailable":
                    var transaction_id = obj["data"]["transaction_id"].ToString();

                    if (Application.Current.Properties.ContainsKey("TransactionId"))
                    {
                        Application.Current.Properties["TransactionId"] = transaction_id;
                    }
                    else
                    {
                        Application.Current.Properties.Add("TransactionId", transaction_id);
                    }



                    if (Application.Current.Properties.ContainsKey("InATransaction"))
                    {
                        objects = obj;
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Constants.IsInTransaction = true;

                            promptUser = new DisplayDialogWithButton(ContinueTransaction, CancelTransaction);
                            promptUser.Text = "The last transaction was not completed.\nWould you like to continue transaction?";
                            promptUser.open();
                            //var choice = await DisplayActionSheet("Do you wish to continue the transaction?", null, null, "Continue Transaction", "Cancel Transaction");

                            //if (choice == "Continue Transaction")
                            //{
                            //    Constants.ResumeTransaction = true;
                            //    Constants.currentPosition = new Xamarin.Forms.Maps.Position(obj["data"]["latitude"].Value<float>(), obj["data"]["longitude"].Value<float>());

                            //    if (Application.Current.Properties["MERCHANT_STATUS"].ToString() == "Buyer")
                            //    {
                            //        Buy_Tapped(null, null);
                            //    }
                            //    else
                            //    {
                            //        Sell_Tapped(null, null);
                            //    }
                            //}
                            //else
                            //{
                            //    Constants.IsInTransaction = false;
                            //    Application.Current.Properties.Remove("InATransaction");

                            //    ActionCableClient acc = Constants.accUsersChannel;
                            //    acc.MessageReceived += AccUsersChannel_MessageReceived;

                            //    JObject cancelData = new JObject();
                            //    cancelData.Add("auth_token", Constants.currentUser.token);
                            //    cancelData.Add("merchant_status", Application.Current.Properties["MERCHANT_STATUS"].ToString());
                            //    cancelData.Add("action", "disconnected_respond");

                            //    acc.Perform(cancelData);

                            //}
                        });
                    }
                    break;
            }

            //if (actionType.Equals("disconnectRespondCancelTransaction"))
            //{
            //}
            //if (actionType.Equals("userAlreadyCancelled"))
            //{
            //}
            //if (actionType.Equals("transactionStillAvailable"))
            //{
            //}
            Application.Current.SavePropertiesAsync();
        }

        JObject objects;

        void ContinueTransaction()
        {
            Constants.ResumeTransaction = true;
            Constants.currentPosition = new Xamarin.Forms.Maps.Position(objects["data"]["latitude"].Value<float>(), objects["data"]["longitude"].Value<float>());

            if (Application.Current.Properties["MERCHANT_STATUS"].ToString() == "Buyer")
            {
                Buy_Tapped(null, null);
            }
            else
            {
                Sell_Tapped(null, null);
            }
        }

        void CancelTransaction(){
            Constants.IsInTransaction = false;
            Application.Current.Properties.Remove("InATransaction");

            ActionCableClient acc = Constants.accUsersChannel;
            acc.MessageReceived += AccUsersChannel_MessageReceived;

            JObject cancelData = new JObject();
            cancelData.Add("auth_token", Constants.currentUser.token);
            cancelData.Add("merchant_status", Application.Current.Properties["MERCHANT_STATUS"].ToString());
            cancelData.Add("action", "disconnected_respond");

            acc.Perform(cancelData);
        }
        //void startTimer(){

        //    remainingSeconds = totalSeconds;
        //    Device.StartTimer(TimeSpan.FromSeconds(1), () => {
        //        remainingSeconds--;
        //        TimeSpan t = TimeSpan.FromSeconds(remainingSeconds);
        //        timerLabel.Text = (t.Minutes < 10 ? "0" + t.Minutes : "" + t.Minutes) + ":" + (t.Seconds < 10 ? "0" + t.Seconds : "" + t.Seconds);

        //        Log.e("Timer","Remaining time: "+remainingSeconds+" sec.");
        //        if (remainingSeconds <= 120 && remainingSeconds > 60)
        //        {
        //            sslTimerContainer.BackgroundColor = Color.Orange;
        //            lblTimerPrompt.TextColor = Color.White;
        //            timerLabel.TextColor = Color.White;
        //            remainingSeconds -= 40;
        //        }
        //        else if (remainingSeconds <= 60)
        //        {
        //            sslTimerContainer.BackgroundColor = Color.Red;
        //            lblTimerPrompt.TextColor = Color.White;
        //            timerLabel.TextColor = Color.White;
        //        }
        //        else
        //        {
        //            sslTimerContainer.BackgroundColor = Color.Transparent;
        //            lblTimerPrompt.TextColor = Color.Gray;
        //            timerLabel.TextColor = Color.Gray;
        //        }

        //        if (remainingSeconds == 0)
        //        {

        //            remainingSeconds = totalSeconds;
        //            //start logout here

        //            //if(!Constants.shouldStopIdleTimer){
        //            //    DisplayAlert("Timeout", "You have been idle for a while, you have been logged out to protect your information", "Okay");
        //            //    Constants.Logout();
        //            //}
        //            return false;
        //        }
        //        else
        //        {
        //            if (Constants.shouldStopIdleTimer)
        //                return false;
        //            else
        //                return true;
        //        }
        //    });


        //}
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            alreadytapped = false;
            //if (!Constants.shouldStopIdleTimer)
            //{
            //    //startTimer();
            //    return;
            //}
            //else{
            //    Constants.shouldStopIdleTimer = false;
            //    //startTimer();
            //}
            Debug.WriteLine("IsMinimized: " + Constants.isMinimized);

            IdleTimerManager.idleTimerService = this;
            IdleTimerManager.idleTimerService.startIdleTimer();

            //var locator = CrossGeolocator.Current;
            //locator.DesiredAccuracy = 100;
            //position = new Position(0, 0);
            //var pos = locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);
            //await pos;
            //if(pos.IsCompleted){
            //    position = pos.Result;
            //}
            position = new Position(0, 0);
        }

        Position position;

        void Buy_Tapped(object sender, System.EventArgs e)
        {
            //CancellableLoadingDialog a = new CancellableLoadingDialog();
            //a.Text = "asd";
            //a.open();

            //DisplayDialogWithButton a = new DisplayDialogWithButton(ContinueTransaction);
            //a.Text = "The last transaction was not completed.\nWould you like to continue transaction?";
            //a.open();


            //Navigation.PushPopupAsync(new SelectLocationType(), false);



            if (!alreadytapped)
            {
                if (isLoadingNewRole)
                    return;

                Constants.shouldStopIdleTimer = true;
                isLoadingNewRole = true;

                loadDialog.Text = "Becoming buyer...";
                loadDialog.open();
                //Navigation.PushPopupAsync(loadDialog, true);
                toBuyerMerchant = new ChangeUserRoleAPI(Constants.currentUser.token, Constants.MERCHANT_STATUS.BUYER);
                toBuyerMerchant.setCallbacks(this);
                toBuyerMerchant.getResponse();
                alreadytapped = true;
            }
        }
        private bool alreadytapped = false;
		void Sell_Tapped(object sender, System.EventArgs e)
        {
            if (!alreadytapped)
            {
                if (isLoadingNewRole)
                    return;
                Constants.shouldStopIdleTimer = true;
                isLoadingNewRole = true;
                loadDialog.Text = "Becoming seller...";
                loadDialog.open();
                //Navigation.PushPopupAsync(loadDialog, true);
                toSellerMerchant = new ChangeUserRoleAPI(Constants.currentUser.token, Constants.MERCHANT_STATUS.SELLER);
                toSellerMerchant.setCallbacks(this);
                toSellerMerchant.getResponse();
                alreadytapped = true;
            }
		}

  //      void SetUserToIdle(){
		//	 toIdleMerchant = new ChangeUserRoleAPI(Constants.currentUser.token, Constants.MERCHANT_STATUS.IDLE);
		//	toIdleMerchant.setCallbacks(this);
		//	toIdleMerchant.getResponse();

		//}

        void automateBuyer(){
            //if (!Constants.AutomateValidActions)
                //return;

            //Handle_Tapped(null,null);

        }


        void subscribeUserNotifications(){


			//return;
			//client = new ActionCableClient(Constants.WEBSOCKET_URL+"?auth_token=" + Constants.currentUser.token, "AppNotificationsChannel");
			//client.IgnorePings = true;
			client = new ActionCableClient(Constants.WEBSOCKET_URL + "?auth_token=" + Constants.currentUser.token, "UsersChannel");
			client.IgnorePings = true;

            //JObject subscribeParams = new JObject();
            //subscribeParams.Add("user_id",Constants.currentUser.id);
            JObject extra = new JObject();
            extra.Add("user_id",Constants.currentUser.id);
            client.identifierExtras = extra;

            client.ConnectAsync();

			client.Connected += _acClient_Connected;

            Constants.accUsersChannel = client;


			return;

			//end of subscription
        }



		void _acClient_Connected(object sender, ActionCableEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("Connected222!");
			//editorSockets.Text = editorSockets.Text +"\nConnected!!";


			client.Connected -= _acClient_Connected;
			//client.MessageReceived += _acClient_MessageReceived;
		}

		void _acClient_MessageReceived(object sender, ActionCableEventArgs e)
		{
            String msg = e.Message;
			System.Diagnostics.Debug.WriteLine("Received: {0}", e.Message);
            //DisplayAlert("Sockets",""+e.Message, "Yey!");

            JObject obj = JsonConvert.DeserializeObject<JObject>(msg);

            String pisti = obj["data"].Value<JObject>()["message"].Value<string>();

            Device.BeginInvokeOnMainThread(()=>{
                DisplayAlert("Message", pisti, "Okay");
            });



			//editorSockets.Text = editorSockets.Text + "\n" + e.Message;
		}


		//void Connection_OnMessage(string strResponse)
		//{
		//	Log.e("sada.", "test: " + strResponse);
		//	return;

		//	JObject jobj = JObject.Parse(strResponse);

		//	if (jobj["type"].Value<String>().Trim().Equals("confirm_subscription"))
		//	{


		//		Device.StartTimer(TimeSpan.FromSeconds(3), () =>
		//		{


		//			//create message command
		//			JObject msg = new JObject();
		//			msg.Add("command", "message");

		//			//create message identifier
		//			JObject msgIdentifier = new JObject();
		//			msgIdentifier.Add("channel", "TransactionsChannel");
		//			msgIdentifier.Add("transaction_id", 1);


		//			msg.Add("identifier", msgIdentifier.ToString());//ws identifier needs the identifier value stringified

		//			//create the actual message data

		//			JObject data = new JObject();
		//			data.Add("message", "mao ni akong message " + counter);
		//			data.Add("test_key", "valueTest");
		//			msg.Add("data", data);


		//			Log.e("message params", msg.ToString());

		//			connection.Send(msg.ToString());
		//			counter++;
		//			return false;
		//		});
		//	}

		//}

		void Connection_OnOpened()
		{

			Log.e("sada.", "OPENED!");
		}

        public void OnSuccess(JObject response, BaseAPI caller)
        {

            loadDialog.close();
            isLoadingNewRole = false;

            if(caller == toBuyerMerchant){
                Navigation.PushModalAsync(new BuyerHome(position.Latitude, position.Longitude));
			}
            else if(caller == toSellerMerchant){
                Navigation.PushModalAsync(new SellerHome(position.Latitude, position.Longitude));
            }

        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            loadDialog.close();
            isLoadingNewRole = false;
            //startTimer();
            alreadytapped = false;
            Constants.shouldStopIdleTimer = false;

            JObject errorResponse = JObject.Parse(errMsg);
            if(errorResponse!=null){
                if(errorResponse["success"].HasValues){
                    if(errorResponse["transactions"].HasValues){
                        JArray arr = errorResponse["transactions"].Value<JArray>();

                        JObject firstTrans = arr[0].Value<JObject>();

                        if(arr.Count>0){
                            Navigation.PushPopupAsync(new RateBuyer(firstTrans["rate_user"]["id"].Value<string>(),
                                                                    firstTrans["rate_user"]["image"].Value<string>(),
                                                                    firstTrans["rate_user"]["name"].Value<string>(),
                                                                    firstTrans["transaction_id"].Value<string>())
                            {
                                RatedRole = "Buyer"
                            });
                            return;
                        }
                    }
                }
            }

            DisplayAlert("Failed",errMsg,"Okay");
            return;
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
		{	
            loadDialog.close();
            isLoadingNewRole = false;
        }



		void TransactionHistoryTapped(object sender, System.EventArgs e)
		{
            var parent = (Application.Current.MainPage as NavigationPage).CurrentPage as SidePanelPage;
            MenuPage menu = parent.Master as MenuPage;

            menu.TransactionsHistoryTapped(sender,e);
            Constants.shouldStopIdleTimer = true;

		}


        void HelpTapped(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new HelpPage());
        }
    }
}
