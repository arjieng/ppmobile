﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PP.Pages.Dialogs;
using PP.tools.API;
using PP.tools.models;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class TransactionsHistory : ContentPage,IBaseAPIInterface,HistoryFilterDialog.FilterCallbacks
    {

        string moreTransactionsURL = null;
        HistoryFilterDialog.TRANSACTION_FILTER currentFilter = HistoryFilterDialog.TRANSACTION_FILTER.ALL;
        ObservableCollection<Transaction> allTransactions;

        private DateTime dateRangeStart, dateRangeEnd;

        public TransactionsHistory()
        {
			NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            fetchTransactions();
        }

      

        void back(object sender, System.EventArgs e)
        {
            Constants.shouldStopIdleTimer = false;
            Navigation.PopModalAsync(true);
		}
        protected override bool OnBackButtonPressed()
        {
           back(null,null);
            return true;
        }

		void FilterTapped(object sender, System.EventArgs e)
		{
            Navigation.PushPopupAsync(new HistoryFilterDialog(currentFilter){callbacks = this}, true);
		}
		void SummaryTapped(object sender, System.EventArgs e)
		{


            if(currentFilter == HistoryFilterDialog.TRANSACTION_FILTER.BUYING){
                //FetchTransactionsSummaryAPI api = new FetchTransactionsSummaryAPI(Constants.currentUser.token, 0);
                //api.setCallbacks(this);
                //api.getResponse();


				Navigation.PushPopupAsync(new BuyingSummaryDialog(), true);
            }
			else if (currentFilter == HistoryFilterDialog.TRANSACTION_FILTER.SELLING)
			{
				//FetchTransactionsSummaryAPI api = new FetchTransactionsSummaryAPI(Constants.currentUser.token, 1);
				//api.setCallbacks(this);
				//api.getResponse();


				Navigation.PushPopupAsync(new SellingSummaryDialog(), true);
			}
		}

		
        void fetchTransactions(){
            FetchTransactionsAPI api = new FetchTransactionsAPI(Constants.currentUser.token);
            api.setCallbacks(this);
            api.getResponse();

        }


        void RefreshList(object sender, System.EventArgs e)
        {
            fetchTransactions();
        }

        void loadMoreTransactions(){

			if (moreTransactionsURL == null)
				return;
			FetchMoreTransactionsAPI api = new FetchMoreTransactionsAPI(moreTransactionsURL);
			api.setCallbacks(this);
			api.getResponse();
            aiLoading.IsVisible = true;
        }

		public void OnError(string errMsg, BaseAPI caller)
		{
			throw new NotImplementedException();
		}

		public void OnErrorCode(int errorCode, BaseAPI caller)
		{
			throw new NotImplementedException();
		}

		public void OnSuccess(JObject response, BaseAPI caller)
		{

            if ((caller as FetchTransactionsAPI) != null)
            {
                aiLoading.IsVisible = false;
                lvTransactions.EndRefresh();

                ObservableCollection<Transaction> transactions = JsonConvert.DeserializeObject<ObservableCollection<Transaction>>(response["transactions"].ToString(), new JsonSerializerSettings()
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                });

                allTransactions = transactions;
                ApplyFilter();

                //lvTransactions.ItemsSource = transactions;

                if (response["has_more"].Value<int>() > 0)
                {
                    moreTransactionsURL = response["url"].Value<string>();
                }
                else moreTransactionsURL = null;

                

            }
			else if ((caller as FetchMoreTransactionsAPI) != null)
			{

                aiLoading.IsVisible = false;
				ObservableCollection<Transaction> transactions = JsonConvert.DeserializeObject<ObservableCollection<Transaction>>(response["transactions"].ToString(), new JsonSerializerSettings()
				{
					MissingMemberHandling = MissingMemberHandling.Ignore,
					NullValueHandling = NullValueHandling.Ignore
				});

                allTransactions = new ObservableCollection<Transaction>(allTransactions.Cast<Transaction>().Concat(transactions));
                ApplyFilter();


                //lvTransactions.ItemsSource = new ObservableCollection<Transaction>(lvTransactions.ItemsSource.Cast<Transaction>().Concat(transactions));

                if (response["has_more"].Value<int>() > 0)
				{
					moreTransactionsURL = response["url"].Value<string>();
				}
				else moreTransactionsURL = null;
            }
            else if((caller as FetchTransactionsSummaryAPI) != null){
                Log.e("ada",response.ToString());
            }
		}

        void ListItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            //throw new NotImplementedException();

            lvTransactions.ItemAppearing -= ListItemAppearing;
            Transaction lastItem = lvTransactions.ItemsSource.Cast<Transaction>().LastOrDefault();

            if(lastItem.Equals(e.Item)){
                //then the last item is shown.
                loadMoreTransactions();
            }

			lvTransactions.ItemAppearing += ListItemAppearing;

        }

        void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            lvTransactions.SelectedItem = null;
        }

        public void didSetFilter(HistoryFilterDialog.TRANSACTION_FILTER filter, HistoryFilterDialog dialog)
        {
            currentFilter = filter;
            ApplyFilter();

            string filterText = "";
			switch (filter)
			{
				case HistoryFilterDialog.TRANSACTION_FILTER.ALL: filterText = "All"; break;
				case HistoryFilterDialog.TRANSACTION_FILTER.BUYING: filterText = "Buy"; break;
				case HistoryFilterDialog.TRANSACTION_FILTER.SELLING: filterText = "Sell"; break;
				case HistoryFilterDialog.TRANSACTION_FILTER.DATE_RANGE: filterText = "Range"; break;
            }
            lblFilterHeader.Text = filterText;
        }

        public void didSetDateRangeFilter(DateTime startDate, DateTime endDate, HistoryFilterDialog dialog)
        {
            //currentFilter = HistoryFilterDialog.TRANSACTION_FILTER.DATE_RANGE;
            dateRangeStart = startDate; dateRangeEnd = endDate;
           //ApplyFilter();

            didSetFilter(HistoryFilterDialog.TRANSACTION_FILTER.DATE_RANGE,dialog);
        }

        public void didCancelFilter(HistoryFilterDialog dialog)
        {
            throw new NotImplementedException();
        }

        void ApplyFilter(){

            ObservableCollection<Transaction> filteredTransactions = new ObservableCollection<Transaction>();

            lvTransactions.ItemsSource = null;


             foreach(Transaction t in allTransactions){
                if (currentFilter == HistoryFilterDialog.TRANSACTION_FILTER.SELLING){
					if (t.User.role.ToLower().Equals("buyer"))
					{

						filteredTransactions.Add(t);
					} 
                }
				else if (currentFilter == HistoryFilterDialog.TRANSACTION_FILTER.BUYING)
				{

					if (t.User.role.ToLower().Equals("seller"))
						filteredTransactions.Add(t);
				}
				else if (currentFilter == HistoryFilterDialog.TRANSACTION_FILTER.DATE_RANGE)
				{
                    Log.e("STARTED DATE: ", t.StartedAt);
                    if (t.StartedAt != null)
                    {
                        DateTime transactionDate = DateTime.ParseExact(t.StartedAt, "MM/dd/yyyy", null);

                        if (transactionDate >= dateRangeStart && transactionDate <= dateRangeEnd)
                        {
                            filteredTransactions.Add(t);
                        }
                    }
				}
                else 
                    filteredTransactions.Add(t);
            }


            if (filteredTransactions.Count == 0)
                loadMoreTransactions();
            else
             lvTransactions.ItemsSource = filteredTransactions;

        }
		

        void LoadMore(object sender, System.EventArgs e)
        {
            loadMoreTransactions();
        }
    }
}
