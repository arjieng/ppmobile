﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.LocalNotifications;
using PP.CustomLayouts;
using PP.Helpers;
using PP.Helpers.DrawerHelper;
using PP.Pages.Dialogs;
using PP.Pages.Dialogs.Dynamic;
using PP.tools;
using PP.tools.API;
using PP.tools.models;
using PP.tools.Sockets;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using static PP.tools.RouteFinder;

using static PP.Pages.Dialogs.EnterAmountPopUpPage;

namespace PP.Pages
{
    public partial class BuyerHome : ContentPage, IBaseAPIInterface, IRouteFinderCallback, IEnterAmount
    {


        #region Seller Details Region
        public ObservableCollection<BankAccount> accounts;
        public ObservableCollection<NearbySeller> sellers;
        public static bool dialogIsShowed = false;
        private static NearbySeller selectedSeller;
        private Product selectedSellerProduct;
        public ObservableCollection<Feedback> selectedSellerFeedbacks;
        private int currentFeedbackIndex = 0;
        private bool IsMonitoringMapChanges = false;

        private Plugin.Geolocator.Abstractions.Position currentLocation;


        //route finders
        //RouteFinder userToMeetupFinder;
        //RouteFinder sellerToMeetupFinder;


        PointToPointRouteFinder userToMeetupFinder;
        PointToPointRouteFinder sellerToMeetupFinder;


        Loading_Dialog loadDialog;

        DisplayDialog d;

        static String currentTransactionId;

        static string seller_id;

        Xamarin.Forms.Maps.Position currentMeetupPosition;

        Pin currentMeetupPin;

        /// <summary>
        /// Boolean flag to check if routefinder is for 
        /// setting new meetup or if for the received meetup from
        /// the other party
        /// </summary>
        bool IsForSetNewMeetup = false, IsNowMeeting = false;

        DisplayDialog noSellersDialog;

        static ActionCableClient usersClient;
        BankAccount selectedBankAccount;
        public ObservableCollection<string> paymentOptions = new ObservableCollection<string>();



        bool isInitiallyLoaded = false;
        #endregion



        async void back(object sender, System.EventArgs e)
        {

            if (!string.IsNullOrEmpty(currentTransactionId))
            {
                var leave = await LeaveQuestion();

                if (leave)
                {
                    Constants.IsInTransaction = false;
                    ActionCableClient acc = Constants.accTransactionsChannel;

                    JObject cancelData = new JObject();
                    cancelData.Add("auth_token", Constants.currentUser.token);
                    cancelData.Add("user_transaction_id", currentTransactionId);
                    cancelData.Add("action", "cancel_transaction");

                    acc.Perform(cancelData);
                    QuitPage();
                }
            }
            else
            {
                //Constants.IsInTransaction = false;
                //ActionCableClient acc = Constants.accTransactionsChannel;

                //JObject cancelData = new JObject();
                //cancelData.Add("auth_token", Constants.currentUser.token);
                //cancelData.Add("user_transaction_id", currentTransactionId);
                //cancelData.Add("action", "cancel_transaction");

                //acc.Perform(cancelData);
                QuitPage();
            }



        }

        void QuitPage()
        {
            resetBuyerProcess();
            Constants.shouldStopIdleTimer = false;
            //change role back to idle
            ChangeUserRoleAPI api = new ChangeUserRoleAPI(Constants.currentUser.token, Constants.MERCHANT_STATUS.IDLE);
            api.setCallbacks(this);
            api.getResponse();
            Constants.IsInTransaction = false;

            loadDialog.Text = "Cleaning up..";
            loadDialog.open();

        }

        async Task<bool> LeaveQuestion()
        {
            return await DisplayAlert("Close", "Transaction will be cancelled. Continue?", "Cancel transaction", "Stay");
        }

        void SFRangeSlider_ValueChaging(object sender, Syncfusion.SfRangeSlider.XForms.ValueEventArgs e)
        {
            if (e.Value < 10)
                sliderLbl.Text = "0" + Math.Round(e.Value, 0) + " miles";
            else
                sliderLbl.Text = Math.Round(e.Value, 0) + " miles";
        }
        void AddPaymentMethodPickerOptions()
        {
            paymentOptions.Add("Once");
            paymentOptions.Add("Weekly");
            paymentOptions.Add("Biweekly");
            paymentOptions.Add("Monthly");
            paymentOptions.Add("Quarterly");
            paymentOptions.Add("Semi-Annually");
            paymentOptions.Add("Annually");

            pickerPaymentOptions.ItemsSource = paymentOptions;


        }


        public BuyerHome(double lat, double lng)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            noSellersDialog = new DisplayDialog();
            loadDialog = new Loading_Dialog();

            AddPaymentMethodPickerOptions();

            var tapS = new TapGestureRecognizer();
            tapS.Tapped += (sender, e) =>
            {
                if (sliderStack.IsVisible)
                {
                    sliderStack.IsVisible = false;
                    dropDown.Rotation = 0;
                }
                else
                {
                    dropDown.Rotation = 180;
                    sliderStack.IsVisible = true;
                }
            };
            dropDown.GestureRecognizers.Add(tapS);

            usersClient = Constants.accUsersChannel;
            usersClient.MessageReceived += AccUsersChannel_MessageReceived;
            Device.BeginInvokeOnMainThread(() => {
                map.MoveCamera(CameraUpdateFactory.NewPosition(new Xamarin.Forms.GoogleMaps.Position(lat, lng)));
            });

        }

        protected override void OnAppearing()
        {


            base.OnAppearing();

            Debug.WriteLine("IsMinimized?: " + Constants.isMinimized);
            Debug.WriteLine("=======================================");



            Constants.IsInBuyer = true;
            if (isInitiallyLoaded)
                return;

            //setup map
            map.UiSettings.MyLocationButtonEnabled = false;
            map.SelectedPinChanged += SellerPinChanged;

            Pin a = new Pin()
            {
                Icon = BitmapDescriptorFactory.DefaultMarker(Color.Gray),
                Type = PinType.Place,
                Label = "Second pin",
                Position = new Xamarin.Forms.GoogleMaps.Position(35.71d, 139.815d),
                ZIndex = 5
            };
            map.Pins.Add(a);

            startLocationUpdates();
            startNearbySellersUpdates();
            fetchBankAccounts();
            isInitiallyLoaded = true;

        }
        #region Finding Seller Region

        async Task startLocationUpdates()
        {
            var geolocator = CrossGeolocator.Current;

            if (geolocator.IsListening)
                return;

            geolocator.DesiredAccuracy = 1;

            await geolocator.StartListeningAsync(TimeSpan.FromSeconds(3), 1, true, new ListenerSettings()
            {
                ActivityType = ActivityType.AutomotiveNavigation,
                AllowBackgroundUpdates = true,
                DeferLocationUpdates = true,
                DeferralDistanceMeters = 2,
                DeferralTime = TimeSpan.FromSeconds(3),
                ListenForSignificantChanges = true,
                PauseLocationUpdatesAutomatically = false

            });

            var p = await geolocator.GetLastKnownLocationAsync();
            if (p != null)
                LocationChanged(null, new PositionEventArgs(p));
            geolocator.PositionChanged += LocationChanged;


            ////get current location for initial display
            //var x = geolocator.
        }
        void startNearbySellersUpdates()
        {

            Device.StartTimer(TimeSpan.FromSeconds(5), () =>
            {
                //if ( !IsNowMeeting && !IsForSetNewMeetup)
                //{
                //    if (currentLocation != null)
                //        fetchNearbySellers(currentLocation);
                //    return true;
                //}
                if (IsNowMeeting || IsForSetNewMeetup)
                    return false;
                else
                {
                    if (currentLocation != null)
                        fetchNearbySellers(currentLocation);
                    return true;
                }

            });

        }

        void UpdateMyLocation(Plugin.Geolocator.Abstractions.Position p)
        {
            UpdateUserLocationAPI api = new UpdateUserLocationAPI(Constants.currentUser.token, p.Latitude, p.Longitude);
            api.setCallbacks(this);
            api.getResponse();
        }

        /// <summary>
        /// Gets called when there are changes on device coordinates/location
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void LocationChanged(object sender, PositionEventArgs e)
        {
            if (currentLocation == null)
            {
                map.MoveCamera(CameraUpdateFactory.NewPosition(new Xamarin.Forms.GoogleMaps.Position(e.Position.Latitude, e.Position.Longitude)));
            }

            currentLocation = e.Position;

            if (currentTransactionId != null)
            {

            }


            UpdateMyLocation(e.Position);


        }
        protected override bool OnBackButtonPressed()
        {
            stopLocationUpdates();
            IsMonitoringMapChanges = false;
            //BuyButtonTapped(null,null);
            back(null, null);
            //return base.OnBackButtonPressed();
            return true;

        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Constants.IsInBuyer = false;
            stopLocationUpdates();
        }

        /// <summary>
        /// Stops location updates.
        /// </summary>
        async void stopLocationUpdates()
        {
            var geolocator = CrossGeolocator.Current;

            await geolocator.StopListeningAsync();
            geolocator.PositionChanged -= LocationChanged;
        }

        void PaymentOptionChanged(object sender, System.EventArgs e)
        {
            if (!(pickerPaymentOptions.SelectedItem as string).Equals(paymentOptions[0]))
            {
                pickerPaymentOptions.SelectedIndex = 0;
                DisplayAlert("Payment Option", "Sorry, we only support one time payment for now.", "Okay");
            }
        }

        /// <summary>

        /// Handles the radio buttons clicked on buyer's payment plans
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        //    void Handle_RadioClicked(object sender, System.EventArgs e)
        //    {

        //        var x = sender as XFRadio;

        //        if(x == radioOnce){
        //radioOnce.IsSelected = false;
        //radioWeek.IsSelected = false;
        //radioMonth.IsSelected = false;

        //x.IsSelected = x.IsSelected ? x.IsSelected : !x.IsSelected;
        //    }

        //}
        /// <summary>
        /// API: Fetches the nearby sellers.
        /// </summary>
        /// <param name="p">P.</param>
        void fetchNearbySellers(Plugin.Geolocator.Abstractions.Position p)
        {
            if (Constants.currentUser == null || p == null)
                return;


            FetchNearbySellersAPI api = new FetchNearbySellersAPI(Constants.currentUser.token, p.Latitude, p.Longitude, sfRangeSlider2.Value);
            api.setCallbacks(this);
            api.getResponse();
        }
        /// <summary>
        /// API: Fetches the bank accounts.
        /// </summary>
        void fetchBankAccounts()
        {
            FetchBankAccountsAPI api = new FetchBankAccountsAPI(Constants.currentUser.token);
            api.setCallbacks(this);
            api.getResponse();
        }

        void ResumeTransaction()
        {
            if (Constants.ResumeTransaction)
            {
                ActionCableClient cl = Constants.accUsersChannel;
                cl.MessageReceived += AccUsersChannel_MessageReceived;



                Constants.IsInTransaction = true;
                Constants.TransactionInProgress = true;
                var acs = accounts.Where(x => x.ID == Application.Current.Properties["BuyerBankAccount"].ToString());
                pickerAccounts.SelectedIndex = accounts.IndexOf(acs.FirstOrDefault());

                entryQuantity.Text = Application.Current.Properties["BuyerQuantity"].ToString();
                pickerPaymentOptions.SelectedIndex = 0;

                sslFindSellerHeader.IsVisible = false;
                currentTransactionId = Application.Current.Properties["TransactionId"].ToString();
                currentMeetupPosition = Constants.currentPosition;

                selectedSeller = JsonConvert.DeserializeObject<NearbySeller>(Application.Current.Properties["selectedSeller"].ToString());

                Pin pin = new Pin()
                {
                    Type = PinType.SavedPin,
                    Position = new Xamarin.Forms.GoogleMaps.Position(selectedSeller.Latitude, selectedSeller.Longitude),
                    Label = selectedSeller.ProductName,
                    Icon = BitmapDescriptorFactory.FromBundle("UserLocation.png")
                };
                if (!map.Pins.Contains(pin))
                    map.Pins.Add(pin);


                PointToPointRouteFinder rfMeetupSetupFromUser = new PointToPointRouteFinder(currentLocation.Latitude, currentLocation.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                rfMeetupSetupFromUser.TravelTimeDisplay = lblBuyerETA;
                rfMeetupSetupFromUser.RouteColor = Color.DimGray;
                rfMeetupSetupFromUser.StartRouteFinderUpdates(map);

                PointToPointRouteFinder rfMeetupSetupFromSeller = new PointToPointRouteFinder(selectedSeller.Latitude, selectedSeller.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                rfMeetupSetupFromSeller.TravelTimeDisplay = lblSellerETA;
                rfMeetupSetupFromSeller.RouteColor = Color.Brown;
                rfMeetupSetupFromSeller.StartRouteFinderUpdates(map);



                Constants.SubscribeToTransactions(currentTransactionId);
                ActionCableClient transactionClient = Constants.accTransactionsChannel;
                transactionClient.MessageReceived += AccTransactionsChannel_MessageReceived;
                sslMeetingEstimates.IsVisible = true;
                hideMeetupSetup();
                hideReceivedMeetUp();

                updateMeetupEstimates();
                GetMeetupAddress();

                updateCurrentMeetupPin();
                stopLocationUpdates();
                map.SelectedPinChanged -= SellerPinChanged;
                map.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.GoogleMaps.Position(currentLocation.Latitude, currentLocation.Longitude), Distance.FromMiles(0.4)));

            }
        }


        /// <summary>
        /// API calls this method for successful api calls
        /// </summary>
        /// <param name="response">Response.</param>
        /// <param name="caller">Caller.</param>
        public void OnSuccess(JObject response, BaseAPI caller)
        {
            if ((caller as FetchNearbySellersAPI) != null)
            {
                Log.e("Nearby", response.ToString());

                if (sellers != null)
                    sellers.Clear();
                sellers = JsonConvert.DeserializeObject<ObservableCollection<NearbySeller>>(response["users"].ToString(), new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });

                Device.BeginInvokeOnMainThread(() =>
                {

                    if (sellers.Count > 0)
                    {
                        lblNearbySellerCount.Text = sellers.Count + " seller(s) nearby";
                        lblNearbySellerCount.TextColor = Color.Green;
                    }
                    else
                    {
                        lblNearbySellerCount.Text = "No sellers nearby...";
                        lblNearbySellerCount.TextColor = Color.Red;
                    }
                });

                try
                {
                    plotSellers();
                }
                catch (Exception c)
                {
                    Log.e("BuyerHome", "Plotting exception: " + c.Message);
                }

            }
            else if ((caller as FetchBankAccountsAPI) != null)
            {

                Log.e("bank accounts", response.ToString());


                accounts = JsonConvert.DeserializeObject<ObservableCollection<BankAccount>>(response["bank_accounts"].ToString(), new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });


                pickerAccounts.ItemsSource = accounts;
                pickerAccounts.SelectedIndex = 0;
                pickerPaymentOptions.SelectedIndex = 0;

                ResumeTransaction();

            }
            else if ((caller as FetchProfileDetailsAPI) != null)
            {

                Log.e("Profile Response", response.ToString());


                User u = JsonConvert.DeserializeObject<User>(response["user"].ToString(), new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });

                Product p = JsonConvert.DeserializeObject<Product>(response["product"].ToString(), new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });
                selectedSellerProduct = p;
                //response = TestData.getTestData("seller_feedbacks.json");

                selectedSellerFeedbacks = JsonConvert.DeserializeObject<ObservableCollection<Feedback>>(response["feedbacks"].ToString());
                setSellerDetails(u, p);
            }
            else if ((caller as UpdateUserLocationAPI) != null)
            {
                Log.e("Locationupdated", response.ToString());
            }
            else if ((caller as ChangeUserRoleAPI) != null)
            {

                //start leaving
                loadDialog.close();
                Navigation.PopModalAsync(true);
                stopLocationUpdates();
            }
        }

        /// <summary>
        /// API calls this method when API connects successfully but receives an error response(JSON)
        /// </summary>
        /// <param name="errMsg">Error message.</param>
        /// <param name="caller">Caller.</param>
        public void OnError(string errMsg, BaseAPI caller)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if ((caller as FetchNearbySellersAPI) != null)
                {
                    lblNearbySellerCount.Text = "Failed getting nearby sellers...";
                    lblNearbySellerCount.TextColor = Color.Red;
                }
                else if ((caller as UpdateUserLocationAPI) != null)
                {

                }
                else
                    DisplayAlert("Failed", errMsg, "Okay");
            });

        }
        /// <summary>
        /// API calls this method when API connection errors (e.g. 404 Not Found)
        /// </summary>
        /// <param name="errorCode">Error code.</param>
        /// <param name="caller">Caller.</param>
        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            Log.e("BuyerHome", "ErrorCode: " + 400 + " from " + caller.getURLTail());
        }

        /// <summary>
        /// Creates markers/pins representing nearby sellers and displays them on the map
        /// </summary>
        void plotSellers()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                map.Pins.Clear();

                //if(sellers.Count()==0){
                //    noSellersDialog.Text = "No nearby sellers..";
                //    noSellersDialog.open();
                //}

                foreach (NearbySeller seller in sellers)
                {

                    Pin pin = new Pin()
                    {
                        Type = PinType.SavedPin,
                        Position = new Xamarin.Forms.GoogleMaps.Position(seller.Latitude, seller.Longitude),
                        Label = seller.ProductName,// + " " + seller.LastName,
                        Icon = BitmapDescriptorFactory.FromBundle("UserLocation.png")
                    };


                    // create circle pin
                    //   CachedImage image = new CachedImage(){ DownsampleToViewSize = true, WidthRequest = 20, HeightRequest = 20, DownsampleUseDipUnits = true, LoadingPlaceholder = "ic_profile_default", ErrorPlaceholder = "ic_profile_default"};
                    //image.Source = Constants.ROOT_URL + seller.Image;

                    //CircleTransformation cTrans = new CircleTransformation(1, "2097ED");
                    //image.Transformations.Add(cTrans);
                    //pin.Icon = BitmapDescriptorFactory.FromView(image);


                    // Put seller pin on the map    
                    if (pin == null)
                        return;

                    if (map != null && !map.Pins.Contains(pin))
                        map.Pins.Add(pin);
                }

            });
        }

        /// <summary>
        /// Gets called when a map marker/pin is selected(not selected)
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void SellerPinChanged(object sender, Xamarin.Forms.GoogleMaps.SelectedPinChangedEventArgs e)
        {
            if (e.SelectedPin == null)
            {

                sslSellerSummary.IsVisible = false;

                return;
            }
            System.Diagnostics.Debug.WriteLine("Pin Changed");
            NearbySeller ns = sellers[map.Pins.IndexOf(e.SelectedPin)];
            ns.LocationPin = e.SelectedPin;
            selectedSeller = ns;

            if (ns == null)
                return;
            sslSellerSummary.IsVisible = true;
            ciSellerImage.Source = Constants.ROOT_URL + ns.Image;
            lblSellerName.Text = ns.FirstName + " " + ns.LastName;

            ciSellerImage2.Source = Constants.ROOT_URL + ns.Image;
            lblSellerName2.Text = ns.FirstName + " " + ns.LastName;

            stkImageStar.Rating = ns.Rating;
            stkImageStar2.Rating = ns.Rating;

            lblSellerDistance.Text = DistanceTo(currentLocation.Latitude, currentLocation.Longitude, e.SelectedPin.Position.Latitude, e.SelectedPin.Position.Longitude).ToString("0.##") + " km";
        }

        public static double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }

            return dist;
        }

        /// <summary>
        /// Opens the seller details dialog
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void ViewSellerTapped(object sender, System.EventArgs e)
        {
            sslSellerDetails.IsVisible = true;

            FetchProfileDetailsAPI api = new FetchProfileDetailsAPI(selectedSeller.Id, Constants.currentUser.token);
            api.setCallbacks(this);
            api.getResponse();
        }

        /// <summary>
        /// Dismisses the seller details dialog
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void CloseUserDetailsTapped(object sender, System.EventArgs e)
        {
            sslSellerDetails.IsVisible = false;
            seller_id = "";
        }

        /// <summary>
        /// Displays the seller information including the product and feedback information
        /// </summary>
        /// <param name="user">User.</param>
        /// <param name="product">Product.</param>
        void setSellerDetails(User user, Product product)
        {
            seller_id = user.id;
            lblProductName.Text = product.ProductName;
            lblQty.Text = product.Quantity;
            lblPrice.Text = "$" + product.SellingPrice;



            ((CorneredView)ciProductPhotoOne.Parent).IsVisible = false;
            ((CorneredView)ciProductPhotoTwo.Parent).IsVisible = false;
            ((CorneredView)ciProductPhotoThree.Parent).IsVisible = false;


            for (int index = 0; index < product.Images.Count; index++)
            {

                ProductImage img = product.Images[index];
                switch (index)
                {
                    case 0:
                        ciProductPhotoOne.Source = Constants.ROOT_URL + img.Url;
                        ((CorneredView)ciProductPhotoOne.Parent).IsVisible = true;
                        break;
                    case 1:
                        ciProductPhotoTwo.Source = Constants.ROOT_URL + img.Url;
                        ((CorneredView)ciProductPhotoTwo.Parent).IsVisible = true;
                        break;
                    case 2:
                        ciProductPhotoThree.Source = Constants.ROOT_URL + img.Url;
                        ((CorneredView)ciProductPhotoThree.Parent).IsVisible = true;
                        break;
                }
            }



            if (selectedSellerFeedbacks.Count > 0)
            {
                currentFeedbackIndex = 0;
                loadCurrentFeedback();

                sslFeedbackDisplay.IsVisible = true;
                lblNoFeedbacks.IsVisible = false;
            }
            else
            {
                sslFeedbackDisplay.IsVisible = false;
                lblNoFeedbacks.IsVisible = true;
            }


        }

        /// <summary>
        /// Moves feedback navigation to previous feedback
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void PreviousFeedbackTapped(object sender, System.EventArgs e)
        {
            if (currentFeedbackIndex == 0)
                currentFeedbackIndex = selectedSellerFeedbacks.Count - 1;
            else currentFeedbackIndex--;


            loadCurrentFeedback();
        }

        /// <summary>
        /// Moves feedback navigation to next feedback
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void NextFeedbackTapped(object sender, System.EventArgs e)
        {
            if (currentFeedbackIndex == selectedSellerFeedbacks.Count - 1)
                currentFeedbackIndex = 0;
            else currentFeedbackIndex++;

            loadCurrentFeedback();
        }

        /// <summary>
        /// Displays the currently selected feedback.
        /// </summary>

        void loadCurrentFeedback()
        {
            if (selectedSellerFeedbacks.Count < 1)
                return
                    ;

            Feedback f = selectedSellerFeedbacks[currentFeedbackIndex];

            Log.e("Feedback", "Loading " + f.RatingUserName);

            lblFeedbackUserName.Text = f.RatingUserName;
            ciFeedbackUserImage.Source = Constants.ROOT_URL + f.RatingUserImage;

            sslFeedbackStars.Rating = f.Rate;

            lblFeebackMessage.Text = f.RatingMessage;
        }
        private StoppableTimer stoppableTimer;
        void BuyButtonTapped(object sender, System.EventArgs e)
        {
            //check if all entries are valid

            int quantity = 0;

            selectedBankAccount = pickerAccounts.SelectedItem as BankAccount;

            if (selectedBankAccount != null)
            {
                if (!string.IsNullOrEmpty(entryQuantity.Text.Trim()))
                {

                    quantity = int.Parse(entryQuantity.Text.Trim());

                    if (quantity > 0)
                    {
                        if (selectedSellerProduct == null)
                            return;


                        int sellerProductQuantity = int.Parse(selectedSellerProduct.Quantity);

                        if (sellerProductQuantity >= quantity)
                        {
                            if (pickerPaymentOptions.SelectedItem == null)
                            {
                                DisplayAlert("Incomplete", "Please select payment option.", "Okay");
                                return;
                            }

                        }
                        else
                        {
                            Navigation.PushPopupAsync(new EnterAmountPopUpPage { callback = this, Text = "You can not purchase more items than the seller have. (Max. " + sellerProductQuantity + ")", Amount = sellerProductQuantity.ToString() }, true);
                            //DisplayAlert("Invalid", "You can not purchase more items than the seller have.", "Okay");
                            return;
                        }

                    }
                    else
                    {
                        Navigation.PushPopupAsync(new EnterAmountPopUpPage { callback = this, Text = "Quantity cannot be lesser than one. (Min. 1)", Amount = "1" }, true);
                        //DisplayAlert("Invalid", "Quantity cannot be lesser than one", "Okay");
                        return;
                    }

                }
                else
                {
                    Navigation.PushPopupAsync(new EnterAmountPopUpPage() { callback = this }, true);
                    //DisplayAlert("Incomplete", "Please enter quantity of items to be purchased", "Okay");
                    return;
                }
            }
            else
            {

                DisplayAlert("Incomplete", "Please select a bank account. If you haven't set it up yet, go to your bank accounts and add a new account there.", "Okay");
                return;
            }

            //check if selected seller is still in the list of nearby sellers

            bool stillSelling = false;
            foreach (NearbySeller ns in sellers)
            {
                if (ns.Id.Equals(selectedSeller.Id))
                {
                    stillSelling = true;
                    break;
                }
            }


            if (!stillSelling)
            {

                if (!dialogIsShowed)
                {
                    dialogIsShowed = true;
                    d = null;
                    d = new DisplayDialog();
                    d.Title = "Seller Not found";
                    d.Text = "Selected Seller cannot be found. Seller might be no longer in range or has stopped selling.";
                    d.open();
                }


                return;
            }



            ActionCableClient client = Constants.accUsersChannel;
            client.MessageReceived += AccUsersChannel_MessageReceived;

            seller_id = selectedSeller.Id;
            JObject buyData = new JObject();
            buyData.Add("auth_token", Constants.currentUser.token);
            buyData.Add("seller_id", selectedSeller.Id);
            buyData.Add("quantity", quantity);
            buyData.Add("bank_account", selectedBankAccount.ID);
            buyData.Add("existing", 1);
            //buyData.Add("total_amount", 3.14);
            buyData.Add("payment_option", 0);
            buyData.Add("number_of_payment", 1);
            buyData.Add("action", "purchase_order");

            client.Perform(buyData);

            if (Application.Current.Properties.ContainsKey("BuyerBankAccount"))
            {
                Application.Current.Properties["BuyerBankAccount"] = selectedBankAccount.ID;
            }
            else
            {
                Application.Current.Properties.Add("BuyerBankAccount", selectedBankAccount.ID);
            }

            if (Application.Current.Properties.ContainsKey("BuyerQuantity"))
            {
                Application.Current.Properties["BuyerQuantity"] = quantity;
            }
            else
            {
                Application.Current.Properties.Add("BuyerQuantity", quantity);
            }

            if (Application.Current.Properties.ContainsKey("selectedSeller"))
            {
                Application.Current.Properties["selectedSeller"] = JsonConvert.SerializeObject(selectedSeller, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            }
            else
            {
                Application.Current.Properties.Add("selectedSeller", JsonConvert.SerializeObject(selectedSeller, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }));
            }

            if (cancellableLoadDialog == null)
                cancellableLoadDialog = new CancellableLoadingDialog(CancelBuyRequest);
            cancellableLoadDialog.Text = "Waiting for seller to respond...";

            //Navigation.PushPopupAsync(loadDialog, true);
            cancellableLoadDialog.open();
            if (stoppableTimer != null)
                stoppableTimer = null;

            stoppableTimer = new StoppableTimer(TimeSpan.FromSeconds(1), StopOnFiveMinutes);
            timeToStop = 300;
            stoppableTimer.Start();

            return;

        }


        CancellableLoadingDialog cancellableLoadDialog;
        void CancelBuyRequest()
        {
            stoppableTimer.Stop();
            ActionCableClient client = Constants.accUsersChannel;
            client.MessageReceived += AccUsersChannel_MessageReceived;

            seller_id = selectedSeller.Id;
            JObject buyData = new JObject();
            buyData.Add("auth_token", Constants.currentUser.token);
            buyData.Add("seller_id", seller_id);
            buyData.Add("type", 1);
            buyData.Add("action", "buy_request_timeout");
            client.Perform(buyData);
        }

        void lblNearbySellerCount_Tapped(object sender, EventArgs e)
        {
            if (map != null && currentLocation != null)
                map.AnimateCamera(CameraUpdateFactory.NewPosition(new Xamarin.Forms.GoogleMaps.Position(currentLocation.Latitude, currentLocation.Longitude)));
            if (currentLocation != null)
                fetchNearbySellers(currentLocation);
        }

        void CancelTransactionRequest(){
            ActionCableClient acc = Constants.accTransactionsChannel;

            JObject cancelData = new JObject();
            cancelData.Add("auth_token", Constants.currentUser.token);
            cancelData.Add("user_transaction_id", currentTransactionId);
            cancelData.Add("action", "cancel_transaction");

            acc.Perform(cancelData);
        }

        int timeToStop;
        private void StopOnFiveMinutes()
        {
            System.Diagnostics.Debug.WriteLine(timeToStop);
            timeToStop--;
            if (timeToStop == 0)
            {
                stoppableTimer.Stop();


                ActionCableClient client = Constants.accUsersChannel;
                client.MessageReceived += AccUsersChannel_MessageReceived;

                seller_id = selectedSeller.Id;
                JObject buyData = new JObject();
                buyData.Add("auth_token", Constants.currentUser.token);
                buyData.Add("seller_id", seller_id);
                buyData.Add("type", 0);
                buyData.Add("action", "buy_request_timeout");
                client.Perform(buyData);



                DisplayAlert("Buy Request Timeout", "The seller took so long to respond to your request", "OK");
                cancellableLoadDialog.close();
            }
        }

        public void SetFindSellersVisiblle(bool visible)
        {

        }

        public void setFindSellersVisiblle(bool visible)
        {
            sslFindSellerHeader.IsVisible = visible;
            map.Pins.Clear();
            if (!visible)
            {
                sslSellerSummary.IsVisible = false;
                sslSellerDetails.IsVisible = false;
            }
        }

        void AccUsersChannel_MessageReceived(object sender, tools.Sockets.ActionCableEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
            JObject obj = JsonConvert.DeserializeObject<JObject>(e.Message);
            String actionType = obj["data"]["action_type"].Value<string>();

            switch (actionType)
            {
                case "disconnectRespondCancelTransaction":

                    Application.Current.Properties.Remove("InATransaction");
                    Constants.IsInTransaction = false;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (!dialogIsShowed)
                        {
                            dialogIsShowed = true;
                            DisplayDialog dd = new DisplayDialog();
                            dd.Text = obj["data"]["receiver"]["message"].ToString();
                            //if(d.IsOpen)
                            dd.open();
                            resetBuyerProcess();
                        }
                    });
                    break;

                case "purchaseOrder":
                    currentOrder = JsonConvert.DeserializeObject<Order>(obj["data"]["order"].ToString(), new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    });

                    Constants.IsInTransaction = true;
                    break;

                case "promptOnDisconnect":
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (!dialogIsShowed)
                        {
                            dialogIsShowed = true;
                            DisplayDialog dialog = new DisplayDialog();
                            dialog.Text = obj["data"]["message"].ToString();
                            dialog.open();
                        }
                        //Constants.IsInTransaction = false;
                    });
                    break;
                case "respondOrder": //Constants.USERS_RESPOND_ORDER
                    bool accepted = obj["data"]["is_accepted"].Value<bool>();
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        stoppableTimer.Stop();
                        cancellableLoadDialog.close();
                        if (accepted)
                        {
                            showMeetupSetup();
                            currentTransactionId = obj["data"]["transaction_id"].Value<string>();
                            Constants.SubscribeToTransactions(currentTransactionId);
                            Constants.accTransactionsChannel.MessageReceived += AccTransactionsChannel_MessageReceived;
                            map.SelectedPinChanged -= SellerPinChanged;
                            IsForSetNewMeetup = true;
                            //map.Pins.Clear();
                            Constants.IsInTransaction = true;
                        }
                        else
                        {
                            Constants.TransactionInProgress = false;
                            if (Application.Current.Properties.ContainsKey("InATransaction"))
                            {
                                Application.Current.Properties.Remove("InATransaction");
                            }
                            Application.Current.SavePropertiesAsync();

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                if (!dialogIsShowed)
                                {
                                    dialogIsShowed = true;
                                    DisplayDialog dialog = new DisplayDialog();
                                    dialog.Text = "Seller declined your request...";
                                    dialog.open();
                                    stoppableTimer.Stop();
                                }
                                Constants.IsInTransaction = false;
                            });
                        }
                    });
                    break;

                case "userLocationUpdate": //Constants.USERS_USER_LOCATION_UPDATE

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Log.e("Location Update " + selectedSeller.Id + " " + (selectedSeller.LocationPin != null), obj.ToString());
                        var setterId = obj["data"]["setter"]["id"];
                        if (Constants.ResumeTransaction)
                        {

                            UpdateLocationOnResume(obj);
                        }
                        else
                        {
                            Constants.IsInTransaction = true;
                            var pin = new Pin()
                            {
                                Type = PinType.SavedPin,
                                Position = new Xamarin.Forms.GoogleMaps.Position(selectedSeller.Latitude, selectedSeller.Longitude),
                                Label = selectedSeller.ProductName,
                                Icon = BitmapDescriptorFactory.FromBundle("UserLocation.png")
                            };

                            if (map.Pins.Contains(pin))
                                map.Pins.Remove(pin);

                            pin.Position = new Xamarin.Forms.GoogleMaps.Position(obj["data"]["setter"]["latitude"].Value<double>(), obj["data"]["setter"]["longitude"].Value<double>());
                            selectedSeller.Latitude = obj["data"]["setter"]["latitude"].Value<double>();
                            selectedSeller.Longitude = obj["data"]["setter"]["longitude"].Value<double>();
                            selectedSeller.LocationPin = pin;

                            if (Application.Current.Properties.ContainsKey("selectedSeller"))
                            {
                                Application.Current.Properties["selectedSeller"] = JsonConvert.SerializeObject(selectedSeller);
                            }
                            else
                            {
                                Application.Current.Properties.Add("selectedSeller", JsonConvert.SerializeObject(selectedSeller));
                            }


                            map.Pins.Add(pin);
                            map.Polylines.Remove(map.Polylines.Where(x => x.StrokeColor == Color.Brown).FirstOrDefault());

                            PointToPointRouteFinder rfMeetupSetupFromSeller = new PointToPointRouteFinder(selectedSeller.Latitude, selectedSeller.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                            rfMeetupSetupFromSeller.TravelTimeDisplay = lblSellerETA;
                            rfMeetupSetupFromSeller.RouteColor = Color.Brown;
                            rfMeetupSetupFromSeller.StartRouteFinderUpdates(map);
                        }

                    });

                    break;
            }
        }

        /// <summary>
        /// Whenever there are socket messages received through
        /// the transactions channel
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void AccTransactionsChannel_MessageReceived(object sender, tools.Sockets.ActionCableEventArgs e)
        {

            System.Diagnostics.Debug.WriteLine(e.Message);
            JObject obj = JsonConvert.DeserializeObject<JObject>(e.Message);
            String actionType = obj["data"]["action_type"].Value<string>();


            switch (actionType)
            {
                case "setMeetup": //TRANSACTIONS_SET_MEETUP
                    map.Polygons.Clear();
                    Log.e("SET MEETUP", obj.ToString());

                    string setterId = obj["data"]["setter"]["id"].Value<String>();
                    string receiverId = obj["data"]["receiver"]["id"].Value<String>();

                    if (setterId.Equals(Constants.currentUser.id))
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            setMeetUpCancellableDialog.Text = obj["data"]["setter"]["message"].Value<string>();
                            //loadDialog.open();
                        });
                    }

                    if (receiverId.Equals(Constants.currentUser.id))
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            setMeetUpCancellableDialog.close();
                            lblHeaderDisplay.Text = obj["data"]["receiver"]["message"].Value<String>();

                            currentMeetupPosition = new Xamarin.Forms.Maps.Position(
                                obj["data"]["meet_up"]["meeting_location"]["latitude"].Value<double>(),
                                obj["data"]["meet_up"]["meeting_location"]["longitude"].Value<double>()
                            );
                            showReceivedMeetup(obj["data"]["meet_up"]["address"].Value<String>());
                        });
                    }

                    Constants.IsInTransaction = true;
                    break;

                case "declineMeetup": //TRANSACTIONS_DECLINE_MEETUP
                    Constants.TransactionInProgress = false;

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        loadDialog.Text = obj["data"]["receiver"]["message"].Value<String>();

                    });

                    Constants.IsInTransaction = false;
                    break;

                case "acceptMeetup": //TRANSACTIONS_ACCEPT_MEETUP
                    var lat = obj["data"]["meet_up"]["meeting_location"]["latitude"].Value<double>();
                    var lng = obj["data"]["meet_up"]["meeting_location"]["longitude"].Value<double>();
                    currentMeetupPosition = new Xamarin.Forms.Maps.Position(lat, lng);
                    Constants.TransactionInProgress = true;
                    if (Application.Current.Properties.ContainsKey("InATransaction"))
                    {
                        Application.Current.Properties["InATransaction"] = true;
                    }
                    else
                    {
                        Application.Current.Properties.Add("InATransaction", true);
                    }

                    if (Application.Current.Properties.ContainsKey("MERCHANT_STATUS"))
                    {
                        Application.Current.Properties["MERCHANT_STATUS"] = "Buyer";
                    }
                    else
                    {
                        Application.Current.Properties.Add("MERCHANT_STATUS", "Buyer");
                    }




                    Application.Current.SavePropertiesAsync();
                    Constants.IsInTransaction = true;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        setMeetUpCancellableDialog.close();
                        hideMeetupSetup();
                        hideReceivedMeetUp();

                        updateMeetupEstimates();
                        GetMeetupAddress();

                        updateCurrentMeetupPin();
                        stopLocationUpdates();
                        StartMeetingUpLocationUpdates();
                    });
                    break;

                case "cancelTransaction": //TRANSACTIONS_CANCEL_TRANSACTION
                    Constants.TransactionInProgress = false;
                    if (Application.Current.Properties.ContainsKey("InATransaction"))
                    {
                        Application.Current.Properties["InATransaction"] = false;
                        Application.Current.Properties.Remove("InATransaction");
                    }
                    Application.Current.SavePropertiesAsync();

                    String msg = "";

                    d = null;
                    d = new DisplayDialog();

                    if (obj["data"]["setter"]["id"].Value<string>().Equals(Constants.currentUser.id))
                        msg = obj["data"]["setter"]["message"].Value<string>();
                    else if (obj["data"]["receiver"]["id"].Value<string>().Equals(Constants.currentUser.id))
                        msg = obj["data"]["receiver"]["message"].Value<string>();
                    d.Text = msg;

                    Constants.IsInTransaction = false;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Debug.WriteLine(Constants.isMinimized + " =====================================");
                        if (Constants.isMinimized)
                        {
                            CrossLocalNotifications.Current.Show("Transaction Cancelled", msg);
                        }
                        else
                        {
                            if(setMeetUpCancellableDialog != null){
                                setMeetUpCancellableDialog.close();
                            }

                            if (!dialogIsShowed)
                            {
                                dialogIsShowed = true;
                                d.open();
                            }
                        }
                        resetBuyerProcess();

                    });
                    break;

                case "completeTransaction": //Constants.TRANSACTIONS_COMPLETE
                    Constants.TransactionInProgress = false;
                    if (Application.Current.Properties.ContainsKey("InATransaction"))
                    {
                        Application.Current.Properties["InATransaction"] = false;
                        Application.Current.Properties.Remove("InATransaction");
                    }
                    Application.Current.SavePropertiesAsync();

                    Navigation.PushPopupAsync(new RateBuyer(selectedSeller.Id,
                                                            selectedSeller.Image,
                                                            selectedSeller.FirstName + " " + selectedSeller.LastName,
                                                            currentTransactionId)
                    {
                        RatedRole = "Seller"
                    });

                    Constants.IsInTransaction = false;
                    resetBuyerProcess();
                    break;

                case "sendMessage":
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        chatIsTapped = false;
                        Device.StartTimer(TimeSpan.FromSeconds(1), () => {
                            //return true;
                            chatIsTapped = true;
                            chatIsTapped = false;
                            if (cvNewMessageIndicator.IsVisible)
                            {
                                cvNewMessageIndicator.IsVisible = false;
                            }
                            else
                            {
                                cvNewMessageIndicator.IsVisible = true;
                            }


                            if (chatIsTapped)
                                return false;
                            else
                                return true;
                        });


                    });

                    break;
            }


        }

        bool chatIsTapped = false;

        void NavigateMap_Tapped(object sender, EventArgs e)
        {
            DependencyService.Get<iDrawer>().OpenDrawer(currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
        }

        void resetBuyerProcess()
        {

            cvHeaderDisplay.IsVisible = false;


            Constants.IsInTransaction = false;

            currentTransactionId = null;
            currentLocation = null;
            stopLocationUpdates();
            startLocationUpdates();

            hideMeetupSetup();
            showBuySetup();
            hideReceivedMeetUp();
            stopMeetingUpLocationUpdates();

            sslSellerSummary.IsVisible = false;
            sslMeetingEstimates.IsVisible = false;
            selectedSeller = null;
            IsNowMeeting = false;
            IsForSetNewMeetup = false;
            startNearbySellersUpdates();
            map.SelectedPinChanged += SellerPinChanged;


            if (Constants.accTransactionsChannel != null)
                Constants.accTransactionsChannel.Disconnect();


            map.Pins.Clear();
            map.Polylines.Clear();
        }


        async void StartMeetingUpLocationUpdates()
        {
            var geolocator = CrossGeolocator.Current;

            if (geolocator.IsListening)
                await geolocator.StopListeningAsync();

            geolocator.DesiredAccuracy = 10;

            await geolocator.StartListeningAsync(TimeSpan.FromSeconds(3), 3, true, new ListenerSettings()
            {
                ActivityType = ActivityType.AutomotiveNavigation,
                AllowBackgroundUpdates = true,
                DeferLocationUpdates = true,
                DeferralDistanceMeters = 3,
                DeferralTime = TimeSpan.FromSeconds(3),
                ListenForSignificantChanges = true,
                PauseLocationUpdatesAutomatically = false

            });

            var p = await geolocator.GetLastKnownLocationAsync();
            if (p != null)
                MeetingUpLocationChanged(null, new PositionEventArgs(p));

            geolocator.PositionChanged += MeetingUpLocationChanged;

        }



        void MeetingUpLocationChanged(object sender, PositionEventArgs e)
        {

            if (currentLocation != null)
            {
                //dont update if location is the same
                if (Misc.DoubleEquals(currentLocation.Latitude, e.Position.Latitude) && Misc.DoubleEquals(currentLocation.Longitude, e.Position.Longitude))
                {
                    return;
                }

                //if(Misc.getCoordinatesDistanceInMeters(currentLocation,e.Position)< 3){
                //    return;
                //}

            }

            if (Constants.IsInTransaction)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    map.Polylines.Remove(map.Polylines.Where(x => x.StrokeColor == Color.DimGray).FirstOrDefault());

                    PointToPointRouteFinder rfMeetupSetupFromUser = new PointToPointRouteFinder(currentLocation.Latitude, currentLocation.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                    rfMeetupSetupFromUser.TravelTimeDisplay = lblBuyerETA;
                    rfMeetupSetupFromUser.RouteColor = Color.DimGray;
                    rfMeetupSetupFromUser.StartRouteFinderUpdates(map);
                });
            }


            ActionCableClient transClient = Constants.accTransactionsChannel;

            currentLocation = e.Position;
            JObject positionUpdateData = new JObject();
            positionUpdateData.Add("user_transaction_id", currentTransactionId);
            positionUpdateData.Add("user_id", Constants.currentUser.id);
            positionUpdateData.Add("latitude", currentLocation.Latitude);
            positionUpdateData.Add("longitude", currentLocation.Longitude);
            positionUpdateData.Add("action", "user_location_update");

            transClient.Perform(positionUpdateData);

            //updateMeetupEstimates();

            //GetMeetupAddress();

            //FindMeetupRoutes();
        }

        async void stopMeetingUpLocationUpdates()
        {
            var geolocator = CrossGeolocator.Current;

            if (geolocator.IsListening)
                await geolocator.StopListeningAsync();

            geolocator.PositionChanged -= MeetingUpLocationChanged;
        }

        void updateMeetupEstimates()
        {

            IsNowMeeting = true;
            IsForSetNewMeetup = false;
            sslMeetingEstimates.IsVisible = true;
            FindMeetupRoutes();


            lblHeaderDisplay.Text = "Please proceed to your meetup address.";

        }

        void showReceivedMeetup(string address)
        {
            hideMeetupSetup();
            cvHeaderDisplay.IsVisible = true;
            sslReceivedMeetup.IsVisible = true;
            lblMeetupAddressRequest.Text = address;

            lblHeaderDisplay.FontSize = 12;
            lblHeaderDisplay.TextColor = Color.Gray;

            updateCurrentMeetupPin();

            IsForSetNewMeetup = false;
            FindSetupMeetupRoutes();

            centerMapPositions();

            hideBuySetup();
        }



        void updateCurrentMeetupPin()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (currentMeetupPin != null)
                    map.Pins.Remove(currentMeetupPin);


                currentMeetupPin = new Pin()
                {
                    Position = new Xamarin.Forms.GoogleMaps.Position(currentMeetupPosition.Latitude, currentMeetupPosition.Longitude),
                    Label = "Meetup Position",
                    Icon = BitmapDescriptorFactory.FromBundle("MeetUpPin.png")
                };

                map.Pins.Add(currentMeetupPin);

            });

        }
        void hideReceivedMeetUp()
        {
            sslReceivedMeetup.IsVisible = false;

        }
        //


        void showMeetupSetup()
        {


            map.CameraChanged += Map_CameraChanged;
            IsMonitoringMapChanges = true;
            MonitorMapChanges();

            sslMeetupSection.IsVisible = true;
            sslGeocodeDisplay.IsVisible = true;
            ciMeetupCenterMarker.IsVisible = true;

            sslSellerDetails.IsVisible = false;

            hideBuySetup();

        }

        void hideMeetupSetup()
        {

            sslSellerDetails.IsVisible = false;
            map.CameraChanged -= Map_CameraChanged;
            IsMonitoringMapChanges = false;
            //MonitorMapChanges();

            sslMeetupSection.IsVisible = false;
            sslGeocodeDisplay.IsVisible = false;
            ciMeetupCenterMarker.IsVisible = false;
        }



        void showBuySetup()
        {

            sslFindSellerHeader.IsVisible = true;
        }
        void hideBuySetup()
        {

            sslFindSellerHeader.IsVisible = false;
        }

        #endregion
        //
        //
        // THE FOLLOWING ARE MEETUPS SETTING METHODS
        //
        // 

        private DateTime lastDragged;

        private bool geocodingDone = false;
        void MonitorMapChanges()
        {
            Device.StartTimer(TimeSpan.FromMilliseconds(500), () =>
            {
                if (lastDragged.Add(TimeSpan.FromMilliseconds(500)) <= DateTime.Now && !geocodingDone)
                    GetAddress();
                return IsMonitoringMapChanges;
            });
        }

        async void GetAddress()
        {
            // get address here
            try
            {
                lblGeocodedAddress.Text = "Finding...";
                var x = map.CameraPosition.Target;
                Geocoder geocoder = new Geocoder();

                var addresses = await geocoder.GetAddressesForPositionAsync(x);
                Log.e("sada", "" + addresses);
                //addresses.RunSynchronously();
                List<string> lstAddresses = addresses.ToList();
                String geocodedAddress = lstAddresses.Count() > 0 ? lstAddresses[0] : "Not found";

                Log.e("Found address", geocodedAddress);
                lblGeocodedAddress.Text = geocodedAddress == null ? "Finding..." : geocodedAddress.Replace(System.Environment.NewLine, " ");
                lblMeetupAddress.Text = lblGeocodedAddress.Text;

            }
            catch (Exception e)
            {
                Log.e("sada", e.StackTrace);
                lblMeetupAddress.Text = "Not found...";
                lblGeocodedAddress.Text = "Not found...";
            }
            geocodingDone = true;

        }

        void Map_CameraChanged(object sender, CameraChangedEventArgs e)
        {
            lastDragged = DateTime.Now;
            geocodingDone = false;

        }
        void StreetViewTapped(object sender, System.EventArgs e)
        {

            var x = map.CameraPosition.Target;
            DependencyService.Get<IStreetViewService>().openStreetView(x.Latitude, x.Longitude);
        }

        CancellableLoadingDialog setMeetUpCancellableDialog;
        void SetMeetupTapped(object sender, System.EventArgs e)
        {
            if (setMeetUpCancellableDialog == null)
                setMeetUpCancellableDialog = new CancellableLoadingDialog(CancelTransactionRequest);
            setMeetUpCancellableDialog.Text = "Finding routes...";
            //Navigation.PushPopupAsync(loadDialog, true);
            setMeetUpCancellableDialog.open();

            currentMeetupPosition = new Xamarin.Forms.Maps.Position(map.CameraPosition.Target.Latitude, map.CameraPosition.Target.Longitude);

            updateCurrentMeetupPin();
            //map.Polylines.Clear();

            IsForSetNewMeetup = true;
            //findMeetupRoutes();
            FindSetupMeetupRoutes();

        }
        void centerMapPositions()
        {
            ///center map for all points
            List<Xamarin.Forms.Maps.Position> _positions = new List<Xamarin.Forms.Maps.Position>();
            _positions.Add(new Xamarin.Forms.Maps.Position(currentLocation.Latitude, currentLocation.Longitude));
            _positions.Add(new Xamarin.Forms.Maps.Position(selectedSeller.LocationPin.Position.Latitude, selectedSeller.LocationPin.Position.Longitude));
            _positions.Add(currentMeetupPosition);

            var distance = MapHelper.TotalDistance(_positions);
            var center = MapHelper.GetCentralGeoCoordinate(_positions);
            map.MoveToRegion(
                MapSpan.FromCenterAndRadius(new Xamarin.Forms.GoogleMaps.Position(center.Latitude, center.Longitude),
                                            Distance.FromKilometers(distance * 0.65)), /// this was supposed to be /2 but the markers gets covered by other views
                true
            );


            ///maps built in
            //List<Xamarin.Forms.GoogleMaps.Position> _positions = new List<Xamarin.Forms.GoogleMaps.Position>();
            //_positions.Add(new Xamarin.Forms.GoogleMaps.Position(currentLocation.Latitude, currentLocation.Longitude));
            //_positions.Add(new Xamarin.Forms.GoogleMaps.Position(currentBuyerPosition.Latitude, currentBuyerPosition.Longitude));
            //_positions.Add(new Xamarin.Forms.GoogleMaps.Position(currentMeetupPosition.Latitude, currentMeetupPosition.Longitude));
            //map.MoveToRegion(MapSpan.FromPositions(_positions),true);
        }

        void FindSetupMeetupRoutes()
        {
            map.Polylines.Clear();

            PointToPointRouteFinder rfMeetupSetupFromUser = new PointToPointRouteFinder(currentLocation.Latitude, currentLocation.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
            rfMeetupSetupFromUser.TravelTimeDisplay = lblBuyerETA;
            rfMeetupSetupFromUser.RouteColor = Color.DimGray;
            rfMeetupSetupFromUser.RouteFound += (sender, e) =>
            {
                MeetupSetupRouteFound();
            };
            rfMeetupSetupFromUser.StartRouteFinderUpdates(map);

            //asdasdasdasd
            PointToPointRouteFinder rfMeetupSetupFromSeller;
            if (Constants.ResumeTransaction)
            {
                rfMeetupSetupFromSeller = new PointToPointRouteFinder(selectedSeller.Latitude, selectedSeller.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
            }
            else
            {
                rfMeetupSetupFromSeller = new PointToPointRouteFinder(selectedSeller.LocationPin.Position.Latitude, selectedSeller.LocationPin.Position.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
            }
            map.Polylines.Remove(map.Polylines.Where(x => x.StrokeColor == Color.Brown).FirstOrDefault());
            rfMeetupSetupFromSeller.TravelTimeDisplay = lblSellerETA;
            rfMeetupSetupFromSeller.RouteColor = Color.Brown;
            rfMeetupSetupFromSeller.StartRouteFinderUpdates(map);

        }
        void MeetupSetupRouteFound()
        {

            if (IsForSetNewMeetup)
            {
                setMeetUpCancellableDialog.Text = "Waiting for seller to agree on the meeting place...";

                ActionCableClient client = Constants.accTransactionsChannel;


                JObject buyData = new JObject();
                buyData.Add("auth_token", Constants.currentUser.token);
                buyData.Add("user_transaction_id", currentTransactionId);
                buyData.Add("latitude", currentMeetupPosition.Latitude);
                buyData.Add("longitude", currentMeetupPosition.Longitude);

                ///set_another parameter is a reusable parameter when setting up location
                /// true  = to set new location
                /// false = to prompt that the other party has declined set location and is setting
                /// another
                buyData.Add("set_another", true);
                buyData.Add("action", "set_meetup");

                client.Perform(buyData);
            }

        }

        void FindMeetupRoutes()
        {

            if (userToMeetupFinder == null)
            {
                userToMeetupFinder = new PointToPointRouteFinder(currentLocation.Latitude, currentLocation.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                userToMeetupFinder.TravelTimeDisplay = lblBuyerETA;
                userToMeetupFinder.RouteColor = Color.DimGray;

                userToMeetupFinder.StartRouteFinderUpdates(map);
                userToMeetupFinder.RouteFound += (sender, e) =>
                {
                    MeetupRouteFound();
                };
            }
            else
            {
                userToMeetupFinder.EndPin = currentMeetupPin;
                userToMeetupFinder.StartLatitude = currentLocation.Latitude;
                userToMeetupFinder.StartLongitude = currentLocation.Longitude;
                userToMeetupFinder.EndLatitude = currentMeetupPosition.Latitude;
                userToMeetupFinder.EndLongitude = currentMeetupPosition.Longitude;

                userToMeetupFinder.StartRouteFinderUpdates(map);
            }



            if (sellerToMeetupFinder == null)
            {
                if (Constants.ResumeTransaction)
                {
                    sellerToMeetupFinder = new PointToPointRouteFinder(selectedSeller.Latitude, selectedSeller.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                }
                else
                {
                    sellerToMeetupFinder = new PointToPointRouteFinder(selectedSeller.LocationPin.Position.Latitude, selectedSeller.LocationPin.Position.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                }
                sellerToMeetupFinder.TravelTimeDisplay = lblSellerETA;
                sellerToMeetupFinder.RouteColor = Color.Brown;

                sellerToMeetupFinder.StartRouteFinderUpdates(map);
                sellerToMeetupFinder.RouteFound += (sender, e) =>
                {
                    MeetupRouteFound();
                };
            }
            else
            {
                sellerToMeetupFinder.EndPin = currentMeetupPin;
                sellerToMeetupFinder.StartPin = selectedSeller.LocationPin;
                sellerToMeetupFinder.StartLatitude = selectedSeller.LocationPin.Position.Latitude;
                sellerToMeetupFinder.StartLongitude = selectedSeller.LocationPin.Position.Longitude;
                sellerToMeetupFinder.EndLatitude = currentMeetupPosition.Latitude;
                sellerToMeetupFinder.EndLongitude = currentMeetupPosition.Longitude;

                sellerToMeetupFinder.StartRouteFinderUpdates(map);
            }




        }

        void MeetupRouteFound()
        {
            if (userToMeetupFinder.Polyline != null && sellerToMeetupFinder.Polyline != null)
            {


                if (IsNowMeeting)
                {

                    if (userToMeetupFinder.Arrived && sellerToMeetupFinder.Arrived)
                    {
                        cvConfirmPurchase.IsVisible = true;
                        cvHeaderDisplay.IsVisible = false;
                        lblHeaderDisplay.Text = "Receive the item and tap complete to complete transaction. Transaction will complete once payment is received by the seller.";
                    }
                    else
                    {
                        cvConfirmPurchase.IsVisible = false;
                        cvHeaderDisplay.IsVisible = true;
                        lblHeaderDisplay.Text = "Please proceed to your meetup address.";

                    }
                }
            }
        }

        public void didFindRouteSuccess(Polyline polyline, String travelTime, RouteFinder caller)
        {
        }

        public void didFindRouteFailed(string error, RouteFinder caller)
        {
            Log.e("FindRoute", error);
            loadDialog.close();
        }

        void BuyerAgreeMeetup(object sender, System.EventArgs e)
        {
            loadDialog.Text = "Starting meetup...";

            ActionCableClient client = Constants.accTransactionsChannel;


            JObject buyData = new JObject();
            buyData.Add("auth_token", Constants.currentUser.token);
            buyData.Add("user_transaction_id", currentTransactionId);
            //buyData.Add("latitude", currentMeetupPosition.Latitude);
            //buyData.Add("longitude", currentMeetupPosition.Longitude);

            ///set_another parameter is a reusable parameter when setting up location
            /// true  = to set another location
            /// false = to accept (lat and long not needed)
            buyData.Add("set_another", false);
            buyData.Add("action", "set_meetup");
            client.Perform(buyData);


            loadDialog.close();

            updateMeetupEstimates();


            GetMeetupAddress();
        }


        void SetAnotherMeetupTapped(object sender, System.EventArgs e)
        {
            //use decline meetup
            JObject declineData = new JObject();
            declineData.Add("auth_token", Constants.currentUser.token);
            declineData.Add("user_transaction_id", currentTransactionId);
            declineData.Add("action", "decline_meetup");

            ActionCableClient client = Constants.accTransactionsChannel;

            client.Perform(declineData);

            hideReceivedMeetUp();
            showMeetupSetup();
        }


        void ChatTapped(object sender, System.EventArgs e)
        {
            chatIsTapped = true;
            cvNewMessageIndicator.IsVisible = false;

            Navigation.PushModalAsync(new Message.ConversationPage(selectedSeller.FirstName + " " + selectedSeller.LastName, currentTransactionId));
        }

        /// <summary>
        /// Cancels the transaction tapped.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        async void CancelTransactionTapped(object sender, System.EventArgs e)
        {
            var canceltransaction = await DisplayAlert("Cancel Transaction", "Do you want to cancel transaction?", "Yes", "No");
            if (canceltransaction)
            {
                ActionCableClient acc = Constants.accTransactionsChannel;

                JObject cancelData = new JObject();
                cancelData.Add("auth_token", Constants.currentUser.token);
                cancelData.Add("user_transaction_id", currentTransactionId);
                cancelData.Add("action", "cancel_transaction");

                acc.Perform(cancelData);
            }
        }

        static Order currentOrder;

        /// <summary>
        /// Gets the meetup address.
        /// </summary>
        async void GetMeetupAddress()
        {
            // get address here
            try
            {
                //lblGeocodedAddress.Text = "Finding...";
                var x = currentMeetupPosition;
                Geocoder geocoder = new Geocoder();
                var addresses = await geocoder.GetAddressesForPositionAsync(new Xamarin.Forms.GoogleMaps.Position(x.Latitude, x.Longitude));


                List<string> lstAddresses = addresses.ToList();
                String geocodedAddress = lstAddresses.Count() > 0 ? lstAddresses[0] : "Not found";


                Log.e("Found address", geocodedAddress);
                lblMeetupAddress.Text = geocodedAddress;

            }
            catch (Exception e)
            {
                Log.e("sada", e.StackTrace);
            }

        }

        /// <summary>
        /// Confirms the purchase tapped.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void ConfirmPurchaseTapped(object sender, System.EventArgs e)
        {
            ActionCableClient client = Constants.accTransactionsChannel;

            JObject data = new JObject();
            data.Add("user_transaction_id", currentTransactionId);
            data.Add("action", "complete_transaction");

            client.Perform(data);
        }

        void CenterToMyLocation(object sender, System.EventArgs e)
        {

            if (map != null && currentLocation != null)
                map.AnimateCamera(CameraUpdateFactory.NewPosition(new Xamarin.Forms.GoogleMaps.Position(currentLocation.Latitude, currentLocation.Longitude)));



        }

        void ProductOnePhotoTapped(object sender, System.EventArgs e)
        {
            if (selectedSellerProduct != null)
                Navigation.PushModalAsync(new ProductImagesViewerPage(selectedSellerProduct.Images, 0));

        }

        void ProductTwoPhotoTapped(object sender, System.EventArgs e)
        {
            if (selectedSellerProduct != null)
                Navigation.PushModalAsync(new ProductImagesViewerPage(selectedSellerProduct.Images, 1));

        }

        void ProductThreePhotoTapped(object sender, System.EventArgs e)
        {
            if (selectedSellerProduct != null)
                Navigation.PushModalAsync(new ProductImagesViewerPage(selectedSellerProduct.Images, 2));

        }

        void HideSellerMenu()
        {
            sslSellerSummary.IsVisible = false;
            sslMeetingEstimates.IsVisible = false;
            selectedSeller = null;
            IsNowMeeting = false;
            IsForSetNewMeetup = false;
            startNearbySellersUpdates();
            map.SelectedPinChanged += SellerPinChanged;
        }

        void UpdateLocation(Plugin.Geolocator.Abstractions.Position p)
        {
            if (Constants.ResumeTransaction)
            {
                ActionCableClient cl = Constants.accUsersChannel;
                cl.MessageReceived += AccUsersChannel_MessageReceived;


                Constants.IsInTransaction = true;
                Constants.TransactionInProgress = true;
                var acs = accounts.Where(x => x.ID == Application.Current.Properties["BuyerBankAccount"].ToString());
                pickerAccounts.SelectedIndex = accounts.IndexOf(acs.FirstOrDefault());

                entryQuantity.Text = Application.Current.Properties["BuyerQuantity"].ToString();
                pickerPaymentOptions.SelectedIndex = 0;

                sslFindSellerHeader.IsVisible = false;
                currentTransactionId = Application.Current.Properties["TransactionId"].ToString();
                currentMeetupPosition = Constants.currentPosition;

                selectedSeller = JsonConvert.DeserializeObject<NearbySeller>(Application.Current.Properties["selectedSeller"].ToString());

                Pin pin = new Pin()
                {
                    Type = PinType.SavedPin,
                    Position = new Xamarin.Forms.GoogleMaps.Position(selectedSeller.Latitude, selectedSeller.Longitude),
                    Label = selectedSeller.ProductName,
                    Icon = BitmapDescriptorFactory.FromBundle("UserLocation.png")
                };
                if (!map.Pins.Contains(pin))
                    map.Pins.Add(pin);


                PointToPointRouteFinder rfMeetupSetupFromUser = new PointToPointRouteFinder(currentLocation.Latitude, currentLocation.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                rfMeetupSetupFromUser.TravelTimeDisplay = lblBuyerETA;
                rfMeetupSetupFromUser.RouteColor = Color.DimGray;
                rfMeetupSetupFromUser.StartRouteFinderUpdates(map);

                PointToPointRouteFinder rfMeetupSetupFromSeller = new PointToPointRouteFinder(selectedSeller.Latitude, selectedSeller.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
                rfMeetupSetupFromSeller.TravelTimeDisplay = lblSellerETA;
                rfMeetupSetupFromSeller.RouteColor = Color.Brown;
                rfMeetupSetupFromSeller.StartRouteFinderUpdates(map);



                Constants.SubscribeToTransactions(currentTransactionId);
                ActionCableClient transactionClient = Constants.accTransactionsChannel;
                transactionClient.MessageReceived += AccTransactionsChannel_MessageReceived;
                sslMeetingEstimates.IsVisible = true;
                hideMeetupSetup();
                hideReceivedMeetUp();

                updateMeetupEstimates();
                GetMeetupAddress();

                updateCurrentMeetupPin();
                stopLocationUpdates();
                map.SelectedPinChanged -= SellerPinChanged;
                map.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.GoogleMaps.Position(currentLocation.Latitude, currentLocation.Longitude), Distance.FromMiles(0.4)));

            }
        }

        void UpdateLocationOnResume(JObject obj){
            var pin = new Pin()
            {
                Type = PinType.SavedPin,
                Position = new Xamarin.Forms.GoogleMaps.Position(selectedSeller.Latitude, selectedSeller.Longitude),
                Label = selectedSeller.ProductName,
                Icon = BitmapDescriptorFactory.FromBundle("UserLocation.png")
            };

            if (map.Pins.Contains(pin))
                map.Pins.Remove(pin);

            pin.Position = new Xamarin.Forms.GoogleMaps.Position(obj["data"]["setter"]["latitude"].Value<double>(), obj["data"]["setter"]["longitude"].Value<double>());
            selectedSeller.Latitude = obj["data"]["setter"]["latitude"].Value<double>();
            selectedSeller.Longitude = obj["data"]["setter"]["longitude"].Value<double>();
            selectedSeller.LocationPin = pin;

            if (Application.Current.Properties.ContainsKey("selectedSeller"))
            {
                Application.Current.Properties["selectedSeller"] = JsonConvert.SerializeObject(selectedSeller);
            }
            else
            {
                Application.Current.Properties.Add("selectedSeller", JsonConvert.SerializeObject(selectedSeller));
            }


            map.Pins.Add(pin);
            map.Polylines.Remove(map.Polylines.Where(x => x.StrokeColor == Color.Brown).FirstOrDefault());

            PointToPointRouteFinder rfMeetupSetupFromSeller = new PointToPointRouteFinder(selectedSeller.Latitude, selectedSeller.Longitude, currentMeetupPosition.Latitude, currentMeetupPosition.Longitude);
            rfMeetupSetupFromSeller.TravelTimeDisplay = lblSellerETA;
            rfMeetupSetupFromSeller.RouteColor = Color.Brown;
            rfMeetupSetupFromSeller.StartRouteFinderUpdates(map);
        }

        async public void SetQuantity(int? quantity)
        {
            if (quantity.HasValue)
            {
                entryQuantity.Text = quantity.ToString();
            }
            else
            {
                entryQuantity.Text = "";
            }

            await Task.Delay(500);
            BuyButtonTapped(null, null);
        }
    }
}
