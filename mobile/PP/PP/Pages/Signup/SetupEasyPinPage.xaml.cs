﻿using System;
using System.Collections.Generic;
using PP.CustomLayouts;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class SetupEasyPinPage : ContentPage
    {
        String currentPass = "";

        int passMinLength = 4;
        public SetupEasyPinPage()
        {
            InitializeComponent();
        }

        void Handle_RadioClicked(object sender, System.EventArgs e)
        {
            
        }

        protected override  bool OnBackButtonPressed()
        {
           
            AskToSkipSetup();

            return true;
        }
        void TappedBackPage(object sender, System.EventArgs e)
        {
            OnBackButtonPressed();
        }


        async void AskToSkipSetup(){
            var skipEasyPinSetup = await DisplayAlert("Skip", "Do you want to skip setting up your easy pin?", "Yes", "Continue setup");

            if(skipEasyPinSetup){
                SidePanelPage spp = new SidePanelPage();
                Application.Current.MainPage = new NavigationPage(spp);
                Constants.shouldStopIdleTimer = false;

            }
        }

        void KeyTapped(object sender, System.EventArgs e)
        {

            var key = ((Label)((CorneredView)sender).Content).Text;

            Log.e("sadadads", "Key: "+key);

            //check if its a number
            if(int.TryParse(key,out int n)){
                //check if its already full
                if(currentPass.Length<4){
                    //append new key
                    currentPass = currentPass + "" + key;
                }
            }
            //otherwise it's either clear or check
            else{

                if(key.ToLower().Equals("C".ToLower())){
                    currentPass = "";
                }
                else{

                    //neextt

                    if(currentPass.Length==4){
                        //okayy
                        Navigation.PushModalAsync(new ConfirmEasyPinPage() { toComparePass = currentPass });
                    }
                    else
                        DisplayAlert("Incomplete", "Please fill in " + passMinLength + " keys", "Okay");
                }
            }

            updateCheckboxes();
            Log.e("Current pass", currentPass);
        }

        void updateCheckboxes(){

   //         radOne.IsSelected = false;
			//radTwo.IsSelected = false;
			//radThree.IsSelected = false;
			//radFour.IsSelected = false;

    //        for (int x = 0; x < currentPass.Length; x++){
    //            if (x == 0)
    //                radOne.IsSelected = true;
    //            if (x == 1)
				//	radTwo.IsSelected = true;
				//if (x == 2)
				//	radThree.IsSelected = true;
				//if (x == 3)
					//radFour.IsSelected = true;
            //}
            switch (currentPass.Length)
            {
                case 1:
                    setupRadio(radOne, true);
                    setupRadio(radTwo, false);
                    setupRadio(radThree, false);
                    setupRadio(radFour, false);
                    break;
                case 2:
                    setupRadio(radOne, true);
                    setupRadio(radTwo, true);
                    setupRadio(radThree, false);
                    setupRadio(radFour, false);
                    break;
                case 3:
                    setupRadio(radOne, true);
                    setupRadio(radTwo, true);
                    setupRadio(radThree, true);
                    setupRadio(radFour, false);
                    break;
                case 4:

                    setupRadio(radOne, true);
                    setupRadio(radTwo, true);
                    setupRadio(radThree, true);
                    setupRadio(radFour, true);
                    break;
                default:
                    setupRadio(radOne, false);
                    setupRadio(radTwo, false);
                    setupRadio(radThree, false);
                    setupRadio(radFour, false);
                    break;
            }
        }
        void setupRadio(XFRadio rad, bool select)
        {
            if (select && !rad.IsSelected)
            {
                rad.IsSelected = select;
            }
            else if (!select && rad.IsSelected)
            {
                rad.IsSelected = select;
            }
        }
    }
}
