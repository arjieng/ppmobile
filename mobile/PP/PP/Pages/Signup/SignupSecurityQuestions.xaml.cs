﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PP.tools.API;
using PP.tools.models;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class SignupSecurityQuestions : ContentPage,IBaseAPIInterface
    {

		// an instance of the list of questions with Question object
		ObservableCollection<Question> questions;
		// an instance of the list of answers
		ObservableCollection<String> answers;

        //the callback page that will need the result (quenstion, answers) of this page
        public ContentPage callerPage;

        /// <summary>
        /// Closes the entire page and goes back to the previous one.
        /// Gets fired when the back button is tapped.
        /// </summary>
		void back(object sender, System.EventArgs e)
		{
			Navigation.PopModalAsync(true);
        }

       
        public SignupSecurityQuestions()
        {
            InitializeComponent();



            //Constants.isDebug = false;
            FetchDefaultQuestionsAPI api = new FetchDefaultQuestionsAPI();
            api.setCallbacks(this);
            api.getResponse();
        }

		public void OnSuccess(JObject response, BaseAPI caller)
		{
			Log.e("sdada", response.ToString());

			if ((caller as FetchDefaultQuestionsAPI) != null)
			{
                JArray arr = response["default_questions"].Value<JArray>();
                if(arr.Count>=10)
                    setupQuesions(arr);
			}
		}

		public void OnError(string errMsg, BaseAPI caller)
		{
			throw new NotImplementedException();
		}

		public void OnErrorCode(int errorCode, BaseAPI caller)
		{
			throw new NotImplementedException();
		}

        void setupQuesions(JArray questionResponse){

            questions = JsonConvert.DeserializeObject<ObservableCollection<Question>>(questionResponse.ToString(), new JsonSerializerSettings()
            {
                MissingMemberHandling = MissingMemberHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            });
            //Log.e("sada",questions.ToString());

            if(questions != null){

				ObservableCollection<Question> setone = new ObservableCollection<Question>();
				ObservableCollection<Question> settwo = new ObservableCollection<Question>();
                for (int index = 0; index < 10; index++){
                    Question q = questions[index];
                    if (index <= 4)
                        setone.Add(q);
                    else if (index > 4 && index < 10)
                        settwo.Add(q);
                }

                setOnePicker.ItemsSource = setone;
                setTwoPicker.ItemsSource = settwo;
            }
        }

        void CompleteTapped(object sender, System.EventArgs e)
        {

            if (setOnePicker.SelectedItem == null || setTwoPicker.SelectedItem == null)
            {
                DisplayAlert("Incomplte","Please Select Questions.","Okay");
                return;
            }

            if (String.IsNullOrEmpty(entryCustomQuestion.Text.Trim()))
            {

                DisplayAlert("Incomplte","Please add a custom question.","Okay");
                return;
            }

            if (String.IsNullOrEmpty(entryFirstAnswer.Text.Trim()) ||
                String.IsNullOrEmpty(entrySecondAnswer.Text.Trim())||
                String.IsNullOrEmpty(entryCustomAnswer.Text.Trim())){


                DisplayAlert("Incomplete","Please complete all answers.","Okay");
                return;
            }


            //build questions
            ObservableCollection<Question> selectedQuestions = new ObservableCollection<Question>();
			selectedQuestions.Add(setOnePicker.SelectedItem as Question);
			selectedQuestions.Add(setTwoPicker.SelectedItem as Question);
            selectedQuestions.Add(new Question(){question_id = null, question = entryCustomQuestion.Text.Trim()});

            answers = new ObservableCollection<string>();
            answers.Add(entryFirstAnswer.Text.Trim());
			answers.Add(entrySecondAnswer.Text.Trim());
            answers.Add(entryCustomAnswer.Text.Trim());

            SignUpPage s = callerPage as SignUpPage;

            if(s != null){
                s.didSetSecurityQuestions(selectedQuestions,answers);
                back(null,null);
            }
		}

	}
}
