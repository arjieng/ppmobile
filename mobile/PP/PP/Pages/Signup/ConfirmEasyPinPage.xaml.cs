﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PP.CustomLayouts;
using PP.tools.API;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class ConfirmEasyPinPage : ContentPage,IBaseAPIInterface
	{
		String currentPass = "";
        public String toComparePass { get; set; }

        int passMinLength = 4;
        public ConfirmEasyPinPage()
        {
            InitializeComponent();
        }
		void back(object sender, System.EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}
		void KeyTapped(object sender, System.EventArgs e)
		{

			var key = ((Label)((CorneredView)sender).Content).Text;

			Log.e("sadadads", "Key: " + key);

			//check if its a number
			if (int.TryParse(key, out int n))
			{
				//check if its already full
				if (currentPass.Length < 4)
				{
					//append new key
					currentPass = currentPass + "" + key;
				}
			}
			//otherwise it's either clear or check
			else
			{

				if (key.ToLower().Equals("c".ToLower()))
				{
					currentPass = "";
				}
				else
				{

					//neextt

					if (currentPass.Length == passMinLength)
					{
                        //okayy
                        if(toComparePass.Equals(currentPass)){

                            SetEasyPinAPI api = new SetEasyPinAPI(Constants.currentUser.token, currentPass);
                            api.setCallbacks(this);
                            api.getResponse();

                        }
                        else{
                            DisplayAlert("Mismatch", "Confirm pin must match.", "Okay");
                        }
                    }else{
						DisplayAlert("Incomplete", "Please fill in "+passMinLength+ " keys", "Okay");
                    }
				}
			}

			updateCheckboxes();
			Log.e("Current pass", currentPass);
		}

		void updateCheckboxes()
		{

			//radOne.IsSelected = false;
			//radTwo.IsSelected = false;
			//radThree.IsSelected = false;
			//radFour.IsSelected = false;

			//for (int x = 0; x < currentPass.Length; x++)
			//{
			//	if (x == 0)
			//		radOne.IsSelected = true;
			//	if (x == 1)
			//		radTwo.IsSelected = true;
			//	if (x == 2)
			//		radThree.IsSelected = true;
			//	if (x == 3)
			//		radFour.IsSelected = true;
			//}
            switch (currentPass.Length)
            {
                case 1:
                    setupRadio(radOne, true);
                    setupRadio(radTwo, false);
                    setupRadio(radThree, false);
                    setupRadio(radFour, false);
                    break;
                case 2:
                    setupRadio(radOne, true);
                    setupRadio(radTwo, true);
                    setupRadio(radThree, false);
                    setupRadio(radFour, false);
                    break;
                case 3:
                    setupRadio(radOne, true);
                    setupRadio(radTwo, true);
                    setupRadio(radThree, true);
                    setupRadio(radFour, false);
                    break;
                case 4:

                    setupRadio(radOne, true);
                    setupRadio(radTwo, true);
                    setupRadio(radThree, true);
                    setupRadio(radFour, true);
                    break;
                default:
                    setupRadio(radOne, false);
                    setupRadio(radTwo, false);
                    setupRadio(radThree, false);
                    setupRadio(radFour, false);
                    break;
            }
        }
        void setupRadio(XFRadio rad, bool select)
        {
            if (select && !rad.IsSelected)
            {
                rad.IsSelected = select;
            }
            else if (!select && rad.IsSelected)
            {
                rad.IsSelected = select;
            }
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            if((caller as SetEasyPinAPI) != null){
				// open the SidePanel (User homepage with side menu) page
                Constants.shouldStopIdleTimer = false;
				SidePanelPage spp = new SidePanelPage();
				Application.Current.MainPage = new NavigationPage(spp);
            }
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            if ((caller as SetEasyPinAPI) != null)
            { 
                DisplayAlert("Setup failed","Easy pin setup failed. "+errMsg,"Okay");
            }
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
			if ((caller as SetEasyPinAPI) != null)
			{

                DisplayAlert("Setup failed","Easy pin setup failed. There was a problem with the connection: Code: "+errorCode,"Okay");
			}
        }
    }
}
