﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Media.Abstractions;
using PP.Pages.Profile;
using PP.tools;
using PP.tools.API;
using PP.tools.models;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class SignUpPage : ContentPage,IBaseAPIInterface,TermsAndServicesPage.TermsCallbacks
    {

        MediaFile profileFile;
		ObservableCollection<Question> questions = new ObservableCollection<Question>();
		ObservableCollection<String> answers = new ObservableCollection<String>();

        string termVersion;
        public SignUpPage()
        {
            InitializeComponent();

            // sorry, dont know how to access custom view's subviews on xaml. Im so ashamed.
            checkbox.ExtendedLabel.IsUnderline = true;

            TapGestureRecognizer tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) => {
                //checkbox.Handle_Tapped(sender,e);
                Log.e("sada","sadadad");

                Navigation.PushModalAsync(new TermsAndServicesPage() { IsSignupMode = true, callbacks = this});
            };
            checkbox.ExtendedLabel.GestureRecognizers.Add(tap);
        }

       
        void DatePickerEntry_TextChanged(object sender, TextChangedEventArgs e){
            var dis = sender as Entry;

            if (e.OldTextValue.Length > e.NewTextValue.Length)
            {
                if (e.OldTextValue.Substring(e.OldTextValue.Length - 1) == "/")
                {
                    dis.Text = e.OldTextValue.Remove(e.OldTextValue.Length - 2);
                }
            }else{
                if (dis.Text.Length == 2 || dis.Text.Length == 5)
                {
                    dis.Text = dis.Text + "/";
                }
                if (dis.Text.Length == 11)
                {
                    dis.Text = e.OldTextValue;
                }
            }
        }

        void DatePickerEntry_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            //entryBirthDate.Unfocus();
            //Device.BeginInvokeOnMainThread(() =>
            //{
            //    datePickerBirthDate.Focus();
            //});
        
        }

        void BirthDatePicker_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (e.PropertyName == "Date")
                    entryBirthDate.Text = datePickerBirthDate.Date.ToString("M/d/yyyy");
            });
            //if(e.PropertyName == "Date"){
            //    entryBirthDate.Text = datePickerBirthDate.Date.ToString("M/d/yyyy");
            //}
        }

		void back(object sender, System.EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}

		void passwordShowHide(object sender, System.EventArgs e)
        {
            entryPassword.IsPassword = !entryPassword.IsPassword;
            if(entryPassword.IsPassword){
                imgPassword.Source = "ic_visible";
            }
            else{
                imgPassword.Source = "ic_visible_off";
            }

        }
		void confirmPasswordShowHide(object sender, System.EventArgs e)
		{
			entryConfirmPassword.IsPassword = !entryConfirmPassword.IsPassword;
			if (entryConfirmPassword.IsPassword)
			{
				imgConfirmPass.Source = "ic_visible";
			}
			else
			{
				imgConfirmPass.Source = "ic_visible_off";
			}

		}

        void uploadTapped(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new SignupPhotoUpload() {callerPage = this});
        }

        void securityQuestionsTapped(object sender, System.EventArgs e)
        {
            SignupSecurityQuestions ssq = new SignupSecurityQuestions();
            ssq.callerPage = this;

            Navigation.PushModalAsync(ssq);
        }
        void signupTapped(object sender, System.EventArgs e)
        {
            if (!isValidUsername(entryUsername.Text.Trim()))
            {
                DisplayAlert("Invalid", "Your username is invalid. Please use alpha-numeric characters (a-z,0-9),underscore and period (in between them) only.", "Okay");
                entryUsername.Focus();
                return;
            }

            //do checks before

            //check profile image
            if (profileFile == null){

                DisplayAlert("Incomplete", "Please add a profile picture","Okay");
                return;
            }
           

            // check fields

            if(String.IsNullOrEmpty(entryUsername.Text.Trim())){
                DisplayAlert("Incomplete","Please fill in username","Okay");
                entryUsername.Focus();
                return;
            }
            else{
				if (String.IsNullOrEmpty(entryPassword.Text.Trim()))
				{
					DisplayAlert("Incomplete", "Please fill in password", "Okay");
					entryPassword.Focus();
					return;
				}
				else
				{

                    if(entryPassword.Text.Trim().Length<8){
						DisplayAlert("Incomplete", "Password should be at least 8 characters long", "Okay");
						entryPassword.Focus();
                    }
                    else{

						if (String.IsNullOrEmpty(entryConfirmPassword.Text.Trim()))
						{
							DisplayAlert("Incomplete", "Please fill in confirm password", "Okay");
							entryConfirmPassword.Focus();
							return;
						}
						else
						{
                            if (!entryPassword.Text.Trim().Equals(entryConfirmPassword.Text.Trim()))
							{
								DisplayAlert("Incomplete", "Password and confirm password must match", "Okay");
								entryConfirmPassword.Focus();
								return;
							}
							else
							{
								if (String.IsNullOrEmpty(entryFirstname.Text.Trim()))
								{
									DisplayAlert("Incomplete", "Please fill in firstname", "Okay");
									entryFirstname.Focus();
									return;
								}
								else
								{
									if (String.IsNullOrEmpty(entryLastname.Text.Trim()))
									{

										DisplayAlert("Incomplete", "Please fill in lastname", "Okay");
                                        entryLastname.Focus();
										return;
									}
									else
									{
                                        if(String.IsNullOrEmpty(entryBirthDate.Text.Trim())){
                                            DisplayAlert("Incomplete", "Please fill in birth date", "Okay");
                                            entryBirthDate.Focus();
                                            return;
                                        }else{
                                            DateTime datevalue;
                                            if (!DateTime.TryParse(entryBirthDate.Text, out datevalue))
                                            {
                                                DisplayAlert("Incomplete", "Please enter a valid date", "Okay");
                                                entryEmail.Focus();
                                                return;
                                            }else{
                                                DateTime bd = DateTime.Parse(entryBirthDate.Text);
                                                int years = DateTime.Now.Year - bd.Year;
                                                if (DateTime.Now.DayOfYear < bd.DayOfYear)
                                                {
                                                    years = years - 1;
                                                }

                                                if (years < 18)
                                                {
                                                    DisplayAlert("Incomplete", "You must be 18 or above to register", "Okay");
                                                    entryEmail.Focus();
                                                    return;
                                                }else{
                                                    if (String.IsNullOrEmpty(entryEmail.Text.Trim()))
                                                    {

                                                        DisplayAlert("Incomplete", "Please fill in email", "Okay");
                                                        entryEmail.Focus();
                                                        return;
                                                    }
                                                    else
                                                    {
                                                        if (!emailIsValid(entryEmail.Text.Trim()))
                                                        {
                                                            DisplayAlert("Invalid", "Please add a valid email address", "Okay");
                                                            entryEmail.Focus();
                                                            return;

                                                        }
                                                        else
                                                        {
                                                            if (!checkbox.IsChecked)
                                                            {

                                                                DisplayAlert("Incomplete", "Please accept terms and conditions", "Okay");
                                                                entryUsername.Focus();
                                                                return;
                                                            }
                                                            else
                                                            {

                                                                if (!isValidUsername(entryUsername.Text.Trim()))
                                                                {
                                                                    DisplayAlert("Invalid", "Your username is invalid. Please use alpha-numeric characters (a-z,0-9),underscore and period (in between them) only.", "Okay");
                                                                    entryUsername.Focus();
                                                                    return;
                                                                }

                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
									}
								}
							}
						}
                    }
				}
            }

			// check questions
			if (questions.Count != 3 || answers.Count != 3)
			{

				DisplayAlert("Incomplete", "Please Complete setting security questions", "Okay");
				return;
			}


            //generate base 64 image
            String base64 = Convert.ToBase64String(SignupPhotoUpload.ReadFully(profileFile.GetStream()));

           

            SignupAPI api = new SignupAPI(
                entryUsername.Text.Trim(),
	            entryPassword.Text.Trim(),
	            entryConfirmPassword.Text.Trim(),
	            entryEmail.Text.Trim(),
                entryBirthDate.Text.Trim(),
	            base64,
	            entryFirstname.Text.Trim(),
				entryLastname.Text.Trim(),
				"", //null for phone
                termVersion,
                questions.ToList(),
                answers.ToList(),
                null
            );

            api.setCallbacks(this);
            api.getResponse();


        }

		public static bool emailIsValid(string email)
		{
			string expresion;
			expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
			if (Regex.IsMatch(email, expresion))
			{
				if (Regex.Replace(email, expresion, string.Empty).Length == 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}

        public static bool isValidUsername(string username){
            string expresion = @"^(?=[a-zA-Z])[-\w.]{0,23}([a-zA-Z\d]|(?<![-.])_)$";

            if (Regex.IsMatch(username, expresion))
            {
                if (Regex.Replace(username, expresion, string.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        // callback methods

        public void didSaveProfileImage(MediaFile file){

            profileFile = file;
            imgProfileImage.Source= ImageSource.FromStream(() =>
					{
						var stream = file.GetStream();
						return stream;
					});

            //imgProfileImage.Source = source;
        }

        public void didSetSecurityQuestions(ObservableCollection<Question> questions, ObservableCollection<String> answers){
            this.questions = questions;
            this.answers = answers;

            lblCheckQuestions.TextColor = questions.Count == 3 && answers.Count == 3 ? Color.Green : Color.Gray;
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            if( (caller as SignupAPI) != null){
                //sign up is a success
				Navigation.PushModalAsync(new SetupEasyPinPage());


				//creates a user instance of the login response json
				User u = JsonConvert.DeserializeObject<User>(response["user"].ToString(), new JsonSerializerSettings()
				{
					NullValueHandling = NullValueHandling.Ignore,
					MissingMemberHandling = MissingMemberHandling.Ignore
				});



				//assigns that user instance to be globally accessible on the app.
				Constants.currentUser = u;

				// initiates caching of the logged in user
				Device.StartTimer(TimeSpan.FromSeconds(1), () =>
				{
					Cacher.CacheCurrentUser();
					return false;
				});


            }
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
			if ((caller as SignupAPI) != null)
				DisplayAlert("Signup failed", errMsg, "Okay");
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            throw new NotImplementedException();
        }


        //terms callbacks
        public void didAgree(string termVersion, TermsAndServicesPage termsPage)
        {

            this.termVersion = termVersion;
            checkbox.IsChecked = true;
        }

        public void didDecline(TermsAndServicesPage termsPage)
        {
            checkbox.IsChecked = false;
        }
    }
}
