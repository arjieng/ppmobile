﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using PP.tools.API;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class SignupPhotoUpload : ContentPage,IBaseAPIProgressInterface
    {
        public MediaFile file;

        public ContentPage callerPage;

        public SignupPhotoUpload()
        {
            InitializeComponent();

            CrossMedia.Current.Initialize();
        }

        async void SelectImageAsync(object sender, System.EventArgs e)
        {
            if (IsInProgress())
                return;

			var action = await DisplayActionSheet("Select image from: ", "Cancel", null, "Camera", "Gallery");
			if (action.ToString() == "Camera")
			{
				Log.e("Camera");

				await Task.Delay(10);

				if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
				{
					await DisplayAlert("Not Supported", "No camera detected.", "OK");
					return;
				}

				file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions { CompressionQuality = 92, SaveToAlbum = true });

				if (file != null)
				{
					imgProfile.Source = ImageSource.FromStream(() =>
					{
						var stream = file.GetStream();
						return stream;
					});

				}
			}
			else if (action.ToString() == "Gallery")
			{
				Log.e("Gallery");

				await Task.Delay(10);
				
				file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions { CompressionQuality = 92 });

				if (file != null)
				{
					imgProfile.Source = ImageSource.FromStream(() =>
					{
						var stream = file.GetStream();
						return stream;
					});

				}
			}            
        }
        bool IsInProgress(){
            if(aiLoading.IsVisible){
                DisplayAlert("In Progress","Face verification is progress.. please wait","Okay");
            }

            return aiLoading.IsVisible;
        }

        void SaveImageTapped(object sender, System.EventArgs e)
		{
            if(!IsInProgress())
			    checkImageAsync();
                
		}

		void CancelSelect(object sender, System.EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}

        public void checkImageAsync()
        {

            if (file != null)
            {

                //Constants.isDebug = false;
                aiLoading.IsVisible = true;
                CheckImageFaceAPI api = new CheckImageFaceAPI(Convert.ToBase64String(ReadFully(file.GetStream())));
                api.setCallbacks(this);
                api.getResponse();
            }
            else{
                DisplayAlert("Opps", "Please select an image first.", "Okay");
            }

        }

		public static byte[] ReadFully(Stream input)
		{
			byte[] buffer = new byte[16 * 1024];
			using (MemoryStream ms = new MemoryStream())
			{
				int read;
				while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
				{
					ms.Write(buffer, 0, read);
				}
				return ms.ToArray();
			}
		}

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            //if (file != null) file.Dispose();
            //Log.e("Sad",response+"");
            int faceCount = response["image_count"].Value<Int32>();
            if( faceCount == 0){
                // no face detected
                DisplayAlert("Facial Detection", "It seems no faces were detected. Please try another photo.","Okay");
            }
            else if (faceCount> 1){
				//multiple face detected
                DisplayAlert("Facial Detection", "Multiple faces were detected. Please use a photo that shows only you.", "Okay");
            }
            else{
				//awryt
				if ((callerPage as SignUpPage) != null)
				{
					(callerPage as SignUpPage).didSaveProfileImage(file);
				}
				else if ((callerPage as EditUserProfilePage) != null)
				{
					(callerPage as EditUserProfilePage).didSaveProfileImage(file);
				}
                Navigation.PopModalAsync(true);
            }

            aiLoading.IsVisible = false;
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            
            aiLoading.IsVisible = false;
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {

            aiLoading.IsVisible = false;
        }

        public void OnProgressChanged(long progress, long totalSize, BaseAPI caller)
        {
            Log.e("Tasda", progress+" of "+totalSize);
        }
    }
}
