﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class TutorialPage : ContentPage
    {
        ObservableCollection<string> photoUrls;
        public TutorialPage()
        {
            InitializeComponent();
            photoUrls = new ObservableCollection<string>();



            photoUrls.Add("https://images.pexels.com/photos/370799/pexels-photo-370799.jpeg?h=350&auto=compress&cs=tinysrgb");
            photoUrls.Add("https://image.freepik.com/free-psd/abstract-background-design_1297-73.jpg");
            photoUrls.Add("http://www.kinyu-z.net/data/wallpapers/129/1155464.jpg");
            photoUrls.Add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToLJTQCk6v6-vH0SERjQC4vZpU6DwDWftEst-fJ0V3-NrFiDu6");

            BindingContext = this.photoUrls;
        }
        void NextClicked(object sender, System.EventArgs e)
        {
            if (cvPhotos.Position < photoUrls.Count - 1)
            {
                cvPhotos.Position = cvPhotos.Position + 1;
                if (cvPhotos.Position == photoUrls.Count - 1)
                {
                    (sender as Label).Text = "Done";
                }
            }
            else
            {
                Navigation.PopModalAsync();
            }
        }
    }
}
