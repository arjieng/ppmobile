﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using PP.Pages;
using PP.Pages.Dialogs;
using PP.Pages.Profile;
using PP.tools;
using PP.tools.API;
using PP.tools.API.BaseAPI;
using PP.tools.Sockets;
using Websockets;
using Xamarin.Forms;
using ZXing;
using ZXing.Net.Mobile.Forms;


namespace PP
{
	public partial class SplashScreen : ContentPage, IBaseAPIInterface
	{


        //IWebSocketConnection connection;


        //ActionCableClient client;


        //Loading_Dialog loadDialog;
		public SplashScreen()
		{
            NavigationPage.SetHasNavigationBar(this, false);
			InitializeComponent();

		}

        public void OnError(string errMsg, BaseAPI caller)
        {
			//loadDialog.close();
            DisplayAlert("Failed", errMsg, "Okay");
            aiLoading.IsRunning = false;
            lblLoading.IsVisible = false;
            cvTryAgain.IsVisible = true; 
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
		{
			//loadDialog.close();
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            //loadDialog.close();

            if((caller as CheckTokenAPI) !=null){
                if (response["valid_authentication"].Value<bool>())
                {


                    //SidePanelPage spp = new SidePanelPage();
                    //Application.Current.MainPage = new NavigationPage(spp);

                    checkTermsVersion();

                    //Navigation.PushModalAsync(new EnterEasyPinOrFingerprint());

                }
                else
                {
                    LoginPage p = new LoginPage();
                    Application.Current.MainPage = p;
                }
            }
            else if((caller as FetchTermVersionAPI)!=null){

                if(response["has_new_term"].Value<bool>()){
                    DisplayAlert("Terms Update", "Terms and conditions have been updated, you need to agree on it to continue using this app.","Okay");
                    Navigation.PushModalAsync(new TermsAndServicesPage() { IsAgreeingMode = true});
                }
                else {
                    checkForEasyPin();

                }

            }
            else if ((caller as CheckEasyPinExistAPI) != null){


                Log.e("TAG",response.ToString());

                if(response["has_easy_pin"].Value<bool>()){
                    Navigation.PushModalAsync(new EnterEasyPinOrFingerprint());
                }
                else{
                    SidePanelPage spp = new SidePanelPage();
                    Application.Current.MainPage = new NavigationPage(spp);}

            }
           
			
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();



            // checkRequiredFeatures();

            //loadDialog = new Loading_Dialog();
            check();

        }
   



        async void check(){
            aiLoading.IsRunning = true;
            lblLoading.IsVisible = true;
            cvTryAgain.IsVisible = false;
            Device.BeginInvokeOnMainThread(()=>{
                lblLoading.Text = "Checking required features...";
            });
			
			var s = await Misc.checkRequiredFeatures();

            if (s.returnValue)
            {
				User user = Cacher.GetCachedUser();

                Constants.currentUser = user;
				Device.BeginInvokeOnMainThread(() => {
					lblLoading.Text = "Checking current user...";
				});

                if (user != null){
                    Log.e("sadada", "Already has a user..." + JsonConvert.SerializeObject(user));

                    try{


						

                        Device.BeginInvokeOnMainThread(()=>{

							//loadDialog.Text = "Checking user authenticity...";
							//loadDialog.open();
                            lblLoading.Text = "Checking user authenticity...";

                            CheckTokenAPI api = new CheckTokenAPI(user.token);
                            api.setCallbacks(this);
                            api.getResponse();
                        });

						//SidePanelPage spp = new SidePanelPage();
						//Application.Current.MainPage = new NavigationPage(spp);
                    }
                    catch(NullReferenceException ex){
                        Log.e("sada","sada"+ex.StackTrace);

                    }
					
                }
                else{
					Log.e("sadada", "Dont have a user...");
					//okay
					LoginPage p = new LoginPage();
					Application.Current.MainPage = p;
					return;
                }
            }
            else
            {
    //            await DisplayAlert("Missing", s.errorMessage, "Okay");

    //            //Stream s = DependencyService.Get<IQRCodeCreatorService>().ConvertImageStream(data, codeWidth, codeHeight);

    //            var close = DependencyService.Get<ICloseApplicationService>();
				//if (close != null)
                    //close.closeApplication();

                OnError(s.errorMessage,null);
            }


        }





        //      void _acClient_Connected(object sender, ActionCableEventArgs e)
        //{
        //	System.Diagnostics.Debug.WriteLine("Connected!");
        //	// client.Connected -= _acClient_Connected;
        //	//client.MessageReceived += _acClient_MessageReceived;
        //}

        //void _acClient_MessageReceived(object sender, ActionCableEventArgs e)
        //{
        //	System.Diagnostics.Debug.WriteLine("Received: {0}", e.Message);

        //}







        //void Connection_OnMessage(string strResponse)
        //{
        //	Log.e("sada.", "test: " + strResponse);
        //	return;

        //	JObject jobj = JObject.Parse(strResponse);

        //	if (jobj["type"].Value<String>().Trim().Equals("confirm_subscription"))
        //	{


        //		Device.StartTimer(TimeSpan.FromSeconds(3), () =>
        //		{


        //			//create message command
        //			JObject msg = new JObject();
        //			msg.Add("command", "message");

        //			//create message identifier
        //			JObject msgIdentifier = new JObject();
        //			msgIdentifier.Add("channel", "TransactionsChannel");
        //			msgIdentifier.Add("transaction_id", 1);


        //			msg.Add("identifier", msgIdentifier.ToString());//ws identifier needs the identifier value stringified

        //			//create the actual message data

        //			//JObject data = new JObject();
        //			//data.Add("message", "mao ni akong message " + counter);
        //			//data.Add("test_key", "valueTest");
        //			//msg.Add("data", data);


        //			//Log.e("message params", msg.ToString());

        //			//connection.Send(msg.ToString());
        //			//counter++;
        //			return false;
        //		});
        //	}

        //}

        //void Connection_OnOpened()
        //{

        //	Log.e("sada.", "OPENED!");
        //}


        void TryAgainTapped(object sender, System.EventArgs e)
		{
			
           check();
        }


        void checkTermsVersion(){
            FetchTermVersionAPI api = new FetchTermVersionAPI();
            api.setCallbacks(this);
            api.getResponse();
        }

        /// <summary>
        /// Checks if user have setup easy pin.
        /// </summary>
        void checkForEasyPin(){
            CheckEasyPinExistAPI api = new CheckEasyPinExistAPI(Constants.currentUser.token);
            api.setCallbacks(this);
            api.getResponse();

        }
    }
}
