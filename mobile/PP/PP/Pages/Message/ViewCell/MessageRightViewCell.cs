﻿using System;
using System.Diagnostics;
using FFImageLoading.Forms;
using PP.CustomLayouts;
using Xamarin.Forms;

namespace PP.Pages.Message.ViewCell
{
    public class MessageRightViewCell : ScaledViewCell
    {
        public MessageRightViewCell()
        {
			var image = new CircledImage
			{
				HeightRequest = 30,
				WidthRequest = 30,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Start
			};
			image.SetBinding(CachedImage.SourceProperty, "muserImage");

            var textMess = new Label()
            {
                TextColor = Color.Black,
                FontSize = 13,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Start
			};
			textMess.SetBinding(Label.TextProperty, "mmessage");



            var source = new UriImageSource { CachingEnabled = false };
            source.SetBinding(UriImageSource.UriProperty, "mimage");

            var imageS = new Image
            {
                Source = source,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
            };
            imageS.SetBinding(Image.IsVisibleProperty, "mhasImage");


            var indicator = new ActivityIndicator { Color = Color.Red };
            indicator.SetBinding(ActivityIndicator.IsRunningProperty, "IsLoading");
            indicator.BindingContext = imageS;
            imageS.PropertyChanged += (sender, e) => {
                if (e.PropertyName.Equals("IsLoading"))
                {
                    indicator.IsVisible = imageS.IsLoading;
                }
            };

            var stackview = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Children = { textMess, imageS, indicator }
            };

            View = new ScaledStackLayout
            {
                Padding = new Thickness(10, 5, 10, 30),
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Start,
                Orientation = StackOrientation.Horizontal,
                Children = {
                     new CorneredView {
                        Padding = new Thickness(8,8,8,8),
                        HorizontalOptions = LayoutOptions.EndAndExpand,
                        CornerRadius = 10,
                        BackgroundColor = Color.FromRgb(79, 171, 237),
                        Content = stackview
                    },
                    new BoxView{
                        BackgroundColor = Color.FromRgb(79, 171, 237),
                        WidthRequest = 11 ,
						HeightRequest = 11 ,
                        VerticalOptions = LayoutOptions.Start,
                        HorizontalOptions = LayoutOptions.Start,
                        Rotation = 45,
                        Margin = new Thickness(-13,7,0,0)
                    },
                    image
				}
			};
        }
    }
}

