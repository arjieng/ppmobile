﻿using System;

using Xamarin.Forms;

namespace PP.Pages.Message.ViewCell
{
    public class MessageTemplateSelector : DataTemplateSelector
    {

		private readonly DataTemplate leftDataTemplate;
		private readonly DataTemplate rightDataTemplate;

        public MessageTemplateSelector()
        {
            this.leftDataTemplate = new DataTemplate(typeof(PP.Pages.Message.ViewCell.MessageLeftViewCell));
            this.rightDataTemplate = new DataTemplate(typeof(PP.Pages.Message.ViewCell.MessageRightViewCell));

		}

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
			var messageVm = item as iConversationModel;
			if (messageVm == null)
			{
				return null;
			}
            else
            {
                if (messageVm.mnotYours)
                {
                    return this.leftDataTemplate;
                }
                else
                {
                    return this.rightDataTemplate;
                }
            }
        }
    }
}

