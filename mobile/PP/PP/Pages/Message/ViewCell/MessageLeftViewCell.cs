﻿using System;
using FFImageLoading.Forms;
using PP.CustomLayouts;
using Xamarin.Forms;

namespace PP.Pages.Message.ViewCell
{
    public class MessageLeftViewCell : ScaledViewCell
    {
        public MessageLeftViewCell()
        {
            var image = new CircledImage
            {
                HeightRequest = 30,
                WidthRequest = 30,
				HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
            };
            image.SetBinding(CachedImage.SourceProperty, "muserImage");

			var textMess = new Label()
			{
                TextColor = Color.Black,
				FontSize = 13,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
			};
			textMess.SetBinding(Label.TextProperty, "mmessage");

            var source = new UriImageSource
            {
                CachingEnabled = false
            };
            source.SetBinding(UriImageSource.UriProperty, "mimage");



			var imageS = new Image
			{
                Source = source,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Start,
			};
			imageS.SetBinding(Image.IsVisibleProperty, "mhasImage");


            var indicator = new ActivityIndicator { Color = Color.Red };
            indicator.SetBinding(ActivityIndicator.IsVisibleProperty, "mhasImage");
            indicator.SetBinding(ActivityIndicator.IsRunningProperty, "IsLoading");
            indicator.BindingContext = imageS;
            imageS.PropertyChanged += (sender, e) => {
                if(e.PropertyName.Equals("IsLoading")){
                    indicator.IsVisible = imageS.IsLoading;
                }
            };


			var stackview = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
                Children = { textMess, imageS, indicator }
			};

            View = new ScaledStackLayout
            {
                Padding = new Thickness(10,5,10,30),
                HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start,
				Orientation = StackOrientation.Horizontal,
                Children = {
                   image,
					 new BoxView{
						BackgroundColor = Color.FromHex("#C0BBBB"),
						WidthRequest = 11 ,
						HeightRequest = 11 ,
						VerticalOptions = LayoutOptions.Start,
						HorizontalOptions = LayoutOptions.Start,
						Rotation = 45,
						Margin = new Thickness(0,7,-13,0)
					},
                    new CorneredView {
                        Padding = new Thickness(8,8,8,8),
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        CornerRadius = 10,
                        BackgroundColor = Color.FromHex("#C0BBBB"),
                        Content = stackview
                    },
                }
            };
        }
    }
}

