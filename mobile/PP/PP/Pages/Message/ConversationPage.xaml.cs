﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using PP.Pages.Message.ViewCell;
using PP.tools.API;
using PP.tools.Sockets;
using Xamarin.Forms;

namespace PP.Pages.Message
{
    public partial class ConversationPage : ContentPage,IBaseAPIInterface
    {
        public ObservableCollection<iConversationModel> MessagesConvo { get; set; }
        private bool _sendClicked = true;

        MediaFile imageFile;

        string IDtran;


        public ConversationPage(string name ,string currentTransactionId)
        {
            // Data template selector

            MessagesConvo = new ObservableCollection<iConversationModel>();
			Resources = new ResourceDictionary();
			Resources.Add("MessageTemplateSelector", new MessageTemplateSelector());

            this.Title = name;
            NavigationPage.SetHasNavigationBar(this,true);
			InitializeComponent();
            IDtran = currentTransactionId;
            //xheader.IsVisible = !Device.RuntimePlatform.ToLower().Trim().Equals("android");
            ActionCableClient acc = Constants.accTransactionsChannel;

           
            NameTxt.Text = name;
            messageList.ItemTemplate = (DataTemplate)Resources["MessageTemplateSelector"];
            messageList.SeparatorVisibility = SeparatorVisibility.None;
            messageList.HasUnevenRows = true;
            messageList.HeightRequest = App.screenHeight - 150;
            messageList.ItemsSource = MessagesConvo;
            messageList.ItemSelected += (sender, e) => {
                if (e.SelectedItem == null) return;
				// do something with e.SelectedItem
				((ListView)sender).SelectedItem = null;
            };
			//entryMessage.Focus();
            entryMessage.Unfocused += (sender, e) => {
                if (Device.RuntimePlatform.ToLower().Trim().Equals("android"))
                {
                    xEntryview.TranslateTo(0, 0);
                    messagContainer.TranslateTo(0, 0);
                }
                else
                {
					//xEntryview.TranslateTo(0, 0);
					//messageList.TranslateTo(0, 0);
                }
                //xheader.TranslateTo(0,0);
                //if (_sendClicked) {
                //    entryMessage.Focus();

                //     _sendClicked = false;
                //}
            };
            entryMessage.Focused += (sender, e) => {
                if (Device.RuntimePlatform.ToLower().Trim().Equals("android"))
                {
                    //xheader.TranslateTo(0, 300);
                    xheader.TranslateTo(0, 0);
                    xEntryview.TranslateTo(0, -250);
                    messagContainer.TranslateTo(0, -250);

                    //Action<double> callback = input => messagContainer.HeightRequest = input;
                    //double startingHeight = messagContainer.Height, endingHeight = 0;
                    //uint rate = 16, length = 3000;
                    //Easing easing = Easing.CubicOut;
                    //messagContainer.Animate("h", callback, startingHeight, endingHeight, rate, length, easing);

                    try
                    {
                        messageList.ScrollTo(MessagesConvo.Last(), ScrollToPosition.MakeVisible, true);
                    }
                    catch (Exception) { }
                }
                else
                {
					//xEntryview.TranslateTo(0, -275);
                    //messageList.TranslateTo(0, -275);
					xheader.TranslateTo(0, 0);
					try
					{
						messageList.ScrollTo(MessagesConvo.Last(), ScrollToPosition.MakeVisible, true);
					}
					catch (Exception) { }
                }
            };
            acc.MessageReceived += (sender, e) => 
            {
                try
                {
                    System.Diagnostics.Debug.WriteLine("Recieved" + e.Message);
                    var item = JObject.Parse(e.Message);
                    MessagesConvo.Add(new iConversationModel { 
                        mid = 1, 
                        mmessage = item["data"]["conversation"]["body"].ToString(), 
                        muserImage = Constants.ROOT_URL + item["data"]["conversation"]["from"]["image"].ToString(), 
                        mnotYours = !(Constants.currentUser.id == item["data"]["conversation"]["from"]["id"].ToString()),
                        mhasImage = !string.IsNullOrEmpty(item["data"]["conversation"]["image"].ToString()),
                        mimage = Constants.ROOT_URL+item["data"]["conversation"]["image"].ToString() });
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            };

            acc.Disconnected += (sender, e) => {
                acc.ConnectAsync();
            };

            var tapC = new TapGestureRecognizer();
            tapC.Tapped += async (sender, e) =>
            {
                await CrossMedia.Current.Initialize();
                var file = await CrossMedia.Current.PickPhotoAsync();

                imageFile = file;
                ResultImage.Source = file.Path;
                ResultImage.IsVisible = true;

            };
            icamera.GestureRecognizers.Add(tapC);

			var tapg = new TapGestureRecognizer();
			tapg.Tapped += (sender, e) => {
				Navigation.PopModalAsync();
			};
            backx.GestureRecognizers.Add(tapg);


            var tapS = new TapGestureRecognizer();
            tapS.Tapped += async (sender, e) =>
            {
                _sendClicked = true;
                if (!string.IsNullOrEmpty(entryMessage.Text))
                {
                    try
                    {
                        string image64 = "";
                        if (imageFile != null)
                        {
                            var stream = imageFile.GetStream();
                            var bytes = new byte[stream.Length];
                            await stream.ReadAsync(bytes, 0, (int)stream.Length);
                            image64 = System.Convert.ToBase64String(bytes);
                        }
                        JObject canData = new JObject();
                        canData.Add("auth_token", Constants.currentUser.token);
                        canData.Add("user_transaction_id", currentTransactionId);
                        canData.Add("message", entryMessage.Text);
                        canData.Add("image", image64);
                        canData.Add("action", "send_message");

                        acc.Perform(canData);

                        entryMessage.Text = "";
                        image64 = "";
                        imageFile = null;
                        ResultImage.IsVisible = false;
                        entryMessage.Focus();
                        messageList.ScrollTo(MessagesConvo.Last(), ScrollToPosition.MakeVisible, true);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };
            xSend.GestureRecognizers.Add(tapS);

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

   //         MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hello", muserImage = "https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg", mnotYours = true });
   //         MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hi ,Okay", muserImage = "http://wallpaper-gallery.net/images/image/image-3.jpg", mnotYours = false });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hello", muserImage = "https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg", mnotYours = true });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hi ,Okay", muserImage = "http://wallpaper-gallery.net/images/image/image-3.jpg", mnotYours = false });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hello", muserImage = "https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg", mnotYours = true });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hi ,Okay", muserImage = "http://wallpaper-gallery.net/images/image/image-3.jpg", mnotYours = false });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hello", muserImage = "https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg", mnotYours = true });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hi ,Okay", muserImage = "http://wallpaper-gallery.net/images/image/image-3.jpg", mnotYours = false });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hello", muserImage = "https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg", mnotYours = true });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hi ,Okay", muserImage = "http://wallpaper-gallery.net/images/image/image-3.jpg", mnotYours = false });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hello", muserImage = "https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg", mnotYours = true });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hi ,Okay", muserImage = "http://wallpaper-gallery.net/images/image/image-3.jpg", mnotYours = false });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hello", muserImage = "https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg", mnotYours = true });
			//MessagesConvo.Add(new iConversationModel { mid = 1, mmessage = "Hi ,Okay", muserImage = "http://wallpaper-gallery.net/images/image/image-3.jpg", mnotYours = false });

			//var request = await App.help.getRequest(Constants.ROOT_URL+"/api/v1/users/transactions/"+IDtran+"/conversations?auth_token="+Constants.currentUser.token);

			FetchMessages fecth = new FetchMessages(IDtran);
            fecth.setCallbacks(this);
            fecth.getResponse();

        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            System.Diagnostics.Debug.WriteLine(response);

            foreach(var item in response["conversations"])
            {
				MessagesConvo.Add(new iConversationModel
				{
					mid = 1,
					mmessage = item["body"].ToString(),
					muserImage = Constants.ROOT_URL + item["from"]["image"].ToString(),
					mnotYours = !(Constants.currentUser.id == item["from"]["id"].ToString()),
					mhasImage = !string.IsNullOrEmpty(item["image"].ToString()),
					mimage = Constants.ROOT_URL + item["image"].ToString()
				});
            }
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            //throw new NotImplementedException();
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            //throw new NotImplementedException();
        }
    }


    public class iConversationModel
    {
        public int mid { get; set; }
		public string mmessage { get; set; }
		public string muserImage { get; set; }
		public bool mnotYours { get; set; }
        public bool mhasImage { get; set; }
        public string mimage { get; set; }
    }
}
