﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PP.tools.models;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class ProductImagesViewerPage : ContentPage
    {
        ObservableCollection<ProductImage> _mImages;
        public ObservableCollection<ProductImage> Images{
            set
            {
                _mImages = value;
                //OnPropertyChanged("MyItemsSource");
            }
            get
            {
                return _mImages;
            }
        }
        int _mIndex = 0;
        void back(object sender, System.EventArgs e)
        {

            Navigation.PopModalAsync(true);

        }
      
        public ProductImagesViewerPage(ObservableCollection<ProductImage> mImages, int index){
            InitializeComponent();

            Images = mImages;



            foreach(ProductImage pi in Images){
                pi.Url = Constants.ROOT_URL + pi.Url;
            }

            _mIndex = index;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            cvPhotos.ItemsSource = null;
            cvPhotos.ItemsSource = Images;
            cvPhotos.Position = 0;
            cvPhotos.Position = _mIndex;
                
        }

    }
}
