﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class ReferAfriendPage : ContentPage
    {
        public ReferAfriendPage()
        {
            InitializeComponent();
        }
        void back(object sender, System.EventArgs e)
        {
            Constants.shouldStopIdleTimer = false;
            Navigation.PopModalAsync(true);
        }

        protected override bool OnBackButtonPressed()
        {
            back(null,null);
            return true;
        }

        public static bool emailIsValid(string email)
        {
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, string.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        void Refer_Tapped(object sender, System.EventArgs e)
        {
           
                if (entryFriendName.Text != null)
                {
                    if ( entryFriendEmail.Text != null)
                    {
                        if (emailIsValid(entryFriendEmail.Text))
                        {

                        }
                        else
                        {
                            DisplayAlert("Invalid", "Please enter a valid email address", "Okay");
                        }
                    }
                    else
                    {
                        DisplayAlert("Invalid", "Friend email cannot be empty", "Okay");
                    }
                }
                else
                {
                    DisplayAlert("Invalid", "Friend name cannot be empty", "Okay");
                }

        }
    }
}
