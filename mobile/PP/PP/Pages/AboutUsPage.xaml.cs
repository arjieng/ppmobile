﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class AboutUsPage : ContentPage
    {
        public AboutUsPage()
        {
            InitializeComponent();
            AppBuildVersion();
            lblAppName.Text = AppName;
            lblCompany.Text = AppCompany;
        }
        void TappedBackPage(object sender, System.EventArgs e)
        {
            Constants.shouldStopIdleTimer = false;
            Navigation.PopModalAsync(true);
        }

        protected override bool OnBackButtonPressed()
        {
            TappedBackPage(null,null);
            return true;
        }

        public void AppBuildVersion()
        {
            // get app assembly attributes
            var assembly = typeof(AboutUsPage).GetTypeInfo().Assembly;
            // get name and version from assembly
            string appnameandversion = assembly.FullName;
            // split string to get the version string
            string[] splitString = appnameandversion.Split(',');
            // get the string with the version number
            string splitIntoVersion = splitString[1];
            // split the string into version and number components
            string[] numversion = splitIntoVersion.Split('=');
            // get the build version number n.n.n.n format
            string version = numversion[1];
            // display build version number
            lblVersion.Text = version;
        }
        public string AppName
        {
            get
            {
                object[] companyAttributes = (object[])typeof(AboutUsPage)
                    .GetTypeInfo().Assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute));

                return ((AssemblyTitleAttribute)companyAttributes[0]).Title;
            }
        }

        public string AppCompany
        {
            get
            {
                object[] companyAttributes = (object[])typeof(AboutUsPage)
                    .GetTypeInfo().Assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute));

                return ((AssemblyCompanyAttribute)companyAttributes[0]).Company;
            }
        }
    }
}
