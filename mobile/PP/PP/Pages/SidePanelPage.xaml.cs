﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace PP.Pages
{
    public partial class SidePanelPage : MasterDetailPage
    {

		TransparentNavBarPage navpage;

        public SidePanelPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);


            Master = new MenuPage() { Title = ".", Icon = "ic_menu",AutomationId = "MenuTap" };


            navpage = new TransparentNavBarPage(new BuyerSellerPage());
			navpage.barColor = Color.FromHex("#0197ED");
            navpage.BarTextColor = Color.White;
			Detail = navpage;



			this.BackgroundColor = Color.Aqua;
			IsPresentedChanged += SidepanelPage_IsPresentedChanged;
        }

		void SidepanelPage_IsPresentedChanged(object sender, EventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("IsPresented = {0}", IsPresented);
			if (IsPresented)
			{
                navpage.ScaleTo(1, 250, Easing.Linear);
			}
			else
			{
				navpage.ScaleTo(1, 250, Easing.Linear);
			}
		}
    }
}
