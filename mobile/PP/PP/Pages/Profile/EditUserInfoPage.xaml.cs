﻿using System;
using System.Collections.Generic;
using FFImageLoading.Forms;
using Newtonsoft.Json.Linq;
using PP.tools.API;
using Xamarin.Forms;

namespace PP
{
	public partial class EditUserInfoPage : ContentPage,IBaseAPIInterface
	{
        public EditUserInfoPage(List<object> data)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            if(data.Count>0){

				lblProduct.Text = data[0].ToString();
				entryCost.Text = data[1].ToString();
				entrySelling.Text = data[2].ToString();
				entryQuantity.Text = data[3].ToString();

				List<string> lists = data[4] as List<string>;
				var scale = 55;


				stkPhotos.Children.Clear();
				foreach (var item in lists)
				{
					stkPhotos.Children.Add(
					new CachedImage
					{
						Source = item,
						WidthRequest = scale,
						HeightRequest = scale
					});
                }
            }
           




            btnSubmit.Clicked += (sender, e) => {
                //EditProduct edit = new EditProduct(Convert.ToSingle(entryCost.Text), Convert.ToSingle(entrySelling.Text), Convert.ToSingle(entryQuantity.Text));
                //edit.setCallbacks(this);
                //edit.getResponse();
            };
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            DisplayAlert("PP App","Something went wrong. \nPlease try again later","Okay");
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            throw new NotImplementedException();
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            Navigation.PopAsync();
        }

        private void TappedBackPage(object sender, System.EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}
	}
}
