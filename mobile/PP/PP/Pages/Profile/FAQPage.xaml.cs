﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace PP.Pages.Profile
{
    public partial class FAQPage : ContentPage
    {
        public FAQPage()
        {
            InitializeComponent();
        }
		void TappedBackPage(object sender, System.EventArgs e)
        {
            Constants.shouldStopIdleTimer = false;
			Navigation.PopModalAsync(true);
		}
        protected override bool OnBackButtonPressed()
        {
            TappedBackPage(null,null);
            return true;
        }
    }
}
