﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using PP.Pages.Dialogs;
using PP.Pages.Profile;
using PP.tools;
using PP.tools.API;
using PP.tools.API.BaseAPI;
using Xamarin.Forms;

namespace PP
{
    public partial class ProfilePage : ContentPage, IBaseAPIInterface
    {

        List<string> photos = new List<string>();
        MediaFile file;
        Loading_Dialog loadDialog;
        StackLayout addImage;
        JObject userInfo, userProduct;
        JArray imageChange = new JArray();

        string ProfileBase64 = "", UserImageUrl = "", ProductID = "";
        bool hasProduct, productEditButton_isClicked;
        int temp_id = 0;


        ObservableCollection<FeedBackModel> feedbacks = new ObservableCollection<FeedBackModel>();

        public ProfilePage()
        {
            
            InitializeComponent();
            loadDialog = new Loading_Dialog();
            addImage = addImageNewProduct;
        }

        private async void ProfileImage_Tapped(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();
            try
            {
                EditProfile_Tapped(null, null);
                file = await CrossMedia.Current.PickPhotoAsync();
                if (file != null)
                {
                    var stream = file.GetStream();
                    string image64 = "";


                    var bytes = new byte[stream.Length];
                    await stream.ReadAsync(bytes, 0, (int)stream.Length);
                    image64 = System.Convert.ToBase64String(bytes);
                
                    ProfileBase64 = image64;
                    imgProfileImage.Source = ImageSource.FromStream(() => { return stream; });
                }
                file = null;
            }
            catch (Exception ex)
            {
                CancelEditProfile_Tapped(null, null);
            }
        }

        async void SaveProductDetail_Tapped(object sender, EventArgs e){
            if(hasProduct){
                loadDialog.Text = "Updating...";
                loadDialog.open();
                EditProduct edit = new EditProduct(ProductID, Convert.ToSingle(costPerItem.Text.Replace("$,", "")), Convert.ToSingle(sellingPrice.Text.Replace("$", "")), Convert.ToSingle(qtyOnHand.Text), imageChange);
                edit.setCallbacks(this);
                edit.getResponse();
            }else{
                if (string.IsNullOrEmpty(productName.Text))
                {
                    productName.Placeholder = "Product name is needed";
                    productName.PlaceholderColor = Color.Red;
                }
                else
                {
                    loadDialog.Text = "Creating...";
                    loadDialog.open();
                    CreateProduct create = new CreateProduct(productName.Text, Convert.ToSingle(costPerItem.Text.Replace("$,", "")), Convert.ToSingle(sellingPrice.Text.Replace("$", "")), Convert.ToSingle(qtyOnHand.Text), imageChange);
                    create.setCallbacks(this);
                    create.getResponse();
                }
            }
        }

        async void AddProductImage_Tapped(object sender, EventArgs e){
            productEditButton_isClicked = true;
            if(!string.IsNullOrEmpty(productName.Text)){
                await CrossMedia.Current.Initialize();
                try{
                    file = await CrossMedia.Current.PickPhotoAsync();
                    if(file != null){
                        var stream = file.GetStream();
                        var temp = temp_id;
                        string image64 = "";

                        var bytes = new byte[stream.Length];
                        await stream.ReadAsync(bytes, 0, (int)stream.Length);
                        image64 = System.Convert.ToBase64String(bytes);

                        var itemChange = new JObject();
                        itemChange.Add("temp_id", temp_id);
                        itemChange.Add("image", image64);

                        imageChange.Add(itemChange);
                        temp_id++;

                        var img = new ImageWithDelete(ImageSource.FromStream(() => { return file.GetStream(); }))
                        {
                            WidthRequest = 70,
                            HeightRequest = 70
                        };
                        img.CloseTapped += (sende, ev) => {
                            var imageWithDelete = (sende as ImageWithDelete);
                            removeThisImage(imageWithDelete.ID, imageWithDelete, temp);
                        };

                        if(stkPhotos.Children.Count == 3){
                            stkPhotos.Children.RemoveAt(2);
                            stkPhotos.Children.Add(img);
                        }else{
                            stkPhotos.Children.Insert(0, img);
                        }
                        stream = null;
                        image64 = null;
                        productSaveButton.IsVisible = true;
                    }
                    file = null;
                }catch(Exception ex){
                    ex.ToString();
                }
            }else{
                productName.Placeholder = "Product name is needed.";
                productName.PlaceholderColor = Color.Red;
            }
        }

        void fetchUserDetails()
        {
            FetchProfileDetailsAPI detail = new FetchProfileDetailsAPI(Constants.currentUser.id, Constants.currentUser.token);
            detail.setCallbacks(this);
            detail.getResponse();
        }

        void SaveProfile_Tapped(object sender, EventArgs e){
            bool hasNoError = true;
            if(string.IsNullOrEmpty(firstNameEntry.Text)){
                firstNameEntry.Placeholder = "Cannot be null";
                firstNameEntry.PlaceholderColor = Color.Red;
                hasNoError = false;
            }
            if(string.IsNullOrEmpty(lastNameEntry.Text)){
                lastNameEntry.Placeholder = "Cannot be null";
                lastNameEntry.PlaceholderColor = Color.Red;
                hasNoError = false;
            }
            if(hasNoError){
                loadDialog.Text = "Updating...";
                loadDialog.open();

                EditProfile edit = new EditProfile(firstNameEntry.Text, lastNameEntry.Text, ProfileBase64);
                edit.setCallbacks(this);
                edit.getResponse();
            }

        }

        void ViewFeedBacksTapped(object sender, EventArgs args){
            Navigation.PushModalAsync(new ViewFeedbackPage(feedbacks.ToList()));
        }

        void EditProfile_Tapped(object sender, EventArgs e){
            profileBox.IsVisible = false;
            editNameBox.IsVisible = true;
        }

        void PencilDark_Tapped(object sender, EventArgs e){
            if(!productEditButton_isClicked){
                productEditButton_isClicked = true;

                costPerItemLabel.IsVisible = false;
                sellingPriceLabel.IsVisible = false;
                quantityOnHandLabel.IsVisible = false;
                if (string.IsNullOrEmpty(productName.Text))
                {
                    productNameLabel.IsVisible = false;
                    productName.IsVisible = true;
                }
                costPerItem.IsVisible = true;
                sellingPrice.IsVisible = true;
                qtyOnHand.IsVisible = true;
                productSaveButton.IsVisible = true;
            }else{
                productEditButton_isClicked = false;

                productName.IsVisible = false;
                costPerItem.IsVisible = false;
                sellingPrice.IsVisible = false;
                qtyOnHand.IsVisible = false;
                productSaveButton.IsVisible = false;

                productNameLabel.IsVisible = true;
                costPerItemLabel.IsVisible = true;
                sellingPriceLabel.IsVisible = true;
                quantityOnHandLabel.IsVisible = true;


            }

        }

        void CancelEditProfile_Tapped(object sender, EventArgs e)
        {
            editNameBox.IsVisible = false;
            profileBox.IsVisible = true;
        }

        void TappedBackPage(object sender, System.EventArgs e)
        {
            Constants.shouldStopIdleTimer = false;
            Navigation.PopModalAsync(true);
        }

        protected override bool OnBackButtonPressed()
        {
            TappedBackPage(null, null);
            return true;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            fetchUserDetails();
        }

        void removeThisImage(int ID, View v, int? temp = null)
        {
            //saveDetail.IsVisible = true;
            var itemChange = new JObject();
            if (temp.HasValue)
            {
                imageChange.Remove(imageChange.Where(x => (int?)x["temp_id"] == temp).First());
            }
            else
            {
                itemChange.Add("id", ID);
                imageChange.Add(itemChange);
                System.Diagnostics.Debug.WriteLine("dsada" + ID);
            }
            stkPhotos.Children.Remove(v);

            if (stkPhotos.Children.Count != 3)
            {
                stkPhotos.Children.Add(addImage);
            }

            productEditButton_isClicked = true;
            productSaveButton.IsVisible = true;
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            Device.BeginInvokeOnMainThread(() => {
                loadDialog.close();
            });

            try
            {
                if (caller.GetType() == typeof(EditProfile))
                {
                    if (response["status"].ToString() == "200")
                    {
                        fullName.Text = firstNameEntry.Text + " " + lastNameEntry.Text;
                        CancelEditProfile_Tapped(null, null);
                    }else{
                        DisplayAlert("", response["success"].ToString(), "OK");
                    }
                }

                if (caller.GetType() == typeof(EditProduct))
                {
                    costPerItemLabel.Text = "$" + costPerItem.Text;
                    sellingPrice.Text = "$" + sellingPrice.Text;
                    quantityOnHandLabel.Text = qtyOnHand.Text;
                    PencilDark_Tapped(null, null);
                }

                userInfo = response["user"].Value<JObject>();
                userProduct = response["product"].Value<JObject>();

                Debug.WriteLine(response.ToString());

                if (userInfo != null)
                {

                    UserImageUrl = Constants.ROOT_URL + userInfo["image"].Value<string>();
                    imgProfileImage.Source = UserImageUrl;

                    User u = Constants.currentUser;
                    u.first_name = userInfo["first_name"].Value<string>();
                    u.last_name = userInfo["last_name"].ToString();
                    u.email = userInfo["email"].ToString();
                    u.image = UserImageUrl;
                    Cacher.CacheCurrentUser();

                    string firstName = userInfo["first_name"].ToString(), lastName = userInfo["last_name"].ToString();

                    fullName.Text = firstName + " " + lastName;
                    email.Text = userInfo["email"].ToString();
                    firstNameEntry.Text = firstName;
                    lastNameEntry.Text = lastName;

                    int nStars = userInfo["rating"].ToObject<int>();

                    nStars = (nStars < 0) ? 0 : nStars;

                    //lblStar.Text = nStars.ToString();

                    stkImageStar.Rating = nStars;


                }


                if (userProduct != null)
                {
                    hasProduct = true;

                    string productNameString = userProduct["name"].ToString(), costPerItemString = userProduct["cost"].ToString(),
                    sellingPriceString = userProduct["selling_price"].ToString(), quantityOnHandString = userProduct["quantity"].ToString();

                    productNameLabel.Text = productNameString;
                    costPerItemLabel.Text = "$" + costPerItemString;
                    sellingPriceLabel.Text = "$" + sellingPriceString;
                    quantityOnHandLabel.Text = quantityOnHandString;

                    ProductID = userProduct["id"].ToString();
                    var scale = 55;

                    productName.Text = productNameString;
                    costPerItem.Text = costPerItemString;
                    sellingPrice.Text = sellingPriceString;
                    qtyOnHand.Text = quantityOnHandString;

                    stkPhotos.Children.Clear();
                    photos.Clear();
                    foreach (var item in userProduct["images"])
                    {
                        photos.Add(Constants.ROOT_URL + item["url"].Value<string>());
                        var itemImage = new ImageWithDelete(Constants.ROOT_URL + item["url"].ToString())
                        {
                            ID = item["id"].ToObject<int>(),
                            imageSource = Constants.ROOT_URL + item["url"].ToString(),
                            WidthRequest = 70,
                            HeightRequest = 70
                        };
                        itemImage.CloseTapped += (sender, e) => {
                            System.Diagnostics.Debug.WriteLine("Tappde");
                            System.Diagnostics.Debug.WriteLine("imageSource: " + (sender as ImageWithDelete).imageSource);
                            removeThisImage((sender as ImageWithDelete).ID, (sender as ImageWithDelete));
                        };
                        stkPhotos.Children.Add(itemImage);
                    }

                    if (stkPhotos.Children.Count != 3)
                    {
                        stkPhotos.Children.Add(addImage);
                    }
                }

                feedbacks.Clear();
                foreach (var ii in response["feedbacks"])
                {
                    feedbacks.Add(new FeedBackModel
                    {
                        userId = ii["user_id"].ToObject<int>(),
                        image = Constants.ROOT_URL + ii["image"].ToObject<string>(),
                        name = ii["name"].ToObject<string>(),
                        rate = ii["rate"].ToObject<int>(),
                        message = ii["message"].ToObject<string>(),
                    });
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Something is wrong");
            }
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            Device.BeginInvokeOnMainThread(() => {
                loadDialog.close();
            });
            DisplayAlert("Error", errMsg, "OK");
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            Device.BeginInvokeOnMainThread(() => {
                loadDialog.close();
            });
            DisplayAlert(errorCode.ToString(), "Error", "OK");
        }
    }

}