﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace PP.Pages.Profile
{
    public partial class SettingsPage : ContentPage
    {
        public SettingsPage()
        {
            InitializeComponent();
        }
		void TappedBackPage(object sender, System.EventArgs e)
		{

            Constants.shouldStopIdleTimer = false;
			Navigation.PopModalAsync(true);
		}

        protected override bool OnBackButtonPressed()
        {
            TappedBackPage(null,null);
            return true;
        }

        public void ChangeEasyPinTapped(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new SetupEasyPinPage());
        }

        void FAQ_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new FAQPage());
            stopHomeTimer();
        }
        void Help_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new HelpPage());
            stopHomeTimer();
        }

        void AboutUs_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new AboutUsPage());
            stopHomeTimer();
        }
        void Terms_Tapped(object sender, System.EventArgs e)
        {
            stopHomeTimer();

            Navigation.PushModalAsync(new TermsAndServicesPage());
        }


        void DeactivateAccountTapped(object sender, System.EventArgs e)
        {
            stopHomeTimer();

            Navigation.PushModalAsync(new TermsAndServicesPage());
        }
        void stopHomeTimer()
        {
            //var parent = (Application.Current.MainPage as NavigationPage).CurrentPage as SidePanelPage;
            //BuyerSellerPage home = (parent.Detail as TransparentNavBarPage).CurrentPage as BuyerSellerPage;

            Constants.shouldStopIdleTimer = true;
        }
    }
}
