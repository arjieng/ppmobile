﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using PP.Pages;
using PP.tools.API;
using PP.tools.API.BaseAPI;
using Xamarin.Forms;

namespace PP
{
	public partial class EditUserProfilePage : ContentPage,IBaseAPIInterface
	{
		MediaFile file;

        public EditUserProfilePage(List<string> user)
		{
			NavigationPage.SetHasNavigationBar(this, false);
			InitializeComponent();

            if(user.Count>0){
				entryFirstName.Text = user[0];
				entryLastName.Text = user[1];
				lblEmailAdd.Text = user[2];
				imgProfileImage.Source = user[4];
				lblStar.Text = user[3];
                stkImageStar.Rating = Convert.ToInt32(user[3]);
				//FetchedData(Convert.ToInt32(user[3]));
            }


            //btnSubmit.Clicked += (sender, e) => {
            //    EditProfile edit = new EditProfile(entryFirstName.Text, entryLastName.Text);
            //    edit.setCallbacks(this);
            //    edit.getResponse();
            //};
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			//imgProfileImage.GestureRecognizers.Add(new TapGestureRecognizer
			//{
			//	//Command = new Command(() =>
			//	//{
			//	//	TappedProfileImage();
			//	//})

			//});

			entryFirstName.TextChanged += TextChangedProperty;
			entryLastName.TextChanged += TextChangedProperty;
		}

        void Submit_Clicked(object sender, System.EventArgs e)
        {
			EditProfile edit = new EditProfile(entryFirstName.Text, entryLastName.Text, "");
			edit.setCallbacks(this);
			edit.getResponse();
        }


		protected override void OnDisappearing()
		{
			base.OnDisappearing();

			imgProfileImage.GestureRecognizers.Clear();
			entryFirstName.TextChanged -= TextChangedProperty;
			entryLastName.TextChanged -= TextChangedProperty;
		}

		private void TappedBackPage(object sender, System.EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}

        async void ProfileImageTapped(object sender, System.EventArgs e)
        {
            try
            {
                await Task.Delay(10);

                (imgProfileImage.GestureRecognizers[0] as TapGestureRecognizer).Tapped -= ProfileImageTapped;
                //imgProfileImage.GestureRecognizers.Clear();

                var caller = this;
                await Navigation.PushModalAsync(new SignupPhotoUpload() { callerPage = caller });

                TapGestureRecognizer tap = new TapGestureRecognizer();
                tap.Tapped += ProfileImageTapped;

                imgProfileImage.GestureRecognizers.Add(tap);
            }
            catch(Exception ex){
                Log.e("PhotoUploadError", ex.StackTrace);
                Log.e("RootException", ex.GetBaseException().StackTrace);
            }


			//var action = await DisplayActionSheet("Select image from: ", "Cancel", null, "Camera", "Gallery");
			//if (action.ToString() == "Camera")
			//{
			//	Log.e("Camera");
			//	TakePhoto();
   //             //checkImageAsync();
			//}
			//else if (action.ToString() == "Gallery")
			//{
			//	Log.e("Gallery");
			//	PickPhoto();
			//	//checkImageAsync();
			//}
        }

  //      private void TappedProfileImage()
		//{
		//	//Navigation.PushModalAsync(new SignupPhotoUpload() { callerPage = this });
  //          //
		//	//var action = await DisplayActionSheet("Select image from: ", "Cancel", null, "Camera", "Gallery");
		//	//if (action.ToString() == "Camera")
		//	//{
		//	//	Log.e("Camera");
		//	//	TakePhoto();
		//	//	checkImageAsync();
		//	//}
		//	//else if (action.ToString() == "Gallery")
		//	//{
		//	//	Log.e("Gallery");
		//	//	PickPhoto();
		//	//	checkImageAsync();
		//	//}
		//}

		private void TextChangedProperty(object sender, TextChangedEventArgs e)
		{
			Entry ent = (Entry)sender;
			char[] typedText = ent.Text.ToCharArray();
			string result = "";
			for (int i = 0; i < typedText.Length; i++)
			{
				if (i == 0)
				{
					result += typedText[i].ToString().ToUpper();
				}
				else if (typedText[i - 1] == ' ')
				{
					result += typedText[i].ToString().ToUpper();
				}
				else {
					result += typedText[i];
				}
			}
			ent.Text = result;
		}

		async private void TakePhoto()
		{
			await CrossMedia.Current.Initialize();
			try
			{
				if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
				{
					await DisplayAlert("Not Supported", "No camera detected.", "OK");
					return;
				}

				file = await CrossMedia.Current.TakePhotoAsync(
					new Plugin.Media.Abstractions.StoreCameraMediaOptions
					{
						SaveToAlbum = true
					}
				);

				if (file != null)
				{
					//this.hasNewImage = true;

					var stream = file.GetStream();

					imgProfileImage.Source = ImageSource.FromStream(() =>
					{
						return stream;
					});
				}
			}
			catch (Exception ex)
			{
				ex.ToString();
			}
		}

		public void didSaveProfileImage(MediaFile file)
		{

			this.file = file;
			imgProfileImage.Source = ImageSource.FromStream(() =>
			 {
				 var stream = file.GetStream();
				 return stream;
			 });
		}

		async private void PickPhoto()
		{
			await CrossMedia.Current.Initialize();
			try
			{
				//if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsPickPhotoSupported)
				//{
				//	await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
				//	return;
				//}

				file = await CrossMedia.Current.PickPhotoAsync();

				if (file != null)
				{
					//this.hasNewImage = true;

					var stream = file.GetStream();

					imgProfileImage.Source = ImageSource.FromStream(() =>
					{
						return stream;
					});
				}
			}
			catch (Exception ex)
			{
				ex.ToString();
			}
		}

		//private void FetchedData(int ii)
		//{
		//	var nStars = ii;

		//	for (var i = 1; i <= 5; i++)
		//	{
		//		if (i <= nStars)
		//		{
		//			stkImageStar.Children.Add(
		//				new Image
		//				{
		//					Source = "starGreen.png",
		//					WidthRequest = 15
		//				}
		//			);
		//		}
		//		else {
		//			stkImageStar.Children.Add(
		//				new Image
		//				{
		//					Source = "starGray.png",
		//					WidthRequest = 15
		//				}
		//			);
		//		}
		//	}
		//}

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            Navigation.PopAsync();
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            DisplayAlert("PP App", "Something went wrong. \nPlease try again later", "Okay");
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            throw new NotImplementedException();
        }

        //public static byte[] ReadFully(Stream input)
        //{
        //	byte[] buffer = new byte[16 * 1024];
        //	using (MemoryStream ms = new MemoryStream())
        //	{
        //		int read;
        //		while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        //		{
        //			ms.Write(buffer, 0, read);
        //		}
        //		return ms.ToArray();
        //	}
        //}

        //public void checkImageAsync()
        //{
        //	if (file != null)
        //	{
        //		Constants.isDebug = false;
        //		CheckImageFaceAPI api = new CheckImageFaceAPI(Convert.ToBase64String(ReadFully((Stream)file.GetStream())));
        //		api.setCallbacks(this);
        //		api.getResponse();
        //	}
        //	else {
        //		DisplayAlert("Opps", "Please select an image first.", "Okay");
        //	}
        //}

        //public void OnProgressChanged(long progress, long totalSize, BaseAPI caller)
        //{
        //	//throw new NotImplementedException();
        //	Log.e("progress");
        //}

        //public void OnSuccess(JObject response, BaseAPI caller)
        //{
        //	int faceCount = response["image_count"].Value<Int32>();
        //	if (faceCount == 0)
        //	{
        //		// no face detected
        //		DisplayAlert("Facial Detection", "It seems no faces were detected. Please try another photo.", "Okay");
        //	}
        //	else if (faceCount > 1)
        //	{
        //		//multiple face detected
        //		DisplayAlert("Facial Detection", "Multiple faces were detected. Please use a photo that shows only you.", "Okay");
        //	}
        //	else {
        //	}
        //}

        //public void OnError(string errMsg, BaseAPI caller)
        //{
        //	//throw new NotImplementedException();
        //	Log.e(errMsg);
        //}

        //public void OnErrorCode(int errorCode, BaseAPI caller)
        //{
        //	//throw new NotImplementedException();
        //	Log.e(errorCode.ToString());
        //}
    }
}
