﻿using System;
using System.Collections.Generic;
using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using PP.CustomLayouts;
using Xamarin.Forms;

namespace PP.Pages.Profile
{
    public partial class ViewFeedbackPage : ContentPage
    {
        public ViewFeedbackPage(List<FeedBackModel> feedbacks)
        {
            InitializeComponent();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
            if (feedbacks.Count != 0)
            {
                foreach (var ii in feedbacks)
                {

                    int nStars = ii.rate;
                    nStars = (nStars < 0) ? 0 : nStars;

              
                    feedView.Children.Add(
                        new ScaledStackLayout
                        {
                            Orientation = StackOrientation.Horizontal,
                            BackgroundColor = Color.FromHex("#efefef"),
                            Padding = new Thickness(20, 10, 20, 10),
							Margin = new Thickness(10, 10, 10, 0),
                            Children ={
                            new CachedImage{
                                Source = ii.image,
                                HeightRequest = 50,
                                WidthRequest = 50,
                                Aspect = Aspect.Fill,
								VerticalOptions = LayoutOptions.Start,Transformations = new System.Collections.Generic.List<ITransformation>() {
            				    new CircleTransformation()},
            					TransformPlaceholders = true
                            },
							new ScaledStackLayout{
                                Orientation = StackOrientation.Vertical,
                                VerticalOptions = LayoutOptions.CenterAndExpand,
                                Spacing = 0,
                                Children = 
                                { 
                                    new Label{Text = ii.name,VerticalOptions= LayoutOptions.CenterAndExpand, FontSize = 12},
                                    new ScaledStackLayout {
                                        Orientation = StackOrientation.Horizontal,
                                        Children = {
											new XFRatingBar{HeightRequest = 12, MaxStars = 5,DefaultStarImageSource="starGray.png",HighlightStarImageSource="starGreen.png",Rating = nStars, IsEnabled = false },
                                            new Label{Text = ii.rate.ToString(),VerticalOptions= LayoutOptions.CenterAndExpand,FontAttributes = FontAttributes.Bold, FontSize = 12, TextColor = Color.FromHex("#2097ED")},
                                        }
                                    },
                                    new Label{Text = "\t"+ii.message.ToString(),VerticalOptions= LayoutOptions.CenterAndExpand, FontSize = 12, Margin = new Thickness(0,8,0,0)}
                                }},
                        }
                        });
                }
            }
            else
            {
                feedView.Children.Add(
                       new ScaledStackLayout
                       {
                           Orientation = StackOrientation.Horizontal,
                           Padding = new Thickness(20, 60, 20, 10),
                           Children ={
                            new Label{Text = "No Feedbacks",HorizontalOptions= LayoutOptions.CenterAndExpand, FontSize = 14},
                       }
                       });
            }
        
        
        }
        void TappedBackPage(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync(true);
        }
    }

    public class FeedBackModel
    {
        public int userId {get;set;}
        public string image { get; set; }
        public string name { get; set; }
        public int rate { get; set; }
        public string message { get; set; }
    }

}
