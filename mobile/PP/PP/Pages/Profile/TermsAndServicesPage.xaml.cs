﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PP.Pages.Dialogs;
using PP.tools;
using PP.tools.API;
using PP.tools.API.BaseAPI;
using Xamarin.Forms;

namespace PP.Pages.Profile
{
    public partial class TermsAndServicesPage : ContentPage,IBaseAPIInterface
    {


        Loading_Dialog loadDialog;
        string terms_version;


        public interface TermsCallbacks{
            void didAgree(string termVersion, TermsAndServicesPage termsPage);
            void didDecline(TermsAndServicesPage termsPage);
        }



        public TermsCallbacks callbacks;


        public TermsAndServicesPage()
        {
            InitializeComponent();

            //containerTerm.Children.Add(new Label{Text ="\tLorem Ipsum dolor sit amet, consectetuer adipiscingelit. Duis tellus. Donec ante dolor, iaculis nec, gravidaac, cursus in, eros. Mauris vestibulum, felis et egestasullamcorper, purus nibh vehicula sem, eu egestas antenisl non justo. Fusce tincidunt, lorem nev dapibusconsectetuer, leo orci mollis ipsum, eget suscipit erospurus in ante.", TextColor = Color.Black});
            //containerTerm.Children.Add(new Label { Text = "\tAt ipsum vitae est lacinia tincidunt. Maecenas elit orci,gravida ut, molestie non, venenatis vel, lorem. Sedlacinia. Suspendisse potenti. Sed ultricies cursuslectus. In id magna sit amet nibh suspicit euismod.Integer enim. Donec sapien ante, accumsan ut,sodales commodo, auctor quis, lacus. Maecenas a elitlacinia urna posuere sodales. Curabitur pede pede,molestie id, blandit vitae, varius ac, purus. Mauris atipsum vitae est lacinia tincidunt. Maecenas elit orci, gravida ut, molestie non, venenatis vel,lorem. Sed lacinia. Suspendisse potenti. Sed ultrucies cursus lectus. In id magna sit amet nibhsuspicit euismod. Integer enim. Donec sapien ante, accumsan ut, sodales commodo, auctorquis, lacus. Maecenas a elit lacinia urna posuere sodales. Curabitur pede pede, molestie id,blandit vitae, varius ac, purus.", TextColor = Color.Black });

            loadDialog = new Loading_Dialog();

            loadDialog.Text = "Fetching latest terms and conditions..";


			
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            loadDialog.open();

            if (IsSignupMode)
            {
                fetchSignupTermsVersion();
            }
            else fetchTermsVersion();
        }

        void fetchTermsVersion(){
            FetchTermVersionAPI api = new FetchTermVersionAPI();
            api.setCallbacks(this);
            api.getResponse();
        }

        void fetchSignupTermsVersion(){
            FetchSignupTermsAPI api = new FetchSignupTermsAPI();
            api.setCallbacks(this);
            api.getResponse();
        }


        protected bool OnAgreeingMode = false;
        public bool IsAgreeingMode{
            get { return OnAgreeingMode; }
            set{
                sslAgreeingModeContainer.IsVisible = value;
                OnAgreeingMode = value;
            }
        }

        protected bool OnSignupMode = false;
        public bool IsSignupMode{
            get { return OnSignupMode; }
            set{
                OnSignupMode = value;
                OnAgreeingMode = value;
            }
        }



        public void OnError(string errMsg, BaseAPI caller)
        {
            loadDialog.close();
            DisplayAlert("Failed",errMsg,"Okay");
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {

            loadDialog.close();
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            loadDialog.close();
            if((caller as FetchTermVersionAPI) != null || (caller as FetchSignupTermsAPI)!= null){

                if(response["term_version"]!=null){
                    terms_version = response["term_version"].Value<string>();
                }
                else if(response["version"]!=null){
                    terms_version = response["version"].Value<string>();
                }


                String url = Constants.ROOT_URL + response["path"].Value<String>();

                if (Constants.platform == 0) //android
                {
                    //url = "http://docs.google.com/gview?embedded=true&url=" + url;
                    url = "http://docs.google.com/viewer?url=" + url;

                }


                wvTermsContents.Source = url;
            }
            else if ((caller as AgreeTermsAPI) != null){


                if (callbacks != null)
                    callbacks.didAgree(terms_version, this);

                TappedBackPage(null,null);
            }
          
        }

        void TappedBackPage(object sender, System.EventArgs e)
		{
            //if(IsAgreeingMode){
                
            //}
            //else
			Navigation.PopModalAsync(true);
		}

        void AgreeTapped(object sender, System.EventArgs e)
        {
            if(cbAcceptTerms.IsChecked){

                if(IsSignupMode){
                    if (callbacks != null)
                        callbacks.didAgree(terms_version, this);
                    TappedBackPage(null, null);
                }
                else{
                    AgreeTermsAPI api = new AgreeTermsAPI(terms_version);
                    api.setCallbacks(this);
                    api.getResponse();

                    loadDialog.Text = "Accepting terms...please wait.";
                    loadDialog.open();
                }

               
            }
            else{
                DisplayAlert("Accept Terms", "Please acknowledge that you accept the terms and conditions for PP by enabling the accept checkmark.", "Okay");
            }
           
        }

        void DeclineTapped(object sender, System.EventArgs e)
        {
            if (callbacks != null)
                callbacks.didDecline(this);
            
            TappedBackPage(null,null);

            //DependencyService.Get<ICloseApplicationService>().closeApplication();
        }
    }
}
