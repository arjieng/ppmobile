﻿using System;
using System.Windows.Input;
using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using Xamarin.Forms;

namespace PP.Pages.Profile
{
    public class ImageWithDelete : ContentView
    {
        public string imageSource { get; set;}
        public int scale { get; set; }
        public int ID { get; set; }

        public event EventHandler CloseTapped;

        public ImageWithDelete(Xamarin.Forms.ImageSource imgURL)
        {

            var abs = new ScaledAbsoluteLayout();
            abs.WidthRequest = this.WidthRequest;
            abs.HeightRequest = this.HeightRequest;
			
			abs.Children.Add(new CachedImage
            {
                Source = imgURL,
                Aspect = Aspect.Fill,
				WidthRequest = 65,
				HeightRequest = 65
			},
			new Rectangle(0, 10, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize), AbsoluteLayoutFlags.None);


            var circleImg = new CorneredView
            {
                BackgroundColor = Color.FromRgb(32, 151, 237),
                CornerRadius = 10,
                Content = new CachedImage
                {
                    Transformations = new System.Collections.Generic.List<ITransformation>() {
                    new CircleTransformation()},
                    Source = "ic_close_white.png",
                    BackgroundColor = Color.Transparent
                }
            };
            var tapX = new TapGestureRecognizer();
            tapX.Tapped += OnCloseTapped;
            circleImg.GestureRecognizers.Add(tapX);
			abs.Children.Add(circleImg,
			new Rectangle(1, 0, 20, 20), AbsoluteLayoutFlags.XProportional);
            
            Content = abs;
        }

        protected void OnCloseTapped(object sender, EventArgs e){
            if (CloseTapped!=null)
                CloseTapped(this, EventArgs.Empty);
        }

    }
}

