﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PP.CustomLayouts;
using PP.Pages.Dialogs;
using PP.tools.API;
using PP.tools.models;
using Xamarin.Forms;

namespace PP.Pages
{
    public partial class BankAccounts : ContentPage,IBaseAPIInterface
    {
        Loading_Dialog loadDialog;
        public JObject originalData;

        string loadMoreAccountsURL;


        ObservableCollection<USState> states;
        USState selectedState = null;

        void back(object sender, System.EventArgs e)
        {
            Constants.shouldStopIdleTimer = false;
            Navigation.PopModalAsync(true);
        }

        protected override bool OnBackButtonPressed()
        {
            back(null,null);
            return true;
        }
        public BankAccounts()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            PullToRefreshLayout.BindingContext = this;

            loadDialog = new Loading_Dialog();



        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            fetchUSStates();
        }


        ICommand refreshCommand;

        public ICommand RefreshCommand
        {
            get { return refreshCommand ?? (refreshCommand = new Command( () =>  fetchBankAccounts())); }
        }
      

        public void fetchBankAccounts()
        {
            PullToRefreshLayout.IsRefreshing = true;
            loadDialog.Text = "Getting information...";
            loadDialog.open();
                Device.StartTimer(TimeSpan.FromSeconds(1),() => {
                FetchBankAccountsAPI api = new FetchBankAccountsAPI(Constants.currentUser.token);
                    api.setCallbacks(this);
                    api.getResponse();
                    return false;
                });
                

            
        }

        public void fetchUSStates(){

            FetchUSStatesAPI api = new FetchUSStatesAPI(Constants.currentUser.token);
            api.setCallbacks(this);
            api.getResponse();
        }
        void AddAccountTapped(object sender, System.EventArgs e)
        {
    //        if (!canAddAccount()){
                //DisplayAlert("Opps..", "Please complete setting up your address and phone information and save it before you can add an account.", "Okay");
    //        }
    //        else{
                //if (!lvBankAccounts.CheckHasEditEnabledItem(null))
                //{

                //    new AddBankAccountDialog(entryAddress.Text.Trim(),
                //                                              entryCity.Text.Trim(),
                //                                              states[pickerStates.SelectedIndex].Abbreviation,
                //                                              entryZip.Text.Trim(),
                //                                              entryPhone.Text.Trim()) { callerPage = this }.open();
                //}

                //else DisplayAlert("Opps", "Please finish editing a bank account before adding a new one.", "Okay");
            //}
               
            var browser = new WebView
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Source = Constants.ROOT_URL+"/api/v1/users/create_fund_source?auth_token="+Constants.currentUser.token
            };

            Navigation.PushModalAsync(new WebViewContentPage(Constants.ROOT_URL + "/api/v1/users/create_fund_source?auth_token=" + Constants.currentUser.token));

            System.Diagnostics.Debug.WriteLine(browser.Source.ToString());
        }

        public void AddNewAccount(BankAccount a){
            if (lvBankAccounts.ItemSource.Count == 0)
                a.IsDefault = true;
            
            lvBankAccounts.AddNewAccount(a);
        }

        public void OnSuccess(JObject response, BaseAPI caller)
        {
            if ((caller as FetchBankAccountsAPI) != null)
            {
                PullToRefreshLayout.IsRefreshing = false;

                loadDialog.close();
                Log.e("bank accounts", response.ToString());


                ObservableCollection<BankAccount>  accounts = JsonConvert.DeserializeObject<ObservableCollection<BankAccount>>(response["bank_accounts"].ToString(), new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });

                /// Display list of bank accounts
                lvBankAccounts.ItemSource = accounts;


                /// Display the address info

                entryAddress.Text = response["user_detail"]["address"].Value<string>();
                entryCity.Text = response["user_detail"]["city"].Value<string>();
                //entryState.Text = response["user_detail"]["state"].Value<string>();

                foreach(USState s in states){
                    if(s.Abbreviation.Equals(response["user_detail"]["state"].Value<string>())){
                        pickerStates.SelectedItem = s;
                        selectedState = s;
                        break;
                    }
                }


                entryZip.Text = response["user_detail"]["zip"].Value<string>();
                entryPhone.Text = response["user_detail"]["phone"].Value<string>();


                //save original data to be compared to 
                //when checking if there are changes
                originalData = response;

                if (originalData["has_more"].Value<int>() > 0){

                    loadMoreAccountsURL = originalData["url"].Value<string>();
                    cvLoadMore.IsVisible = true;
                }
                else{

                    loadMoreAccountsURL = null;
                    cvLoadMore.IsVisible = false;
                }
                
            }
            else if((caller as SaveBankAccountsAPI) !=null){
                lvBankAccounts.ToRemoveAccounts.Clear();
                loadDialog.close();
                //DisplayAlert("Changes Saved", "Changes have been saved", "Okay");
            }
            else if((caller as FetchMoreBankAccountsAPI) !=null){
                Log.e("More Accounts:",response.ToString());

                ObservableCollection<BankAccount> accounts = JsonConvert.DeserializeObject<ObservableCollection<BankAccount>>(response["bank_accounts"].ToString(), new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });

                lvBankAccounts.ItemSource = new ObservableCollection<BankAccount>(lvBankAccounts.ItemSource.Concat(accounts) );

                lvBankAccounts.ForceLayout();
                if (response["has_more"].Value<int>() > 0){

                    loadMoreAccountsURL = response["url"].Value<string>();
                    cvLoadMore.IsVisible = true;
                }
                else{

                    loadMoreAccountsURL = null;
                    cvLoadMore.IsVisible = false;
                }
            }
            else if((caller as FetchUSStatesAPI) !=null){
                states = JsonConvert.DeserializeObject<ObservableCollection<USState>>(response["state_list"].ToString(), new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                });

                pickerStates.ItemsSource = states;

                //continue fetching accounts
                fetchBankAccounts();
            }
        }

        public void OnError(string errMsg, BaseAPI caller)
        {
            if((caller as SaveBankAccountsAPI) != null){
                DisplayAlert("Update failed", errMsg, "Okay");
            }
        }

        public void OnErrorCode(int errorCode, BaseAPI caller)
        {
            DisplayAlert("Failed", "There was an error connecting to our server. Error: "+errorCode, "Okay");
        }

        void LoadMoreAccountsTapped(object sender, System.EventArgs e)
        {

            if (loadMoreAccountsURL == null)
                return;

            FetchMoreBankAccountsAPI api = new FetchMoreBankAccountsAPI(loadMoreAccountsURL);
            api.setCallbacks(this);
            api.getResponse();
        }

        public bool hasProfileChanges(){
            //check for address
            string address = originalData["user_detail"]["address"].Value<string>();
            if(!address.Equals(entryAddress.Text.Trim())){
                return true;
            }
            //check for city
            string city = originalData["user_detail"]["city"].Value<string>();
            if (!city.Equals(entryCity.Text.Trim()))
            {
                return true;
            }
            //check for state
            string state = originalData["user_detail"]["state"].Value<string>();
            if (!state.Equals(states[pickerStates.SelectedIndex].Abbreviation))
            {
                return true;
            }
            //check for zip
            string zip = originalData["user_detail"]["zip"].Value<string>();
            if (!zip.Equals(entryZip.Text.Trim()))
            {
                return true;
            }
            //check for zip
            string phone = originalData["user_detail"]["phone"].Value<string>();

            if (phone == null)
                phone = "";
            if (!phone.Equals(entryPhone.Text.Trim()))
            {
                return true;
            }
            return false;
        }

        public bool canAddAccount(){

            string address = originalData["user_detail"]["address"].Value<string>();
            //check for city
            string city = originalData["user_detail"]["city"].Value<string>();
            //check for state
            string state = originalData["user_detail"]["state"].Value<string>();
            //check for zip
            string zip = originalData["user_detail"]["zip"].Value<string>();
            //check for zip
            string phone = originalData["user_detail"]["phone"].Value<string>();
            if (phone == null)
                phone = "";


            if (String.IsNullOrEmpty(address) ||
               String.IsNullOrEmpty(city) ||
                String.IsNullOrEmpty(state) ||
                String.IsNullOrEmpty(zip) ||
                  String.IsNullOrEmpty(phone)){


                return false;
            }
            else
                return true;
        }

        void SaveTapped(object sender, System.EventArgs e)
        {


            loadDialog.Text = "Saving changes...";
            //loadDialog.open();




            string j = JsonConvert.SerializeObject(lvBankAccounts.ItemSource, new JsonSerializerSettings());
            JArray accounts = JArray.Parse(j);


            //change account type values to corresponding numbers
            foreach(JObject obj in accounts){

                var accountType = obj["account_type"].Value<string>();

                if (accountType == null)
                    return;


                if (accountType.ToLower().Equals("savings"))
                {
                    obj["account_type"] = 2;
                }
                else if (accountType.ToLower().Equals("personal checking"))
                {
                    obj["account_type"] = 0;
                }
                else if (accountType.ToLower().Equals("business checking"))
                {
                    obj["account_type"] = 1;
                }
            }



            string r = JsonConvert.SerializeObject(lvBankAccounts.ToRemoveAccounts, new JsonSerializerSettings());
            JArray removeAccounts = JArray.Parse(r);


            //check for invalid inputs

            //check address
            if(!string.IsNullOrEmpty(entryAddress.Text.Trim())){
                //check city
                if(!string.IsNullOrEmpty(entryCity.Text.Trim())){
                    //check zip length
                    if (entryZip.Text.Trim().Length == 5)
                    {
                        //check if phone is in valid format
                        if (entryPhone.Text.Trim().Replace("-", "").Replace("+", "").Length == 10)
                        {   
                            bool hasprofilechanges = hasProfileChanges();
                             
                            SaveBankAccountsAPI api = new SaveBankAccountsAPI(Constants.currentUser.token,
                                                             accounts,
                                                              removeAccounts,
                                                              hasprofilechanges,
                                                              entryAddress.Text.Trim(),
                                                              entryCity.Text.Trim(),
                                                              states[pickerStates.SelectedIndex].Abbreviation,
                                                              entryZip.Text.Trim(),
                                                              entryPhone.Text.Trim()
                                                             );


                            api.setCallbacks(this);
                            api.getResponse();

                        }
                        else
                            DisplayAlert("Incomplete", "Phone number is invalid", "Okay");
                    }
                    else
                        DisplayAlert("Incomplete", "Zip code must be 5 characters long", "Okay");
                }
                else
                    DisplayAlert("Incomplete", "City is invalid", "Okay");
            }
            else
                DisplayAlert("Incomplete", "Address is empty", "Okay");

           







        }

        void Phone_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            try
            {
                string trimmedText = entryPhone.Text.Trim().Replace("-", "");
                trimmedText = trimmedText.Replace("+", "");//disallow entering with country code


                if (trimmedText.Length > 10)
                {
                    /////this is to prevent cutting the text when the input is changed in the middle of 
                    ///// the text
                    //trimmedText = e.OldTextValue;
                    //return;

                    trimmedText = trimmedText.Substring(0, 10);
                }

                string formattedText = "";
                for (int index = 0; index < trimmedText.Length; index++)
                {
                    if (index == 3 || index == 6)
                    {
                        formattedText += "-" + trimmedText[index];
                    }
                    //else if(index ==0){
                    //    formattedText += "+" + trimmedText[index];
                    //}
                    else
                    {
                        formattedText += trimmedText[index];
                    }
                }

                entryPhone.TextChanged -= Phone_TextChanged;
                entryPhone.Text = formattedText;
                entryPhone.TextChanged += Phone_TextChanged;
            }
            catch (Exception ex)
            {

            }



        }

        void Zip_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            string trimmedText = entryZip.Text.Trim();
            if (trimmedText.Length > 5)
                trimmedText = trimmedText.Substring(0, 5);

            entryZip.TextChanged -= Zip_TextChanged;
            entryZip.Text = trimmedText;
            entryZip.TextChanged += Zip_TextChanged;
        }
    }
}
