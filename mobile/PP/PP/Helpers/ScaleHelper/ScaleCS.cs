﻿using System;
using Xamarin.Forms;

namespace PP
{
    public static class ScaleCS
    {
        public static float ScaleHeight(this int number)
        {
            return (float)(number * (App.screenHeight / 568.0));
        }

        public static float ScaleHeight(this double number)
        {
            return (float)(number * (App.screenHeight / 568.0));
        }

        public static float ScaleHeight(this float number)
        {
            return (float)(number * (App.screenHeight / 568.0));
        }

        public static float ScaleWidth(this int number)
        {
            return (float)(number * (App.screenWidth / 320.0));
        }

        public static float ScaleWidth(this double number)
        {
            return (float)(number * (App.screenWidth / 320.0));
        }

        public static float ScaleWidth(this float number)
        {
            return (float)(number * (App.screenWidth / 320.0));
        }

        public static double ScaleFont(this int number)
        {
            return (number * (App.screenHeight / 568.0) - (Device.RuntimePlatform == Device.iOS ? 0 : 0.5));
        }
    }
}
