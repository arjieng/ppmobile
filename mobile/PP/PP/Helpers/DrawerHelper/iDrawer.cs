﻿using System;
namespace PP.Helpers.DrawerHelper
{
    public interface iDrawer
    {
        void OpenDrawer(double latitude, double longitude);
    }
}
