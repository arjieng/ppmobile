﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PP.Pages;
using PP.Pages.Message;
using PP.tools.API.BaseAPI;
using PP.tools;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace PP
{
    public partial class App : Application
    {

        public static float screenWidth { get; set; }
        public static float screenHeight { get; set; }
        public static float DPI { get; set; }
        public static float screenScale
        {
            get
            {
                return (App.screenWidth + App.screenHeight) / (320 + 569);
            }
        }

        private static HelpR _help;
        public static HelpR help
        {
            get { return _help ?? (_help = new HelpR()); }
        }

        public App()
        {
            InitializeComponent();

            //MainPage = new EnterEasyPinOrFingerprint();
            MainPage = new SplashScreen();
            //MainPage = new MyPage();
            //MainPage = new ConversationPage("fdsd","35");



            //MainPage = new NewPages.SplashScreen();
            //MainPage = new NewPages.Login.LoginPage();

        }

        protected override void OnStart()
        {
            // Handle when your app starts

            AppCenter.Start("android=8f1e587b-bd91-4503-86d3-23a93a6d5e72;" + "uwp={Your UWP App secret here};" + "ios=04db28ed-ee45-4571-aaaf-a5cf44867e0e;", typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

       
    }
}
