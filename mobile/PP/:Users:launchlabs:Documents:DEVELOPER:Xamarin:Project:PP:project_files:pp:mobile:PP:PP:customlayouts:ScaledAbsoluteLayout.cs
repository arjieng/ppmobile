﻿using System;
using Xamarin.Forms;

namespace PP.customlayouts
{
	public class ScaledAbsoluteLayout : AbsoluteLayout
	{
		protected override void OnChildAdded(Element child)
		{

			Element newChild = Resizer.scaleChild(child);

			base.OnChildAdded(newChild);
		}

	}
}

