﻿using System;
using Foundation;
using PP.Helpers.DrawerHelper;
using PP.iOS.Helpers;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(IDrawerService))]
namespace PP.iOS.Helpers
{
    public class IDrawerService : iDrawer
    {
        public void OpenDrawer(double latitude, double longitude)
        {
            var url = "http://maps.apple.com/?daddr=" + latitude + "," + longitude;
            if (UIApplication.SharedApplication.CanOpenUrl(new NSUrl(url)))
            {
                UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
            }
            else
            {
                url = "http://maps.google.com/maps?&daddr=" + latitude + "," + longitude;
                if (UIApplication.SharedApplication.CanOpenUrl(new NSUrl(url)))
                {
                    UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
                }
                else
                {
                    new UIAlertView("Error", "Maps is not supported on this device", null, "Ok").Show();
                }
            }
        }
    }
}
