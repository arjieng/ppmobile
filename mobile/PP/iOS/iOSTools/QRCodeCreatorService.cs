﻿using System;
using System.IO;
using PP.iOS.iOSTools;
using PP.tools;

[assembly: Xamarin.Forms.Dependency(typeof(QRCodeCreatorService))]
namespace PP.iOS.iOSTools
{
    public class QRCodeCreatorService:IQRCodeCreatorService
    {
       

        public QRCodeCreatorService(){
            
        }
        public Stream ConvertImageStream(string data, int width = 300, int height = 300)
        {
			var barcodeWriter = new ZXing.Mobile.BarcodeWriter
			{
				Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new ZXing.Common.EncodingOptions
				{
					Width = width,
					Height = height,
					Margin = 0
				}
			};
            barcodeWriter.Renderer = new ZXing.Mobile.BitmapRenderer();
			var bitmap = barcodeWriter.Write(data);
			var stream = bitmap.AsPNG().AsStream(); // this is the difference 
			stream.Position = 0;

			return stream;
        }
    }
}
