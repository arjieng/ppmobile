﻿using System;
using System.Threading.Tasks;
using Foundation;
using PP.iOS.iOSTools;
using PP.tools;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(iOSLocationSettingsService))]
namespace PP.iOS.iOSTools
{
    public class iOSLocationSettingsService:ILocationSettingsService
    {
        TaskCompletionSource<String> completionSource;

        public Task<String> OpenLocationSettings()
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl(UIApplication.OpenSettingsUrlString), new NSDictionary(),(bool obj) => {

                completionSource.SetResult("Okay");
            });

            completionSource = new TaskCompletionSource<String>();

            return completionSource.Task;
        }

    }
}
