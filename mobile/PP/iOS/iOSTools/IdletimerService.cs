﻿using System;
using PP.iOS.iOSTools;
using PP.tools;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(IdletimerService))]
namespace PP.iOS.iOSTools
{
    public class IdletimerService:IIdleTimerService
    {
        public IdletimerService()
        {
        }

        public void didElapseTime(int secondsRemaining)
        {
            
        }

        public void didIdleTimedOut()
        {
           
        }

        public void didIdleTimerCancelledOrReset()
        {
           
        }

        public void didIdleTimerStarted()
        {
           
        }

        public void startIdleTimer()
        {
            ((AppDelegate)UIApplication.SharedApplication.Delegate).StartIdleTimer(new System.Threading.CancellationTokenSource(), 15);
        }
    }
}
