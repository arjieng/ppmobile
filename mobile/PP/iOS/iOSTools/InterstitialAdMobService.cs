﻿using System;
using PP.tools;
using UIKit;
using Google.MobileAds;
using PP.iOS.iOSTools;

[assembly:Xamarin.Forms.Dependency(typeof(InterstitialAdMobService))]
namespace PP.iOS.iOSTools
{
    public class InterstitialAdMobService : IInterstitialAdMobService{

        Interstitial interstitial;
        string currentAdUnitId;


        public InterstitialAdMobService()
        {
        }

        public IInterstitialAdMobService InitWithAdUnitId(string adunitid)
        {
            currentAdUnitId = adunitid;
            interstitial = new Interstitial(adunitid);
            interstitial.ScreenDismissed += (object sender, EventArgs e) => {
                InitWithAdUnitId(currentAdUnitId);
            };
            interstitial.AdReceived += (object sender, EventArgs e) => {
                Log.e("Received ad");
                //ShowInterstitialAd();
            };
            interstitial.ReceiveAdFailed += (object sender, InterstitialDidFailToReceiveAdWithErrorEventArgs e) => {
                Log.e("Failed to receive ad."+e.ToString() + " ---- "+e.Error.LocalizedDescription);

                //InitWithAdUnitId(adunitid);
            };


            Request request = Request.GetDefaultRequest();

            if (Constants.SHOULD_USE_TESTDEVICES)
                request.TestDevices = Constants.TEST_DEVICES;

            interstitial.LoadRequest(request);


           

            //Request GetRequest()
            //{
            //    var request = Request.GetDefaultRequest();
            //    return request;
            //}

            return this;

        }


		//public override void DidDismissScreen(Interstitial ad)
		//{
  //          base.DidDismissScreen(ad);

  //          //recreate interstitial ad
  //          InitWithAdUnitId(currentAdUnitId);


		//}

		public bool IsReady()
        {
            if (interstitial == null)
                throw new NullReferenceException("Interstitial ad has not been initialized");
            
            Log.e("READY: " + interstitial.IsReady);

            return interstitial.IsReady;
        }



        private UIViewController GetVisibleViewController()
        {
            var windows = UIApplication.SharedApplication.Windows;
            foreach (var window in windows)
            {
                if (window.RootViewController != null)
                {
                    return window.RootViewController;
                }
            }
            return null;
        }

        public void ShowInterstitialAd()
        {
            if (IsReady())
                this.interstitial.PresentFromRootViewController(GetVisibleViewController());
            
        }


    }
}
