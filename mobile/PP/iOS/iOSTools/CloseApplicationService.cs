﻿using System;
using System.Threading;
using PP.iOS.iOSTools;
using PP.tools;
using Xamarin.Forms.Internals;

[assembly: Xamarin.Forms.Dependency(typeof(CloseApplicationService))]
namespace PP.iOS.iOSTools
{
    [Preserve(AllMembers = true)]
    public class CloseApplicationService:ICloseApplicationService
    {
        public CloseApplicationService()
        {
        }
        public void closeApplication()
        {
            Thread.CurrentThread.Abort();
        }
    }
}
