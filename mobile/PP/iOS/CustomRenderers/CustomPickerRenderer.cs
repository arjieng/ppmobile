﻿using System;
using System.Drawing;
using Foundation;
using PP;
using PP.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace PP.iOS.CustomRenderers
{
    public class CustomPickerRenderer:PickerRenderer
    {
        public CustomPickerRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            try
            {
                base.OnElementChanged(e);

                UITextField tf = Control;

                tf.BorderStyle = UITextBorderStyle.None;
                tf.BackgroundColor = UIColor.Clear;
                //setting padding

                UIView padView = new UIView(new RectangleF(1, 3, 1, 1));
                tf.LeftView = padView;


                CustomPicker picker = (CustomPicker)Element;

                tf.Font = UIFont.FromName(tf.Font.Name, picker == null ? 0 : (float)picker.FontSize);
                tf.AttributedPlaceholder = new NSAttributedString(tf.Placeholder, null, picker.PlaceholderColor.ToUIColor());


                switch (picker.TitleAlignment)
                {
                    case CustomPicker.Alignment.START_ALIGN:
                        tf.TextAlignment = UITextAlignment.Left;
                        break;
                    case CustomPicker.Alignment.CENTER_ALIGN:
                        tf.TextAlignment = UITextAlignment.Center;
                        break;
                    case CustomPicker.Alignment.END_ALIGN:
                        tf.TextAlignment = UITextAlignment.Right;
                        break;
                }
            } catch (Exception ex) {
                
            }
        }
    }
}
