﻿using System;
using CoreAnimation;
using CoreGraphics;
using PP;
using PP.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TransparentEntryExtended), typeof(TransparentEntryExtendedRenderer))]
namespace PP.iOS.CustomRenderers
{

    public class UIBackwardsTextField : UITextField
    {
        public delegate void DeleteBackwardEventHandler(object sender, EventArgs e);
        public event DeleteBackwardEventHandler OnDeleteBackward;


        public void OnDeleteBackwardPressed()
        {
            if (OnDeleteBackward != null)
            {
                OnDeleteBackward(null, null);
            }
        }

        public UIBackwardsTextField()
        {
            BorderStyle = UITextBorderStyle.RoundedRect;
            ClipsToBounds = true;
        }

        public override void DeleteBackward()
        {
            base.DeleteBackward();
            OnDeleteBackwardPressed();
        }
    }

    public class TransparentEntryExtendedRenderer:TransparentEntryRenderer, IUITextFieldDelegate
    {

        TransparentEntryExtended fControl;
        IElementController ElementController => Element as IElementController;

        public TransparentEntryExtendedRenderer()
        {
        }



		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
            base.OnElementChanged(e);


            UITextField textField = this.Control;
            fControl = Element as TransparentEntryExtended;


            if(Element != null){
                var entry = (TransparentEntryExtended)Element;
                var tf = new UIBackwardsTextField();
                tf.EditingChanged += OnEditingChanged;
                tf.OnDeleteBackward += (sender, a) =>
                {
                    entry.OnBackspacePressed();
                };

                SetNativeControl(textField);

                base.OnElementChanged(e);
            }
            //var border = new CALayer();
            //nfloat width = 5;
            //border.BorderColor = v.UnderlineColor.ToCGColor();
            //border.Frame = new CoreGraphics.CGRect(0, textField.Frame.Size.Height - width, textField.Frame.Size.Width, textField.Frame.Size.Height);
            //border.BorderWidth = width;
            //textField.Layer.AddSublayer(border);
            //textField.Layer.MasksToBounds = true;



            //textField.BorderStyle = UITextBorderStyle.None;
            //textField.Layer.BackgroundColor = UIColor.Clear.CGColor;
            //textField.Layer.MasksToBounds = false;
            //textField.Layer.ShadowColor = UIColor.White.CGColor;
            //textField.Layer.ShadowOffset = new CoreGraphics.CGSize(0.0, 1.0);
            //textField.Layer.ShadowOpacity = 1.0f;
            //textField.Layer.ShadowRadius = 0.0f;
            //textField.Layer.CornerRadius = 0;



           
		}

        private void OnEditingChanged(object sender, EventArgs e)
        {
            ElementController.SetValueFromRenderer(Entry.TextProperty, Control.Text);
        }

        public override void Draw(CGRect rect)
		{
			//base.Draw(rect);

            CGPoint startPoint = new CGPoint(0, rect.Size.Height - 1);
            CGPoint endPoint = new CGPoint(rect.Size.Width, rect.Size.Height);


            UIBezierPath path = new UIBezierPath();

            path.MoveTo(startPoint);
            path.AddLineTo(endPoint);
            path.LineWidth = 1.0f;
            fControl.UnderlineColor.ToUIColor().SetStroke();
            path.Stroke();

		}

	}
}
