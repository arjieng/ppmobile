﻿using System;
using System.Drawing;
using System.Reflection;
using PP;
using PP.iOS;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(TransparentNavBarPage), typeof(TransparentNavbarRenderer))]
namespace PP.iOS
{
	public class TransparentNavbarRenderer : NavigationRenderer
	{
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			base.ViewDidLayoutSubviews();
			this.NavigationBar.BarTintColor = ((TransparentNavBarPage)this.Element).barColor.ToUIColor();
			this.NavigationBar.Translucent = false;
			this.NavigationBar.Layer.ShadowColor = UIColor.Green.CGColor;

			this.NavigationItem.LeftBarButtonItem = new UIBarButtonItem(

				UIImage.FromFile("ic_back"),
				UIBarButtonItemStyle.Plain, (sender, e) =>
				{
						NavigationController.PopViewController(true);
				}
			);






			//////adding the bottom tabs
			//UIView mainView = this.View;
			//float y = ((float)mainView.Frame.Top + (float)mainView.Frame.Size.Height) - 50;
			//UIView tabView = new UIView(new CGRect(0, y, (float)mainView.Frame.Size.Width, 50));
			////UIView fullView = new UIView(new CGRect(0, 50, 100, 50));

			//Log.e("Sdasa", ">" + (((float)mainView.Frame.Top + (float)mainView.Frame.Size.Height) - 50) + " Height:" +App.screenHeight);

			//tabView.BackgroundColor = UIColor.Blue;
			//mainView.AddSubview(tabView);


			////UIView tabView = new UIView(new RectangleF(0, (float)fullView.Frame.Size.Height - 50, (float)fullView.Frame.Size.Width, 50));
			////tabView.BackgroundColor = UIColor.Red;

			////fullView.AddSubview(tabView);
		}

		//public override void ViewWillAppear(bool animated)
		//{
		//	base.ViewWillAppear(animated);

		//	var navigationItem = this.TopViewController.NavigationItem;

	 //        foreach (var rightItem in navigationItem.RightBarButtonItems)
	 //        {
	 //            var button = new UIButton(new CGRect(0, 0, 40, 40));
		//		button.SetImage(rightItem.Image, UIControlState.Normal);

		//     	FieldInfo fi = rightItem.GetType().GetField("clicked", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
		//		Delegate del = (Delegate)fi.GetValue(rightItem);
		//		button.TouchUpInside += (EventHandler)del;

	 //           rightItem.CustomView = button;
	 //       }
		//}
	}
}


