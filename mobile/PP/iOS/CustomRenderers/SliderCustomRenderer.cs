﻿using System;
using PP.iOS.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(Xamarin.Forms.Slider), typeof(SliderCustomRenderer))]
namespace PP.iOS.CustomRenderers
{
    public class SliderCustomRenderer : SliderRenderer
    {
        public SliderCustomRenderer()
        {
            
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            Control.MaximumTrackTintColor = UIKit.UIColor.Gray;
            Control.MinimumTrackTintColor = UIKit.UIColor.Black;
            //Control.SetThumbImage(UIKit.UIImage.FromFile("starGreen.png"),UIKit.UIControlState.Normal);
            //Control.SetMinTrackImage(UIKit.UIImage.FromFile("starGreen.png"),UIKit.UIControlState.Normal);

        }
    }
}

