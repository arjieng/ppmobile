﻿using System;
using PP;
using PP.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;



[assembly: ExportRenderer(typeof(NonOverScrollView), typeof(NonOverScrollViewRenderer))]
namespace PP.iOS
{
	public class NonOverScrollViewRenderer:ScrollViewRenderer
	{


		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);
			this.Bounces = false;
			this.ShowsVerticalScrollIndicator = false;
			this.ShowsHorizontalScrollIndicator = false;
		}
	}
}
