﻿using System;
using PP;
using PP.iOS;
using CoreAnimation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(UnderlinedEntry), typeof(UnderlinedEntryRenderer))]
namespace PP.iOS
{
	public class UnderlinedEntryRenderer : EntryRenderer
	{


		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			

			base.OnElementChanged(e);



			UITextField txtField = this.Control;

			this.Control.BorderStyle = UITextBorderStyle.None;
			//this.Control.Layer.BorderWidth = 1;
			//this.Control.Layer.BorderColor = Color.FromHex("#F8F8F8").ToCGColor();

			CALayer layer = txtField.Layer;
			layer.BackgroundColor = UIColor.White.CGColor;
			layer.MasksToBounds = false;
			layer.ShadowColor = UIColor.LightGray.CGColor;
			layer.ShadowOffset = new CoreGraphics.CGSize(width: 0.0, height: 1.0);
			layer.ShadowOpacity = 1.0f;
			layer.ShadowRadius = 0.0f;


			//CALayer border = new CALayer();
   //         float borderWidth = 2;
   //         border.BorderColor = UIColor.DarkGray.CGColor;//[UIColor darkGrayColor].CGColor;
   //         border.Frame = new CoreGraphics.CGRect(0, txtField.Bounds.Size.Height - borderWidth, txtField.Bounds.Size.Width, txtField.Bounds.Size.Height); //CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
			//border.BorderWidth = borderWidth;
            //txtField.Layer.AddSublayer(border); //[textField.layer addSublayer:border];
            //txtField.Layer.MasksToBounds = true;//textField.layer.masksToBounds = YES;

		}


	}
}

