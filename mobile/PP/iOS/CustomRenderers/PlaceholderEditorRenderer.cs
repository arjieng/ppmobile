﻿using System;
using PP;
using PP.iOS.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(PlaceholderEditor), typeof(PlaceholderEditorRenderer))]
namespace PP.iOS.CustomRenderers
{
    public class PlaceholderEditorRenderer : EditorRenderer
    {
        public PlaceholderEditorRenderer()
        {
        }
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            Control.InputAccessoryView = null;
        }

    }
}
