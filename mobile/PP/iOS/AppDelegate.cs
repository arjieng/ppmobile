﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CarouselView.FormsPlugin.iOS;
using FFImageLoading.Forms.Touch;
using Foundation;
using KeyboardOverlap.Forms.Plugin.iOSUnified;
using PP.iOS.iOSTools;
using PP.tools;
using UIKit;
using Xamarin.Forms;
using Google.MobileAds;
using UserNotifications;

#if NETFX_CORE
[assembly: Xamarin.Forms.Platform.WinRT.ExportRenderer(typeof(Xamarin.RangeSlider.Forms.RangeSlider), typeof(Xamarin.RangeSlider.Forms.RangeSliderRenderer))]
#else
[assembly: Xamarin.Forms.ExportRenderer(typeof(Xamarin.RangeSlider.Forms.RangeSlider), typeof(Xamarin.RangeSlider.Forms.RangeSliderRenderer))]
#endif


namespace PP.iOS
{

	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate 
    {
        PPTicksTimer pptickstimer;
        int remainingIdleSeconds;
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{

			setAppFrame();

            MobileAds.Configure(Constants.ADMOB_APP_ID_IOS);

			global::Xamarin.Forms.Forms.Init();

            ZXing.Net.Mobile.Forms.iOS.Platform.Init();
            var x = new QRCodeCreatorService();
            Syncfusion.SfRangeSlider.XForms.iOS.SfRangeSliderRenderer.Init();

			CachedImageRenderer.Init();
            Xamarin.FormsGoogleMaps.Init(Constants.GOOGLE_MAPS_KEY);

            Websockets.Ios.WebsocketConnection.Link();

            KeyboardOverlapRenderer.Init();

            CarouselViewRenderer.Init();

            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                // Ask the user for permission to get notifications on iOS 10.0+
                UNUserNotificationCenter.Current.RequestAuthorization(
                        UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound,
                        (approved, error) => { });
                UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();
            }
            else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                // Ask the user for permission to get notifications on iOS 8.0+
                var settings = UIUserNotificationSettings.GetSettingsForTypes(
                        UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                        new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }

            LoadApplication(new App());
            Xamarin.Calabash.Start();



			return base.FinishedLaunching(app, options);


		}

		void setAppFrame()
		{
			App.screenWidth = (float)UIScreen.MainScreen.Bounds.Width;
			App.screenHeight = (float)UIScreen.MainScreen.Bounds.Height;
		}

        public override void WillTerminate(UIApplication uiApplication)
        {
            base.WillTerminate(uiApplication);

        }
        public override void DidEnterBackground(UIApplication uiApplication)
        {
            Constants.isMinimized = true;
            //if(Constants.IsInBuyer){
            //    Pages.BuyerHome.DeclineOnClosed();
            //    Pages.BuyerHome.CancelTransactionOnClosed();
            //}

            //if (Constants.IsInSeller)
            //{
                //Pages.Seller.SellerHome.CancelTransactionOnClosed();
            //    Pages.Seller.SellerHome.DeclineOnClosed();
            //    tools.API.ChangeUserRoleAPI api = new tools.API.ChangeUserRoleAPI(Constants.currentUser.token, Constants.MERCHANT_STATUS.IDLE);
            //    api.getResponse();
            //}

            base.DidEnterBackground(uiApplication);
        }
	    
        public override void OnActivated(UIApplication uiApplication)
        {
            base.OnActivated(uiApplication);
            Constants.isMinimized = false;
            //if (Constants.IsInSeller)
            //{
            //    tools.API.ChangeUserRoleAPI api = new tools.API.ChangeUserRoleAPI(Constants.currentUser.token, Constants.MERCHANT_STATUS.SELLER);
            //    api.getResponse();
            //}
        }
            
        private CancellationTokenSource idleTimerCancellationTokenSource;
        public void ResetIdleTimer()
        {
            //use, then gid rid of the old CancellationTokenSource
            if (idleTimerCancellationTokenSource != null)
            {
                idleTimerCancellationTokenSource.Cancel();
                idleTimerCancellationTokenSource.Dispose();
                idleTimerCancellationTokenSource = null;
            }
            // Restart the timer with a new CancellationTokenSource



            StartIdleTimer(new CancellationTokenSource(),15);
        }

        public void popToRoot()
        {
            var window = UIApplication.SharedApplication.KeyWindow;
            var vc = window.RootViewController;
            while (vc.PresentedViewController != null)
            {
                vc = vc.PresentedViewController;
            }
            UINavigationController nav = new UINavigationController();
            UIViewController title = (UIViewController)this.Window.RootViewController;

            //UIViewController title = (UIViewController)this.GetWindow().RootViewController;


            UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(title, false, null);
            nav.PresentViewController(title, false, null);

        }

        public void StopIdleTimer()
        {
            // use, then get rid of the CancellationTokenSource

                if (idleTimerCancellationTokenSource != null)
                {
                    idleTimerCancellationTokenSource.Cancel();
                    idleTimerCancellationTokenSource.Dispose();
                    idleTimerCancellationTokenSource = null;
                }

        }
        // async void, since we want to just start and forget about this thread
        public async void StartIdleTimer(CancellationTokenSource tokenSource, int sec)
        {
            try
            {
                

                //maintain a reference to the token so we can cancel when needed
                idleTimerCancellationTokenSource = tokenSource;

                Log.e("Idle Timer Thread Started");

                if (IdleTimerManager.idleTimerService != null)
                    IdleTimerManager.idleTimerService.didIdleTimerStarted();



                //Create ticks timer

                if (pptickstimer != null)
                    pptickstimer.Stop();

                remainingIdleSeconds = IdleTimerManager.TIME_OUT_SECONDS-2;
                pptickstimer = new PPTicksTimer(TimeSpan.FromSeconds(1), () => {
                    if (IdleTimerManager.idleTimerService != null)
                    {
                        IdleTimerManager.idleTimerService.didElapseTime(remainingIdleSeconds--);
                    }
                });

                pptickstimer.Start();


                //create the actual delayed timer
                await Task.Delay(TimeSpan.FromSeconds(IdleTimerManager.TIME_OUT_SECONDS), tokenSource.Token);
                pptickstimer.Stop();


                //int remainingSeconds = IdleTimerManager.TIME_OUT_SECONDS;
                //Device.StartTimer(TimeSpan.FromSeconds(1), () => {
                //    remainingSeconds--;

                //    if(IdleTimerManager.idleTimerService!=null)
                //    IdleTimerManager.idleTimerService.didElapseTime(remainingSeconds);

                //    if (remainingSeconds == 0)
                //    {
                //        Log.e("Idle Timeout Detected, Do Stuff!");
                //        if (IdleTimerManager.idleTimerService != null)
                //            IdleTimerManager.idleTimerService.didIdleTimedOut();
                //        IdleTimerManager.idleTimerService = null;

                //        remainingSeconds = IdleTimerManager.TIME_OUT_SECONDS;

                //        return false;
                //    }
                //    else
                //    {
                //        if (Constants.shouldStopIdleTimer)
                //            return false;
                //        else
                //            return true;
                //    }
                //});

                Log.e("Idle Timeout Detected, Do Stuff!");
                if (IdleTimerManager.idleTimerService != null)
                    IdleTimerManager.idleTimerService.didIdleTimedOut();
                //Do something here, like show a screensaver or something
                InvokeOnMainThread(() => /* Do Some navigation or something */
                {
                    //create alert
                    //var alert = UIAlertController.Create("Session Expired", "Return to title screen", UIAlertControllerStyle.Alert);

                    //add action
                    //alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, action => popToRoot()));

                    //alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

                    //present alert
                    //UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(alert, true, null);
                });
                IdleTimerManager.idleTimerService = null;



            }
            catch (TaskCanceledException ex)
            {
                    Log.e("Idle Timer Thread Cancelled");
                    if (IdleTimerManager.idleTimerService != null)
                        IdleTimerManager.idleTimerService.didIdleTimerCancelledOrReset();
                //if we cancel/reset, this catch block gets called

            }
            // if we reach here, this timer has stopped
            Log.e("Idle Timer Thread Complete");
        }

       
    }

    public class PPTicksTimer{
        private readonly TimeSpan timespan;
        private readonly Action callback;

        private CancellationTokenSource cancellation;

        public PPTicksTimer(TimeSpan timeSpan, Action callback){
            this.timespan = timeSpan;
            this.callback = callback;
            this.cancellation = new CancellationTokenSource();

        }
        public void Start()
        {
            CancellationTokenSource cts = this.cancellation; // safe copy
            Device.StartTimer(this.timespan,
                () => {
                    if (cts.IsCancellationRequested) return false;
                    this.callback.Invoke();
                    //return false; // or true for periodic behavior
                    return true;
            });
        }

        public void Stop()
        {
            Interlocked.Exchange(ref this.cancellation, new CancellationTokenSource()).Cancel();
        }
    }
}
