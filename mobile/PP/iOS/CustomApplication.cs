﻿using UIKit;

namespace PP.iOS
{
    public class CustomApplication : UIApplication
    {
        public CustomApplication()
        {
        }

        public override void SendEvent(UIEvent uievent)
        {

            //if(uievent.Type == UIEventType.Touches){
            //    if(uievent.AllTouches.Cast<UITouch>().Any((UITouch arg) =>arg.Phase == UITouchPhase.Began )){
            //        ((AppDelegate)Delegate).ResetIdleTimer();
            //    }
            //}
            base.SendEvent(uievent);

            var allTouches = uievent.AllTouches;
            if (allTouches.Count > 0)
            {
                var phase = ((UITouch)allTouches.AnyObject).Phase;
                if (phase == UITouchPhase.Began || phase == UITouchPhase.Ended)
                    ((AppDelegate)Delegate).ResetIdleTimer();
            }
        }
    }
}
 