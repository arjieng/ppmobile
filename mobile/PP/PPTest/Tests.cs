﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace PPTest
{
    //[TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        //[Test]
        //public void AppLaunches()
        //{
        //    //app.Screenshot("First screen.");
        //}

        [Test]
        public void TesSignIn()
        {
            app.EnterText("LoginEntry","berrycream");
            app.DismissKeyboard();
            app.Tap("LoginTap");
        }

        [Test]
        public void ViewFeedBack()
        {
            app.EnterText("LoginEntry", "berrycream");
            app.DismissKeyboard();
            app.Tap("LoginTap");

            app.WaitForElement(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("ProfileTap"));

            app.WaitForElement(x => x.Marked("Berry"));
            app.Tap(x => x.Marked("View Feedbacks"));

            app.Tap(x => x.Marked("ic_close_white"));

        }

        [Test]
        public void ViewTransactionHistory()
        {
            app.EnterText("LoginEntry", "berrycream");
            app.DismissKeyboard();
            app.Tap("LoginTap");

            app.WaitForElement(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("Transaction History"));

            app.Tap(x => x.Marked("ic_close_white"));
        }

        [Test]
        public void ViewBankAccounts()
        {
            app.EnterText("LoginEntry", "berrycream");
            app.DismissKeyboard();
            app.Tap("LoginTap");

            app.WaitForElement(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("Bank Accounts"));

        }

        [Test]
        public void ReferFriend()
        {
            app.EnterText("LoginEntry", "berrycream");
            app.DismissKeyboard();
            app.Tap("LoginTap");

            app.WaitForElement(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("Refer a friend"));

        }

        [Test]
        public void ChangePin()
        {
            app.EnterText("LoginEntry", "berrycream");
            app.DismissKeyboard();
            app.Tap("LoginTap");

            app.WaitForElement(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("Settings"));

            app.Tap(x => x.Marked("Change Easy Pin"));

            app.Tap(x => x.Marked("1"));
            app.Tap(x => x.Marked("9"));
            app.Tap(x => x.Marked("9"));
            app.Tap(x => x.Marked("5"));

            app.Tap(x => x.Marked("Next"));

            app.WaitForElement(x => x.Marked("Confirm Your Easy Pin"));
            app.Tap(x => x.Marked("1"));
            app.Tap(x => x.Marked("9"));
            app.Tap(x => x.Marked("9"));
            app.Tap(x => x.Marked("5"));

            app.Tap(x => x.Marked("Done"));
            //app.WaitForElement(x => x.Marked("Continue setup"));
            //app.Tap(x => x.Marked("Continue setup"));
        }

        [Test]
        public void GoToProfile()
        {
            app.EnterText("LoginEntry", "berrycream");
            app.DismissKeyboard();
            app.Tap("LoginTap");

            app.WaitForElement(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("ic_menu"));
            app.Tap(x => x.Marked("ProfileTap"));

            app.Tap(x => x.Marked("FirstNameText"));
            app.ClearText("LastNameEntry");
            app.EnterText(x => x.Marked("LastNameEntry"),"Cream");
            app.ClearText("FirstNameEntry");
            app.EnterText(x => x.Marked("FirstNameEntry"), "Berry");
            app.DismissKeyboard();

            app.Tap(x => x.Marked("SaveProfile"));

            app.WaitForElement(x => x.Marked("PP Profile"));
            app.Tap(x => x.Marked("Okay"));

            app.ScrollDownTo("Photos :","ScrollViewScroll",ScrollStrategy.Auto);

            app.Tap(x => x.Marked("CostText"));
            app.ClearText("CostEntry");
            app.EnterText(x => x.Marked("CostEntry"), "50");
            app.DismissKeyboard();
            app.ClearText("SellingEntry");
            app.EnterText(x => x.Marked("SellingEntry"), "55");
            app.DismissKeyboard();
            app.ClearText("QuantityEntry");
            app.EnterText(x => x.Marked("QuantityEntry"), "100");
            app.DismissKeyboard();

            app.Tap(x => x.Marked("SaveDetail"));

            app.WaitForElement(x => x.Marked("PP Profile"));
            app.Tap(x => x.Marked("Okay"));

            app.Tap("CloseTap");

        }

        [Test]
        public void TestSignU()
        {
            app.Tap("CreateAccount");
            app.EnterText("UsernameText", "DarkKnightoo");
            app.EnterText("PasswordText", "password");
            app.EnterText("ConfirmPasswordText", "password");
            app.EnterText("FirstNameText", "Dark");
            app.EnterText("LastNameText", "Knight");
            app.DismissKeyboard();
            app.EnterText("EmailText", "DarkKnightoo@gmail.com");
            app.DismissKeyboard();
            app.EnterText("CodeText", "0421532A");
            app.ClearText("CodeText");
            app.DismissKeyboard();
            app.Tap("QuestionTap");
            /// First Question
            app.Tap(x => x.Marked("FirstPicker"));

            // Assuming a single column picker
            var picker1 = app.Query(x => x.Class("CustomPicker").Index(0));
            // Try to scroll and find the text item only 5 times
            for (int i = 0; i < 5; i++)
            {
                if (app.Query(m => m.Text("United States")).Length == 0)
                    app.ScrollDown(x => x.Class("UIPickerTableView").Index(0));
                else
                {
                    app.Tap(x => x.Text("United States"));
                    break;
                }
            }
            app.Tap(x => x.Class("UIToolbarTextButton"));
            app.EnterText("FirstAnswerText", "Hello123");

            /// Second Question
            app.Tap(x => x.Marked("SecondPicker"));

            // Assuming a single column picker
            var picker2 = app.Query(x => x.Class("CustomPicker").Index(0));
            // Try to scroll and find the text item only 5 times
            for (int i = 0; i < 3; i++)
            {
                if (app.Query(m => m.Text("United States")).Length == 0)
                    app.ScrollDown(x => x.Class("UIPickerTableView").Index(0));
                else
                {
                    app.Tap(x => x.Text("United States"));
                    break;
                }
            }
            app.Tap(x => x.Class("UIToolbarTextButton"));
            app.EnterText("SecondAnswerText", "BayeBye123");
            app.DismissKeyboard();

            app.EnterText("CustomQuestionText", "What is you pets name?");
            app.DismissKeyboard();

            app.EnterText("CustomAnswerText", "Gerard");
            app.DismissKeyboard();

            app.Tap("CompleteTap");

            app.Tap("UploadTap");

            app.Tap("AddImageTap");

            app.WaitForElement(x => x.Marked("Gallery"));
            app.Tap(x => x.Marked("Gallery"));

            //app.WaitForElement(x => x.Marked("OK"));
            //app.Tap(x => x.Marked("OK"));

            app.WaitForElement(x => x.Marked("Photos"));
            //app.TapCoordinates(20,70);
            app.Tap(x => x.Marked("Camera Roll"));

            app.WaitForElement(x => x.Marked("Photos"));
            app.TapCoordinates(100, 150);

            app.WaitForElement(x => x.Marked("SaveTap"));
            app.Tap("SaveTap");


            app.Tap("TermsTap");

            app.WaitForNoElement(x => x.Marked("ReadTap"), "", TimeSpan.FromSeconds(2));
            app.Tap("ReadTap");

            app.Tap("AcceptTap");

            app.WaitForElement(x => x.Marked("SignUpTap"));
            app.Tap("SignUpTap");


            app.WaitForElement(x => x.Marked("Setup Your Easy Pin"));
            app.Tap(x => x.Marked("1"));
            app.Tap(x => x.Marked("9"));
            app.Tap(x => x.Marked("9"));
            app.Tap(x => x.Marked("5"));

            app.Tap(x => x.Marked("Next"));

            app.WaitForElement(x => x.Marked("Confirm Your Easy Pin"));
            app.Tap(x => x.Marked("1"));
            app.Tap(x => x.Marked("9"));
            app.Tap(x => x.Marked("9"));
            app.Tap(x => x.Marked("5"));

            app.Tap(x => x.Marked("Done"));

        }
    }
}
