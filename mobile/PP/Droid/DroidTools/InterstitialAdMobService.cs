﻿using System;
using Android.Gms.Ads;
using PP.Droid.DroidTools;
using PP.tools;
using Xamarin.Forms;

[assembly: Dependency(typeof(InterstitialAdMobService))]
namespace PP.Droid.DroidTools
{
    public class InterstitialAdMobService : IInterstitialAdMobService
    {
        InterstitialAd interstitialAd;
        public InterstitialAdMobService()
        {
        }

        public IInterstitialAdMobService InitWithAdUnitId(string adunitid)
        {
            interstitialAd = new InterstitialAd(Android.App.Application.Context);
            interstitialAd.AdUnitId = adunitid;

            var requestbuilder = new AdRequest.Builder();
            interstitialAd.LoadAd(requestbuilder.Build());
            return this;
        }

        public bool IsReady()
        {
            if(interstitialAd == null)
                throw new NullReferenceException("Interstitial ad has not been initialized");

            return interstitialAd.IsLoaded;
        }

        public void ShowInterstitialAd()
        {
            interstitialAd.Show();
        }
    }
}
