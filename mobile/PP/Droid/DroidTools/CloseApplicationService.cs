﻿using System;
using Android.App;
using PP.Droid.DroidTools;
using PP.tools;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(CloseApplicationService))]
namespace PP.Droid.DroidTools
{
    public class CloseApplicationService : ICloseApplicationService
    {
        public CloseApplicationService()
        {

        }
        public void closeApplication()
        {
			var activity = (Activity)Forms.Context;
			activity.FinishAffinity();
        }
    }
}
