﻿using System;

using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Gms.Location;
using Android.OS;
using Android.Runtime;
using Android.Support.Compat;
using Java.Lang;
using PP.Droid.DroidTools;
using PP.tools;
using Android.Gms.Tasks;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(DroidLocationSettingsService))]
namespace PP.Droid.DroidTools
{
    
    public class DroidLocationSettingsService : ILocationSettingsService
                                                //IResultCallback, 
                                                //GoogleApiClient.IConnectionCallbacks, 
                                                //GoogleApiClient.IOnConnectionFailedListener
    {
        //IntPtr IJavaObject.Handle => throw new NotImplementedException();

        //readonly int REQUEST_LOCATION = 199;
        //GoogleApiClient googleApiClient;

        public Task<string> OpenLocationSettings()
        {
                MainActivity activity = Xamarin.Forms.Forms.Context as MainActivity;

            activity.ChangeLocationSettingsTaskCompletionSource = new TaskCompletionSource<string>();

            //activity.StartActivityForResult(new Android.Content.Intent(Android.Provider.Settings.ActionLocationSourceSettings), MainActivity.SettingsOpenedRequestCode);


            activity.enableLocationUsingGoogleAPIClient();

            return activity.ChangeLocationSettingsTaskCompletionSource.Task;
        }

        //public void EnableGPSAutomatically(MainActivity activity)
        //{
            //googleApiClient = null;
            //if (googleApiClient == null)
            //{
                //googleApiClient = new GoogleApiClient
                //    .Builder(activity)

                //    .AddApi(LocationServices.Api)
                //    .AddConnectionCallbacks(this)

                //    .AddOnConnectionFailedListener(this).Build();


                //googleApiClient.Connect();


                //LocationRequest locationRequest = LocationRequest.Create();
                //locationRequest.SetPriority(LocationRequest.PriorityHighAccuracy);
                //locationRequest.SetInterval(30 * 1000);
                //locationRequest.SetFastestInterval(5 * 1000);
                //LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                //        .AddLocationRequest(locationRequest);

                //// **************************
                //builder.SetAlwaysShow(true); // this is the key ingredient
                //                             // **************************

                ////PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                ////.checkLocationSettings(googleApiClient, builder.build());

                //PendingResult result = LocationServices
                //    .SettingsApi
                //    .CheckLocationSettings(googleApiClient, builder.Build());


                //result.SetResultCallback(this);


        //        //task.Result.LocationSettingsStates.
        //    }
        //}

        //void IResultCallback.OnResult(Java.Lang.Object result)
        //{
        //    //throw new NotImplementedException();
        //    LocationSettingsResult r = result as LocationSettingsResult;


        //    switch(r.Status.StatusCode){
        //        case LocationSettingsStatusCodes.ResolutionRequired:
        //            r.Status.StartResolutionForResult(Android.App.Application.Context as MainActivity, REQUEST_LOCATION);
        //            break;

        //    }

        //}

        //void IDisposable.Dispose()
        //{
           
        //}

        //public void OnConnected(Bundle connectionHint)
        //{
        //    googleApiClient.Connect();
        //}

        //public void OnConnectionSuspended(int cause)
        //{
           
        //}

        //public void OnConnectionFailed(ConnectionResult result)
        //{
            
        //}
    }


}
