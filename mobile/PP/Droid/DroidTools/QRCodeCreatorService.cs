﻿using System;
using System.IO;
using PP.tools;
using PP.Droid.DroidTools;
using Android.Graphics;

[assembly: Xamarin.Forms.Dependency(typeof(QRCodeCreatorService))]
namespace PP.Droid.DroidTools
{
    public class QRCodeCreatorService:IQRCodeCreatorService
    {
		public QRCodeCreatorService()
		{

		}
		public Stream ConvertImageStream(string data, int width = 300, int height = 300)
		{
			var barcodeWriter = new ZXing.Mobile.BarcodeWriter
			{
				Format = ZXing.BarcodeFormat.QR_CODE,
				Options = new ZXing.Common.EncodingOptions
				{
					Width = width,
					Height = height,
					Margin = 0
				}
			};
			barcodeWriter.Renderer = new ZXing.Mobile.BitmapRenderer();
			var bitmap = barcodeWriter.Write(data);
			var stream = new MemoryStream();
			bitmap.Compress(Bitmap.CompressFormat.Png, 100, stream);  // this is the diff between iOS and Android
			stream.Position = 0;

			return stream;
		}
    }
}
