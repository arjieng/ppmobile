﻿using System;
using Android.Content;
using PP.Droid.DroidTools;
using PP.tools;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(DroidStreetViewService))]
namespace PP.Droid.DroidTools
{
    public class DroidStreetViewService:IStreetViewService
    {
   
        public void openStreetView(double latitude, double longitude)
        {
			Android.Net.Uri gmmIntentUri = Android.Net.Uri.Parse("google.streetview:cbll="+latitude+","+longitude);
			Intent mapIntent = new Intent(Intent.ActionView, gmmIntentUri);
            mapIntent.SetPackage("com.google.android.apps.maps");
            Forms.Context.StartActivity(mapIntent);

		}
    }
}
