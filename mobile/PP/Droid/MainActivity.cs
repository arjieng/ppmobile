﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using PP.Droid.DroidTools;
using Plugin.Permissions;
using FFImageLoading.Forms.Droid;
using Plugin.Fingerprint;
using Plugin.CurrentActivity;
using System.Threading.Tasks;
using CarouselView.FormsPlugin.Android;
using Java.Lang;
using PP.tools;
using Android.Gms.Ads;
using Android.Gms.Common.Apis;
using Android.Gms.Location;
using Android.Gms.Common;
using Plugin.LocalNotifications;

namespace PP.Droid
{
    [Activity(Label = "PP App", Icon = "@drawable/pp_logo", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait, ResizeableActivity = true)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity,
                                                GoogleApiClient.IConnectionCallbacks,
                                                GoogleApiClient.IOnConnectionFailedListener,
                                                IResultCallback
    {


        Handler handler;
        Runnable r;
        CountDownTimer idleElapsedTimer;

        GoogleApiClient googleApiClient;
        int GOOGLEAPI_REQUEST_LOCATION = 199;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);


            setAppFrame();
            ZXing.Net.Mobile.Forms.Android.Platform.Init();
            var x = new QRCodeCreatorService();

            CachedImageRenderer.Init();
            CrossFingerprint.SetCurrentActivityResolver(() => CrossCurrentActivity.Current.Activity);
            //Syncfusion.SfRangeSlider.XForms.Droid.SfRangeSliderRenderer()
            Xamarin.FormsGoogleMaps.Init(this, bundle);

            Websockets.Droid.WebsocketConnection.Link();
            CrossCurrentActivity.Current.Init(this, bundle);

            //Forms.SetFlags("FastRenderers_Experimental");
            LocalNotificationsImplementation.NotificationIconId = Resource.Drawable.pp_logo;


            //admob initiation
            MobileAds.Initialize(ApplicationContext, "ca-app-pub-5721146493095294~7422528096");

            global::Xamarin.Forms.Forms.Init(this, bundle);

            CarouselViewRenderer.Init();

            LoadApplication(new App());



            //GorillaSDKInit();

            handler = new Handler();
            r = new Runnable(() =>
            {
                Log.e("Idle Timeout Detected, Do Stuff!");
                if (IdleTimerManager.idleTimerService != null)
                    IdleTimerManager.idleTimerService.didIdleTimedOut();
            });


        }



        //void GorillaSDKInit(){
        //    LoadApplication(UXDivers.Gorilla.Droid.Player.CreateApplication(this,
        //        new UXDivers.Gorilla.Config("Good Gorilla")
        //        .RegisterAssemblyFromType<PP.ScaleXAML>()
        //        .RegisterAssemblyFromType<FFImageLoading.Forms.CachedImage>()
        //   ));
        //}

        public override void OnUserInteraction()
        {
            base.OnUserInteraction();

            stopHandler();
            if(!Constants.IsInTransaction){
                startHandler();
            }
                
            Log.e("Idle Timer Thread Cancelled");
            if (IdleTimerManager.idleTimerService != null)
                IdleTimerManager.idleTimerService.didIdleTimerCancelledOrReset();
        }
        void stopHandler()
        {
            handler.RemoveCallbacks(r);
        }
        void startHandler()
        {
            Log.e("Idle Timer Thread Started");
            if (IdleTimerManager.idleTimerService != null)
                IdleTimerManager.idleTimerService.didIdleTimerStarted();


            //int counter = 0;
            //Device.StartTimer(TimeSpan.FromSeconds(1), () => {
            //    counter++;
            //    if (counter == IdleTimerManager.TIME_OUT_SECONDS)
            //    {
            //        return false;
            //    }

            //    if (IdleTimerManager.idleTimerService != null)
            //        IdleTimerManager.idleTimerService.didElapseTime(IdleTimerManager.TIME_OUT_SECONDS - counter);

            //    return true;
            //});

            handler.PostDelayed(r, IdleTimerManager.TIME_OUT_SECONDS * 1000);


            //create elapsed time timer
            //if (idleElapsedTimer != null)
            //    idleElapsedTimer.Cancel();


            if (idleElapsedTimer != null)
                idleElapsedTimer.Cancel();

            idleElapsedTimer = new PPCoundownTimer(IdleTimerManager.TIME_OUT_SECONDS * 1000, 1000);
            idleElapsedTimer.Start();
        }

        void setAppFrame()
        {

            App.DPI = (float)Resources.DisplayMetrics.Density;
            App.screenWidth = Resources.DisplayMetrics.WidthPixels / App.DPI;//* App.DPI /160;
            App.screenHeight = Resources.DisplayMetrics.HeightPixels / App.DPI;// * App.DPI /16
        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public static int SettingsOpenedRequestCode = 9999;
        public TaskCompletionSource<string> ChangeLocationSettingsTaskCompletionSource { set; get; }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == SettingsOpenedRequestCode)
            {
                ChangeLocationSettingsTaskCompletionSource.SetResult("Okay");
            }
            else if(requestCode == GOOGLEAPI_REQUEST_LOCATION){
                ChangeLocationSettingsTaskCompletionSource.SetResult("Okay");
            }
        }
        protected override void OnPause()
        {
            Constants.isMinimized = true;
            //Constants.GetConnect = "Paused";
            base.OnPause();
        }
        protected override void OnStop()
        {
            //if (Constants.IsInBuyer)
            //{
            //    Pages.BuyerHome.CancelTransactionOnClosed();
            //    Pages.BuyerHome.DeclineOnClosed();
            //}

            //if (Constants.IsInSeller)
            //{
            //    tools.API.ChangeUserRoleAPI api = new tools.API.ChangeUserRoleAPI(Constants.currentUser.token, Constants.MERCHANT_STATUS.IDLE);
            //    api.getResponse();
            //    PP.Pages.Seller.SellerHome.CancelTransactionOnClosed();
            //    Pages.Seller.SellerHome.DeclineOnClosed();
            //}
            Constants.isMinimized = true;
            base.OnStop();
        }

        protected override void OnStart()
        {
            Constants.isMinimized = false;
            //if (Constants.IsInSeller)
            //{
            //    tools.API.ChangeUserRoleAPI api = new tools.API.ChangeUserRoleAPI(Constants.currentUser.token, Constants.MERCHANT_STATUS.SELLER);
            //    api.getResponse();
            //}
            base.OnStart();
        }

        protected override void OnResume()
        {
            base.OnResume();
            Constants.isMinimized = false;
        }


        public void enableLocationUsingGoogleAPIClient()
        {

            googleApiClient = null;
            if (googleApiClient == null)
            {
                googleApiClient = new GoogleApiClient
                    .Builder(this)

                    .AddApi(LocationServices.Api)
                    .AddConnectionCallbacks(this)

                    .AddOnConnectionFailedListener(this).Build();


                googleApiClient.Connect();


                LocationRequest locationRequest = LocationRequest.Create();
                locationRequest.SetPriority(LocationRequest.PriorityHighAccuracy);
                locationRequest.SetInterval(30 * 1000);
                locationRequest.SetFastestInterval(5 * 1000);
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .AddLocationRequest(locationRequest);

                // **************************
                builder.SetAlwaysShow(true); // this is the key ingredient
                                             // **************************

                //PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                //.checkLocationSettings(googleApiClient, builder.build());

                PendingResult result = LocationServices
                    .SettingsApi
                    .CheckLocationSettings(googleApiClient, builder.Build());


                result.SetResultCallback(this);

            }
        }
        public void OnConnected(Bundle connectionHint)
        {
            
        }

        public void OnConnectionSuspended(int cause)
        {
            googleApiClient.Connect();
        }

        public void OnConnectionFailed(ConnectionResult result)
        {

        }
        void IResultCallback.OnResult(Java.Lang.Object result)
        {
            //throw new NotImplementedException();
            LocationSettingsResult newResult = result as LocationSettingsResult;


            switch (newResult.Status.StatusCode)
            {
                case LocationSettingsStatusCodes.ResolutionRequired:
                    newResult.Status.StartResolutionForResult(this, GOOGLEAPI_REQUEST_LOCATION);
                    break;
                case LocationSettingsStatusCodes.Success:
                    ChangeLocationSettingsTaskCompletionSource.SetResult("Okay");
                    break;

            }

        }


    }

    public class PPCoundownTimer : CountDownTimer
    {
    
        public PPCoundownTimer(long startTime, long interval):base(startTime, interval)
        {

        }
        public override void OnFinish()
        {
           
        }

        public override void OnTick(long millisUntilFinished)
        {
            if(IdleTimerManager.idleTimerService!=null){
                IdleTimerManager.idleTimerService.didElapseTime( (int)(millisUntilFinished / 1000));
            }
        }
    }
}
