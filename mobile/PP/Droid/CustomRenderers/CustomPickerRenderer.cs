﻿using System;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using PP;
using PP.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace PP.Droid.CustomRenderers
{
    
    public class CustomPickerRenderer: PickerRenderer
    {
        public CustomPickerRenderer()
        {

            //this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);

            //Picker p = Element as Picker;
            //this.Control.SetTextColor(p.TextColor.ToAndroid());

           
        }

        //protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        //{
        //    base.OnElementChanged(e);

			
        //}
        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

			EditText et = Control;

            et.Background = ContextCompat.GetDrawable(this.Context, Android.Resource.Color.Transparent);
			et.SetPadding(1, 3, 1, 1);


			//get pcl properties
			CustomPicker cp = (CustomPicker)Element;

            et.Hint = cp.Title;
            et.SetHintTextColor(cp.PlaceholderColor.ToAndroid());
			et.TextSize = (float)cp.FontSize;
            et.SetHintTextColor(cp.PlaceholderColor.ToAndroid());


            //alignments

            switch(cp.TitleAlignment){
                case CustomPicker.Alignment.START_ALIGN:
                    et.Gravity = (GravityFlags.Start | GravityFlags.CenterVertical);
                    break;
				case CustomPicker.Alignment.CENTER_ALIGN:
                    et.Gravity = GravityFlags.Center ;//| GravityFlags.CenterVertical);
                    break;
                case CustomPicker.Alignment.END_ALIGN:
					et.Gravity = (GravityFlags.End | GravityFlags.CenterVertical);
                    break;
            }
        }
    }
}
