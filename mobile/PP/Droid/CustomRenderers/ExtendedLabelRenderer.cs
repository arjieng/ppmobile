﻿using System;
using Android.Graphics;
using Android.Widget;
using PP;
using PP.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ExtendedLabel), typeof(ExtendedLabelRenderer))]
namespace PP.Droid.CustomRenderers
{
    public class ExtendedLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            var view = (ExtendedLabel)Element;
            var control = Control;

            UpdateUi(view, control);
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var view = (ExtendedLabel)Element;

            if (e.PropertyName == ExtendedLabel.IsBoldProperty.PropertyName)
            {
                Control.PaintFlags = view.IsBold ? Control.PaintFlags | PaintFlags.FakeBoldText : Control.PaintFlags &= ~PaintFlags.FakeBoldText;
            }

            if (e.PropertyName == ExtendedLabel.IsUnderlineProperty.PropertyName)
            {
                Control.PaintFlags = view.IsUnderline ? Control.PaintFlags | PaintFlags.UnderlineText : Control.PaintFlags &= ~PaintFlags.UnderlineText;
            }

            if (e.PropertyName == ExtendedLabel.IsStrikeThroughProperty.PropertyName)
            {
                Control.PaintFlags = view.IsStrikeThrough ? Control.PaintFlags | PaintFlags.StrikeThruText : Control.PaintFlags &= ~PaintFlags.StrikeThruText;
            }
        }

        static void UpdateUi(ExtendedLabel view, TextView control)
        {
            if (view.FontSize > 0)
            {
                control.TextSize = (float)view.FontSize;
            }

            if (view.IsBold)
            {
                control.PaintFlags = control.PaintFlags | PaintFlags.FakeBoldText;
            }

            if (view.IsUnderline)
            {
                control.PaintFlags = control.PaintFlags | PaintFlags.UnderlineText;
            }

            if (view.IsStrikeThrough)
            {
                control.PaintFlags = control.PaintFlags | PaintFlags.StrikeThruText;
            }
        }
    }
}
