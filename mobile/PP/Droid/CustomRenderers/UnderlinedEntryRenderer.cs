﻿using System;
using PP;
using PP.Droid;
using Android.Graphics;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Support.V4.Content;
using Android.Widget;

[assembly: ExportRenderer(typeof(UnderlinedEntry), typeof(UnderlinedEntryRenderer))]
namespace PP.Droid
{
	public class UnderlinedEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);
            //Control.Background.ClearColorFilter();


            //Control.Background.SetColorFilter(Android.Graphics.Color.Gray, PorterDuff.Mode.SrcIn);
            //e.NewElement.Focused += (sender, el) =>
            //{
            //	Control.Background.SetColorFilter(Android.Graphics.Color.Gray, PorterDuff.Mode.SrcIn);
            //};
            //e.NewElement.Unfocused += (sender, el) =>
            //{
            //	Control.Background.SetColorFilter(Android.Graphics.Color.LightGray, PorterDuff.Mode.SrcIn);
            //};

            if(Control!=null && Element !=null){
                var v = Element as UnderlinedEntry;

                if(v!=null){
                    var native = (EditText)Control;

                    native.Background.SetColorFilter(Android.Graphics.Color.Gray, PorterDuff.Mode.SrcIn);
                }
            }
		} 
	}
}

