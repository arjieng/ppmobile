﻿using System;
using Android.Graphics;
using Android.Text;
using Android.Widget;
using Java.Lang;
using PP;
using PP.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(TransparentEntryExtended), typeof(TransparentEntryExtendedRenderer))]
namespace PP.Droid.CustomRenderers
{
    public class TransparentEntryExtendedRenderer:TransparentEntryRenderer, Android.Text.IInputFilter
    {
        public TransparentEntryExtendedRenderer(Android.Content.Context context):base(context)
        {
        }

		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
            base.OnElementChanged(e);

            if (Control != null && Element != null)
            {
                var v = Element as TransparentEntryExtended;

                if (v != null)
                {
                    var native = (EditText)Control;

                    if(OriginalBackground!=null){

                        native.SetBackground(OriginalBackground);
                        native.Background.SetColorFilter(v.UnderlineColor.ToAndroid(), PorterDuff.Mode.SrcIn);
                    }
                }

                Control.SetFilters(new IInputFilter[] { this });

            }
		}

        ICharSequence IInputFilter.FilterFormatted(ICharSequence source, int start, int end, ISpanned dest, int dstart, int dend)
        {
            if(string.IsNullOrWhiteSpace(source.ToString())){
                var entry = (TransparentEntry)Element;
            }
            return source;
        }
    }
}
