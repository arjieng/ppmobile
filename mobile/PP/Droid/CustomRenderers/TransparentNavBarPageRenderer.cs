﻿using System;
using PP;
using PP.Droid;
using Android.App;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Platform.Android.AppCompat;

[assembly: ExportRenderer(typeof(TransparentNavBarPage), typeof(TransparentNavBarPageRenderer))]
namespace PP.Droid
{
	public class TransparentNavBarPageRenderer : NavigationPageRenderer
	{

		protected override void OnLayout(bool changed, int left, int top, int right, int bottom)
		{
			base.OnLayout(changed, left, top, right, bottom);

			var actionBar = ((Activity)Context).ActionBar;
			//actionBar.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.YourImageInDrawable));

			///
			var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);

			toolbar.SetBackgroundColor(((TransparentNavBarPage)this.Element).barColor.ToAndroid());
		}
	}
}

