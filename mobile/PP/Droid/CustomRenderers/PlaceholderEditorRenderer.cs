﻿using System;
using PP;
using PP.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(PlaceholderEditor), typeof(PlaceholderEditorRenderer))]
namespace PP.Droid
{
	public class PlaceholderEditorRenderer:EditorRenderer
	{
		public PlaceholderEditorRenderer()
		{
		}
		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
		}
	}
}
