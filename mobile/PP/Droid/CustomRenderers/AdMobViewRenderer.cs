﻿using System;
using System.ComponentModel;
using Android.Gms.Ads;
using Android.Widget;
using PP;
using PP.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(AdMobView), typeof(AdMobViewRenderer))]
namespace PP.Droid.CustomRenderers
{
    public class AdMobViewRenderer : ViewRenderer<AdMobView, AdView>
    {
        public AdMobViewRenderer(Android.Content.Context context):base(context)
        {
            
        }


		protected override void OnElementChanged(ElementChangedEventArgs<AdMobView> e)
		{
            base.OnElementChanged(e);

            if (e.NewElement != null && Control == null)
                SetNativeControl(CreateAdView());
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == nameof(AdView.AdUnitId))
                Control.AdUnitId = Element.AdUnitId;
		} 


	
        private AdView CreateAdView()
        {
            var adView = new AdView(Context)
            {
                AdSize = AdSize.SmartBanner,
                AdUnitId = Element.AdUnitId
            };

            adView.LayoutParameters = new LinearLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);

            if (Constants.SHOULD_USE_TESTDEVICES == false)
            {
                adView.LoadAd(new AdRequest.Builder().Build());
            }
            else
            {
                AdRequest.Builder adBuilder = new AdRequest.Builder();
                foreach (string s in Constants.TEST_DEVICES)
                {
                    adBuilder = adBuilder.AddTestDevice(s);
                }
                adView.LoadAd(adBuilder.Build());
            }

            //adView.LoadAd(new AdRequest.Builder().AddTestDevice("85A35CE43ECFC4B45D7DF7B693D0DD04").Build());

            return adView;
        }
    }
}

