﻿using System;
using Android.Content;
using PP.Droid.Helpers;
using PP.Helpers.DrawerHelper;
using Xamarin.Forms;
using Uri = Android.Net.Uri;

[assembly: Dependency(typeof(IDrawerService))]
namespace PP.Droid.Helpers
{
    public class IDrawerService : iDrawer
    {
        public void OpenDrawer(double latitude, double longitude)
        {
            Intent intent = new Intent(Intent.ActionView);

            String uri = "geo: " + latitude + "," + longitude + "?q=" + latitude + "," + longitude;


            intent.SetData(Uri.Parse(uri));
            //intent.SetComponent(new ComponentName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity"));
            intent.AddFlags(ActivityFlags.NewTask);

            //Intent chooser = Intent.CreateChooser(intent, "Select App");
            //chooser.AddFlags(ActivityFlags.NewTask);
            Android.App.Application.Context.StartActivity(intent);
        }
    }
}
